package org.cweb.identity;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.AccountUtils;
import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IdentityTest extends CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(IdentityTest.class);

	@Test
	public void testStorageProfileUpdate() {
		Pair<Boolean, String> updatedSame = this.instance[0].updatePrivateStorageProfile(privateStorageProfile);
		Assert.assertEquals(Pair.of(Boolean.FALSE, null), updatedSame);

		LocalIdentityDescriptorState fetchedIdentityDescriptorState1 = instance[1].getRemoteIdentityService().get(id[0]);
		Assert.assertNotNull(fetchedIdentityDescriptorState1);
		Assert.assertEquals("test1", fetchedIdentityDescriptorState1.getDescriptor().getStorageProfile().getPublicS3StorageProfile().getPathPrefix());

		PrivateStorageProfile newPrivateStorageProfile = new PrivateStorageProfile(privateStorageProfile);
		newPrivateStorageProfile.getPrivateS3StorageProfile().getPublicS3StorageProfile().setPathPrefix("test2");

		Pair<Boolean, String> updatedNew = this.instance[0].updatePrivateStorageProfile(newPrivateStorageProfile);
		Assert.assertEquals(Pair.of(Boolean.TRUE, null), updatedNew);
		this.instance[0].flushUploadQueue(SLEEP_TIME);

		instance[1].getRemoteIdentityService().deleteLocalCache();

		LocalIdentityDescriptorState fetchedIdentityDescriptorState2 = instance[1].getRemoteIdentityService().get(id[0]);
		Assert.assertNotNull(fetchedIdentityDescriptorState2);
		Assert.assertEquals("test2", fetchedIdentityDescriptorState2.getDescriptor().getStorageProfile().getPublicS3StorageProfile().getPathPrefix());
	}

	@Test
	public void testNewIdentity() {
		InstanceContainer instanceContainer = AccountUtils.createNewIdentity(LOCAL_ROOT_PATH, privateStorageProfile, null);
		Assert.assertNotNull(instanceContainer);
		log.debug("Created new Id " + Utils.getDebugStringFromBytes(instanceContainer.getOwnId(), 1000));
	}

	private static void fillProperties(InstanceContainer instance, String prefix) {
		instance.getIdentityService().updateProperties(Lists.newArrayList(
				new Property("email", PropertyValue.str(prefix + "1@host.com")),
				new Property("email", PropertyValue.str(prefix + "2@host.com")),
				new Property("Twitter:handle", PropertyValue.str("@" + prefix))));
	}

	@Test
	public void testEndorsements() {
		IdentityDescriptor identityDescriptor1 = instance[0].getIdentityService().getIdentityDescriptor();
		fillProperties(instance[0], "foo");
		IdentityDescriptor identityDescriptor2 = instance[1].getIdentityService().getIdentityDescriptor();
		fillProperties(instance[1], "bar");

		Property propertyToEndorse = new Property("barId", PropertyValue.str("barGuest123456"));
		{
			List<Property> propertiesToGetEndorsement = new ArrayList<>();
			propertiesToGetEndorsement.add(propertyToEndorse);
			instance[0].getEndorsementService().publishEndorsementRequestFor(identityDescriptor2, propertiesToGetEndorsement);
		}

		Threads.sleepChecked(SLEEP_TIME);

		instance[1].getEndorsementService().checkAndProcessEndorsementRequestsFrom(identityDescriptor1, new EndorsementVerifier() {
			@Override
			public Pair<Result, String> verify(IdentityDescriptor identityDescriptor, List<Property> propertiesToEndorse) {
				return Pair.of(Result.SUCCESS, null);
			}
		});
		instance[0].getEndorsementService().checkAndProcessEndorsementResponsesFrom(identityDescriptor2, new EndorsementResponseFilter() {
			@Override
			public Action getAction(IdentityDescriptor identityDescriptor, EndorsementPayload endorsementPayload) {
				return Action.APPLY;
			}
		});
		Threads.sleepChecked(SLEEP_TIME);

		Assert.assertEquals(1, instance[0].getIdentityService().getIdentityDescriptor().getEndorsementSignedEnvelopes().size());
		ByteBuffer endorsementEnvelope = instance[0].getIdentityService().getIdentityDescriptor().getEndorsementSignedEnvelopes().get(0);
		List<Pair<SignatureMetadata, EndorsementPayload>> endorsements = instance[0].getEndorsementService().getEndorsementsWithoutVerification();
		Assert.assertEquals(1, endorsements.size());
		Pair<SignatureMetadata, EndorsementPayload> endorsement1 = endorsements.get(0);
		Assert.assertArrayEquals(id[1], endorsement1.getLeft().getSignerId());
		Assert.assertEquals(1, endorsement1.getRight().getProperties().size());
		Property endorsedProperty = endorsement1.getRight().getProperties().get(0);
		Assert.assertEquals(propertyToEndorse, endorsedProperty);
	}

	@Test
	public void testIdentityProfile() {
		Property property1 = new Property("name", PropertyValue.str("john"));
		instance[0].getIdentityProfilePostService().updateProperties(Collections.singletonList(property1));
		instance[0].getIdentityProfilePostService().setSubscribers(Collections.singletonList(id[1]));
		flushUploadQueue(0);

		receiveDirectFrom(1, 0);
		instance[1].getIdentityProfileReadService().requestProfileFetch(id[0]);
		waitForCommSchedulerMessageLoop(1);
		IdentityProfile profile = instance[1].getIdentityProfileReadService().getProfile(id[0]);
		Assert.assertEquals(1, profile.getProperties().size());
		Assert.assertEquals(property1, profile.getProperties().get(0));

		Property property2 = new Property("email", PropertyValue.str("john@gmail.com"));
		instance[0].getIdentityProfilePostService().updateProperties(Collections.singletonList(property2));
		flushUploadQueue(0);

		receiveDirectFrom(1, 0);
		instance[1].getIdentityProfileReadService().requestProfileFetch(id[0]);
		waitForCommSchedulerMessageLoop(1);
		profile = instance[1].getIdentityProfileReadService().getProfile(id[0]);
		Assert.assertEquals(2, profile.getProperties().size());
		Assert.assertEquals(property2, profile.getProperties().get(1));
	}
}
