package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.AccountUtils;
import org.cweb.Config;
import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

public class BackupRestoreTest {
	private static final Logger log = LoggerFactory.getLogger(BackupRestoreTest.class);
	private static final String LOCAL_ROOT_PATH_ORIG = "/tmp/cweb-test-data";
	private static final String LOCAL_ROOT_PATH_RESTORE = "/tmp/cweb-test-data2";
	private static final PrivateStorageProfile privateStorageProfile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(CwebTestBase.PRIVATE_STORAGE_PROFILE_STR);

	@Before
	public void setUp() {
		Config.remoteStorageType = Config.RemoteStorageType.LOCAL_MEMORY;
		Config.localStorageType = Config.LocalStorageType.MEMORY;
		Config.localFileSystemType = Config.LocalFileSystemType.MEMORY;
	}

	@Test
	public void testBackupAndRestore() {
		InstanceContainer instanceContainer = AccountUtils.createNewIdentity(LOCAL_ROOT_PATH_ORIG, privateStorageProfile, null);
		log.debug("Created new Id " + Utils.getDebugStringFromBytes(instanceContainer.getOwnId(), 1000));
		Property property1 = new Property("name", PropertyValue.str("backup"));
		instanceContainer.getIdentityProfilePostService().updateProperties(Collections.singletonList(property1));
		byte[] key = new byte[32];
		ByteArrayOutputStream backupOut = new ByteArrayOutputStream();
		AccountUtils.backup(instanceContainer, backupOut, key, null);
		instanceContainer.permanentlyDestroy();

		ByteArrayInputStream backupIn = new ByteArrayInputStream(backupOut.toByteArray());
		Pair<byte[], byte[]> restoreResult = AccountUtils.restore(LOCAL_ROOT_PATH_RESTORE, backupIn, key);
		InstanceContainer instanceContainerRestored = new InstanceContainer(LOCAL_ROOT_PATH_RESTORE, restoreResult.getLeft(), null);
		List<Property> properties = instanceContainerRestored.getIdentityProfilePostService().getProfile().getProperties();
		Assert.assertEquals(1, properties.size());
		Assert.assertEquals(property1, properties.get(0));
		instanceContainerRestored.permanentlyDestroy();
	}
}
