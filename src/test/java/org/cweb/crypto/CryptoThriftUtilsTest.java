package org.cweb.crypto;

import org.cweb.schemas.crypto.DoubleRatchetMessage;
import org.cweb.schemas.crypto.DoubleRatchetSkippedMessageKey;
import org.cweb.schemas.crypto.DoubleRatchetState;
import org.cweb.schemas.crypto.X3DHInitialMessage;
import org.cweb.schemas.crypto.X3DHPreKeyBundle;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.keys.KeyType;
import org.cweb.utils.Utils;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CryptoThriftUtilsTest {
	private static final ByteBuffer[] data;

	static {
		data = new ByteBuffer[50];
		for (int i = 0; i < data.length; i++) {
			data[i] = ByteBuffer.wrap(Utils.generateRandomBytes(32));
		}
	}

	@Test
	public void testKeyPairs() {
		KeyPair source = new KeyPair(KeyType.EC25519_256, data[0], data[1]);
		KeyPair restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source));
		assertEquals(source, restored);
	}

	@Test
	public void testX3DHPreKeyBundle() {
		X3DHPreKeyBundle source = new X3DHPreKeyBundle(data[0], data[1]);
		source.setIdPublicKey(data[2]);
		X3DHPreKeyBundle restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source, null));
		assertEquals(source, restored);
	}

	@Test
	public void testX3DHInitialMessage() {
		X3DHInitialMessage source = new X3DHInitialMessage(data[0], data[1], data[2]);
		source.setIdPublicKey(data[3]);
		X3DHInitialMessage restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source, null));
		assertEquals(source, restored);
	}

	@Test
	public void testDoubleRatchetSkippedMessageKey() {
		DoubleRatchetSkippedMessageKey source = new DoubleRatchetSkippedMessageKey(data[0], 11, data[1]);
		DoubleRatchetSkippedMessageKey restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source));
		assertEquals(source, restored);
	}

	@Test
	public void testDoubleRatchetState() {
		DoubleRatchetState source = new DoubleRatchetState(new KeyPair(KeyType.EC25519_256, data[0], data[1]),
				data[2], data[3], data[4], data[5],
				101, 102, 103,
				data[6], data[7], data[8], data[9],
				Arrays.asList(new DoubleRatchetSkippedMessageKey(data[10], 11, data[11]),
						new DoubleRatchetSkippedMessageKey(data[12], 11, data[13])));
		DoubleRatchetState restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source));
		assertEquals(source, restored);
	}

	@Test
	public void testDoubleRatchetMessage() {
		DoubleRatchetMessage source = new DoubleRatchetMessage(data[0], data[1]);
		DoubleRatchetMessage restored = CryptoThriftUtils.toThrift(CryptoThriftUtils.fromThrift(source));
		assertEquals(source, restored);
	}
}
