package org.cweb.crypto.lib;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class FernetTest {
	private static final byte[] data = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	@Test
	public void testFernet() {
		byte[] key = FernetBinary.generateKey();
		byte[] encoded = FernetBinary.encrypt(key, data);
		byte[] decoded = FernetBinary.decrypt(key, encoded, null);
		assertTrue(Arrays.equals(data, decoded));
	}
}
