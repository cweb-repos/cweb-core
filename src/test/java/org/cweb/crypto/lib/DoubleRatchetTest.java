package org.cweb.crypto.lib;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

public class DoubleRatchetTest {
	@Test
	public void testDoubleRatchet() {
		ECKeyPair idKeyPair1 = ECUtils.generateKeyPair();
		ECKeyPair idKeyPair2 = ECUtils.generateKeyPair();
		ECKeyPair preKeyPair2 = ECUtils.generateKeyPair();
		Pair<byte[], byte[]> secrets = X3DFTest.calculateSecrets(idKeyPair1, idKeyPair2, preKeyPair2);
		assertArrayEquals(secrets.getLeft(), secrets.getRight());

		DoubleRatchet.InitialSharedSecrets initialSharedSecrets1 = DoubleRatchet.generateInitialSharedSecrets(secrets.getLeft());
		DoubleRatchet.State state1 = DoubleRatchet.initStateFirst(initialSharedSecrets1, preKeyPair2.publicKey);

		DoubleRatchet.InitialSharedSecrets initialSharedSecrets2 = DoubleRatchet.generateInitialSharedSecrets(secrets.getRight());
		DoubleRatchet.State state2 = DoubleRatchet.initStateSecond(preKeyPair2, initialSharedSecrets2);

		assertArrayEquals(state1.headerKeySelf, state2.nextHeaderKeyRemote);
		assertArrayEquals(state1.nextHeaderKeyRemote, state2.nextHeaderKeySelf);
		assertArrayEquals(state1.chainKeyRemote, state2.chainKeySelf);

		byte[] associatedData = {8};

		testMessageSendAndReceive(state1, state2, associatedData, "Message1");
		testMessageSendAndReceive(state1, state2, associatedData, "Message2");
		testMessageSendAndReceive(state1, state2, associatedData, "Message3");
		testMessageSendAndReceive(state2, state1, associatedData, "Message4");
		testMessageSendAndReceive(state1, state2, associatedData, "Message5");
		testMessageSendAndReceive(state2, state1, associatedData, "Message6");
		testMessageSendAndReceive(state2, state1, associatedData, "Message7");
		testMessageSendAndReceive(state2, state1, associatedData, "Message8");

		DoubleRatchet.Message message9 = testMessageSend(state1, associatedData, "Message9");
		DoubleRatchet.Message message10 = testMessageSend(state1, associatedData, "Message10");
		testMessageSendAndReceive(state1, state2, associatedData, "Message11");
		testMessageSendAndReceive(state2, state1, associatedData, "Message12");
		testMessageSendAndReceive(state1, state2, associatedData, "Message13");

		testMessageReceive(state2, message10, associatedData, "Message10");
		testMessageReceive(state2, message9, associatedData, "Message9");
	}

	private void testMessageSendAndReceive(DoubleRatchet.State stateSender, DoubleRatchet.State stateReceiver, byte[] associatedData, String message) {
		DoubleRatchet.Message encryptedMessage = testMessageSend(stateSender, associatedData, message);
		testMessageReceive(stateReceiver, encryptedMessage, associatedData, message);
	}

	private DoubleRatchet.Message testMessageSend(DoubleRatchet.State stateSender, byte[] associatedData, String message) {
		byte[] data = message.getBytes(StandardCharsets.UTF_8);

		DoubleRatchet.Message encryptedMessage = DoubleRatchet.ratchetEncrypt(stateSender, data, associatedData);
		assertNotNull(encryptedMessage);
		assertNotNull(encryptedMessage.encryptedHeader);
		assertNotNull(encryptedMessage.encryptedData);

		return encryptedMessage;
	}

	private void testMessageReceive(DoubleRatchet.State stateReceiver, DoubleRatchet.Message encryptedMessage, byte[] associatedData, String message) {
		byte[] data = message.getBytes(StandardCharsets.UTF_8);

		byte[] dataDecrypted = DoubleRatchet.ratchetDecrypt(stateReceiver, encryptedMessage, associatedData);
		assertArrayEquals(data, dataDecrypted);
	}
}
