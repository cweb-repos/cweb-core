package org.cweb.crypto.lib;

import org.cweb.utils.Utils;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

public class AEADTest {
	private static final byte[] data = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	@Test
	public void testAEAD() {
		byte[] encryptionKey = Utils.generateRandomBytes(AEAD.getCompositeKeyLengthInBytes() / 2);
		byte[] authKey = Utils.generateRandomBytes(AEAD.getCompositeKeyLengthInBytes() / 2);
		byte[] encoded = AEAD.encrypt(encryptionKey, authKey, data, new byte[]{5});
		byte[] decoded1 = AEAD.decrypt(encryptionKey, authKey, encoded, new byte[]{5});
		assertArrayEquals(data, decoded1);
		byte[] decoded2 = AEAD.decrypt(encryptionKey, authKey, encoded, new byte[]{6});
		assertNull(decoded2);
		byte[] decoded3 = AEAD.decrypt(encryptionKey, encryptionKey, encoded, new byte[]{5});
		assertNull(decoded3);
		byte[] decoded4 = AEAD.decrypt(authKey, authKey, encoded, new byte[]{5});
		assertNull(decoded4);
	}
}
