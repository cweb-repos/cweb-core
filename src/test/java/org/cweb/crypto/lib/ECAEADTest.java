package org.cweb.crypto.lib;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ECAEADTest {
	private static final byte[] data = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	@Test
	public void testECIES() {
		ECKeyPair ecKeyPair = ECUtils.generateKeyPair();

		byte[] encoded = ECAEAD.encrypt(ecKeyPair.publicKey, data, new byte[]{5});
		byte[] decoded1 = ECAEAD.decrypt(ecKeyPair.privateKey, encoded, new byte[]{5});
		assertArrayEquals(data, decoded1);
		byte[] decoded2 = ECAEAD.decrypt(ecKeyPair.privateKey, encoded, new byte[]{6});
		assertEquals(null, decoded2);
		byte[] decoded4 = ECAEAD.decrypt(encoded, encoded, new byte[]{5});
		assertEquals(null, decoded4);
	}
}
