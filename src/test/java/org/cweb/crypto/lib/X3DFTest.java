package org.cweb.crypto.lib;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class X3DFTest {
	private final ECKeyPair idKeyPair1 = ECUtils.generateKeyPair();
	private final ECKeyPair idKeyPair2 = ECUtils.generateKeyPair();

	public static Pair<byte[], byte[]> calculateSecrets(ECKeyPair idKeyPair1, ECKeyPair idKeyPair2, ECKeyPair preKeyPair2) {
		X3DH.PreKeyBundle preKeyBundle = X3DH.generatePreKeyBundle(idKeyPair2.privateKey, idKeyPair2.publicKey, preKeyPair2.publicKey);

		ECKeyPair ephemeralKeyPair = ECUtils.generateKeyPair();
		X3DH.InitialMessageGenerationResult session1Result = X3DH.generateInitialMessage(idKeyPair1, ephemeralKeyPair, preKeyBundle);
		X3DH.InitialMessage message1 = session1Result.initialMessage;
		byte[] secret1 = session1Result.masterSecret;

		X3DH.InitialMessageProcessingResult session2Result = X3DH.processInitialMessage(idKeyPair2, preKeyPair2, message1);
		byte[] secret2 = session2Result.masterSecret;
		return Pair.of(secret1, secret2);
	}

	@Test
	public void testX3DF() {
		ECKeyPair preKeyPair2 = ECUtils.generateKeyPair();
		Pair<byte[], byte[]> secrets = calculateSecrets(idKeyPair1, idKeyPair2, preKeyPair2);
		assertTrue(Arrays.equals(secrets.getLeft(), secrets.getRight()));
	}
}
