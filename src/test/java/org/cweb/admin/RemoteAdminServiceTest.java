package org.cweb.admin;

import org.cweb.CwebTestBase;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.storage.PrivateS3StorageProfile;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicS3StorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.S3Credentials;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.utils.ThriftUtils;
import org.junit.Assert;
import org.junit.Test;

public class RemoteAdminServiceTest extends CwebTestBase {
	private static final PublicStorageProfile publicStorageProfile;
	private static final PrivateStorageProfile privateStorageProfile;

	static {
		PublicS3StorageProfile publicS3StorageProfile = new PublicS3StorageProfile("https://172.17.0.2:9000", "us-east-1", "bucket", "prefix", new S3Credentials("accessKeyIdRead", "keyRead"));
		PrivateS3StorageProfile privateS3StorageProfile = new PrivateS3StorageProfile(publicS3StorageProfile, new S3Credentials("accessKeyIdWrite", "keyWrite"));
		publicStorageProfile = new PublicStorageProfile();
		publicStorageProfile.setPublicS3StorageProfile(publicS3StorageProfile);
		privateStorageProfile = new PrivateStorageProfile();
		privateStorageProfile.setPrivateS3StorageProfile(privateS3StorageProfile);
	}

	@Test
	public void testRequestAndResponse() {
		String fromNickname = "test";
		CryptoEnvelope request = instance[0].getRemoteAdminClientService().generateStorageProfileRequest(fromNickname);
		StorageProfileRequest requestDecoded = instance[2].getRemoteAdminHostService().decodeStorageProfileRequest(ThriftUtils.serialize(request));
		Assert.assertEquals(requestDecoded.fromNickname, fromNickname);
		CryptoEnvelope response = instance[2].getRemoteAdminHostService().generateStorageProfileResponse(requestDecoded, instance[2].getPrivateStorageProfile(), instance[2].getIdentityService().getOwnIdentityReference());
		StorageProfileResponse responseDecoded = instance[0].getRemoteAdminClientService().decodeStorageProfileResponse(ThriftUtils.serialize(response), instance[0].getRemoteIdentityService().getFetcher());
		Assert.assertEquals(responseDecoded.privateStorageProfile, instance[2].getPrivateStorageProfile());
		Assert.assertEquals(responseDecoded.getAdmin(), instance[2].getIdentityService().getOwnIdentityReference());
		Assert.assertEquals(0, instance[0].getRemoteAdminHostService().getLocalHostedProfiles().size());
		Assert.assertEquals(1, instance[2].getRemoteAdminHostService().getLocalHostedProfiles().size());
	}
}
