package org.cweb.files;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.communication.CommSessionService;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileServiceTest extends CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(FileServiceTest.class);

	@Test
	public void testFileService() {
		String filenameToUpload = LOCAL_TEST_DATA_PATH + "/tmpTestFileUL";

		int filesTested = 0;
		for (int dataLength : new int[] {100, 2000, 10000, 30000, 100000}) {
			byte[] data = RandomUtils.nextBytes(dataLength);
			instance[0].getLocalFileSystemInterface().createAndWrite(filenameToUpload, data);

			//////////////// Upload
			byte[] fileId;
			{
				Triple<LocalUploadedFileInfo, FileMetadata, FileUploadService.UploadError> uploadResult = instance[0].getFileUploadService().upload(filenameToUpload, "test", Collections.emptyList(), 20000, false);
				Assert.assertNull(uploadResult.getRight());
				fileId = uploadResult.getLeft().getFileReference().getFileId();
				FileMetadata metadata = uploadResult.getMiddle();
				Assert.assertEquals("test", metadata.getName());

				FileUploadService.UploadState uploadState1 = instance[0].getFileUploadService().getUploadState(fileId);
				Assert.assertNotNull(uploadState1);
				Assert.assertEquals(false, uploadState1.corrupted);

				instance[0].getCommSessionService().establishSessionWith(id[1]);
				boolean success = instance[0].getFileUploadService().share(fileId, new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(id[1])));
				Assert.assertTrue(success);

				flushUploadQueue(0);

				FileUploadService.UploadState uploadState2 = instance[0].getFileUploadService().getUploadState(fileId);
				Assert.assertNotNull(uploadState2);
				Assert.assertEquals(1.0, uploadState2.uploadedFraction, 0.0);
				Assert.assertEquals(false, uploadState2.corrupted);
			}

			//////////////// Download
			{
				Assert.assertEquals(filesTested, instance[1].getFileDownloadService().listAllDownloads().size());
				Assert.assertEquals(0, instance[1].getFileDownloadService().listCurrentDownloads().size());

				FileDownloadService.DownloadState stateBefore = instance[1].getFileDownloadService().getDownloadState(id[0], fileId);
				Assert.assertNull(stateBefore);

				Pair<FileMetadata, FileDownloadService.DownloadError> fileMeta = instance[1].getFileDownloadService().readFileMeta(id[0], fileId);
				Assert.assertEquals(FileDownloadService.DownloadError.FILE_REFERENCE_NOT_FOUND, fileMeta.getRight());

				List<CommSessionService.ReceivedMessage> messages = instance[1].getCommScheduler().immediateCommSessionRead(id[0]);
				Assert.assertEquals(0, messages.size());

				fileMeta = instance[1].getFileDownloadService().readFileMeta(id[0], fileId);
				Assert.assertNull(fileMeta.getRight());
				Assert.assertNotNull(fileMeta.getLeft());
				Assert.assertEquals("test", fileMeta.getLeft().getName());

				String filenameToDownload = LOCAL_TEST_DATA_PATH + "/tmpTestFileDL";
				Pair<FileMetadata, FileDownloadService.DownloadError> fileMetaDownloaded = instance[1].getFileDownloadService().startDownload(id[0], fileId, filenameToDownload);
				Assert.assertNull(fileMetaDownloaded.getRight());
				Assert.assertNotNull(fileMetaDownloaded.getLeft());
				Assert.assertEquals("test", fileMetaDownloaded.getLeft().getName());
				Assert.assertEquals(dataLength, instance[1].getLocalFileSystemInterface().getLength(filenameToDownload));

				FileDownloadService.DownloadState stateStarted = instance[1].getFileDownloadService().getDownloadState(id[0], fileId);
				Assert.assertNotNull(stateStarted.startedAt);
				Assert.assertEquals(filesTested + 1, instance[1].getFileDownloadService().listAllDownloads().size());

				waitForDownloadCompletion(instance[1], id[0], fileId);

				FileDownloadService.DownloadState stateCompleted = instance[1].getFileDownloadService().getDownloadState(id[0], fileId);
				Assert.assertNotNull(stateCompleted.startedAt);
				Assert.assertEquals(1.0, stateCompleted.downloadedFraction, 0.0);
				byte[] downloadedData = instance[1].getLocalFileSystemInterface().read(filenameToDownload, 0, dataLength);
				Assert.assertTrue(Arrays.equals(data, downloadedData));

				Assert.assertEquals(filesTested + 1, instance[1].getFileDownloadService().listAllDownloads().size());
				Assert.assertEquals(0, instance[1].getFileDownloadService().listCurrentDownloads().size());
			}

			//////////////// Download from self
			{
				String filenameToDownload = LOCAL_TEST_DATA_PATH + "/tmpTestFileSelfDL";
				Pair<FileMetadata, FileDownloadService.DownloadError> fileMetaDownloaded = instance[0].getFileDownloadService().startDownload(id[0], fileId, filenameToDownload);
				Assert.assertNull(fileMetaDownloaded.getRight());
				Assert.assertNotNull(fileMetaDownloaded.getLeft());
			}

			//////////////// Delete
			{
				List<byte[]> list = instance[0].getFileUploadService().list();
				Assert.assertEquals(1, list.size());
				Assert.assertArrayEquals(fileId, list.get(0));
				FileMetadata fileMeta = instance[0].getFileUploadService().getMetadata(fileId);
				Assert.assertNotNull(fileMeta);
				Assert.assertEquals("test", fileMeta.getName());

				boolean success = instance[0].getFileUploadService().delete(fileId, false);
				Assert.assertTrue(success);
				FileMetadata fileMeta2 = instance[0].getFileUploadService().getMetadata(fileId);
				Assert.assertNull(fileMeta2);
			}

			filesTested++;
		}
	}

	private void waitForDownloadCompletion(InstanceContainer instance, byte[] fromId, byte[] fileId) {
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			FileDownloadService.DownloadState downloadState = instance.getFileDownloadService().getDownloadState(fromId, fileId);
			Assert.assertNotNull(downloadState.startedAt);
			if (downloadState.completedAt != null) {
				return;
			}
			Threads.sleepChecked(SLEEP_TIME);
		}
		long endTime = System.currentTimeMillis();
		log.debug("Waited " + (endTime - startTime) + " ms for download from " + Utils.getDebugStringFromId(fromId) + " : " + Utils.getDebugStringFromBytesShort(fileId));
	}
}
