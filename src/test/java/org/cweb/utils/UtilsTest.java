package org.cweb.utils;

import org.cweb.crypto.lib.SecureRandomUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTest {
	@Test
	public void testBase64() {
		String base64Str = "g87_-rcVVfQm7FvSQNPC77av5eBjH6pXkAHEc3k8g7PgWahPMDzbRVxkQT2yLs_wGSO5UpUrGZI2-oAfzAQDXQAA";
		byte[] decoded = Utils.decodeBase64(base64Str);
		assertEquals(base64Str, Utils.encodeBase64(decoded));
	}

	@Test
	public void testStringsForSharing() {
		String str = Utils.toBase32String(SecureRandomUtils.generateRandomBytes(1000));
		String strEncoded = ThriftTextUtils.formatShareableString(str, "Test_5", 60);
		String strDecoded = ThriftTextUtils.parseShareableString(strEncoded);
		assertEquals(str, strDecoded);
	}
}
