package org.cweb.communication;

import org.cweb.Config;
import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.remote.InMemoryMockClient;
import org.cweb.storage.remote.RemoteStorageFactory;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CommSessionTest extends CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(CommSessionTest.class);

	private static void send(InstanceContainer instance, byte[] destId, String messageStr) {
		TypedPayload message = TypedPayloadUtils.wrapCustom(messageStr.getBytes(StandardCharsets.UTF_8), SERVICE_NAME, "Message", null);
		boolean success = instance.getCommSessionService().sendMessage(destId, message);
		Assert.assertTrue("failed to send " + message, success);
		log.debug("Sent to " + Utils.getDebugStringFromId(destId) + " : " + new String(TypedPayloadUtils.unwrapCustom(message), StandardCharsets.UTF_8));
	}

	private static void receive(InstanceContainer instance, byte[] fromId, int expectMessagesReceived, int expectMessagesAcked) {
		receiveWithCheckForward(instance, fromId, expectMessagesReceived, expectMessagesAcked, 0);
	}

	private static void receiveWithCheckForward(InstanceContainer instance, byte[] fromId, int expectMessagesReceived, int expectMessagesAcked, int checkFrowardMessages) {
		instance.getCommSessionService().readMessages(fromId, checkFrowardMessages);
		List<CommSessionService.ReceivedMessage> receivedMessages = instance.getCommSessionService().fetchNewMessages(fromId, true);
		List<CommSessionService.ReceivedMessage> ackedMessages = instance.getCommSessionService().fetchAckedMessages(fromId, true);
		for (CommSessionService.ReceivedMessage message : receivedMessages) {
			log.debug("Received from " + Utils.getDebugStringFromId(fromId) + " : " + new String(TypedPayloadUtils.unwrapCustom(message.payload), StandardCharsets.UTF_8));
		}
		Assert.assertEquals("incorrect number of messages received", expectMessagesReceived, receivedMessages.size());
		Assert.assertEquals("incorrect number of messages acked", expectMessagesAcked, ackedMessages.size());
	}

	private static int tryReceiving(InstanceContainer instance, byte[] fromId) {
		instance.getCommSessionService().readMessages(fromId, 0);
		List<CommSessionService.ReceivedMessage> received = instance.getCommSessionService().fetchNewMessages(fromId, true);
		for (CommSessionService.ReceivedMessage message : received) {
			log.debug("Received from " + Utils.getDebugStringFromId(fromId) + " : " + new String(TypedPayloadUtils.unwrapCustom(message.payload), StandardCharsets.UTF_8));
		}
		return received.size();
	}

	@Before
	public void setupCommSession() {
		establishCommSession(instance[0], instance[1]);
	}

	@Test
	public void testCommSessionSimple() {
		send(instance[0], id[1], "message ABCD " + Math.random());
		flushUploadQueue(0);
		receive(instance[1], id[0], 1, 0);
	}

	@Test
	public void testCommSessionSerial() {
		int numMessages = 5;
		for (int i = 0; i < numMessages; i++) {
			send(instance[0], id[1], "message ABCD " + Math.random());
		}
		flushUploadQueue(0);

		receive(instance[1], id[0], numMessages, 0);
		for (int i = 0; i < numMessages; i++) {
			send(instance[1], id[0], "message XYZW " + Math.random());
		}
		flushUploadQueue(1);
		receive(instance[0], id[1], numMessages, numMessages);
	}

	@Test
	public void testCommSessionParallel() {
		int numMessages = 3;
		for (int i = 0; i < numMessages; i++) {
			send(instance[0], id[1], "message ABCD " + Math.random());
			flushUploadQueue(0);
			send(instance[1], id[0], "message XYZW " + Math.random());
		}
		flushUploadQueue(0, 1);
		receive(instance[1], id[0], numMessages, 0);
		receive(instance[0], id[1], numMessages, 0);
	}

	@Test
	public void testCommSessionPingPong() {
		int numMessages = 5;
		for (int i = 0; i < numMessages; i++) {
			send(instance[0], id[1], "message ABCD " + i);
			flushUploadQueue(0);
			receive(instance[1], id[0], 1, i == 0 ? 0 : 1);
			send(instance[1], id[0], "message XYZW " + i);
			flushUploadQueue(1);
			receive(instance[0], id[1], 1, 1);
		}
	}

	@Test
	public void testCommSessionInitSessionConflict() {
		Config.SIMULATE_REMOTE_STORAGE_WRITE_LATENCY = SLEEP_TIME_LONG / 3;

		boolean success1 = instance[1].getCommSessionService().establishSessionWith(id[2]);
		Assert.assertTrue(success1);
		send(instance[1], id[2], "message ABCD " + Math.random());

		boolean success2 = instance[2].getCommSessionService().establishSessionWith(id[1]);
		Assert.assertTrue(success2);
		send(instance[2], id[1], "message XYZW " + Math.random());

		Threads.sleepChecked(SLEEP_TIME_LONG);
		int received1 = tryReceiving(instance[1], id[2]);
		int received2 = tryReceiving(instance[2], id[1]);
		Assert.assertEquals("incorrect number of messages received", 1, received1 + received2);

		Config.SIMULATE_REMOTE_STORAGE_WRITE_LATENCY = 0;

		send(instance[1], id[2], "message ABCD " + Math.random());
		send(instance[2], id[1], "message XYZW " + Math.random());
		flushUploadQueue(1, 2);
		receive(instance[2], id[1], 1, received1);
		receive(instance[1], id[2], 1, received2);
	}

	@Test
	public void testCommSessionWithLoss() {
		InMemoryMockClient storageClient = (InMemoryMockClient) RemoteStorageFactory.getRemoteStorageClient();

		int messageId = 1;
		send(instance[0], id[1], "message ABCD " + messageId);
		send(instance[1], id[0], "message ZYZW " + messageId);
		messageId++;
		send(instance[0], id[1], "message ABCD " + messageId);
		send(instance[1], id[0], "message ZYZW " + messageId);

		flushUploadQueue(0, 1);
		final ArrayList<String> namesToSkip = new ArrayList<>();
		storageClient.setReadPredicate(new InMemoryMockClient.FilePredicate() {
			@Override
			public boolean match(String fullName) {
				if (fullName.endsWith("-sessionFrame")) {
					namesToSkip.add(fullName);
				}
				return false;
			}
		});

		receiveWithCheckForward(instance[1], id[0], 0, 0, 1);
		receiveWithCheckForward(instance[0], id[1], 0, 0, 1);

		messageId++;
		send(instance[0], id[1], "message ABCD1 " + messageId);
		send(instance[0], id[1], "message ABCD2 " + messageId);
		send(instance[1], id[0], "message ZYZW " + messageId);
		flushUploadQueue(0, 1);
		storageClient.setReadPredicate(new InMemoryMockClient.FilePredicate() {
			@Override
			public boolean match(String fullName) {
				return !namesToSkip.contains(fullName);
			}
		});
		receiveWithCheckForward(instance[1], id[0], 2, 0, 2);
		receiveWithCheckForward(instance[0], id[1], 1, 0, 2);

		storageClient.setReadPredicate(null);
		flushUploadQueue(0, 1);
		receiveWithCheckForward(instance[1], id[0], 2, 0, -1);
		receiveWithCheckForward(instance[0], id[1], 2, 0, -1);

		messageId++;
		send(instance[0], id[1], "message ABCD " + messageId);
		send(instance[1], id[0], "message ZYZW " + messageId);
		flushUploadQueue(0, 1);
		receiveWithCheckForward(instance[1], id[0], 1, 3, 0);
		receiveWithCheckForward(instance[0], id[1], 1, 4, 0);
	}

	@Test
	public void testCommSessionOverflow() {
		for (int i = 0; i < 200; i++) {
			send(instance[0], id[1], "message " + i);
		}
		TypedPayload message = TypedPayloadUtils.wrapCustom("overflow".getBytes(StandardCharsets.UTF_8), SERVICE_NAME, "Message", null);
		boolean success1 = instance[0].getCommSessionService().sendMessage(id[1], message);
		Assert.assertFalse(success1);

		flushUploadQueue(0);
		receive(instance[1], id[0], 200, 0);

		flushUploadQueue(1);
		receive(instance[0], id[1], 0, 200);

		boolean success2 = instance[0].getCommSessionService().sendMessage(id[1], message);
		Assert.assertTrue(success2);
	}
}
