package org.cweb.communication;

import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.shared.SharedSessionSyncStats;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SharedSessionTest extends CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(SharedSessionTest.class);

	private static byte[] createSession(InstanceContainer instance, List<byte[]> participants) {
		byte[] sessionId = instance.getSharedSessionService().createNewSession(Collections.emptyList());
		boolean added = instance.getSharedSessionService().updateParticipants(sessionId, true, participants);
		Assert.assertTrue("Added participants", added);
		return sessionId;
	}

	private static void send(InstanceContainer instance, byte[] sessionId, String messageStr) {
		send(instance, sessionId, messageStr, true);
	}

	private static void send(InstanceContainer instance, byte[] sessionId, String messageStr, boolean expectSendSuccess) {
		TypedPayload message = TypedPayloadUtils.wrapCustom(messageStr.getBytes(StandardCharsets.UTF_8), SERVICE_NAME, "Shared Session Message", null);
		boolean success = instance.getSharedSessionService().sendMessage(sessionId, message);
		Assert.assertEquals("success/failure of send " + message, expectSendSuccess, success);
		log.debug(Utils.getDebugStringFromId(instance.getIdentityService().getIdentityDescriptor().getId()) + " Sent to " + Utils.getDebugStringFromId(sessionId) + " : " + new String(TypedPayloadUtils.unwrapCustom(message), StandardCharsets.UTF_8));
	}

	private void flushQueues(Integer... instanceIds) {
		for (Integer instanceId : instanceIds) {
			instance[instanceId].getCommScheduler().flushSharedSessionQueue();
			flushUploadQueue(instanceId);
		}
	}

	private static List<SharedSessionService.ReceivedMessage> receive(InstanceContainer instance, byte[] sessionId, int expectMessagesReceived, int expectMessagesAcked) {
		List<SharedSessionService.ReceivedMessage> receivedMessages = instance.getSharedSessionService().fetchNewMessages(sessionId, true);
		List<SharedSessionService.ReceivedMessage> ackedMessages = instance.getSharedSessionService().fetchAckedMessages(sessionId, true);
		for (SharedSessionService.ReceivedMessage message : receivedMessages) {
			log.debug(Utils.getDebugStringFromId(instance.getIdentityService().getIdentityDescriptor().getId()) + " Received from " + Utils.getDebugStringFromId(sessionId) + ":" + Utils.getDebugStringFromId(message.fromId) + ":" + Utils.formatDateTime(message.createdAt) + " / " + Utils.formatDateTime(message.receivedAt) + " : " + new String(TypedPayloadUtils.unwrapCustom(message.payload), StandardCharsets.UTF_8));
		}
		for (SharedSessionService.ReceivedMessage message : ackedMessages) {
			Assert.assertArrayEquals(instance.getOwnId(), message.fromId);
		}
		Assert.assertEquals("incorrect number of messages received", expectMessagesReceived, receivedMessages.size());
		Assert.assertEquals("incorrect number of messages acked", expectMessagesAcked, ackedMessages.size());
		return receivedMessages;
	}

	private boolean isOrderEqual(List<SharedSessionService.ReceivedMessage> messages, List<String> messageStrings) {
		if (messages.size() != messageStrings.size()) {
			return false;
		}
		for (int i = 0; i < messages.size(); i++) {
			if (!messageStrings.get(i).equals(new String(TypedPayloadUtils.unwrapCustom(messages.get(i).payload), StandardCharsets.UTF_8))) {
				return false;
			}
		}
		return true;
	}

	private void assertStats(int instanceId, byte[] sessionId, int syncMessagesSent, int syncMessagesReceived, int descriptorsReceived, int messagesFetched, int previousMessagesFetched, int prevTailsAdded, int prevTailsRemoved) {
		SharedSessionSyncStats stats = instance[instanceId].getSharedSessionService().getSyncStats(sessionId);
		Assert.assertEquals("syncMessagesSent", syncMessagesSent, stats.getSyncMessagesSent());
		Assert.assertEquals("syncMessagesReceived", syncMessagesReceived, stats.getSyncMessagesReceived());
		Assert.assertEquals("descriptorsReceived", descriptorsReceived, stats.getDescriptorsReceived());
		Assert.assertEquals("messagesFetched", messagesFetched, stats.getMessagesFetched());
		Assert.assertEquals("previousMessagesFetched", previousMessagesFetched, stats.getPreviousMessagesFetched());
		Assert.assertEquals("prevTailsAdded", prevTailsAdded, stats.getPrevTailsAdded());
		Assert.assertEquals("prevTailsRemoved", prevTailsRemoved, stats.getPrevTailsRemoved());
	}

	@Test
	public void testSharedSessionSimple() {
		establishCommSession(instance[0], instance[1]);
		byte[] sessionId = createSession(instance[0], Collections.singletonList(id[1]));

		flushQueues(0);
		receiveDirectFromAll(1);

		send(instance[0], sessionId, "message ABCD " + Math.random());
		flushQueues(0);

		receiveDirectFromAll(1);
		flushQueues(1);
		receive(instance[1], sessionId, 1, 0);

		send(instance[1], sessionId, "message XYZ " + Math.random());
		flushQueues(1);

		receiveDirectFromAll(0);
		receive(instance[0], sessionId, 1, 1);

		assertStats(0, sessionId, 2, 2, 0, 1, 0, 1, 1);
		assertStats(1, sessionId, 2, 1, 1, 1, 0, 1, 0);
	}

	@Test
	public void testSharedSession2hop() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[1], instance[2]);
		byte[] sessionId = createSession(instance[1], Arrays.asList(id[0], id[2]));

		flushQueues(1);
		receiveDirectFromAll(0, 2);

		send(instance[0], sessionId, "message ABCD " + Math.random());
		flushQueues(0);

		receiveDirectFromAll(1);
		flushQueues(1);
		receiveDirectFromAll(2);
		flushQueues(2);
		receive(instance[1], sessionId, 1, 0);
		receive(instance[2], sessionId, 1, 0);

		send(instance[2], sessionId, "message XYZ " + Math.random());
		flushQueues(2);

		receiveDirectFromAll(1);
		flushQueues(1);
		receiveDirectFromAll(0);
		flushQueues(0);
		receive(instance[1], sessionId, 1, 0);
		receive(instance[0], sessionId, 1, 1);

		assertStats(0, sessionId, 2, 2, 1, 1, 0, 1, 1);
		assertStats(1, sessionId, 4, 3, 0, 2, 0, 2, 1);
		assertStats(2, sessionId, 2, 1, 1, 1, 0, 1, 0);
	}

	@Test
	public void testSharedSessionStorageProfilePropagation() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[1], instance[2]);
		instance[0].getRemoteIdentityService().setRemoteStorageProfile(id[2], instance[2].getPublicStorageProfile());
		byte[] sessionId = createSession(instance[0], Arrays.asList(id[1], id[2]));
		flushQueues(0);
		receiveDirectFromAll(1);

		flushQueues(1);
		send(instance[0], sessionId, "message ABCD " + Math.random());
		flushQueues(0);

		receiveDirectFromAll(1);
		receive(instance[1], sessionId, 1, 0);
		flushQueues(1);
		receiveDirectFromAll(2);  // First message ref from 1, request sync from admin 0
		flushQueues(2);  // Fetch descriptor from 0
		flushQueues(2);  // Fetch message from 0
		receive(instance[2], sessionId, 1, 0);

		assertStats(0, sessionId, 2, 0, 0, 0, 0, 0, 0);
		assertStats(1, sessionId, 2, 1, 1, 1, 0, 1, 0);
		assertStats(2, sessionId, 2, 2, 1, 1, 0, 1, 0);
	}

	@Test
	public void testSharedSessionBuffering() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[0], instance[2]);
		byte[] sessionId = createSession(instance[0], Arrays.asList(id[1], id[2]));
		flushQueues(0);

		receiveDirectFromAll(1, 2);
		flushQueues(1);

		for (int i = 0; i < 6; i++) {
			send(instance[1], sessionId, "message ABCD " + i);
		}
		flushQueues(1);

		receiveDirectFromAll(0);
		flushQueues(0);
		receiveDirectFromAll(2);
		flushQueues(2);
		receive(instance[2], sessionId, 6, 0);

		receiveDirectFromAll(1);

		assertStats(0, sessionId, 2, 1, 0, 6, 5, 1, 0);
		assertStats(1, sessionId, 1, 1, 1, 0, 0, 0, 0);
		assertStats(2, sessionId, 1, 1, 1, 6, 5, 1, 0);
	}

	@Test
	public void testAddingRemoving() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[1], instance[2]);
		byte[] sessionId = createSession(instance[1], Arrays.asList(id[0]));
		flushQueues(1);

		receiveDirectFromAll(0);
		flushQueues(0);

		for (int i = 0; i < 3; i++) {
			send(instance[0], sessionId, "message ABCD " + i);
		}
		flushQueues(0);

		receiveDirectFromAll(1);
		receive(instance[1], sessionId, 3, 0);

		assertStats(0, sessionId, 1, 0, 1, 0, 0, 0, 0);
		assertStats(1, sessionId, 1, 1, 0, 3, 2, 1, 0);

		instance[1].getSharedSessionService().updateParticipants(sessionId, true, Arrays.asList(id[2]));
		flushQueues(1);

		send(instance[0], sessionId, "message RC");
		flushQueues(0);

		receiveDirectFromAll(0, 2, 1);
		receive(instance[1], sessionId, 1, 0);
		receive(instance[2], sessionId, 0, 0);

		assertStats(0, sessionId, 2, 1, 2, 0, 0, 0, 0);
		assertStats(1, sessionId, 3, 2, 0, 4, 2, 2, 1);
		assertStats(2, sessionId, 0, 0, 1, 0, 0, 0, 0);

		for (int i = 0; i < 3; i++) {
			send(instance[0], sessionId, "message XYZ " + i);
		}
		flushQueues(0);

		receiveDirectFromAll(1);
		receive(instance[1], sessionId, 3, 0);
		flushQueues(1);

		receiveDirectFromAll(2);
		receive(instance[2], sessionId, 3, 0);

		assertStats(0, sessionId, 3, 1, 2, 0, 0, 0, 0);
		assertStats(1, sessionId, 5, 3, 0, 7, 4, 3, 2);
		assertStats(2, sessionId, 1, 2, 1, 3, 2, 1, 0);

		instance[1].getSharedSessionService().updateParticipants(sessionId, false, Arrays.asList(id[0]));

		for (int i = 0; i < 3; i++) {
			send(instance[1], sessionId, "message N " + i);
		}
		flushQueues(1);

		receiveDirectFromAll(2);
		flushQueues(2);
		receive(instance[2], sessionId, 3, 0);

		assertStats(0, sessionId, 3, 1, 2, 0, 0, 0, 0);
		assertStats(1, sessionId, 6, 3, 0, 7, 4, 3, 2);
		assertStats(2, sessionId, 2, 3, 2, 6, 4, 2, 1);

		receiveDirectFromAll(0);
		flushQueues(0);
		receive(instance[0], sessionId, 0, 7);

		send(instance[0], sessionId, "message after unsubscription", false);
		flushQueues(0);

		receiveDirectFromAll(1);
		flushQueues(1);
		receive(instance[1], sessionId, 0, 3);
	}

	@Test
	public void testMessageOrdering() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[1], instance[2]);
		byte[] sessionId = createSession(instance[1], Arrays.asList(id[0], id[2]));
		flushQueues(1);

		receiveDirectFromAll(0, 2);
		flushQueues(0, 2);
		for (int i = 0; i < 2; i++) {
			send(instance[0], sessionId, "message ABCD " + i);
		}
		Threads.sleepChecked(SLEEP_TIME);
		for (int i = 0; i < 2; i++) {
			send(instance[2], sessionId, "message XYZ " + i);
		}
		flushQueues(0, 2);

		receiveDirectFromAll(1);
		flushQueues(1);
		List<SharedSessionService.ReceivedMessage> received = receive(instance[1], sessionId, 4, 0);
		Assert.assertTrue(isOrderEqual(received, Arrays.asList(
				"message ABCD 0",
				"message ABCD 1",
				"message XYZ 0",
				"message XYZ 1"
		)));
	}

	@Test
	public void testCommSessionPush() {
		establishCommSession(instance[0], instance[1]);
		establishCommSession(instance[1], instance[2]);
		byte[] sessionId = createSession(instance[1], Arrays.asList(id[0], id[2]));

		flushQueues(1);
		receiveDirectFromAll(0, 2);

		Assert.assertFalse(instance[0].getCommSessionService().haveSessionWith(id[2]));
		Assert.assertFalse(instance[2].getCommSessionService().haveSessionWith(id[0]));

		instance[0].getCommSessionService().establishSessionWith(id[2]);

		Assert.assertTrue(instance[0].getCommSessionService().haveSessionWith(id[2]));
		Assert.assertFalse(instance[2].getCommSessionService().haveSessionWith(id[0]));

		flushQueues(0);
		receiveDirectFromAll(1);
		flushQueues(1);
		receiveDirectFromAll(2);
		flushQueues(2);

		Assert.assertTrue(instance[2].getCommSessionService().haveSessionWith(id[0]));
	}

	@Test
	public void testDelayedMessages() {
		establishCommSession(instance[0], instance[1]);
		byte[] sessionId = createSession(instance[1], Collections.singletonList(id[0]));
		flushQueues(1);
		receiveDirectFromAll(0);

		send(instance[0], sessionId, "message1");

		send(instance[1], sessionId, "message2");
		flushQueues(1);

		receiveDirectFromAll(0);
		receive(instance[0], sessionId, 1, 0);

		flushQueues(0);
		receiveDirectFromAll(1);
		flushQueues(1);
		receive(instance[1], sessionId, 1, 1);

		assertStats(0, sessionId, 1, 1, 1, 1, 0, 1, 0);
		assertStats(1, sessionId, 2, 1, 0, 1, 0, 1, 0);
	}
}
