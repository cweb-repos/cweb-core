package org.cweb.communication;

import org.cweb.CwebTestBase;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.object.SharedObjectDeliveryType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

public class SharedObjectTest extends CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(SharedObjectTest.class);

	private static TypedPayload wrapString(String str) {
		return TypedPayloadUtils.wrapCustom(str.getBytes(StandardCharsets.UTF_8), SERVICE_NAME, "Object", null);
	}

	private static byte[] create(InstanceContainer instance, SharedObjectDeliveryType type, String str, List<byte[]> subscribers) {
		TypedPayload payload = wrapString(str);
		byte[] objectId = instance.getSharedObjectPostService().create(type, 3600, 3600, 3600, payload);
		instance.getSharedObjectPostService().updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.SET, subscribers, null);
		Assert.assertNotNull("CreatedObject", objectId);
		return objectId;
	}

	private static void update(InstanceContainer instance, byte[] objectId, String str) {
		TypedPayload payload = wrapString(str);
		boolean success = instance.getSharedObjectPostService().updatePayload(objectId, payload);
		Assert.assertTrue("success/failure updating object " + payload, success);
		log.debug(Utils.getDebugStringFromId(instance.getIdentityService().getIdentityDescriptor().getId()) + " Updated object " + Utils.getDebugStringFromId(objectId) + " : " + new String(TypedPayloadUtils.unwrapCustom(payload), StandardCharsets.UTF_8));
	}

	private static TypedPayload getCurrent(InstanceContainer instance, byte[] objectId) {
		return instance.getSharedObjectReadService().getCurrent(objectId);
	}

	private static void readUpdates(InstanceContainer instance, byte[] fromId, byte[] objectId, String... strs) {
		List<TypedPayload> objects = readUpdates(instance, fromId, objectId, strs.length);
		Assert.assertEquals(strs.length, objects.size());
		for (int i = 0; i< strs.length; i++) {
			Assert.assertEquals(strs[i], new String(TypedPayloadUtils.unwrapCustom(objects.get(i)), StandardCharsets.UTF_8));
		}
	}

	private static List<TypedPayload> readUpdates(InstanceContainer instance, byte[] fromId, byte[] objectId, int expectUpdatesReceived) {
		List<CommSessionService.ReceivedMessage> messages = instance.getCommScheduler().immediateCommSessionRead(fromId);
		Assert.assertEquals("not expecting direct messages", 0, messages.size());
		List<TypedPayload> objects = instance.getCommScheduler().immediateSharedObjectRead(objectId);
		for (TypedPayload object : objects) {
			log.debug(Utils.getDebugStringFromId(instance.getIdentityService().getIdentityDescriptor().getId()) + " Received updated object " + Utils.getDebugStringFromId(objectId) + " : " + new String(TypedPayloadUtils.unwrapCustom(object), StandardCharsets.UTF_8));
		}
		Assert.assertEquals("incorrect number of updates received", expectUpdatesReceived, objects.size());
		return objects;
	}

	private static boolean isOrderEqual(List<SharedSessionService.ReceivedMessage> messages, List<String> messageStrings) {
		if (messages.size() != messageStrings.size()) {
			return false;
		}
		for (int i = 0; i < messages.size(); i++) {
			if (!messageStrings.get(i).equals(new String(TypedPayloadUtils.unwrapCustom(messages.get(i).payload), StandardCharsets.UTF_8))) {
				return false;
			}
		}
		return true;
	}

	@Test
	public void testSharedSessionSimpleDeliverLast() {
		String str1 = "message 1";
		byte[] objectId = create(instance[0], SharedObjectDeliveryType.DELIVER_LAST, str1, Collections.singletonList(id[1]));
		String str2 = "message 2";
		update(instance[0], objectId, str2);
		flushUploadQueue(0);
		readUpdates(instance[1], id[0], objectId, str2);
	}

	@Test
	public void testSharedSessionSimpleDeliverAll() {
		String str1 = "message 1";
		byte[] objectId = create(instance[0], SharedObjectDeliveryType.DELIVER_ALL, str1, Collections.singletonList(id[1]));
		String str2 = "message 2";
		update(instance[0], objectId, str2);
		flushUploadQueue(0);
		readUpdates(instance[1], id[0], objectId, str1, str2);
	}

	@Test
	public void testAddingRemoving() {
		String str1 = "message 1";
		byte[] objectId = create(instance[1], SharedObjectDeliveryType.DELIVER_ALL, str1, Collections.singletonList(id[0]));
		flushUploadQueue(1);

		readUpdates(instance[0], id[1], objectId, str1);
		readUpdates(instance[2], id[1], objectId);

		instance[1].getSharedObjectPostService().updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.ADD, Collections.singletonList(id[2]), null);
		flushUploadQueue(1);

		readUpdates(instance[0], id[1], objectId, str1);
		readUpdates(instance[2], id[1], objectId, str1);

		String str2 = "message 2";
		update(instance[1], objectId, str2);
		flushUploadQueue(1);

		readUpdates(instance[0], id[1], objectId, str2);
		readUpdates(instance[2], id[1], objectId, str2);

		instance[1].getSharedObjectPostService().updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.REMOVE, Collections.singletonList(id[0]), null);
		flushUploadQueue(1);

		readUpdates(instance[2], id[1], objectId);
		readUpdates(instance[0], id[1], objectId);

		String str3 = "message 3";
		update(instance[1], objectId, str3);
		flushUploadQueue(1);

		readUpdates(instance[2], id[1], objectId, str3);
		readUpdates(instance[0], id[1], objectId);

		String str4 = "message 4";
		instance[1].getSharedObjectPostService().updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.ADD, Collections.singletonList(id[0]), wrapString(str4));
		flushUploadQueue(1);

		readUpdates(instance[2], id[1], objectId, str4);
		readUpdates(instance[0], id[1], objectId, str4);
	}
}
