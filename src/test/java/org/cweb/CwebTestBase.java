package org.cweb;

import org.cweb.communication.CommSessionService;
import org.cweb.communication.DefaultSchedulingProvider;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CwebTestBase {
	private static final Logger log = LoggerFactory.getLogger(CwebTestBase.class);
	public static final String PRIVATE_STORAGE_PROFILE_STR = "s3!https://s3-us-west-2.amazonaws.com!us-west-2!cweb-test!test1!!!AKSGRGETGSFGSQWZT2Q!ksjdvhkfjh4r34knsvmv";
	protected static final String LOCAL_ROOT_PATH = "/tmp/cweb-test-data";
	protected static final String LOCAL_TEST_DATA_PATH = "/tmp/cweb-test-localdata";
	protected static final long SLEEP_TIME = 20;
	protected static final long SLEEP_TIME_LONG = 100;
	protected static final String SERVICE_NAME = "Test";
	protected static final int NUM = 3;

	protected PrivateStorageProfile privateStorageProfile;
	protected PublicStorageProfile publicStorageProfile;

	protected byte[][] id;
	protected InstanceContainer[] instance;

	@Before
	public void setUp() {
		Config.remoteStorageType = Config.RemoteStorageType.LOCAL_MEMORY;
		Config.localStorageType = Config.LocalStorageType.MEMORY;
		Config.localFileSystemType = Config.LocalFileSystemType.MEMORY;
		Config.COMM_SESSION_REMOTE_MESSAGE_DELETION_DELAY = SLEEP_TIME;

		privateStorageProfile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(PRIVATE_STORAGE_PROFILE_STR);
		publicStorageProfile = StorageProfileUtils.toPublicStorageProfile(privateStorageProfile);

		id = new byte[NUM][];
		instance = new InstanceContainer[NUM];

		for (int i = 0; i < NUM; i++) {
			instance[i] = AccountUtils.createNewIdentity(LOCAL_ROOT_PATH, privateStorageProfile, null);
			id[i] = instance[i].getOwnId();
			instance[i].getCommScheduler().init(new DefaultSchedulingProvider());
		}

		for (int i = 0; i < NUM; i++) {
			instance[i].flushUploadQueue(SLEEP_TIME);
			log.debug("id[" + i + "]=" + IdentityService.toString(id[i]));
		}

		for (int i = 1; i < NUM; i++) {
			instance[i - 1].getRemoteIdentityService().setRemoteStorageProfile(id[i], instance[i].getPublicStorageProfile());
			instance[i].getRemoteIdentityService().setRemoteStorageProfile(id[i - 1], instance[i - 1].getPublicStorageProfile());
		}
	}

	@After
	public void tearDown() {
		for (int i = 0; i < NUM; i++) {
			instance[i].close();
		}
	}

	protected void establishCommSession(InstanceContainer instance1, InstanceContainer instance2) {
		instance1.getRemoteIdentityService().setRemoteStorageProfile(instance2.getOwnId(), instance2.getPublicStorageProfile());
		instance2.getRemoteIdentityService().setRemoteStorageProfile(instance1.getOwnId(), instance1.getPublicStorageProfile());

		boolean success = instance1.getCommSessionService().establishSessionWith(instance2.getOwnId());
		Assert.assertTrue("failed to establish session with " + Utils.getDebugStringFromId(instance2.getOwnId()), success);
		Assert.assertTrue(instance1.getCommSessionService().haveSessionWith(instance2.getOwnId()));

		Threads.sleepChecked(SLEEP_TIME);

		List<CommSessionService.ReceivedMessage> received = instance2.getCommScheduler().immediateCommSessionRead(instance1.getOwnId());
		Assert.assertEquals("incorrect number of messages received", 0, received.size());
		Assert.assertTrue(instance2.getCommSessionService().haveSessionWith(instance1.getOwnId()));

		log.debug("Established CommSession between " + Utils.getDebugStringFromId(instance1.getIdentityService().getIdentityDescriptor().getId()) + " and " + Utils.getDebugStringFromId(instance2.getIdentityService().getIdentityDescriptor().getId()));
	}

	protected void flushUploadQueue(Integer... instanceIds) {
		for (Integer instanceId : instanceIds) {
			instance[instanceId].flushUploadQueue(SLEEP_TIME);
		}
	}

	protected void receiveDirectFrom(int instanceId, int fromInstanceId) {
		InstanceContainer instance = this.instance[instanceId];
		List<byte[]> ids = new ArrayList<>();
		List<CommSessionService.ReceivedMessage> receivedDirect = instance.getCommScheduler().immediateCommSessionRead(id[fromInstanceId]);
		Assert.assertEquals("received direct messages", 0, receivedDirect.size());
	}

	protected void receiveDirectFromAll(Integer... instanceIds) {
		for (Integer instanceId : instanceIds) {
			InstanceContainer instance = this.instance[instanceId];
			List<byte[]> ids = new ArrayList<>();
			for (byte[] id : id) {
				if (Arrays.equals(id, instance.getOwnId())) {
					continue;
				}
				if (!instance.getCommSessionService().haveSessionWith(id)) {
					continue;
				}
				ids.add(id);
			}
			List<CommSessionService.ReceivedMessage> receivedDirect = instance.getCommScheduler().immediateCommSessionRead(ids);
			Assert.assertEquals("received direct messages", 0, receivedDirect.size());
		}
	}

	protected void waitForCommSchedulerMessageLoop(Integer... instanceIds) {
		for (Integer instanceId : instanceIds) {
			InstanceContainer instance = this.instance[instanceId];
			instance.getCommScheduler().waitForMessageLoop();
		}
	}
}
