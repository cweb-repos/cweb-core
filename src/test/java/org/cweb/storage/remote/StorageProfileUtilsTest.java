package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateS3StorageProfile;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicS3StorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.S3Credentials;
import org.cweb.utils.ThriftTextUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StorageProfileUtilsTest {
	private static final PublicStorageProfile publicStorageProfileS3;
	private static final PrivateStorageProfile privateStorageProfileS3;
	private static final PublicStorageProfile publicStorageProfileS3withReadCred;
	private static final PrivateStorageProfile privateStorageProfileS3withReadCred;

	static {
		{
			PublicS3StorageProfile publicS3StorageProfile = new PublicS3StorageProfile("https://172.17.0.2:9000", "us-east-1", "bucket", "prefix", null);
			PrivateS3StorageProfile privateS3StorageProfile = new PrivateS3StorageProfile(publicS3StorageProfile, new S3Credentials("accessKeyId", "key"));
			publicStorageProfileS3 = new PublicStorageProfile();
			publicStorageProfileS3.setPublicS3StorageProfile(publicS3StorageProfile);
			privateStorageProfileS3 = new PrivateStorageProfile();
			privateStorageProfileS3.setPrivateS3StorageProfile(privateS3StorageProfile);
		}
		{
			PublicS3StorageProfile publicS3StorageProfile = new PublicS3StorageProfile("https://172.17.0.2:9000", "us-east-1", "bucket", "prefix", new S3Credentials("accessKeyIdRead", "keyRead"));
			PrivateS3StorageProfile privateS3StorageProfile = new PrivateS3StorageProfile(publicS3StorageProfile, new S3Credentials("accessKeyId", "key"));
			publicStorageProfileS3withReadCred = new PublicStorageProfile();
			publicStorageProfileS3withReadCred.setPublicS3StorageProfile(publicS3StorageProfile);
			privateStorageProfileS3withReadCred = new PrivateStorageProfile();
			privateStorageProfileS3withReadCred.setPrivateS3StorageProfile(privateS3StorageProfile);
		}
	}

	@Test
	public void testParseStorageProfileHumanReadableS3() {
		String privateStorageProfileStr = "s3!https://172.17.0.2:9000!us-east-1!bucket!prefix!!!accessKeyId!key";
		PrivateStorageProfile privateStorageProfileParsed = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(privateStorageProfileStr);
		assertEquals(privateStorageProfileS3.toString(), privateStorageProfileParsed.toString());
	}

	@Test
	public void testParseStorageProfileHumanReadableS3withReadCred() {
		String privateStorageProfileStr = "s3!https://172.17.0.2:9000!us-east-1!bucket!prefix!accessKeyIdRead!keyRead!accessKeyId!key";
		PrivateStorageProfile privateStorageProfileParsed = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(privateStorageProfileStr);
		assertEquals(privateStorageProfileS3withReadCred.toString(), privateStorageProfileParsed.toString());
	}

	@Test
	public void testParseStorageProfileS3() {
		String publicStorageProfileStr = "aagaaaqlaaaqaaaac5uhi5dqom5c6lzrg4zc4mjxfyyc4mr2heydamalaabaaaaabf2xgllfmfzxiljrbmaagaaaaadge5ldnnsxicyaaqaaaaagobzgkztjpaaaa999";
		String privateStorageProfileStr = "aj4jzy3bmdrgcyhemzqgiydacdhsqkjjfc3nfvzxgq35emzu24z5am5swi2damhamzqafstssyloxjrgc2l6qgtsgmyah6lmjgs4twnjexoaylbajzavc2s2mycqgdydcpccz3we4tsnjytc57kevtyurb7on3gujiqakacbxmkcy999";
		PublicStorageProfile publicStorageProfileParsed = ThriftTextUtils.fromUrlSafeString(publicStorageProfileStr, PublicStorageProfile.class);
		assertEquals(publicStorageProfileS3.toString(), publicStorageProfileParsed.toString());
		assertEquals(publicStorageProfileStr, ThriftTextUtils.toUrlSafeString(publicStorageProfileS3));
		PrivateStorageProfile privateStorageProfileParsed = ThriftTextUtils.fromUrlSafeString(privateStorageProfileStr, PrivateStorageProfile.class);
		assertEquals(privateStorageProfileS3.toString(), privateStorageProfileParsed.toString());
		assertEquals(privateStorageProfileStr, ThriftTextUtils.toUrlSafeString(privateStorageProfileS3));
	}
}
