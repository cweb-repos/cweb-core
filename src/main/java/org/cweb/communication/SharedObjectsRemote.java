package org.cweb.communication;

import org.cweb.schemas.comm.object.LocalSharedObjectStateRemote;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

class SharedObjectsRemote extends LocalDataSingleKey<LocalSharedObjectStateRemote> {
	private static final String NAME_SUFFIX = "-sharedObjectStateRemote";

	SharedObjectsRemote(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalSharedObjectStateRemote get(byte[] objectId) {
		return super.get(objectId, LocalSharedObjectStateRemote.class);
	}

	public void put(LocalSharedObjectStateRemote localState) {
		super.put(localState.getCurrentReference().getObjectId(), localState);
	}
}
