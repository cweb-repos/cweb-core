package org.cweb.communication;

public interface CommSessionMessageCallback {
	void onMessagesReceived(byte[] fromId);
}
