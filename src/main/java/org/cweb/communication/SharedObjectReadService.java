package org.cweb.communication;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.crypto.lib.SequenceGenerator;
import org.cweb.identity.RemoteIdentityService;
import org.cweb.payload.GenericPayloadTypePredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.comm.object.LocalSharedObjectStateRemote;
import org.cweb.schemas.comm.object.SharedObject;
import org.cweb.schemas.comm.object.SharedObjectDeliveryType;
import org.cweb.schemas.comm.object.SharedObjectReference;
import org.cweb.schemas.comm.object.SharedObjectSyncMessage;
import org.cweb.schemas.comm.object.SharedObjectUnsubscribedMessage;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SharedObjectReadService {
	private static final Logger log = LoggerFactory.getLogger(SharedObjectReadService.class);
	private static final int MAX_CONSECUTIVE_FETCH_ERRORS = 20;

	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;
	private final RemoteIdentityService remoteIdentityService;

	private final RemoteFileHandler remoteFileHandler;

	private final SharedObjectsRemote localObjects;

	private final Set<ByteBuffer> objectIdsToSync = new LinkedHashSet<>();
	private final List<Pair<PayloadTypePredicate, SharedObjectUpdateProcessor>> updateProcessors = new ArrayList<>();
	private SharedObjectCallbackInternal callback;

	public SharedObjectReadService(String tracePrefix, CryptoHelper cryptoHelper, RemoteIdentityService remoteIdentityService, CommSessionService commSessionService, RemoteFileHandler remoteFileHandler, LocalStorageInterface localStorageInterface) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.remoteIdentityService = remoteIdentityService;
		this.remoteFileHandler = remoteFileHandler;

		this.localObjects = new SharedObjectsRemote(tracePrefix, localStorageInterface, 10, 10);

		PayloadTypePredicate predicate = new GenericPayloadTypePredicate(PayloadType.SHARED_OBJECT_SYNC_MESSAGE, SharedObjectCommon.SERVICE_NAME, null, null);
		commSessionService.addMessageProcessor(predicate, this::processSyncMessage);

		// logObjectsDbg();

		for (byte[] objectId : localObjects.list()) {
			LocalSharedObjectStateRemote localState = localObjects.get(objectId);
			if (!localState.getSyncQueue().isEmpty() || !localState.isSetObject()) {
				objectIdsToSync.add(ByteBuffer.wrap(objectId));
			}
		}
	}

	public static class ObjectMetadata {
		public final byte[] objectId;
		public final byte[] fromId;
		public final Long pollInterval;
		public final boolean isUnsubscribed;

		public ObjectMetadata(byte[] objectId, byte[] fromId, Long pollInterval, boolean isUnsubscribed) {
			this.objectId = objectId;
			this.fromId = fromId;
			this.pollInterval = pollInterval;
			this.isUnsubscribed = isUnsubscribed;
		}
	}

	public synchronized boolean addUpdateProcessor(PayloadTypePredicate predicate, SharedObjectUpdateProcessor processor) {
		updateProcessors.add(Pair.of(predicate, processor));
		return true;
	}

	private SharedObjectUpdateProcessor getUpdateProcessor(TypedPayload payload) {
		for (Pair<PayloadTypePredicate, SharedObjectUpdateProcessor> pair : updateProcessors) {
			if (pair.getLeft().match(payload.getMetadata())) {
				return pair.getRight();
			}
		}
		return null;
	}

	void setCallback(SharedObjectCallbackInternal sharedObjectCallbackInternal) {
		this.callback = sharedObjectCallbackInternal;
	}

	public void requestObjectFetch(byte[] objectId, byte[] fromId) {
		callback.requestImmediateFetch(objectId, fromId);
	}

	public ObjectMetadata getObjectMetadata(byte[] objectId) {
		LocalSharedObjectStateRemote localState = localObjects.get(objectId);
		if (localState == null) {
			return null;
		}

		ObjectMetadata metadata = new ObjectMetadata(objectId, Utils.copyOf(localState.getCurrentReference().getFromId()), localState.isSetObject() ? localState.getObject().getPollInterval() : null, localState.isSetUnsubscribedTime());
		return metadata;
	}

	public synchronized List<byte[]> consumeBufferedIdsToRead() {
		Set<ByteBuffer> idsWrapped = Utils.drain(objectIdsToSync);
		return Utils.toArray(idsWrapped);
	}

	private void logObjectsDbg() {
		for (byte[] objectId : localObjects.list()) {
			LocalSharedObjectStateRemote localState = localObjects.get(objectId);
			log.info(tracePrefix + " DUMP_REMOTE_OBJECT: " + toDebugString(localState));
		}
	}

	private String toDebugString(LocalSharedObjectStateRemote localState) {
		SharedObject object = localState.getObject();
		return Utils.getDebugStringFromId(object.getFromId()) + ":" + Utils.getDebugStringFromId(object.getObjectId()) + ":" + Utils.getDebugStringFromBytes(HashingUtils.SHA256(localState.getCurrentReference().getKey()), 4) + ":v" + localState.getNextVersionForCurrentKey() + " unsubscribedTime=" + Utils.formatDateTime(localState.getUnsubscribedTime()) + ", syncQueueSize=" + localState.getSyncQueue().size();
	}

	private synchronized MessageProcessor.Result processSyncMessage(SessionId sessionId, TypedPayload typedPayload) {
		if (sessionId.getType() != SessionType.COMM_SESSION) {
			log.debug(tracePrefix + " Invalid session type " + sessionId.getType());
			return MessageProcessor.Result.INVALID;
		}
		byte[] fromId = sessionId.getId();

		Pair<SharedObjectSyncMessage, String> syncMessageUnwrapped = TypedPayloadUtils.unwrap(typedPayload, SharedObjectSyncMessage.class, SharedObjectCommon.SERVICE_NAME);
		if (syncMessageUnwrapped.getRight() != null) {
			log.debug(tracePrefix + " Failed to extract sync message: " + syncMessageUnwrapped.getRight());
			return MessageProcessor.Result.INVALID;
		}

		SharedObjectSyncMessage syncMessage = syncMessageUnwrapped.getLeft();

		if (syncMessage.isSetReference()) {
			enqueueReference(fromId, syncMessage);
		} else if (syncMessage.isSetUnsubscribedMessage()) {
			enqueueUnsubscription(fromId, syncMessage);
		}

		return MessageProcessor.Result.SUCCESS;
	}

	private static String formatReference(SharedObjectReference reference) {
		return Utils.getDebugStringFromId(reference.getFromId()) + ":" + Utils.getDebugStringFromBytes(reference.getObjectId()) + ":" + Utils.getDebugStringFromBytes(HashingUtils.SHA256(reference.getKey()), 4) + ":v" + reference.getLastVersionOfPreviousKey();
	}

	private boolean enqueueReference(byte[] fromId, SharedObjectSyncMessage syncMessage) {
		SharedObjectReference reference = syncMessage.getReference();
		byte[] objectId = reference.getObjectId();

		log.trace(tracePrefix + " Received new reference from " + Utils.getDebugStringFromId(fromId) + ": " + formatReference(reference));

		byte[] objectIdCalculated = SharedObjectCommon.generateObjectId(reference.getFromId(), reference.getObjectIdSeed());
		if (!Arrays.equals(objectId, objectIdCalculated)) {
			log.info(tracePrefix + " Mismatching reference objectId for " + Utils.getDebugStringFromBytes(objectId) + " : " + Utils.getDebugStringFromBytes(objectId) + " != " + Utils.getDebugStringFromId(objectIdCalculated));
			return false;
		}
		if (!Arrays.equals(fromId, reference.getFromId())) {
			log.debug(tracePrefix + " Received reference from wrong sender " + Utils.getDebugStringFromBytes(objectId) + " : " + Utils.getDebugStringFromId(fromId) + " : " + Utils.getDebugStringFromId(reference.getFromId()));
			return false;
		}

		LocalSharedObjectStateRemote localState = localObjects.get(objectId);
		long now = System.currentTimeMillis();

		if (localState == null) {
			localState = new LocalSharedObjectStateRemote(reference, 0, now, null, new ArrayList<>(), new ArrayList<>());
			log.trace(tracePrefix + " Received new object " + Utils.getDebugStringFromBytes(objectId));
			objectIdsToSync.add(ByteBuffer.wrap(objectId));
		} else {
			if (!Arrays.equals(reference.getFromId(), localState.getCurrentReference().getFromId())) {
				log.info(tracePrefix + " Mismatching reference fromId for " + Utils.getDebugStringFromBytes(objectId) + " : " + Utils.getDebugStringFromId(localState.getCurrentReference().getFromId()) + " -> " + Utils.getDebugStringFromId(reference.getFromId()));
				return false;
			}
			if (localState.isSetUnsubscribedTime()) {
				log.trace(tracePrefix + " Resubscribed to " + Utils.getDebugStringFromBytes(objectId));
				localState.unsetUnsubscribedTime();
			}
			enqueue(objectId, localState, syncMessage);
		}

		localObjects.put(localState);
		return true;
	}

	private boolean enqueueUnsubscription(byte[] fromId, SharedObjectSyncMessage syncMessage) {
		SharedObjectUnsubscribedMessage unsubscribedMessage = syncMessage.getUnsubscribedMessage();
		byte[] objectId = unsubscribedMessage.getObjectId();
		LocalSharedObjectStateRemote localState = localObjects.get(objectId);
		if (localState == null || !localState.isSetObject()) {
			log.info(tracePrefix + " Unsubscribe message from unknown object " + Utils.getDebugStringFromBytes(objectId));
			return false;
		}
		if (!Arrays.equals(fromId, localState.getCurrentReference().getFromId())) {
			log.info(tracePrefix + " Unsubscribe message from wrong sender " + Utils.getDebugStringFromBytes(objectId) + " : " + Utils.getDebugStringFromId(fromId));
			return false;
		}
		enqueue(objectId, localState, syncMessage);
		localObjects.put(localState);
		return true;
	}

	private void enqueue(byte[] objectId, LocalSharedObjectStateRemote localState, SharedObjectSyncMessage syncMessage) {
		ArrayList<SharedObjectSyncMessage> newSyncQueue;
		if (localState.isSetObject() && localState.getObject().getType() == SharedObjectDeliveryType.DELIVER_LAST) {
			newSyncQueue = new ArrayList<>();
			newSyncQueue.add(syncMessage);
		} else {
			newSyncQueue = new ArrayList<>(localState.getSyncQueue());
			if (!newSyncQueue.contains(syncMessage)) {
				newSyncQueue.add(syncMessage);
			}
		}
		localState.setSyncQueue(newSyncQueue);
		objectIdsToSync.add(ByteBuffer.wrap(objectId));
	}

	private enum FetchResult {
		NETWORK_ERROR,
		NETWORK_ERROR_PERSISTENT,
		NOT_FOUND_CONTINUE,
		NOT_FOUND_STOP,
		DECODING_ERROR,
		SUCCESS
	}

	synchronized int readObject(byte[] objectId) {
		LocalSharedObjectStateRemote localState = localObjects.get(objectId);
		if (localState == null) {
			return 0;
		}

		int checkForward = 0;
		/*if (localState.isSetObject()) {
			long now = System.currentTimeMillis();
			SharedObject object = localState.getObject();
			if (now - object.getCreatedAt() > object.getPollInterval() * 4) {
				checkForward = 1;
			}
		}*/
		int totalObjects = readNextObjects(localState, checkForward);

		if (totalObjects > 0) {
			processUpdates(objectId, localState);
		}

		localObjects.put(localState);

		return totalObjects;
	}

	private int readNextObjects(LocalSharedObjectStateRemote localState, int maxCheckForward) {
		int totalObjects = 0;
		int checkForward = 0;
		while (true) {
			Pair<FetchResult, SharedObject> fetchResultAndObject = readNext(localState, checkForward);
			localObjects.put(localState);
			FetchResult fetchResult = fetchResultAndObject.getLeft();
			if (fetchResult == FetchResult.SUCCESS) {
				if (checkForward > 0) {
					log.debug(tracePrefix + " Skipped " + checkForward + " versions on " + toDebugString(localState));
				}
				checkForward = 0;
				totalObjects++;
			} else if (fetchResult == FetchResult.NOT_FOUND_CONTINUE) {
			} else if (fetchResult == FetchResult.NOT_FOUND_STOP) {
				checkForward++;
				if (checkForward > maxCheckForward) {
					break;
				}
			} else if (fetchResult == FetchResult.DECODING_ERROR) {
			} else if (fetchResult == FetchResult.NETWORK_ERROR || fetchResult == FetchResult.NETWORK_ERROR_PERSISTENT) {
				break;
			} else {
				throw new RuntimeException("Invalid result: " + fetchResult);
			}
		}
		return totalObjects;
	}

	private void processUpdates(byte[] objectId, LocalSharedObjectStateRemote localState) {
		List<TypedPayload> updates = localState.getUnconsumedPastObjects();
		if (updates.isEmpty()) {
			return;
		}
		List<TypedPayload> consumedUpdates = new ArrayList<>();
		for (TypedPayload update : updates) {
			SharedObjectUpdateProcessor processor = getUpdateProcessor(update);
			if (processor != null) {
				SharedObjectUpdateProcessor.Result process = processor.processUpdate(objectId, update);
				if (process == SharedObjectUpdateProcessor.Result.PROCESSED) {
					consumedUpdates.add(update);
				}
			}
		}
		List<TypedPayload> newUpdates = new ArrayList<>(updates);
		newUpdates.removeAll(consumedUpdates);
		localState.setUnconsumedPastObjects(newUpdates);
	}

	private void processUnsubscription(LocalSharedObjectStateRemote localState) {
		byte[] objectId = localState.getObject().getObjectId();
		log.trace(tracePrefix + " Unsubscribed from " + Utils.getDebugStringFromBytes(objectId));
		long now = System.currentTimeMillis();
		localState.setUnsubscribedTime(now);

		SharedObjectUpdateProcessor processor = localState.isSetObject() ? getUpdateProcessor(localState.getObject().getPayload()) : null;
		if (processor != null) {
			processor.processUnsubscribe(objectId);
		}
	}

	private synchronized Pair<FetchResult, SharedObject> readNext(LocalSharedObjectStateRemote localState, int checkForward) {
		List<SharedObjectSyncMessage> syncQueue = localState.getSyncQueue();
		if (!syncQueue.isEmpty() && localState.isSetObject() && localState.getObject().getType() == SharedObjectDeliveryType.DELIVER_LAST) {
			SharedObjectSyncMessage lastSyncMessage = syncQueue.get(syncQueue.size() - 1);
			applySyncMessage(localState, lastSyncMessage);
			localState.setSyncQueue(new ArrayList<>());
			objectIdsToSync.remove(ByteBuffer.wrap(localState.getObject().getObjectId()));
		}

		SharedObjectReference currentReference = localState.getCurrentReference();
		byte[] fileName = SequenceGenerator.encode(currentReference.getObjectId(), currentReference.getKey(), localState.getNextVersionForCurrentKey() + checkForward, 32);

		RemoteFetchResultRaw fetchResult = remoteFileHandler.read(currentReference.getFromId(), fileName);
		if (fetchResult.getError() != null) {
			return Pair.of(fetchResult.getNumConsecutiveErrors() <= MAX_CONSECUTIVE_FETCH_ERRORS ? FetchResult.NETWORK_ERROR : FetchResult.NETWORK_ERROR_PERSISTENT, null);
		}
		if (fetchResult.getData() == null) {
			if (!syncQueue.isEmpty()) {
				SharedObjectSyncMessage syncMessage = syncQueue.remove(0);
				applySyncMessage(localState, syncMessage);
				localState.setSyncQueue(syncQueue);
				if (syncQueue.isEmpty()) {
					objectIdsToSync.remove(ByteBuffer.wrap(currentReference.getObjectId()));
				}
				return Pair.of(FetchResult.NOT_FOUND_CONTINUE, null);
			}
			return Pair.of(FetchResult.NOT_FOUND_STOP, null);
		}
		log.trace(tracePrefix + " Fetched object " + formatReference(currentReference) + "/" + localState.getNextVersionForCurrentKey());

		long now = System.currentTimeMillis();
		localState.setLastReceivedAt(now);
		localState.setNextVersionForCurrentKey(localState.getNextVersionForCurrentKey() + 1);

		Pair<SharedObject, String> objectDecoded = extractObject(currentReference, fetchResult);
		SharedObject object = objectDecoded.getLeft();
		if (object == null || objectDecoded.getRight() != null) {
			log.info(tracePrefix + " Failed to decode object " + Utils.getDebugStringFromBytes(currentReference.getObjectId()) + " : " + objectDecoded.getRight());
			return Pair.of(FetchResult.DECODING_ERROR, null);
		}

		localState.setObject(object);
		List<TypedPayload> unconsumedObjects;
		if (localState.isSetObject() && localState.getObject().getType() == SharedObjectDeliveryType.DELIVER_ALL) {
			unconsumedObjects = new ArrayList<>(localState.getUnconsumedPastObjects());
		} else {
			unconsumedObjects = new ArrayList<>();
		}
		unconsumedObjects.add(object.getPayload());
		localState.setUnconsumedPastObjects(unconsumedObjects);

		return Pair.of(FetchResult.SUCCESS, object);
	}

	private void applySyncMessage(LocalSharedObjectStateRemote localState, SharedObjectSyncMessage syncMessage) {
		if (syncMessage.isSetReference()) {
			SharedObjectReference reference = syncMessage.getReference();
			localState.setCurrentReference(reference);
			localState.setNextVersionForCurrentKey(0);
			log.trace(tracePrefix + " Switched to new ref " + formatReference(reference));
		} else if (syncMessage.isSetUnsubscribedMessage()) {
			processUnsubscription(localState);
		}
	}

	private Pair<SharedObject, String> extractObject(SharedObjectReference reference, RemoteFetchResultRaw fetchResult) {
		Decoded<DecodedTypedPayload> decodedDescriptorWrapper = CryptoHelper.decodeCryptoEnvelope(fetchResult.getData(), CryptoEnvelopeDecodingParams.create().setSymmetricDecryptionKey(reference.getKey()).setSymmetricAssociatedData(SharedObjectCommon.ASSOCIATED_DATA).setFetchSignerIfNeeded(true), CryptoContext.create().setCryptoHelper(cryptoHelper).setRemoteIdentityFetcher(remoteIdentityService.getFetcher()));
		if (decodedDescriptorWrapper.getError() != null || decodedDescriptorWrapper.getData() == null) {
			return Pair.of(null, "Error decoding envelope: " + decodedDescriptorWrapper.getError());
		}
		DecodedTypedPayload decodedTypedPayload = decodedDescriptorWrapper.getData();
		if (decodedTypedPayload.getSignatureMetadata() == null || !Arrays.equals(decodedTypedPayload.getSignatureMetadata().getSignerId(), reference.getFromId())) {
			return Pair.of(null, "Missing or mismatching signature");
		}
		TypedPayload typedPayload = decodedTypedPayload.getPayload();
		Pair<SharedObject, String> objectUnwrapped = TypedPayloadUtils.unwrap(typedPayload, SharedObject.class, SharedObjectCommon.SERVICE_NAME);
		if (objectUnwrapped.getRight() != null) {
			return Pair.of(null, "Failed to extract object: " + objectUnwrapped.getRight());
		}
		SharedObject object = objectUnwrapped.getLeft();
		if (object == null) {
			return Pair.of(null, "Failed to deserialize SharedSessionDescriptor");
		}
		if (!Arrays.equals(object.getFromId(), reference.getFromId())) {
			return Pair.of(null, "Mismatching fromId");
		}
		if (!Arrays.equals(object.getObjectId(), reference.getObjectId())) {
			return Pair.of(null, "Mismatching sessionId");
		}
		return Pair.of(object, null);
	}

	public TypedPayload getCurrent(byte[] objectId) {
		LocalSharedObjectStateRemote localState = localObjects.get(objectId);

		if (localState == null || !localState.isSetObject()) {
			return null;
		}

		return localState.getObject().getPayload();
	}

	public List<TypedPayload> getUnconsumedUpdates(byte[] objectId, boolean consume) {
		LocalSharedObjectStateRemote localState = localObjects.get(objectId);

		if (localState == null) {
			return new ArrayList<>();
		}

		ArrayList<TypedPayload> updates = new ArrayList<>(localState.getUnconsumedPastObjects());

		if (consume) {
			localState.setUnconsumedPastObjects(new ArrayList<>());
			localObjects.put(localState);
		}

		return updates;
	}
}
