package org.cweb.communication;

import org.cweb.schemas.comm.shared.SharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionMessageMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

class SharedSessionMessageSorter {
	private static final Logger log = LoggerFactory.getLogger(SharedSessionMessageSorter.class);
	private static final MessageComparator messageComparator = new MessageComparator();

	private static class MessageComparator implements Comparator<SharedSessionMessage> {
		@Override
		public int compare(SharedSessionMessage m1, SharedSessionMessage m2) {
			SharedSessionMessageMetadata md1 = m1.getMetadata();
			SharedSessionMessageMetadata md2 = m2.getMetadata();
			int result = -Long.compare(md1.getCreatedAt(), md2.getCreatedAt());
			if (result == 0) {
				result = Arrays.hashCode(md1.getMessageId()) - Arrays.hashCode(md2.getMessageId());
			}
			return result;
		}
	}

	static List<SharedSessionMessage> orderMessages(List<SharedSessionMessage> messages) {
		if (messages.size() <= 1) {
			return messages;
		}

		final Map<ByteBuffer, SharedSessionMessage> id2message = new HashMap<>(messages.size());
		final Map<ByteBuffer, Integer> id2incomingCount = new HashMap<>(messages.size());
		Set<SharedSessionMessage> queue = new TreeSet<>(messageComparator);

		for (SharedSessionMessage message : messages) {
			ByteBuffer messageIdWrapped = ByteBuffer.wrap(message.getMetadata().getMessageId());
			id2message.put(messageIdWrapped, message);
			id2incomingCount.put(messageIdWrapped, 0);
		}
		for (SharedSessionMessage message : messages) {
			for (SharedSessionMessageMetadata previousMessageMd : message.getPreviousMessageMetadata()) {
				ByteBuffer previousMessageId = ByteBuffer.wrap(previousMessageMd.getMessageId());
				Integer incomingCount = id2incomingCount.get(previousMessageId);
				if (incomingCount != null) {
					id2incomingCount.put(previousMessageId, incomingCount + 1);
				}
			}
		}
		for (SharedSessionMessage message : messages) {
			ByteBuffer messageIdWrapped = ByteBuffer.wrap(message.getMetadata().getMessageId());
			if (id2incomingCount.get(messageIdWrapped) == 0) {
				queue.add(message);
			}
		}

		List<SharedSessionMessage> outputMessages = new ArrayList<>(messages.size());
		while (!queue.isEmpty()) {
			SharedSessionMessage message = queue.iterator().next();
			queue.remove(message);
			id2message.remove(ByteBuffer.wrap(message.getMetadata().getMessageId()));
			outputMessages.add(message);
			for (SharedSessionMessageMetadata previousMessageMetadata : message.getPreviousMessageMetadata()) {
				ByteBuffer prevMessageIdWrapped = ByteBuffer.wrap(previousMessageMetadata.getMessageId());
				SharedSessionMessage prevMessage = id2message.get(prevMessageIdWrapped);
				if (prevMessage != null) {
					Integer incomingCount = id2incomingCount.get(prevMessageIdWrapped);
					incomingCount--;
					id2incomingCount.put(prevMessageIdWrapped, incomingCount);
					if (incomingCount == 0) {
						queue.add(prevMessage);
					}
				}
			}
		}
		if (!id2message.isEmpty()) {
			log.warn("Cycle in the graph");
		}
		Collections.reverse(outputMessages);
		return outputMessages;
	}
}
