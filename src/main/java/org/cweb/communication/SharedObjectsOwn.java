package org.cweb.communication;

import org.cweb.schemas.comm.object.LocalSharedObjectStateOwn;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

class SharedObjectsOwn extends LocalDataSingleKey<LocalSharedObjectStateOwn> {
	private static final String NAME_SUFFIX = "-sharedObjectStateOwn";

	SharedObjectsOwn(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalSharedObjectStateOwn get(byte[] objectId) {
		return super.get(objectId, LocalSharedObjectStateOwn.class);
	}

	public void put(LocalSharedObjectStateOwn localState) {
		super.put(localState.getObject().getObjectId(), localState);
	}
}
