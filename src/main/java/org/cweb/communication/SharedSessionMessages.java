package org.cweb.communication;

import org.cweb.schemas.comm.shared.LocalSharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionMessageMetadata;
import org.cweb.storage.local.LocalDataWithDir;
import org.cweb.storage.local.LocalStorageInterface;

class SharedSessionMessages extends LocalDataWithDir<LocalSharedSessionMessage> {
	private static final String DIR_NAME = "sharedSessionMsg";

	SharedSessionMessages(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(DIR_NAME, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public void put(LocalSharedSessionMessage message) {
		SharedSessionMessageMetadata metadata = message.getMessageReference();
		super.put(metadata.getSessionId(), metadata.getMessageId(), message);
	}

	public LocalSharedSessionMessage get(byte[] sessionId, byte[] messageId) {
		return super.get(sessionId, messageId, LocalSharedSessionMessage.class);
	}

	public boolean delete(LocalSharedSessionMessage message) {
		SharedSessionMessageMetadata metadata = message.getMessageReference();
		return super.delete(metadata.getSessionId(), metadata.getMessageId());
	}
}
