package org.cweb.communication;

public interface CommSessionEstablishmentCallback {
	void onSessionEstablished(byte[] withId);
}
