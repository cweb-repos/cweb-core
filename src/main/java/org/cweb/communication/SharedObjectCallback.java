package org.cweb.communication;

public interface SharedObjectCallback {
	void onUpdateReceived(byte[] sessionId);
}
