package org.cweb.communication;

import com.google.gson.Gson;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.Config;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.identity.RemoteIdentityService;
import org.cweb.payload.GenericPayloadTypePredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.comm.object.SharedObjectDeliveryType;
import org.cweb.schemas.comm.shared.LocalSharedSessionMessage;
import org.cweb.schemas.comm.shared.LocalSharedSessionPreviousData;
import org.cweb.schemas.comm.shared.LocalSharedSessionRecentPeer;
import org.cweb.schemas.comm.shared.LocalSharedSessionRecentPeerType;
import org.cweb.schemas.comm.shared.LocalSharedSessionState;
import org.cweb.schemas.comm.shared.SharedSessionDescriptor;
import org.cweb.schemas.comm.shared.SharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionMessageMetadata;
import org.cweb.schemas.comm.shared.SharedSessionParticipantInfo;
import org.cweb.schemas.comm.shared.SharedSessionSyncMessage;
import org.cweb.schemas.comm.shared.SharedSessionSyncStats;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.PropertyUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class SharedSessionService {
	private static final Logger log = LoggerFactory.getLogger(SharedSessionService.class);
	private static final String SERVICE_NAME = "SharedSession";
	private static final int MESSAGE_ID_SIZE = 16;
	private static final long DESCRIPTOR_OBJECT_TTL = 1000L * 60 * 60 * 24 * 30 * 2;
	private static final long DESCRIPTOR_KEY_TTL = 1000L * 60 * 60 * 24 * 7;
	private static final long DESCRIPTOR_POLL_INTERVAL = 1000L * 60 * 60 * 24 * 2;
	private static final long SESSION_KEY_TTL = 1000L * 60 * 60 * 24 * 2;
	private static final long SESSION_KEY_ROTATION_CHECK_INTERVAL = 1000L * 60 * 60 * 8;
	private static final long MESSAGE_TTL = 1000L * 60 * 60 * 24 * 14;
	private static final long CONSUMED_MESSAGE_TTL = 1000L * 60 * 60 * 24 * 30;
	private static final long UNCONSUMED_MESSAGE_TTL = 1000L * 60 * 60 * 24 * 60;
	private static final long UNFETCHED_MESSAGE_WARNING_TTL = 1000L * 60 * 60 * 24 * 7;
	private static final long UNFETCHED_MESSAGE_DELETION_TTL = 1000L * 60 * 60 * 24 * 30 * 6;
	private static final long NO_KEY_WARNING_TIME = 1000L * 60 * 60 * 24 * 3;
	private static final int MAX_CONSECUTIVE_FETCH_ERRORS = 20;
	private static final int MAX_PREV_DATA = 100;
	private static final int MAX_RECENT_PEERS = 4;
	private static final int MAX_UNCONSUMED_ACKED_SERIALS = 200;
	private static final int FORMER_PARTICIPANTS_GC_VERSIONS = 4;
	private static final String MESSAGE_NAME_SUFFIX = "-sharedSessionMsg";
	private static final byte[] ASSOCIATED_DATA = new byte[]{29, 121, 84};

	private final String tracePrefix;
	private final PublicStorageProfile ownStorageProfile;
	private final CryptoHelper cryptoHelper;
	private final RemoteIdentityService remoteIdentityService;
	private final CommSessionService commSessionService;
	private final SharedObjectPostService sharedObjectPostService;
	private final SharedObjectReadService sharedObjectReadService;

	private final List<Pair<PayloadTypePredicate, MessageProcessor>> messageProcessors = new ArrayList<>();
	private SharedSessionCallbackInternal callback;

	private final LocalSharedSessions localSessions;
	private final SharedSessionMessages localMessages;

	private final RemoteFileHandler remoteMessageHandler;

	private final SharedSessionWorkState workState;
	private long lastSessionKeyRotationCheckTime;

	private Map<ByteBuffer, SharedSessionSyncStats> session2syncStats = new ConcurrentHashMap<>();

	public static class ReceivedMessage {
		public final byte[] fromId;
		public final long createdAt;
		public final long receivedAt;
		public final TypedPayload payload;

		public ReceivedMessage(byte[] fromId, long createdAt, long receivedAt, TypedPayload payload) {
			this.fromId = fromId;
			this.createdAt = createdAt;
			this.receivedAt = receivedAt;
			this.payload = payload;
		}
	}

	public static class SessionMetadata {
		public final byte[] sessionId;
		public final boolean isUnsubscribed;
		public final byte[] adminId;
		public final byte[] sharedObjectId;
		public final List<Property> properties;
		public final List<byte[]> participantIds;
		public final long lastReceivedTime;

		SessionMetadata(byte[] sessionId, boolean isUnsubscribed, byte[] adminId, byte[] sharedObjectId, List<Property> properties, List<byte[]> participantIds, long lastReceivedTime) {
			this.sessionId = sessionId;
			this.isUnsubscribed = isUnsubscribed;
			this.adminId = adminId;
			this.sharedObjectId = sharedObjectId;
			this.properties = properties;
			this.participantIds = participantIds;
			this.lastReceivedTime = lastReceivedTime;
		}
	}

	private enum FetchResult {
		INTERNAL_STATE_ERROR,
		NETWORK_ERROR,
		NETWORK_ERROR_PERSISTENT,
		NOT_FOUND,
		DECODING_ERROR,
		SUCCESS
	}

	private enum ReferenceFetchState {
		ABORT_SESSION,
		ABORT_ALL,
		PROCEED
	}

	public SharedSessionService(String tracePrefix, PublicStorageProfile ownStorageProfile, CryptoHelper cryptoHelper, RemoteWriteService remoteWriteService, RemoteReadService remoteReadService, LocalStorageInterface localStorageInterface, RemoteIdentityService remoteIdentityService, CommSessionService commSessionService, SharedObjectPostService sharedObjectPostService, SharedObjectReadService sharedObjectReadService) {
		this.tracePrefix = tracePrefix;
		this.ownStorageProfile = ownStorageProfile;
		this.cryptoHelper = cryptoHelper;
		this.remoteIdentityService = remoteIdentityService;
		this.commSessionService = commSessionService;
		this.sharedObjectPostService = sharedObjectPostService;
		this.sharedObjectReadService = sharedObjectReadService;

		this.localSessions = new LocalSharedSessions(tracePrefix, localStorageInterface, 50, 60);
		this.localMessages = new SharedSessionMessages(tracePrefix, localStorageInterface, 30, 5);

		this.remoteMessageHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, MESSAGE_NAME_SUFFIX);

		this.workState = new SharedSessionWorkState(tracePrefix);

		PayloadTypePredicate messagePredicate = new GenericPayloadTypePredicate(PayloadType.SHARED_SESSION_SYNC_MESSAGE, SERVICE_NAME, null, null);
		commSessionService.addMessageProcessor(messagePredicate, new MessageProcessor() {
			@Override
			public Result process(SessionId sessionId, TypedPayload typedPayload) {
				return processSyncMessage(sessionId, typedPayload);
			}
		});

		PayloadTypePredicate descriptorPredicate = new GenericPayloadTypePredicate(PayloadType.SHARED_SESSION_DESCRIPTOR, SERVICE_NAME, null, null);
		sharedObjectReadService.addUpdateProcessor(descriptorPredicate, new SharedObjectUpdateProcessor() {
			@Override
			public Result processUpdate(byte[] objectId, TypedPayload typedPayload) {
				return processDescriptorUpdate(objectId, typedPayload);
			}

			@Override
			public void processUnsubscribe(byte[] objectId) {
				onUnsubscribedMessageReceived(objectId);
			}
		});

		final List<byte[]> sessionIds = getSessionList();
		initSessions(sessionIds);

		Threads.submitBackgroundTaskPeriodically(this::garbageCollectMessages, 0, Config.LOCAL_CACHE_GC_PERIOD);
	}

	private List<byte[]> getSessionList() {
		return localSessions.list();
	}

	public static boolean isValidSessionId(byte[] sessionId) {
		return sessionId.length == SharedObjectCommon.OBJECT_ID_SIZE;
	}

	static byte[] generateKey() {
		return CryptoHelper.generateAEADKey();
	}

	static byte[] generateMessageId(byte[] sessionId, byte[] fromId, byte[] messageIdSeed) {
		return Arrays.copyOf(HashingUtils.SHA256(BinaryUtils.concat(sessionId, fromId, messageIdSeed)), MESSAGE_ID_SIZE);
	}

	private void initSessions(List<byte[]> sessionIds) {
		for (byte[] sessionId : sessionIds) {
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			if (sessionState == null) {
				log.trace(tracePrefix + " Failed to read session state, deleting");
				localSessions.delete(sessionId);
				continue;
			} else {
				SharedObjectReadService.ObjectMetadata objectMetadataRemote = sharedObjectReadService.getObjectMetadata(sessionId);
				TypedPayload objectOwn = sharedObjectPostService.getCurrent(sessionId);
				if (objectMetadataRemote == null && objectOwn == null) {
					log.trace(tracePrefix + " Failed to read session object " + Utils.getDebugStringFromId(sessionId) + ", skipping");
					// localSessions.delete(sessionId);
					continue;
				}
			}
			session2syncStats.put(ByteBuffer.wrap(sessionId), sessionState.getSyncStats());
			if (!sessionState.getMessageFetchQueue().isEmpty()) {
				workState.addReferenceReceivedSession(sessionId);
			}
		}
	}

	private void garbageCollectMessages() {
		long now = System.currentTimeMillis();
		List<byte[]> sessionIds = getSessionList();
		for (byte[] sessionId : sessionIds) {
			List<LocalSharedSessionMessage> messages = new ArrayList<>();
			List<byte[]> messageIds = localMessages.list(sessionId);
			for (byte[] messageId : messageIds) {
				LocalSharedSessionMessage message = localMessages.get(sessionId, messageId);
				messages.add(message);
				if (message.isSetConsumedAt() && now - message.getConsumedAt() > CONSUMED_MESSAGE_TTL) {
					localMessages.delete(message);
				}
				if (!message.isSetFetchedAt() && now - message.getFetchScheduledAt() > UNFETCHED_MESSAGE_WARNING_TTL) {
					log.debug(tracePrefix + " Unfetched message: " + toDebugString(message));
					if (now - message.getMessageReference().getCreatedAt() > UNFETCHED_MESSAGE_DELETION_TTL) {
						log.debug(tracePrefix + " Deleting old unfetched message: " + toDebugString(message));
						localMessages.delete(message);
					}
				}
				if (!message.isSetConsumedAt() && message.isSetFetchedAt() && now - message.getFetchedAt() > UNCONSUMED_MESSAGE_TTL) {
					log.debug(tracePrefix + " Unconsumed message: " + toDebugString(message));
					// localMessages.delete(message);
					// log.debug(tracePrefix + " Unconsumed message: " + (new Gson()).toJson(TypedPayloadDecoder.decodeLocalSharedSessionMessage(message)));
				}
			}
			/*if ("SESSION_ID".equals(Utils.getDebugStringFromId(sessionId))) {
				logMessagesDbg(messages);
			}*/
		}

		long elapsed = System.currentTimeMillis() - now;
		log.trace(tracePrefix + " SharedSessionService.garbageCollectMessages took " + elapsed + " ms");
	}

	private static String toDebugString(LocalSharedSessionMessage message) {
		return Utils.getDebugStringFromId(message.getMessageReference().getSessionId()) + ":" + Utils.getDebugStringFromId(message.getMessageReference().getMessageId()) + ":" + Utils.formatDateTime(message.getMessageReference().getCreatedAt());
	}

	private void logMessagesDbg(List<LocalSharedSessionMessage> messages) {
		Collections.sort(messages, ((o1, o2) -> Long.signum(o1.getFetchScheduledAt() - o2.getFetchScheduledAt())));
        Gson gson = new Gson();
		for (LocalSharedSessionMessage message : messages) {
			log.info(tracePrefix + " DUMP_MESSAGE_SHARED: " + gson.toJson(TypedPayloadDecoder.decodeLocalSharedSessionMessage(message)));
		}
	}

	private synchronized void rotateSessionKeysIfNeeded() {
		long now = System.currentTimeMillis();
		if (now - lastSessionKeyRotationCheckTime < SESSION_KEY_ROTATION_CHECK_INTERVAL) {
			return;
		}
		lastSessionKeyRotationCheckTime = now;

		final List<byte[]> sessionIds = getSessionList();
		for (byte[] sessionId : sessionIds) {
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			if (!sessionState.isSetDescriptor() || !cryptoHelper.isOwnId(sessionState.getDescriptor().getAdminIdentityRef().getId())) {
				continue;
			}
			if (now - sessionState.getLastDescriptorUpdatedAt() < SESSION_KEY_TTL) {
				continue;
			}

			SharedSessionDescriptor newDescriptor = new SharedSessionDescriptor(sessionState.getDescriptor());
			newDescriptor.setContentKey(generateKey());
			int newDescriptorVersion = newDescriptor.getVersion() + 1;
			newDescriptor.setVersion(newDescriptorVersion);
			newDescriptor.setParticipants(filterFormerParticipants(newDescriptor.getParticipants(), newDescriptorVersion));

			boolean success = publishDescriptor(newDescriptor);
			if (!success) {
				log.warn(tracePrefix + " Failed to update descriptor (rotate keys) " + Utils.getDebugStringFromId(sessionId));
				continue;
			}

			setNewDescriptor(sessionState, newDescriptor);
			localSessions.put(sessionState);
		}
	}

	public void addMessageProcessor(PayloadTypePredicate predicate, MessageProcessor processor) {
		messageProcessors.add(Pair.of(predicate, processor));
	}

	public void setCallback(SharedSessionCallbackInternal callback) {
		this.callback = callback;
	}

	boolean hasBufferedWork() {
		return !workState.isEmpty();
	}

	void processBufferedWork() {
		while (true) {
			boolean shouldRepeat = processReceivedReferences();
			if (!shouldRepeat) {
				break;
			}
		}
		processUnconsumed();
		processAckedSessions();
		processSentAndReceivedSessions();
		rotateSessionKeysIfNeeded();
	}

	private boolean processReceivedReferences() {
		List<ByteBuffer> referenceReceivedSessions = new ArrayList<>(workState.drainReferenceReceivedSessions());
		if (referenceReceivedSessions.isEmpty()) {
			return false;
		}
		long now = System.currentTimeMillis();
		ReferenceFetchState prevSessionFetchState = null;
		for(int i = 0; i < referenceReceivedSessions.size(); i++) {
			byte[] sessionId = Utils.toArray(referenceReceivedSessions.get(i));
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			if (sessionState == null) {
				log.trace(tracePrefix + " Session not found " + Utils.getDebugStringFromId(sessionId));
				continue;
			}

			List<SharedSessionMessageMetadata> messageFetchQueue = new ArrayList<>(sessionState.getMessageFetchQueue());

			if (messageFetchQueue.isEmpty()) {
				log.debug(tracePrefix + " Session has no work " + Utils.getDebugStringFromId(sessionId));
				continue;
			}

			boolean amIAdmin = cryptoHelper.isOwnId(sessionState.getDescriptor().getAdminIdentityRef().getId());
			ReferenceFetchState fetchState = null;
			for (SharedSessionMessageMetadata messageMetadata : messageFetchQueue) {
				if (!hasKeyForMessage(sessionState, messageMetadata)) {
					// TODO: abort if key is missing for too long
					if (amIAdmin) {
						log.warn(tracePrefix + " Received message in own session with unknown key " + Utils.getDebugStringFromId(sessionId) + ":" + Utils.getDebugStringFromId(messageMetadata.getFromId())  + " received on " + Utils.formatDateTime(messageMetadata.getCreatedAt()));
					} else {
						if (now - messageMetadata.getCreatedAt() > NO_KEY_WARNING_TIME) {
							log.warn(tracePrefix + " Still no key v" + messageMetadata.getDescriptorVersion() + " for message " + Utils.getDebugStringFromId(sessionId) + ":" + Utils.getDebugStringFromId(messageMetadata.getFromId()) + " received on " + Utils.formatDateTime(messageMetadata.getCreatedAt()));
						}
						sharedObjectReadService.requestObjectFetch(sessionId, null);
					}
					continue;
				}
				fetchState = fetchReference(messageMetadata);
				if (fetchState == ReferenceFetchState.ABORT_SESSION) {
					workState.addReferenceReceivedSession(sessionId);
					break;
				}
				if (fetchState == ReferenceFetchState.ABORT_ALL) {
					break;
				}
			}
			if (fetchState == ReferenceFetchState.ABORT_ALL || (fetchState == ReferenceFetchState.ABORT_SESSION && prevSessionFetchState == ReferenceFetchState.ABORT_SESSION)) {
				for (int j = i; j < referenceReceivedSessions.size(); j++) {
					workState.addReferenceReceivedSession(Utils.toArray(referenceReceivedSessions.get(i)));
				}
				return false;
			}
			prevSessionFetchState = fetchState;
		}

		return true;
	}

	private synchronized void processUnconsumed() {
		Map<ByteBuffer, Set<ByteBuffer>> session2UnconsumedMessages = workState.drainUnconsumedSessions();
		for (Map.Entry<ByteBuffer, Set<ByteBuffer>> unconsumedEntry : session2UnconsumedMessages.entrySet()) {
			ByteBuffer sessionIdWrapped = unconsumedEntry.getKey();
			byte[] sessionId = Utils.toArray(sessionIdWrapped);
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			ArrayList<ByteBuffer> newUnconsumedMessageIds = new ArrayList<>(sessionState.getUnconsumedMessageIds());
			newUnconsumedMessageIds.addAll(unconsumedEntry.getValue());
			sessionState.setUnconsumedMessageIds(newUnconsumedMessageIds);
			localSessions.put(sessionState);
		}
	}

	private void processAckedSessions() {
		Set<ByteBuffer> ackedSessions = workState.drainAckedSessions();
		for (ByteBuffer sessionWrapped : ackedSessions) {
			if (callback != null) {
				callback.onMessagesAcked(Utils.toArray(sessionWrapped));
			}
		}
	}

	private void processSentAndReceivedSessions() {
		Pair<Set<ByteBuffer>, Set<ByteBuffer>> receivedAndSent = workState.drainReceivedAndSentSessions();
		if (receivedAndSent == null) {
			return;
		}
		Set<ByteBuffer> receivedSessions = receivedAndSent.getLeft();
		Set<ByteBuffer> sentSessions = receivedAndSent.getRight();
		Set<ByteBuffer> sessionsToNotify = new LinkedHashSet<>(sentSessions);

		for (ByteBuffer sessionIdWrapped : receivedSessions) {
			byte[] sessionId = Utils.toArray(sessionIdWrapped);
			sessionsToNotify.add(sessionIdWrapped);
			if (callback != null) {
				callback.onMessagesReceived(sessionId);
			}
		}

		sendDeferredNotifications(sessionsToNotify);
	}

	private void addRecentPeer(LocalSharedSessionState sessionState, byte[] id, LocalSharedSessionRecentPeerType type) {
		if (!commSessionService.haveSessionWith(id)) {
			return;
		}
		long now = System.currentTimeMillis();
		LocalSharedSessionRecentPeer peer = new LocalSharedSessionRecentPeer(type, now, ByteBuffer.wrap(id));
		List<LocalSharedSessionRecentPeer> peers = sessionState.getRecentPeers();
		List<LocalSharedSessionRecentPeer> newPeers = new ArrayList<>(peers.size());
		newPeers.add(peer);
		for (int sourceIdx = 0, targetIdx = 1; sourceIdx < peers.size() && targetIdx < MAX_RECENT_PEERS; sourceIdx++) {
			LocalSharedSessionRecentPeer otherPeer = peers.get(sourceIdx);
			if (cryptoHelper.isOwnId(otherPeer.getId()) || Arrays.equals(otherPeer.getId(), id)) {
				continue;
			}
			newPeers.add(otherPeer);
			targetIdx++;
		}
		sessionState.setRecentPeers(newPeers);
	}

	synchronized List<byte[]> getRecentPeers(byte[] sessionId) {
		List<byte[]> result = new ArrayList<>();
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null) {
			return result;
		}
		for (LocalSharedSessionRecentPeer peer : sessionState.getRecentPeers()) {
			result.add(peer.getId());
		}
		return result;
	}

	boolean isActiveSession(byte[] sessionId) {
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		return sessionState != null && !sessionState.isSetUnsubscribedDescriptorVersion();
	}

	private ReferenceFetchState fetchReference(SharedSessionMessageMetadata messageMetadata) {
		FetchResult result = fetchMessage(messageMetadata);

		if (result == FetchResult.SUCCESS || result == FetchResult.DECODING_ERROR || result == FetchResult.INTERNAL_STATE_ERROR) {
			return ReferenceFetchState.PROCEED;
		} else if (result == FetchResult.NETWORK_ERROR || result == FetchResult.NETWORK_ERROR_PERSISTENT || result == FetchResult.NOT_FOUND) {
			return ReferenceFetchState.ABORT_SESSION;
		} else {
			throw new RuntimeException("Invalid result: " + result);
		}
	}

	private void sendDeferredNotifications(Set<ByteBuffer> sessions) {
		for (ByteBuffer sessionIdWrapped : sessions) {
			byte[] sessionId = Utils.toArray(sessionIdWrapped);
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			if (sessionState == null || !sessionState.isSetDescriptor()) {
				continue;
			}
			if (sessionState.getPreviousMessageTails().isEmpty()) {
				continue;
			}
			sendSyncMessage(sessionState.getDescriptor(), sessionState.getPreviousMessageTails());
		}
	}

	public List<byte[]> getActiveSessionIds() {
		List<byte[]> activeSessions = new ArrayList<>();
		for (byte[] sessionId : getSessionList()) {
			LocalSharedSessionState sessionState = localSessions.get(sessionId);
			if (sessionState == null || sessionState.isSetUnsubscribedDescriptorVersion()) {
				continue;
			}
			activeSessions.add(sessionId);
		}
		return activeSessions;
	}

	public SessionMetadata getSessionMetadata(byte[] sessionId) {
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null || !sessionState.isSetDescriptor()) {
			return null;
		}
		SharedSessionDescriptor descriptor = sessionState.getDescriptor();
		byte[] adminId = Utils.copyOf(descriptor.getAdminIdentityRef().getId());
		boolean isUnsubscribed = sessionState.isSetUnsubscribedDescriptorVersion();
		ArrayList<byte[]> participantIds = new ArrayList<>();
		if (!isUnsubscribed) {
			for (SharedSessionParticipantInfo participant : getActiveParticipants(descriptor)) {
				byte[] participantId = participant.getIdentityRef().getId();
				participantIds.add(Arrays.copyOf(participantId, participantId.length));
			}
		} else {
			participantIds.add(adminId);
		}

		long lastReceivedTime = 0;
		for (LocalSharedSessionRecentPeer recentPeer : sessionState.getRecentPeers()) {
			lastReceivedTime = Math.max(lastReceivedTime, recentPeer.getTime());
		}

		return new SessionMetadata(Utils.copyOf(sessionId), isUnsubscribed, adminId, sessionId, descriptor.getProperties(), participantIds, lastReceivedTime);
	}

	public synchronized List<ReceivedMessage> fetchAckedMessages(byte[] sessionId, boolean consume) {
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null) {
			return null;
		}
		if (sessionState.getUnconsumedAckedMessageIds().isEmpty()) {
			return Collections.emptyList();
		}

		List<ReceivedMessage> ackedMessages = new ArrayList<>(sessionState.getUnconsumedAckedMessageIds().size());
		for (ByteBuffer messageIdWrapped : sessionState.getUnconsumedAckedMessageIds()) {
			byte[] messageId = Utils.toArray(messageIdWrapped);
			LocalSharedSessionMessage localMessage = localMessages.get(sessionId, messageId);
			if (localMessage == null) {
				continue;
			}
			if (!localMessage.isSetAckedAt()) {
				log.debug(tracePrefix + " Message not acked: " + Utils.getDebugStringFromId(sessionId) + ":" + Utils.getDebugStringFromId(messageId));
				continue;
			}
			SharedSessionMessage message = localMessage.getMessage();
			ackedMessages.add(new ReceivedMessage(message.getMetadata().getFromId(), message.getMetadata().getCreatedAt(), localMessage.getFetchedAt(), message.getPayload()));
		}

		if (consume) {
			sessionState.setUnconsumedAckedMessageIds(new ArrayList<>());
			localSessions.put(sessionState);
		}
		return ackedMessages;
	}

	public synchronized List<ReceivedMessage> fetchNewMessages(byte[] sessionId, boolean consume) {
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null) {
			return null;
		}
		if (sessionState.getUnconsumedMessageIds().isEmpty()) {
			return Collections.emptyList();
		}

		long now = System.currentTimeMillis();

		List<SharedSessionMessage> receivedMessages = new ArrayList<>(sessionState.getUnconsumedMessageIds().size());
		Map<ByteBuffer, LocalSharedSessionMessage> id2localMessage = new LinkedHashMap<>();
		for (ByteBuffer messageIdWrapped : sessionState.getUnconsumedMessageIds()) {
			byte[] messageId = Utils.toArray(messageIdWrapped);
			LocalSharedSessionMessage localMessage = localMessages.get(sessionId, messageId);
			if (localMessage == null) {
				continue;
			}
			id2localMessage.put(messageIdWrapped, localMessage);
			if (localMessage.isSetConsumedAt()) {
				log.debug(tracePrefix + " Message already consumed " + Utils.getDebugStringFromId(sessionId) + ":" + Utils.getDebugStringFromId(messageId));
				continue;
			}
			receivedMessages.add(localMessage.getMessage());
			if (consume) {
				localMessage.setConsumedAt(now);
				localMessages.put(localMessage);
			}
		}
		List<SharedSessionMessage> orderedMessages = SharedSessionMessageSorter.orderMessages(receivedMessages);
		List<ReceivedMessage> receivedMessagesWrapped = new ArrayList<>();
		for (SharedSessionMessage message : orderedMessages) {
			LocalSharedSessionMessage localMessage = id2localMessage.get(ByteBuffer.wrap(message.getMetadata().getMessageId()));
			receivedMessagesWrapped.add(new ReceivedMessage(message.getMetadata().getFromId(), message.getMetadata().getCreatedAt(), localMessage.getFetchedAt(), message.getPayload()));
		}

		if (consume) {
			sessionState.setUnconsumedMessageIds(new ArrayList<>());
			localSessions.put(sessionState);
		}
		return receivedMessagesWrapped;
	}

	private SharedObjectUpdateProcessor.Result processDescriptorUpdate(byte[] sessionId, TypedPayload typedPayload) {
		Pair<SharedSessionDescriptor, String> descriptorUnwrapped = TypedPayloadUtils.unwrap(typedPayload, SharedSessionDescriptor.class, SERVICE_NAME);
		if (descriptorUnwrapped.getRight() != null) {
			log.info("Failed to extract descriptor: " + descriptorUnwrapped.getRight());
			return SharedObjectUpdateProcessor.Result.NOT_MATCHING;
		}
		SharedSessionDescriptor descriptor = descriptorUnwrapped.getLeft();
		if (descriptor == null) {
			return SharedObjectUpdateProcessor.Result.PROCESSED;
		}
		if (!Arrays.equals(descriptor.getSessionId(), sessionId)) {
			log.info("Mismatching sessionId on " + Utils.getDebugStringFromId(sessionId));
			return SharedObjectUpdateProcessor.Result.PROCESSED;
		}
		if (findActiveParticipant(descriptor.getParticipants(), cryptoHelper.getOwnId()) == null) {
			log.info("Not among participants");
			return SharedObjectUpdateProcessor.Result.PROCESSED;
		}
		SharedObjectReadService.ObjectMetadata descriptorObjectMetadata = sharedObjectReadService.getObjectMetadata(sessionId);
		if (!Arrays.equals(descriptor.getAdminIdentityRef().getId(), descriptorObjectMetadata.fromId)) {
			log.info("Mismatching adminId on " + Utils.getDebugStringFromId(sessionId));
			return SharedObjectUpdateProcessor.Result.PROCESSED;
		}

		for (SharedSessionParticipantInfo participant : getActiveParticipants(descriptor)) {
			byte[] id = participant.getIdentityRef().getId();
			if (cryptoHelper.isOwnId(id)) {
				continue;
			}
			remoteIdentityService.setRemoteStorageProfile(participant.getIdentityRef());
		}

		recordDescriptorFetch(descriptor);

		return SharedObjectUpdateProcessor.Result.PROCESSED;
	}

	private MessageProcessor.Result processSyncMessage(SessionId sessionId, TypedPayload typedPayload) {
		if (sessionId.getType() != SessionType.COMM_SESSION) {
			log.trace(tracePrefix + " Invalid session type " + sessionId.getType());
			return MessageProcessor.Result.INVALID;
		}
		byte[] fromId = sessionId.getId();
		Pair<SharedSessionSyncMessage, String> syncMessageUnwrapped = TypedPayloadUtils.unwrap(typedPayload, SharedSessionSyncMessage.class, SERVICE_NAME);
		if (syncMessageUnwrapped.getRight() != null) {
			log.trace(tracePrefix + " Failed to extract sync message: " + syncMessageUnwrapped.getRight());
			return MessageProcessor.Result.INVALID;
		}
		SharedSessionSyncMessage syncMessage = syncMessageUnwrapped.getLeft();

		boolean addedNew = false;
		SharedSessionSyncStats syncStats = null;
		if (syncMessage.isSetMessageReferences()) {
			for (SharedSessionMessageMetadata messageReference : syncMessage.getMessageReferences()) {
				log.trace(tracePrefix + " Received sync message from " + Utils.getDebugStringFromId(fromId) + " message " + Utils.getDebugStringFromBytes(messageReference.getSessionId()) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()));
				syncStats = getSyncStats(messageReference.getSessionId());
				boolean addedNewTmp = enqueueMessageFetch(messageReference, fromId, null, syncMessage.getAdminIdentityRef());
				addedNew |= addedNewTmp;
			}
		}

		if (syncStats != null) {
			syncStats.setSyncMessagesReceived(syncStats.getSyncMessagesReceived() + 1);
		}

		return addedNew ? MessageProcessor.Result.SUCCESS : MessageProcessor.Result.SUCCESS_REDUNDANT;
	}

	private void onUnsubscribedMessageReceived(byte[] sessionId) {
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null || !sessionState.isSetDescriptor()) {
			log.trace(tracePrefix + " Unsubscribed from unknown session " + Utils.getDebugStringFromBytes(sessionId));
			return;
		}

		SharedSessionDescriptor descriptor = sessionState.getDescriptor();

		sessionState.setUnsubscribedDescriptorVersion(descriptor.getVersion() + 1);
		localSessions.put(sessionState);

		if (callback != null) {
			Threads.submitBackgroundTask(() -> callback.onDescriptorUpdated(sessionId));
		}
	}

	private static LocalSharedSessionPreviousData findVersion(List<LocalSharedSessionPreviousData> previousData, int descriptorVersion) {
		for (LocalSharedSessionPreviousData prevData : previousData) {
			if (prevData.getDescriptorVersion() == descriptorVersion) {
				return prevData;
			}
		}
		return null;
	}

	private synchronized void recordDescriptorFetch(SharedSessionDescriptor descriptor) {
		byte[] sessionId = descriptor.getSessionId();
		log.trace(tracePrefix + " Received descriptor v" + descriptor.getVersion() + " from session " + Utils.getDebugStringFromBytes(sessionId));

		LocalSharedSessionState sessionState = localSessions.get(sessionId);

		if (sessionState == null) {
			sessionState = createNewLocalSession(sessionId);
		}

		if (sessionState.isSetUnsubscribedDescriptorVersion()) {
			if (sessionState.getUnsubscribedDescriptorVersion() <= descriptor.getVersion()) {
				sessionState.unsetUnsubscribedDescriptorVersion();
			}
		}

		if (!sessionState.isSetDescriptor() || descriptor.getVersion() > sessionState.getDescriptor().getVersion()) {
			setNewDescriptor(sessionState, descriptor);
		} else if (findVersion(sessionState.getPreviousData(), descriptor.getVersion()) == null) {
			addPreviousDescriptor(sessionState, descriptor);
		}

		long now = System.currentTimeMillis();
		SharedSessionSyncStats syncStats = getSyncStats(sessionId);
		syncStats.setDescriptorsReceived(syncStats.getDescriptorsReceived() + 1);
		syncStats.setLastDescriptorFetchTime(now);
		sessionState.setSyncStats(syncStats);

		if (!sessionState.getMessageFetchQueue().isEmpty()) {
			workState.addReferenceReceivedSession(sessionId);
		}

		localSessions.put(sessionState);
	}

	private void setNewDescriptor(LocalSharedSessionState sessionState, SharedSessionDescriptor descriptor) {
		long now = System.currentTimeMillis();
		if (sessionState.isSetDescriptor()) {
			addPreviousDescriptor(sessionState, sessionState.getDescriptor());
		}
		sessionState.setDescriptor(descriptor);
		sessionState.setLastDescriptorUpdatedAt(now);
		log.trace(tracePrefix + " Set new descriptor v" + descriptor.getVersion() + " for session " + Utils.getDebugStringFromBytes(sessionState.getSessionId()));

		if (callback != null) {
			Threads.submitBackgroundTask(() -> callback.onDescriptorUpdated(descriptor.getSessionId()));
		}
	}

	private LocalSharedSessionState createNewLocalSession(byte[] sessionId) {
		LocalSharedSessionState sessionState = new LocalSharedSessionState(ByteBuffer.wrap(sessionId), new SharedSessionSyncStats(), null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
		log.trace(tracePrefix + " Created new local session " + Utils.getDebugStringFromBytes(sessionId));
		return sessionState;
	}

	private void addPreviousDescriptor(LocalSharedSessionState sessionState, SharedSessionDescriptor descriptor) {
		LocalSharedSessionPreviousData prevData = new LocalSharedSessionPreviousData(descriptor.getVersion(), ByteBuffer.wrap(descriptor.getContentKey()));
		ArrayList<LocalSharedSessionPreviousData> newPreviousData = new ArrayList<>(sessionState.getPreviousData());
		newPreviousData.add(0, prevData);
		log.trace(tracePrefix + " Added old descriptor v" + descriptor.getVersion() + " for session " + Utils.getDebugStringFromBytes(sessionState.getSessionId()));
		while (newPreviousData.size() > MAX_PREV_DATA) {
			newPreviousData.remove(newPreviousData.size() - 1);
		}
		sessionState.setPreviousData(newPreviousData);
	}

	SharedSessionSyncStats getSyncStats(byte[] sessionId) {
		ByteBuffer sessionIdWrapped = ByteBuffer.wrap(sessionId);
		SharedSessionSyncStats syncStats = session2syncStats.computeIfAbsent(sessionIdWrapped, (id) -> new SharedSessionSyncStats());
		return syncStats;
	}

	private synchronized boolean enqueueMessageFetch(SharedSessionMessageMetadata messageReference, byte[] sourceSyncFromId, byte[] sourcePrevInMessageId, IdentityReference adminIdentityRef) {
		byte[] sessionId = messageReference.getSessionId();
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null) {
			sessionState = createNewLocalSession(sessionId);
			if (adminIdentityRef != null) {
				remoteIdentityService.setRemoteStorageProfile(adminIdentityRef);
				sharedObjectReadService.requestObjectFetch(sessionId, adminIdentityRef.getId());
			}
			log.trace(tracePrefix + " Message on unknown session, enqueueing " + Utils.getDebugStringFromBytes(sessionId));
		}

		LocalSharedSessionMessage messageExisting = localMessages.get(sessionId, messageReference.getMessageId());
		if (messageExisting != null) {
			if (messageExisting.isSetMessage()) {
				SharedSessionMessageMetadata metadata = messageExisting.getMessage().getMetadata();
				if (!Arrays.equals(metadata.getSessionId(), sessionId) || !Arrays.equals(metadata.getFromId(), messageReference.getFromId())) {
					log.warn(tracePrefix + " Conflicting admin or fromId for message " + Utils.getDebugStringFromBytes(sessionId) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()));
				}

				boolean updatedState = updateAcks(sessionState, messageReference);
				if (updatedState) {
					localSessions.put(sessionState);
				}
			} else {
				boolean messageExistingUpdated = false;
				if (sourceSyncFromId != null && !messageExisting.isSetSourceSyncFromId()) {
					messageExisting.setSourceSyncFromId(sourceSyncFromId);
					messageExistingUpdated = true;
				}
				if (sourcePrevInMessageId != null && !messageExisting.isSetSourcePrevInMessageId()) {
					messageExisting.setSourcePrevInMessageId(sourcePrevInMessageId);
					messageExistingUpdated = true;
				}
				if (messageExistingUpdated) {
					localMessages.put(messageExisting);
				}
			}
			return false;
		} else {
			if (cryptoHelper.isOwnId(messageReference.getFromId())) {
				log.warn(tracePrefix + " Received own message " + Utils.getDebugStringFromBytes(sessionId) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()));
				return false;
			}
		}
		ArrayList<SharedSessionMessageMetadata> newMessageFetchQueue = new ArrayList<>(sessionState.getMessageFetchQueue());
		if (newMessageFetchQueue.contains(messageReference)) {
			return false;
		}
		newMessageFetchQueue.add(messageReference);
		sessionState.setMessageFetchQueue(newMessageFetchQueue);
		if (sourceSyncFromId != null) {
			addRecentPeer(sessionState, sourceSyncFromId, LocalSharedSessionRecentPeerType.SYNC);
		}
		localSessions.put(sessionState);

		long now = System.currentTimeMillis();
		LocalSharedSessionMessage localMessage = new LocalSharedSessionMessage(messageReference, now);
		localMessage.setSourceSyncFromId(sourceSyncFromId);
		localMessage.setSourcePrevInMessageId(sourcePrevInMessageId);
		localMessages.put(localMessage);

		if (sessionState.isSetDescriptor()) {
			workState.addReferenceReceivedSession(sessionId);
		}

		return true;
	}

	private boolean updateAcks(LocalSharedSessionState sessionState, SharedSessionMessageMetadata messageReference) {
		if (!cryptoHelper.isOwnId(messageReference.getFromId())) {
			return false;
		}
		byte[] sessionId = sessionState.getSessionId();
		LocalSharedSessionMessage localMessage = localMessages.get(sessionId, messageReference.getMessageId());
		if (localMessage == null || !localMessage.isSetMessage() || localMessage.isSetAckedAt()) {
			return false;
		}

		long now = System.currentTimeMillis();
		localMessage.setAckedAt(now);
		localMessages.put(localMessage);

		ByteBuffer messageIdWrapped = ByteBuffer.wrap(messageReference.getMessageId());
		List<ByteBuffer> unconsumedAckedMessageIds = sessionState.getUnconsumedAckedMessageIds();
		if (unconsumedAckedMessageIds.contains(messageIdWrapped)) {
			return false;
		}
		List<ByteBuffer> newUnconsumedAckedMessageIds = new ArrayList<>(unconsumedAckedMessageIds);
		newUnconsumedAckedMessageIds.add(messageIdWrapped);
		if (newUnconsumedAckedMessageIds.size() > MAX_UNCONSUMED_ACKED_SERIALS) {
			newUnconsumedAckedMessageIds = newUnconsumedAckedMessageIds.subList(newUnconsumedAckedMessageIds.size() - MAX_UNCONSUMED_ACKED_SERIALS, newUnconsumedAckedMessageIds.size());
		}
		sessionState.setUnconsumedAckedMessageIds(newUnconsumedAckedMessageIds);

		workState.addMessageAckedSession(sessionId);
		for (SharedSessionMessageMetadata previousMessageMd : localMessage.getMessage().getPreviousMessageMetadata()) {
			updateAcks(sessionState, previousMessageMd);
		}
		return true;
	}

	private Pair<FetchResult, SharedSessionMessage> fetchAndDecodeMessage(SharedSessionMessageMetadata messageReference) {
		byte[] sessionId = messageReference.getSessionId();
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		if (sessionState == null) {
			log.warn(tracePrefix + " Session not found " + Utils.getDebugStringFromBytes(sessionId));
			return Pair.of(FetchResult.INTERNAL_STATE_ERROR, null);
		}

		if (!sessionState.isSetDescriptor()) {
			log.warn(tracePrefix + " Descriptor missing for session " + Utils.getDebugStringFromBytes(sessionId));
			return Pair.of(FetchResult.NOT_FOUND, null);
		}

		if (findFormerParticipant(sessionState.getDescriptor().getParticipants(), messageReference.getFromId()) != null) {
			return Pair.of(FetchResult.SUCCESS, null);
		}

		if (findActiveParticipant(sessionState.getDescriptor().getParticipants(), messageReference.getFromId()) == null && findFormerParticipant(sessionState.getDescriptor().getParticipants(), messageReference.getFromId()) == null) {
			log.warn(tracePrefix + " Invalid participant on " + Utils.getDebugStringFromBytes(messageReference.getSessionId()) + ": " + Utils.getDebugStringFromId(messageReference.getFromId()));
			return Pair.of(FetchResult.INTERNAL_STATE_ERROR, null);
		}

		byte[] contentKey = getKeyForMessage(sessionState, messageReference);
		if (contentKey == null) {
			log.debug(tracePrefix + " No key v" + messageReference.getDescriptorVersion() + " for message " + Utils.getDebugStringFromBytes(sessionId) + ":" + Utils.getDebugStringFromBytes(messageReference.getFromId()) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()));
			return Pair.of(FetchResult.INTERNAL_STATE_ERROR, null);
		}

		RemoteFetchResultRaw messageFetchResult = remoteMessageHandler.read(messageReference.getFromId(), messageReference.getMessageId());
		if (messageFetchResult.getError() != null) {
			return Pair.of(messageFetchResult.getNumConsecutiveErrors() <= MAX_CONSECUTIVE_FETCH_ERRORS ? FetchResult.NETWORK_ERROR : FetchResult.NETWORK_ERROR_PERSISTENT, null);
		}
		if (messageFetchResult.getData() == null) {
			return Pair.of(FetchResult.NOT_FOUND, null);
		}

		Pair<SharedSessionMessage, String> messageExtracted = extractMessage(messageReference, messageFetchResult, contentKey);
		SharedSessionMessage message = messageExtracted.getLeft();
		if (message == null || messageExtracted.getRight() != null) {
			log.info(tracePrefix + " Failed to decode message " + Utils.getDebugStringFromBytes(sessionId) + ":" + Utils.getDebugStringFromBytes(messageReference.getFromId()) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()) + ": " + messageExtracted.getRight());
			return Pair.of(FetchResult.DECODING_ERROR, null);
		}

		SharedSessionMessageMetadata metadata = message.getMetadata();
		if (metadata == null || !metadata.equals(messageReference)) {
			log.info(tracePrefix + " Mismatching metadata for message " + Utils.getDebugStringFromBytes(sessionId) + ":" + Utils.getDebugStringFromBytes(messageReference.getFromId()) + ":" + Utils.getDebugStringFromBytes(messageReference.getMessageId()));
			return Pair.of(FetchResult.DECODING_ERROR, null);
		}

		// TODO: change to FetchResult.SUCCESS
		return Pair.of(null, message);
	}

	private FetchResult fetchMessage(SharedSessionMessageMetadata messageReference) {
		byte[] sessionId = messageReference.getSessionId();
		Pair<FetchResult, SharedSessionMessage> messageFetchResult = fetchAndDecodeMessage(messageReference);
		LocalSharedSessionMessage localMessage = localMessages.get(sessionId, messageReference.getMessageId());

		boolean doNotAddToPrevMessageTails = localMessage != null && localMessage.getSourcePrevInMessageId() != null;
		recordMessageFetch(messageReference, messageFetchResult, doNotAddToPrevMessageTails);

		FetchResult error = messageFetchResult.getLeft();
		if (error != null) {
			return error;
		}

		long now = System.currentTimeMillis();
		SharedSessionMessage message = messageFetchResult.getRight();

		if (localMessage == null) {
			log.info(tracePrefix + " Null local message " + Utils.getDebugStringFromBytes(message.getMetadata().getMessageId()) + " from session " + Utils.getDebugStringFromBytes(sessionId));
			localMessage = new LocalSharedSessionMessage(messageReference, now);
		}
		localMessage.setFetchedAt(now);
		localMessage.setMessage(message);
		boolean consumed = runThroughMessageProcessors(message);
		if (consumed) {
			localMessage.setConsumedAt(now);
		}
		localMessages.put(localMessage);

		fetchPreviousMessages(message);

		workState.addMessageReceived(sessionId, message.getMetadata().getMessageId(), consumed);

		return FetchResult.SUCCESS;
	}

	private boolean runThroughMessageProcessors(SharedSessionMessage message) {
		byte[] sessionId = message.getMetadata().getSessionId();
		boolean consumed = false;
		TypedPayload payload = message.getPayload();
		for (Pair<PayloadTypePredicate, MessageProcessor> pair : messageProcessors) {
			if (pair.getLeft().match(payload.getMetadata())) {
				SessionId session = new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId));
				MessageProcessor.Result result = pair.getRight().process(session, payload);
				if (result == MessageProcessor.Result.SUCCESS || result == MessageProcessor.Result.SUCCESS_REDUNDANT) {
					consumed = true;
					break;
				} else {
					log.debug(tracePrefix + " Invalid message passed to " + pair.getRight().getClass().getName() + ": " + payload.getMetadata().toString());
				}
			}
		}
		return consumed;
	}

	private void fetchPreviousMessages(SharedSessionMessage message) {
		byte[] sessionId = message.getMetadata().getSessionId();
		LocalSharedSessionState sessionState = localSessions.get(sessionId);
		SharedSessionSyncStats syncStats = getSyncStats(sessionId);
		for (SharedSessionMessageMetadata prevMetadata : message.getPreviousMessageMetadata()) {
			if (!Arrays.equals(prevMetadata.getSessionId(), sessionId)) {
				log.warn(tracePrefix + " Invalid session in message "  + Utils.getDebugStringFromBytes(prevMetadata.getSessionId()) + ": " + Utils.getDebugStringFromBytes(sessionId) + " -> " + Utils.getDebugStringFromBytes(prevMetadata.getSessionId()));
				continue;
			}
			SharedSessionParticipantInfo selfParticipant = findActiveParticipant(sessionState.getDescriptor().getParticipants(), cryptoHelper.getOwnId());
			if (prevMetadata.getDescriptorVersion() < selfParticipant.getJoinedVersion()) {
				continue;
			}
			boolean enqueued = enqueueMessageFetch(prevMetadata, null, message.getMetadata().getMessageId(), null);
			if (enqueued) {
				syncStats.setPreviousMessagesFetched(syncStats.getPreviousMessagesFetched() + 1);
			}
		}
	}

	private void dequeueMessage(SharedSessionMessageMetadata messageReference, LocalSharedSessionState sessionState) {
		List<SharedSessionMessageMetadata> newMessageFetchQueue = sessionState.getMessageFetchQueue();
		boolean removedFromFetchQueue = newMessageFetchQueue.remove(messageReference);
		if (!removedFromFetchQueue) {
			log.trace(tracePrefix + " Message not in persisted queue");
		}
		sessionState.setMessageFetchQueue(newMessageFetchQueue);
	}

	private synchronized void recordMessageFetch(SharedSessionMessageMetadata messageReference, Pair<FetchResult, SharedSessionMessage> messageFetchResult, boolean doNotAddToPrevMessageTails) {
		byte[] sessionId = messageReference.getSessionId();
		FetchResult error = messageFetchResult.getLeft();
		log.trace(tracePrefix + " Fetched message " + Utils.getDebugStringFromBytes(messageReference.getMessageId()) + " from session " + Utils.getDebugStringFromBytes(sessionId) + ": " + (error == null ? "success" : error));

		LocalSharedSessionState sessionState = localSessions.get(sessionId);

		if (error == null || error != FetchResult.NETWORK_ERROR) {
			dequeueMessage(messageReference, sessionState);
		}

		if (error != null) {
			localSessions.put(sessionState);
			return;
		}

		SharedSessionMessage message = messageFetchResult.getRight();
		SharedSessionMessageMetadata metadata = message.getMetadata();

		ArrayList<SharedSessionMessageMetadata> newPreviousMessageTails = new ArrayList<>(sessionState.getPreviousMessageTails());
		int sizeBefore = newPreviousMessageTails.size();
		newPreviousMessageTails.removeAll(message.getPreviousMessageMetadata());
		int sizeAfterRemoval = newPreviousMessageTails.size();
		if (!doNotAddToPrevMessageTails) {
			newPreviousMessageTails.add(metadata);
		}
		int sizeAfterAdding = newPreviousMessageTails.size();
		int prevTailsAdded = sizeAfterAdding - sizeAfterRemoval;
		int prevTailsRemoved = sizeBefore - sizeAfterRemoval;
		log.trace(tracePrefix + " Updated prevTails for " + Utils.getDebugStringFromBytes(sessionId) + ": removed " + prevTailsRemoved + ", added " + prevTailsAdded + " for a total " + sizeAfterAdding);
		sessionState.setPreviousMessageTails(newPreviousMessageTails);

		long now = System.currentTimeMillis();
		SharedSessionSyncStats syncStats = getSyncStats(sessionId);
		syncStats.setMessagesFetched(syncStats.getMessagesFetched() + 1);
		syncStats.setLastMessageFetchTime(now);
		syncStats.setPrevTailsAdded(syncStats.getPrevTailsAdded() + prevTailsAdded);
		syncStats.setPrevTailsRemoved(syncStats.getPrevTailsRemoved() + prevTailsRemoved);
		sessionState.setSyncStats(syncStats);
		log.trace(tracePrefix + " Session sync stats for " + Utils.getDebugStringFromBytes(sessionId) + ": " + syncStats.toString());

		localSessions.put(sessionState);
	}

	private static boolean hasKeyForMessage(LocalSharedSessionState sessionState, SharedSessionMessageMetadata messageMetadata) {
		return getKeyForMessage(sessionState, messageMetadata) != null;
	}

	private static byte[] getKeyForMessage(LocalSharedSessionState sessionState, SharedSessionMessageMetadata messageMetadata) {
		if (!sessionState.isSetDescriptor()) {
			return null;
		}
		byte[] contentKey = null;
		if (messageMetadata.getDescriptorVersion() == sessionState.getDescriptor().getVersion()) {
			contentKey = sessionState.getDescriptor().getContentKey();
		} else {
			LocalSharedSessionPreviousData prevData = findVersion(sessionState.getPreviousData(), messageMetadata.getDescriptorVersion());
			if (prevData != null) {
				contentKey = prevData.getContentKey();
			}
		}
		return contentKey;
	}

	private Pair<SharedSessionMessage, String> extractMessage(SharedSessionMessageMetadata messageReference, RemoteFetchResultRaw messageFetchResult, byte[] contentKey) {
		Decoded<DecodedTypedPayload> decodedDescriptorWrapper = CryptoHelper.decodeCryptoEnvelope(messageFetchResult.getData(), CryptoEnvelopeDecodingParams.create().setSymmetricDecryptionKey(contentKey).setSymmetricAssociatedData(ASSOCIATED_DATA).setFetchSignerIfNeeded(true), CryptoContext.create().setCryptoHelper(cryptoHelper).setRemoteIdentityFetcher(remoteIdentityService.getFetcher()));
		if (decodedDescriptorWrapper.getError() != null || decodedDescriptorWrapper.getData() == null) {
			return Pair.of(null, "Error decoding envelope: " + decodedDescriptorWrapper.getError());
		}
		DecodedTypedPayload decodedTypedPayload = decodedDescriptorWrapper.getData();
		if (decodedTypedPayload.getSignatureMetadata() == null || !Arrays.equals(decodedTypedPayload.getSignatureMetadata().getSignerId(), messageReference.getFromId())) {
			return Pair.of(null, "Missing or mismatching signature");
		}
		TypedPayload typedPayload = decodedTypedPayload.getPayload();
		Pair<SharedSessionMessage, String> messageUnwrapped = TypedPayloadUtils.unwrap(typedPayload, SharedSessionMessage.class, SERVICE_NAME);
		if (messageUnwrapped.getRight() != null) {
			return Pair.of(null, "Failed to extract message: " + messageUnwrapped.getRight());
		}
		SharedSessionMessage message = messageUnwrapped.getLeft();
		if (message == null) {
			return Pair.of(null, "Failed to deserialize SharedSessionMessage");
		}
		SharedSessionMessageMetadata metadata = message.getMetadata();
		if (!Arrays.equals(metadata.getFromId(), messageReference.getFromId())) {
			return Pair.of(null, "Mismatching fromId");
		}
		if (!Arrays.equals(metadata.getSessionId(), messageReference.getSessionId())) {
			return Pair.of(null, "Mismatching sessionId");
		}
		if (metadata.getDescriptorVersion() != messageReference.getDescriptorVersion()) {
			return Pair.of(null, "Mismatching descriptor version");
		}
		if (!Arrays.equals(metadata.getMessageId(), generateMessageId(metadata.getSessionId(), metadata.getFromId(), metadata.getMessageIdSeed()))) {
			return Pair.of(null, "Invalid messageId");
		}
		return Pair.of(message, null);
	}

	private boolean publishDescriptor(SharedSessionDescriptor descriptor) {
		List<byte[]> subscribers = getIds(getActiveParticipants(descriptor));
		TypedPayload descriptorPayload = TypedPayloadUtils.wrap(descriptor, SERVICE_NAME, null, null);
		boolean result = sharedObjectPostService.updateSubscribers(descriptor.getSessionId(), SharedObjectPostService.SubscriberUpdateType.SET, subscribers, descriptorPayload);
		log.trace(tracePrefix + " Published new session descriptor v" + descriptor.getVersion() + " for " + Utils.getDebugStringFromBytes(descriptor.getSessionId()) + " success=" + result);
		return result;
	}

	public byte[] createNewSession(List<Property> properties) {
		TypedPayload dummyPayload = TypedPayloadUtils.wrapCustom(new byte[0], SERVICE_NAME, "dummy", null);
		byte[] sessionId = sharedObjectPostService.create(SharedObjectDeliveryType.DELIVER_ALL, DESCRIPTOR_POLL_INTERVAL, DESCRIPTOR_OBJECT_TTL, DESCRIPTOR_KEY_TTL, dummyPayload);

		long now = System.currentTimeMillis();
		IdentityReference identityReference = new IdentityReference(ByteBuffer.wrap(cryptoHelper.getOwnId()), ownStorageProfile);
		SharedSessionParticipantInfo selfParticipant = new SharedSessionParticipantInfo(identityReference, 0);
		SharedSessionDescriptor descriptor = new SharedSessionDescriptor(ByteBuffer.wrap(sessionId), identityReference, 0, ByteBuffer.wrap(generateKey()), now, properties, Collections.singletonList(selfParticipant));

		boolean success = publishDescriptor(descriptor);
		if (!success) {
			log.warn(tracePrefix + " Failed to update descriptor (rotate keys) " + Utils.getDebugStringFromId(sessionId));
			sharedObjectPostService.deleteObject(sessionId);
			return null;
		}

		LocalSharedSessionState localState = new LocalSharedSessionState(ByteBuffer.wrap(sessionId), new SharedSessionSyncStats(), descriptor, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
		localState.setLastDescriptorUpdatedAt(now);
		localSessions.put(localState);

		return sessionId;
	}

	private static List<byte[]> getIds(List<SharedSessionParticipantInfo> participants) {
		List<byte[]> result = new ArrayList<>(participants.size());
		for (SharedSessionParticipantInfo participant : participants) {
			result.add(participant.getIdentityRef().getId());
		}
		return result;
	}

	private List<SharedSessionParticipantInfo> getActiveParticipants(SharedSessionDescriptor descriptor) {
		List<SharedSessionParticipantInfo> result = new ArrayList<>(descriptor.getParticipants().size());
		for (SharedSessionParticipantInfo participant : descriptor.getParticipants()) {
			if (!participant.isSetLeftVersion()) {
				result.add(participant);
			}
		}
		return result;
	}

	private List<SharedSessionParticipantInfo> filterFormerParticipants(List<SharedSessionParticipantInfo> participants, int currentVersion) {
		List<SharedSessionParticipantInfo> result = new ArrayList<>(participants.size());
		for (SharedSessionParticipantInfo participant : participants) {
			if (!participant.isSetLeftVersion() || currentVersion - participant.getLeftVersion() > FORMER_PARTICIPANTS_GC_VERSIONS) {
				result.add(participant);
			}
		}
		return result;
	}

	private SharedSessionParticipantInfo findActiveParticipant(List<SharedSessionParticipantInfo> participants, byte[] id) {
		for (SharedSessionParticipantInfo participant : participants) {
			if (!participant.isSetLeftVersion() && Arrays.equals(participant.getIdentityRef().getId(), id)) {
				return participant;
			}
		}
		return null;
	}

	private SharedSessionParticipantInfo findFormerParticipant(List<SharedSessionParticipantInfo> participants, byte[] id) {
		for (SharedSessionParticipantInfo participant : participants) {
			if (participant.isSetLeftVersion() && Arrays.equals(participant.getIdentityRef().getId(), id)) {
				return participant;
			}
		}
		return null;
	}

	public synchronized boolean updateParticipants(byte[] sessionId, boolean add, List<byte[]> participants) {
		LocalSharedSessionState localState = localSessions.get(sessionId);
		if (localState == null) {
			return false;
		}
		SharedSessionDescriptor descriptor = localState.getDescriptor();
		if (!cryptoHelper.isOwnId(descriptor.getAdminIdentityRef().getId())) {
			return false;
		}

		int newDescriptorVersion = descriptor.getVersion() + 1;

		List<SharedSessionParticipantInfo> newParticipants = new ArrayList<>(descriptor.getParticipants());
		boolean updated = false;
		for (byte[] participantId : participants) {
			if (add) {
				if (cryptoHelper.isOwnId(participantId) || findActiveParticipant(newParticipants, participantId) != null) {
					updated = true;
					continue;
				}
				LocalIdentityDescriptorState descriptorState = remoteIdentityService.get(participantId);
				if (descriptorState == null) {
					continue;
				}
				IdentityReference participantIdentityReference = new IdentityReference(ByteBuffer.wrap(participantId), descriptorState.getDescriptor().getStorageProfile());
				SharedSessionParticipantInfo participantInfo = new SharedSessionParticipantInfo(participantIdentityReference, newDescriptorVersion);
				newParticipants.add(participantInfo);
				updated = true;
			} else {
				if (cryptoHelper.isOwnId(participantId)) {
					continue;
				}
				updated = true;
				SharedSessionParticipantInfo participantInfo = findActiveParticipant(newParticipants, participantId);
				if (participantInfo != null) {
					participantInfo.setLeftVersion(newDescriptorVersion);
				}
			}
		}

		if (!updated) {
			return true;
		}

		SharedSessionDescriptor newDescriptor = new SharedSessionDescriptor(descriptor);
		newDescriptor.setContentKey(generateKey());
		newDescriptor.setVersion(newDescriptorVersion);
		newDescriptor.setParticipants(filterFormerParticipants(newParticipants, newDescriptorVersion));

		boolean success = publishDescriptor(newDescriptor);
		if (!success) {
			log.warn(tracePrefix + " Failed to update participants " + Utils.getDebugStringFromId(sessionId));
			return false;
		}

		setNewDescriptor(localState, newDescriptor);
		localSessions.put(localState);
		return true;
	}

	public synchronized boolean updateProperties(byte[] sessionId, List<Property> updates) {
		LocalSharedSessionState localState = localSessions.get(sessionId);
		if (localState == null) {
			return false;
		}
		SharedSessionDescriptor descriptor = localState.getDescriptor();
		if (!cryptoHelper.isOwnId(descriptor.getAdminIdentityRef().getId())) {
			return false;
		}

		List<Property> updatedProperties = PropertyUtils.updateProperties(descriptor.getProperties(), updates);
		if (updatedProperties == null) {
			return true;
		}

		int newDescriptorVersion = descriptor.getVersion() + 1;
		SharedSessionDescriptor newDescriptor = new SharedSessionDescriptor(descriptor);
		newDescriptor.setVersion(newDescriptorVersion);
		newDescriptor.setProperties(updatedProperties);

		boolean success = publishDescriptor(newDescriptor);
		if (!success) {
			log.warn(tracePrefix + " Failed to update properties " + Utils.getDebugStringFromId(sessionId));
			return false;
		}

		setNewDescriptor(localState, newDescriptor);
		localSessions.put(localState);
		return true;
	}

	public synchronized boolean sendMessage(byte[] sessionId, TypedPayload payload) {
		LocalSharedSessionState localState = localSessions.get(sessionId);
		if (localState == null || !localState.isSetDescriptor() || localState.isSetUnsubscribedDescriptorVersion()) {
			return false;
		}
		SharedSessionDescriptor descriptor = localState.getDescriptor();
		if (findActiveParticipant(descriptor.getParticipants(), cryptoHelper.getOwnId()) == null) {
			return false;
		}

		long now = System.currentTimeMillis();
		byte[] messageIdSeed = cryptoHelper.generateRandomBytes(MESSAGE_ID_SIZE);
		byte[] messageId = generateMessageId(sessionId, cryptoHelper.getOwnId(), messageIdSeed);
		SharedSessionMessageMetadata metadata = new SharedSessionMessageMetadata(ByteBuffer.wrap(descriptor.getSessionId()), descriptor.getVersion(), ByteBuffer.wrap(cryptoHelper.getOwnId()), ByteBuffer.wrap(messageIdSeed), ByteBuffer.wrap(messageId));
		metadata.setCreatedAt(now);
		SharedSessionMessage message = new SharedSessionMessage(metadata, localState.getPreviousMessageTails(), payload);
		LocalSharedSessionMessage localMessage = new LocalSharedSessionMessage(metadata, now);
		localMessage.setFetchedAt(now);
		localMessage.setConsumedAt(now);
		localMessage.setMessage(message);
		localMessages.put(localMessage);

		TypedPayload messagePayload = TypedPayloadUtils.wrap(message, SERVICE_NAME, null, null);
		CryptoEnvelope messageData = cryptoHelper.signAndEncryptSymmetric(messagePayload, null, MESSAGE_TTL, descriptor.getContentKey(), ASSOCIATED_DATA);
		OutboundDataWrapperRaw messageWrapper = new OutboundDataWrapperRaw(ThriftUtils.serialize(messageData), null, now + MESSAGE_TTL);
		remoteMessageHandler.write(messageId, messageWrapper);

		localState.setPreviousMessageTails(Collections.singletonList(metadata));
		localSessions.put(localState);

		log.trace(tracePrefix + " Sent message " + Utils.getDebugStringFromBytes(messageId) + " to session " + Utils.getDebugStringFromBytes(descriptor.getSessionId()));
		workState.addMessageSentSession(sessionId);

		if (callback != null) {
			callback.onMessagesSent(sessionId);
		}

		return true;
	}

	private void sendSyncMessage(SharedSessionDescriptor descriptor, List<SharedSessionMessageMetadata> messageReferences) {
		SharedSessionSyncMessage syncMessageBase = new SharedSessionSyncMessage(messageReferences);
		int toJoinedAtOrBeforeVersion = descriptor.getVersion();
		boolean amIAdmin = cryptoHelper.isOwnId(descriptor.getAdminIdentityRef().getId());
		for (SharedSessionParticipantInfo participant : getActiveParticipants(descriptor)) {
			byte[] toId = participant.getIdentityRef().getId();
			if (participant.getJoinedVersion() > toJoinedAtOrBeforeVersion) {
				continue;
			}
			if (cryptoHelper.isOwnId(toId)) {
				continue;
			}
			if (!commSessionService.haveSessionWith(toId)) {
				if (amIAdmin) {
					commSessionService.establishSessionWith(toId);
				} else {
					continue;
				}
			}

			SharedSessionSyncMessage syncMessage = new SharedSessionSyncMessage(syncMessageBase);

			// TODO: Only needed for the first sync, do not send always
			syncMessage.setAdminIdentityRef(descriptor.getAdminIdentityRef());

			TypedPayload syncMessageWrapped = TypedPayloadUtils.wrap(syncMessage, SERVICE_NAME, null, null);
			boolean success = commSessionService.sendMessage(toId, syncMessageWrapped);
			log.trace(tracePrefix + (success ? " Sent" : " Failed to send") + " sync message on " + Utils.getDebugStringFromBytes(descriptor.getSessionId()) + " to " + Utils.getDebugStringFromId(toId));

			if (success) {
				SharedSessionSyncStats syncStats = getSyncStats(descriptor.getSessionId());
				syncStats.setSyncMessagesSent(syncStats.getSyncMessagesSent() + 1);
			}
		}
	}
}
