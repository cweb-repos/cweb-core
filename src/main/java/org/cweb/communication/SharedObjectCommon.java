package org.cweb.communication;

import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;

import java.util.Arrays;

public class SharedObjectCommon {
	static final String SERVICE_NAME = "SharedObject";
	static final byte[] ASSOCIATED_DATA = new byte[]{52, 91, 43};
	static final int OBJECT_ID_SIZE = 16;
	private static final String REMOTE_OBJECT_NAME_SUFFIX = "-sharedObject";

	public static boolean isValidObjectId(byte[] objectId) {
		return objectId.length == OBJECT_ID_SIZE;
	}

	static byte[] generateObjectId(byte[] fromId, byte[] objectIdSeed) {
		return Arrays.copyOf(HashingUtils.SHA256(BinaryUtils.concat(fromId, objectIdSeed)), OBJECT_ID_SIZE);
	}

	public static RemoteFileHandler createRemoteFileHandler(RemoteReadService remoteReadService, RemoteWriteService remoteWriteService) {
		return new RemoteFileHandler(remoteReadService, remoteWriteService, REMOTE_OBJECT_NAME_SUFFIX);
	}
}
