package org.cweb.communication;

import com.google.common.base.Preconditions;
import org.cweb.admin.RemoteAdminHostService;
import org.cweb.identity.IdentityProfileReadService;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class CommScheduler {
	private static final Logger log = LoggerFactory.getLogger(CommScheduler.class);
	private static final Long MIN_TIME_VALUE = Long.MIN_VALUE / 100;
	private static final long CHECK_FORWARD_MIN_PERIOD = 1000L * 60 * 60 * 24 * 2;
	private static final long CHECK_FORWARD_RECEIVED_GAP_NORMAL = 1000L * 60 * 60 * 24 * 14;
	private static final long CHECK_FORWARD_RECEIVED_GAP_LARGE = 1000L * 60 * 60 * 24 * 30;
	private static final int COMM_SESSION_CHECK_FORWARD_MESSAGES_NORMAL = 8;
	private static final int COMM_SESSION_CHECK_FORWARD_MESSAGES_LARGE = 150;
	private static final long CHECK_LOST_PERIOD = 1000L * 60 * 60 * 12;
	private static final long CHECK_PERIOD_NEVER = 1000L * 60 * 60 * 24 * 365;
	private static final long MIN_CHECK_PERIOD_FALLBACK = 1000L * 5;
	private static final long SHARED_SESSION_WAIT_TIME = 1000L * 60 * 2;
	private static final long SHARED_SESSION_SEND_BUFFERING_TIME = 1000L * 5;
	private static final long MIN_INTERACTION_TIME_TO_WAKE = 1000L * 5;
	private static final long HOSTED_PROFILE_CHECK_INTERVAL = 1000L * 60 * 5;

	private final String tracePrefix;
	private final byte[] ownId;
	private final RemoteWriteService remoteWriteService;
	private final RemoteIdentityFetcher remoteIdentityFetcher;
	private final RemoteAdminHostService remoteAdminHostService;
	private final CommSessionService commSessionService;
	private final SharedSessionService sharedSessionService;
	private final SharedObjectReadService sharedObjectReadService;
	private final IdentityProfileReadService identityProfileReadService;
	private NativeSchedulingProvider provider;

	private CommSessionMessageCallback commSessionMessageCallback;
	private SharedSessionCallback sharedSessionCallback;
	private SharedObjectCallback sharedObjectCallback;

	private final BlockingQueue<SyncFuture> messageLoopWakeupQueue = new LinkedBlockingQueue<>();
	private final Thread messageLoopThread;
	private final Map<ByteBuffer, DirectSyncState> id2directSyncState = new ConcurrentHashMap<>();
	private final Set<DirectSyncState> directSyncStatesByNextCheckTime = new TreeSet<>(new DirectSyncStateComparator());
	private final Set<SharedObjectReadState> sharedObjectReadStatesByNextCheckTime = new TreeSet<>(new SharedObjectReadStateComparator());

	private boolean closed;
	private long nextWakeupScheduledTime;
	private long nextSharedSessionsCheckTime;
	private long lastHostedProfileCheckTime;

	private enum InteractionType {
		SENT,
		RECEIVED,
		INTERACTION,
	}

	private static class DirectSyncState {
		final ByteBuffer id;
		long minInterval;
		long maxInterval;

		long lastCheckTime;
		long lastForwardCheckTime;
		long lastLostCheckTime;
		Map<InteractionType, Long> lastInteractionTime = new EnumMap<>(InteractionType.class);

		long nextCheckTime;

		private DirectSyncState(ByteBuffer id, long minInterval, long maxInterval) {
			this.id = id;
			this.minInterval = minInterval;
			this.maxInterval = maxInterval;
		}
	}

	private static class SharedObjectReadState {
		final ByteBuffer id;
		long interval;

		long lastCheckTime;
		long nextCheckTime;

		private SharedObjectReadState(ByteBuffer id, long interval) {
			this.id = id;
			this.interval = interval;
		}
	}

	private static class SyncFuture {
		private boolean done;

		private synchronized boolean isDone() {
			return done;
		}

		private synchronized void setDone() {
			// log.info(tracePrefix + " Future wait done " + this);
			done = true;
			this.notifyAll();
		}
	}

	private static class DirectSyncStateComparator implements Comparator<DirectSyncState> {
		@Override
		public int compare(DirectSyncState o1, DirectSyncState o2) {
			int result = Long.signum(o1.nextCheckTime - o2.nextCheckTime);
			if (result == 0) {
				result = o1.id.compareTo(o2.id);
			}
			return result;
		}
	}

	private static class SharedObjectReadStateComparator implements Comparator<SharedObjectReadState> {
		@Override
		public int compare(SharedObjectReadState o1, SharedObjectReadState o2) {
			int result = Long.signum(o1.nextCheckTime - o2.nextCheckTime);
			if (result == 0) {
				result = o1.id.compareTo(o2.id);
			}
			return result;
		}
	}

	public CommScheduler(String tracePrefix, byte[] ownId, RemoteWriteService remoteWriteService, RemoteIdentityFetcher remoteIdentityFetcher, RemoteAdminHostService remoteAdminHostService, CommSessionService commSessionService, SharedSessionService sharedSessionService, SharedObjectReadService sharedObjectReadService, IdentityProfileReadService identityProfileReadService) {
		this.tracePrefix = tracePrefix;
		this.ownId = ownId;
		this.remoteWriteService = remoteWriteService;
		this.remoteIdentityFetcher = remoteIdentityFetcher;
		this.remoteAdminHostService = remoteAdminHostService;
		this.commSessionService = commSessionService;
		this.sharedSessionService = sharedSessionService;
		this.sharedObjectReadService = sharedObjectReadService;
		this.identityProfileReadService = identityProfileReadService;

		sharedObjectReadService.setCallback(sharedObjectCallbackInternal);
		sharedSessionService.setCallback(sharedSessionCallbackInternal);

		this.messageLoopThread = new Thread("CommSchedulerThread") {
			@Override
			public void run() {
				while (!closed) {
					messageLoop();
				}
			}
		};
	}

	private final SharedObjectCallbackInternal sharedObjectCallbackInternal = new SharedObjectCallbackInternal() {
		@Override
		public void requestImmediateFetch(byte[] objectId, byte[] fromId) {
			requestImmediateSharedObjectRead(objectId, fromId);
		}
	};

	private final SharedSessionCallbackInternal sharedSessionCallbackInternal = new SharedSessionCallbackInternal() {
		@Override
		public void onMessagesReceived(final byte[] sessionId) {
			reportSharedSessionInteraction(sessionId, InteractionType.RECEIVED);
			if (sharedSessionCallback != null) {
				Threads.submitTask(new Runnable() {
					@Override
					public void run() {
						sharedSessionCallback.onMessagesReceived(sessionId);
					}
				});
			}
		}

		@Override
		public void onMessagesSent(byte[] sessionId) {
			reportSharedSessionInteraction(sessionId, InteractionType.SENT);
			long now = provider.getTime();
			if (sharedSessionService.hasBufferedWork() && nextSharedSessionsCheckTime > now + SHARED_SESSION_SEND_BUFFERING_TIME) {
				nextSharedSessionsCheckTime = now + SHARED_SESSION_SEND_BUFFERING_TIME;
				scheduleNextWakeup();
			}
		}

		@Override
		public void onMessagesAcked(final byte[] sessionId) {
			if (sharedSessionCallback != null) {
				Threads.submitTask(new Runnable() {
					@Override
					public void run() {
						sharedSessionCallback.onMessagesAcked(sessionId);
					}
				});
			}
		}

		@Override
		public void onDescriptorUpdated(final byte[] sessionId) {
			if (sharedSessionCallback != null) {
				Threads.submitTask(new Runnable() {
					@Override
					public void run() {
						sharedSessionCallback.onDescriptorUpdated(sessionId);
					}
				});
			}
		}
	};

	public void init(NativeSchedulingProvider provider) {
		Preconditions.checkArgument(this.provider == null);
		this.provider = provider;
		this.provider.setCallbacks(new NativeSchedulingProvider.Callbacks() {
			@Override
			public void wakeUp() {
				wakeMessageLoopThread();
			}
		});

		long now = provider.getTime();
		nextSharedSessionsCheckTime = now;

		messageLoopThread.setDaemon(true);
		messageLoopThread.start();
	}

	public void close() {
		closed = true;
		wakeMessageLoopThread();
		Threads.joinThreadSafe(messageLoopThread);
	}

	public void setCommSessionMessageCallback(CommSessionMessageCallback commSessionMessageCallback) {
		this.commSessionMessageCallback = commSessionMessageCallback;
	}

	public void setSharedSessionCallback(SharedSessionCallback sharedSessionCallback) {
		this.sharedSessionCallback = sharedSessionCallback;
	}

	public void setSharedObjectCallback(SharedObjectCallback sharedObjectCallback) {
		this.sharedObjectCallback = sharedObjectCallback;
	}

	private SyncFuture wakeMessageLoopThread() {
		SyncFuture future = new SyncFuture();
		messageLoopWakeupQueue.add(future);
		return future;
	}

	private void messageLoop() {
		SyncFuture firstFuture = null;
		try {
			firstFuture = messageLoopWakeupQueue.poll(CHECK_PERIOD_NEVER, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
		}

		if (closed) {
			return;
		}

		provider.cancelWakeup();
		nextWakeupScheduledTime = 0;
		long now = provider.getTime();

		Set<SyncFuture> futures = Utils.drainAndDedup(messageLoopWakeupQueue);
		futures.add(firstFuture);

		log.trace(tracePrefix + " messageLoop starting with " + futures.size() + " futures");

		if (provider.hasInternetConnection()) {
			long nextWakeTime = getNextWakeTime();
			if (nextWakeTime != Long.MAX_VALUE && now >= nextWakeTime) {
				readCommSessionMessages();
			}
			if (provider.hasInternetConnection()) {
				processSharedObjects();
			}
			if (provider.hasInternetConnection() && sharedSessionService.hasBufferedWork()) {
				processSharedSessions();
			}
			if (provider.hasInternetConnection()) {
				remoteWriteService.retryUploads();
			}
			if (provider.hasInternetConnection()) {
				checkForNewHostedProfiles(false);
			}
		}

		if (closed) {
			return;
		}

		for (SyncFuture future : futures) {
			future.setDone();
		}

		if (sharedSessionService.hasBufferedWork()) {
			nextSharedSessionsCheckTime = now + SHARED_SESSION_WAIT_TIME;
		} else {
			nextSharedSessionsCheckTime = Long.MAX_VALUE;
		}

		scheduleNextWakeup();
	}

	private void scheduleNextWakeup() {
		long now = provider.getTime();
		long nextWakeTime;
		if (provider.hasInternetConnection()) {
			nextWakeTime = getNextWakeTime();
			if (nextWakeTime == Long.MAX_VALUE) {
				nextWakeTime = now + CHECK_PERIOD_NEVER;
			}
		} else {
			nextWakeTime = now + provider.getNoConnectionRetryInterval();
		}

		if (now - nextWakeTime > 0) {
			if (nextWakeTime > MIN_CHECK_PERIOD_FALLBACK && now - nextWakeTime > 1000) {
				log.debug(tracePrefix + " scheduleNextWakeup nextWakeTime in the past: " + nextWakeTime + ", now=" + now);
			}
			nextWakeTime = now + MIN_CHECK_PERIOD_FALLBACK;
		}

		scheduleNextWakeup(now, nextWakeTime);
	}

	private synchronized void scheduleNextWakeup(long now, long nextWakeTime) {
		if (nextWakeupScheduledTime > now && nextWakeupScheduledTime < nextWakeTime) {
			// log.trace(tracePrefix + " scheduleNextWakeup skipping " + (nextWakeTime - now) + " for already scheduled in " + (nextWakeupScheduledTime - now));
			return;
		}
		log.trace(tracePrefix + " scheduleNextWakeup sleeping for " + (nextWakeTime - now));
		nextWakeupScheduledTime = nextWakeTime;
		provider.scheduleWakeup(nextWakeTime);
	}

	private long getNextWakeTime() {
		long nextCheckTime = Long.MAX_VALUE;

		synchronized (directSyncStatesByNextCheckTime) {
			if (!directSyncStatesByNextCheckTime.isEmpty()) {
				DirectSyncState firstDirectSyncState = directSyncStatesByNextCheckTime.iterator().next();
				nextCheckTime = Math.min(nextCheckTime, firstDirectSyncState.nextCheckTime);
			}
		}
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			if (!sharedObjectReadStatesByNextCheckTime.isEmpty()) {
				SharedObjectReadState firstSharedObjectReadState = sharedObjectReadStatesByNextCheckTime.iterator().next();
				nextCheckTime = Math.min(nextCheckTime, firstSharedObjectReadState.nextCheckTime);
			}
		}

		nextCheckTime = Math.min(nextCheckTime, nextSharedSessionsCheckTime);

		return nextCheckTime;
	}

	private void readCommSessionMessages() {
		long startTime = provider.getTime();
		long minNewNextCheckTime = startTime + CHECK_PERIOD_NEVER;
		List<DirectSyncState> queue;
		synchronized (directSyncStatesByNextCheckTime) {
			queue = new ArrayList<>(directSyncStatesByNextCheckTime);
		}
		for (DirectSyncState syncState : queue) {
			byte[] id = Utils.toArray(syncState.id);
			long now = provider.getTime();
			long nextCheckTime = syncState.nextCheckTime;
			boolean checkDue = nextCheckTime <= now;
			boolean checkBatched = nextCheckTime <= minNewNextCheckTime && now >= nextCheckTime - (nextCheckTime - syncState.lastCheckTime) / 3;
			if (!checkDue && !checkBatched) {
				break;
			}

			readCommSessionMessages(id, syncState, now);

			updateNextCheckTime(syncState);
			minNewNextCheckTime = Math.min(minNewNextCheckTime, syncState.nextCheckTime);

			if (!provider.hasInternetConnection()) {
				return;
			}
		}
	}

	private void readCommSessionMessages(byte[] id, DirectSyncState syncState, long now) {
		syncState.lastCheckTime = now;

		log.trace(tracePrefix + " readNewMessages start from " + org.cweb.utils.Utils.getDebugStringFromId(id));

		int checkForwardOrLostMessages = determineForwardOrLostMessageCheck(id, now, syncState);
		CommSessionService.ReceivedMessagesStats receivedMessagesStats = commSessionService.readMessages(id, checkForwardOrLostMessages);

		int receivedMessages = receivedMessagesStats.receivedDirect + receivedMessagesStats.processed;
		if (receivedMessages > 0) {
			syncState.lastInteractionTime.put(InteractionType.RECEIVED, now);
			if (commSessionMessageCallback != null && receivedMessagesStats.receivedDirect > 0) {
				commSessionMessageCallback.onMessagesReceived(id);
			}
		}

		log.trace(tracePrefix + " readNewMessages done from " + org.cweb.utils.Utils.getDebugStringFromId(id) + ", received " + receivedMessages);
	}

	private int determineForwardOrLostMessageCheck(byte[] id, long now, DirectSyncState syncState) {
		if (MIN_TIME_VALUE.equals(syncState.lastInteractionTime.get(InteractionType.RECEIVED))) {
			return 0;
		}

		int checkLostMessages = determineLostMessageCheck(id, now, syncState);
		if (checkLostMessages != 0) {
			syncState.lastLostCheckTime = now;
			return checkLostMessages;
		}

		int checkForwardMessages = determineForwardMessageCheck(id, now, syncState);
		if (checkForwardMessages != 0) {
			syncState.lastForwardCheckTime = now;
			return checkForwardMessages;
		}

		return 0;
	}

	private int determineLostMessageCheck(byte[] id, long now, DirectSyncState syncState) {
		long lastReceived = syncState.lastInteractionTime.get(InteractionType.RECEIVED);
		if (now - lastReceived < CHECK_LOST_PERIOD) {
			return 0;
		}
		long prevCheckInterval = syncState.lastLostCheckTime - lastReceived;
		long nextCheckInterval = Math.max(CHECK_LOST_PERIOD, prevCheckInterval) * 2;
		if (lastReceived + nextCheckInterval > now) {
			return 0;
		}
		log.debug(tracePrefix + " Checking lost messages from " + org.cweb.utils.Utils.getDebugStringFromId(id) + " lastReceived=" + toAbsoluteTime(lastReceived));
		return -1;
	}

	private int determineForwardMessageCheck(byte[] id, long now, DirectSyncState syncState) {
		long lastReceived = syncState.lastInteractionTime.get(InteractionType.RECEIVED);
		if (now - lastReceived < CHECK_FORWARD_RECEIVED_GAP_NORMAL) {
			return 0;
		}
		long lastInteracted = Math.max(syncState.lastInteractionTime.get(InteractionType.INTERACTION), syncState.lastInteractionTime.get(InteractionType.SENT));
		long prevCheckInterval = syncState.lastForwardCheckTime - lastReceived;
		long nextCheckInterval = prevCheckInterval + (lastInteracted < syncState.lastForwardCheckTime ? Math.max(CHECK_FORWARD_MIN_PERIOD, prevCheckInterval) : CHECK_FORWARD_MIN_PERIOD);
		if (lastReceived + nextCheckInterval > now) {
			return 0;
		}
		int checkForwardMessages = now - lastReceived > CHECK_FORWARD_RECEIVED_GAP_LARGE ? COMM_SESSION_CHECK_FORWARD_MESSAGES_LARGE : COMM_SESSION_CHECK_FORWARD_MESSAGES_NORMAL;
		log.debug(tracePrefix + " Checking " + checkForwardMessages + " forward messages from " + org.cweb.utils.Utils.getDebugStringFromId(id) + " lastReceived=" + toAbsoluteTime(lastReceived));
		return checkForwardMessages;
	}

	private void processSharedSessions() {
		log.trace(tracePrefix + " processSharedSessions start");
		sharedSessionService.processBufferedWork();
	}

	private void processSharedObjects() {
		log.trace(tracePrefix + " processSharedObjects start");
		long startTime = provider.getTime();
		long minNewNextCheckTime = startTime + CHECK_PERIOD_NEVER;
		ArrayList<SharedObjectReadState> queue;
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			queue = new ArrayList<>(sharedObjectReadStatesByNextCheckTime);
		}
		for (SharedObjectReadState readState : queue) {
			byte[] objectId = Utils.toArray(readState.id);
			long now = provider.getTime();
			long nextCheckTime = readState.nextCheckTime;
			boolean checkDue = nextCheckTime <= now;
			boolean checkBatched = nextCheckTime <= minNewNextCheckTime && now >= nextCheckTime - (nextCheckTime - readState.lastCheckTime) / 3;
			if (!checkDue && !checkBatched) {
				break;
			}

			readState.lastCheckTime = now;
			readSharedObject(objectId);

			updateNextCheckTime(readState);
			minNewNextCheckTime = Math.min(minNewNextCheckTime, readState.nextCheckTime);

			if (!provider.hasInternetConnection()) {
				return;
			}
		}
		while (true) {
			List<byte[]> objectIds = sharedObjectReadService.consumeBufferedIdsToRead();
			if (objectIds.isEmpty()) {
				break;
			}
			for (byte[] objectId : objectIds) {
				readSharedObject(objectId);
			}
		}
	}

	private void readSharedObject(byte[] objectId) {
		log.trace(tracePrefix + " readSharedObject start from " + org.cweb.utils.Utils.getDebugStringFromId(objectId));

		int receivedObjects = sharedObjectReadService.readObject(objectId);
		if (sharedObjectCallback != null && receivedObjects > 0) {
			sharedObjectCallback.onUpdateReceived(objectId);
		}

		log.trace(tracePrefix + " readSharedObject done from " + org.cweb.utils.Utils.getDebugStringFromId(objectId) + ", received " + receivedObjects);
	}

	private void updateNextCheckTime(DirectSyncState syncState) {
		long lastReceived = syncState.lastInteractionTime.get(InteractionType.RECEIVED);
		long nextTimeToCheckByReceived = syncState.lastCheckTime + (syncState.lastCheckTime - lastReceived) / 2;
		long lastInteracted = Math.max(syncState.lastInteractionTime.get(InteractionType.INTERACTION), syncState.lastInteractionTime.get(InteractionType.SENT));

		long nextTimeToCheckByInteraction;
		if (lastInteracted > syncState.lastCheckTime) {
			nextTimeToCheckByInteraction = lastInteracted + syncState.minInterval;
		} else {
			nextTimeToCheckByInteraction = syncState.lastCheckTime + (syncState.lastCheckTime - lastInteracted) * 2;
		}

		long nextCheckTime = Math.min(nextTimeToCheckByReceived, nextTimeToCheckByInteraction);
		nextCheckTime = Math.min(nextCheckTime, syncState.lastCheckTime + syncState.maxInterval);
		nextCheckTime = Math.max(nextCheckTime, syncState.lastCheckTime + syncState.minInterval);

		setNextCheckTime(syncState, nextCheckTime);
	}

	private void setNextCheckTime(DirectSyncState syncState, long nextCheckTime) {
		synchronized (directSyncStatesByNextCheckTime) {
			directSyncStatesByNextCheckTime.remove(syncState);
			syncState.nextCheckTime = nextCheckTime;
			directSyncStatesByNextCheckTime.add(syncState);
		}
	}

	private void updateNextCheckTime(SharedObjectReadState sharedObjectReadState) {
		long nextCheckTime = sharedObjectReadState.lastCheckTime + sharedObjectReadState.interval;
		setNextCheckTime(sharedObjectReadState, nextCheckTime);
	}

	private void setNextCheckTime(SharedObjectReadState sharedObjectReadState, long nextCheckTime) {
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			sharedObjectReadStatesByNextCheckTime.remove(sharedObjectReadState);
			sharedObjectReadState.nextCheckTime = nextCheckTime;
			sharedObjectReadStatesByNextCheckTime.add(sharedObjectReadState);
		}
	}

	public void requestPeriodicSync(SessionId sessionId, long minInterval, long maxInterval) {
		if (sessionId.getType() == SessionType.COMM_SESSION) {
			requestPeriodicCommSessionSync(sessionId.getId(), minInterval, maxInterval);
			requestPeriodicIdentityProfileFetch(sessionId.getId());
		} else if (sessionId.getType() == SessionType.SHARED_SESSION) {
			requestPeriodicSharedSessionSync(sessionId.getId(), minInterval, maxInterval);
		}
	}

	public void cancelPeriodicSync(SessionId sessionId) {
		if (sessionId.getType() == SessionType.COMM_SESSION) {
			cancelPeriodicCommSessionSync(sessionId.getId());
			cancelPeriodicIdentityProfileFetch(sessionId.getId());
		} else if (sessionId.getType() == SessionType.SHARED_SESSION) {
			cancelPeriodicSharedSessionSync(sessionId.getId());
		}
	}

	private void requestPeriodicCommSessionSync(byte[] id, long minInterval, long maxInterval) {
		if (Arrays.equals(id, ownId)) {
			log.warn(tracePrefix + " Requested sync with self");
			return;
		}
		boolean identityMissingOrStale = isIdentityMissingOrStale(id);
		if (identityMissingOrStale) {
			log.debug(tracePrefix + " Requested sync with stale identity " + Utils.getDebugStringFromId(id) + ", skipping");
			return;
		}
		ByteBuffer idWrapped = ByteBuffer.wrap(id);
		DirectSyncState syncState;
		synchronized (id2directSyncState) {
			syncState = id2directSyncState.get(idWrapped);
			if (syncState != null) {
				syncState.minInterval = Math.min(syncState.minInterval, minInterval);
				syncState.maxInterval = Math.min(syncState.maxInterval, maxInterval);
				return;
			} else {
				syncState = newSyncState(id, idWrapped, minInterval, maxInterval);
				id2directSyncState.put(idWrapped, syncState);
			}
		}
		updateNextCheckTime(syncState);
		scheduleNextWakeup();
	}

	private boolean isIdentityMissingOrStale(byte[] id) {
		if (Arrays.equals(id, ownId)) {
			return false;
		}
		LocalIdentityDescriptorState descriptorState = remoteIdentityFetcher.fetch(id);
		long nowAbsolute = System.currentTimeMillis();
		return descriptorState == null || !descriptorState.isSetValidUntil() || descriptorState.getValidUntil() < nowAbsolute;
	}

	private void cancelPeriodicCommSessionSync(byte[] id) {
		ByteBuffer idWrapped = ByteBuffer.wrap(id);
		synchronized (id2directSyncState) {
			id2directSyncState.remove(idWrapped);
		}
	}

	private void requestPeriodicSharedSessionSync(byte[] sessionId, long minInterval, long maxInterval) {
		SharedSessionService.SessionMetadata sessionMetadata = sharedSessionService.getSessionMetadata(sessionId);
		if (sessionMetadata == null) {
			log.debug(tracePrefix + " Invalid session " + Utils.getDebugStringFromId(sessionId));
			return;
		}
		if (sessionMetadata.isUnsubscribed) {
			return;
		}
		boolean identityMissingOrStale = isIdentityMissingOrStale(sessionMetadata.adminId);
		if (identityMissingOrStale) {
			log.info(tracePrefix + " Requested shared session sync from stale identity " + Utils.getDebugStringFromId(sessionMetadata.adminId) + ", skipping");
			return;
		}
		if (!Arrays.equals(sessionMetadata.adminId, ownId)) {
			requestPeriodicObjectFetch(sessionMetadata.sharedObjectId);
		}
		for (byte[] participantId : sessionMetadata.participantIds) {
			if (Arrays.equals(participantId, ownId)) {
				continue;
			}
			if (commSessionService.haveSessionWith(participantId)) {
				requestPeriodicCommSessionSync(participantId, minInterval, maxInterval);
			}
		}
	}

	private void cancelPeriodicSharedSessionSync(byte[] sessionId) {
		SharedSessionService.SessionMetadata sessionMetadata = sharedSessionService.getSessionMetadata(sessionId);
		if (sessionMetadata == null) {
			log.debug(tracePrefix + " Canceling invalid session " + Utils.getDebugStringFromId(sessionId));
			return;
		}
		cancelPeriodicObjectFetch(sessionMetadata.sharedObjectId);
	}

	private void requestPeriodicObjectFetch(byte[] objectId) {
		SharedObjectReadService.ObjectMetadata metadata = sharedObjectReadService.getObjectMetadata(objectId);
		if (metadata == null || metadata.isUnsubscribed || metadata.pollInterval == null) {
			log.debug(tracePrefix + " Cannot set periodic fetch for object " + Utils.getDebugStringFromId(objectId));
			return;
		}
		requestPeriodicCommSessionSync(metadata.fromId, metadata.pollInterval, metadata.pollInterval);
		ByteBuffer objectIdWrapped = ByteBuffer.wrap(objectId);
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			SharedObjectReadState sharedObjectReadState = findObject(sharedObjectReadStatesByNextCheckTime, objectIdWrapped);
			if (sharedObjectReadState != null) {
				sharedObjectReadState.interval = metadata.pollInterval;
				return;
			} else {
				sharedObjectReadState = new SharedObjectReadState(objectIdWrapped, metadata.pollInterval);
			}
			updateNextCheckTime(sharedObjectReadState);
		}
		scheduleNextWakeup();
	}

	private void cancelPeriodicObjectFetch(byte[] objectId) {
		SharedObjectReadService.ObjectMetadata metadata = sharedObjectReadService.getObjectMetadata(objectId);
		if (metadata == null) {
			log.debug(tracePrefix + " Cannot cancel periodic fetch for object " + Utils.getDebugStringFromId(objectId));
			return;
		}
		ByteBuffer objectIdWrapped = ByteBuffer.wrap(objectId);
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			SharedObjectReadState sharedObjectReadState = findObject(sharedObjectReadStatesByNextCheckTime, objectIdWrapped);
			if (sharedObjectReadState != null) {
				sharedObjectReadStatesByNextCheckTime.remove(sharedObjectReadState);
			}
		}
	}

	private SharedObjectReadState findObject(Set<SharedObjectReadState> sharedObjectReadStates, ByteBuffer objectIdWrapped) {
		for (SharedObjectReadState sharedObjectReadState : sharedObjectReadStates) {
			if (objectIdWrapped.equals(sharedObjectReadState.id)) {
				return sharedObjectReadState;
			}
		}
		return null;
	}

	private void requestPeriodicIdentityProfileFetch(byte[] id) {
		byte[] identityProfileObjectId = identityProfileReadService.getObjectId(id);
		if (identityProfileObjectId == null) {
			return;
		}
		boolean identityMissingOrStale = isIdentityMissingOrStale(id);
		if (identityMissingOrStale) {
			log.info(tracePrefix + " Requested identity profile sync from stale identity " + Utils.getDebugStringFromId(id) + ", skipping");
			return;
		}
		requestPeriodicObjectFetch(identityProfileObjectId);
	}

	private void cancelPeriodicIdentityProfileFetch(byte[] id) {
		byte[] identityProfileObjectId = identityProfileReadService.getObjectId(id);
		if (identityProfileObjectId == null) {
			return;
		}
		cancelPeriodicObjectFetch(identityProfileObjectId);
	}

	private long fromAbsoluteTime(long absoluteTime) {
		if (absoluteTime == 0) {
			return MIN_TIME_VALUE;
		}
		long nowProvider = provider.getTime();
		long nowAbsolute = System.currentTimeMillis();
		return absoluteTime - nowAbsolute + nowProvider;
	}

	private long toAbsoluteTime(long providerTime) {
		long nowProvider = provider.getTime();
		long nowAbsolute = System.currentTimeMillis();
		return providerTime - nowProvider + nowAbsolute;
	}

	private DirectSyncState newSyncState(byte[] id, ByteBuffer idWrapped, long minInterval, long maxInterval) {
		long now = provider.getTime();
		DirectSyncState syncState = new DirectSyncState(idWrapped, minInterval, maxInterval);
		syncState.lastCheckTime = 0;
		syncState.nextCheckTime = -1;
		syncState.lastForwardCheckTime = now;
		syncState.lastLostCheckTime = now;
		for (InteractionType source : InteractionType.values()) {
			syncState.lastInteractionTime.put(source, 0L);
		}

		CommSessionService.SessionMetadata metadata = commSessionService.getSessionMetadata(id);
		if (metadata != null) {
			long lastReceived = fromAbsoluteTime(metadata.lastReceivedTime);
			syncState.lastInteractionTime.put(InteractionType.RECEIVED, lastReceived);
			syncState.lastForwardCheckTime = fromAbsoluteTime(metadata.lastForwardCheckTime);
			syncState.lastLostCheckTime = fromAbsoluteTime(metadata.lastLostCheckTime);
		}

		return syncState;
	}

	public void reportInteraction(SessionId sessionId) {
		reportInteraction(sessionId, InteractionType.INTERACTION);
	}

	private void reportInteraction(SessionId sessionId, InteractionType source) {
		if (sessionId.getType() == SessionType.COMM_SESSION) {
			reportDirectInteraction(sessionId.getId(), source);
		} else if (sessionId.getType() == SessionType.SHARED_SESSION) {
			reportSharedSessionInteraction(sessionId.getId(), source);
		}
	}

	private void reportSharedSessionInteraction(byte[] sessionId, InteractionType source) {
		if (!sharedSessionService.isActiveSession(sessionId)) {
			return;
		}
		List<byte[]> recentPeers = sharedSessionService.getRecentPeers(sessionId);
		if (!recentPeers.isEmpty()) {
			for (byte[] recentPeer : recentPeers) {
				reportDirectInteraction(recentPeer, source);
			}
		} else {
			SharedSessionService.SessionMetadata metadata = sharedSessionService.getSessionMetadata(sessionId);
			reportDirectInteraction(metadata.adminId, source);
		}
	}

	private void reportDirectInteraction(byte[] id, InteractionType source) {
		DirectSyncState syncState = id2directSyncState.get(ByteBuffer.wrap(id));
		if (syncState == null) {
			return;
		}
		long now = provider.getTime();
		long prevInteraction = syncState.lastInteractionTime.get(source);
		syncState.lastInteractionTime.put(source, now);
		if (now - prevInteraction < MIN_INTERACTION_TIME_TO_WAKE) {
			return;
		}
		updateNextCheckTime(syncState);
		scheduleNextWakeup();
	}

	public void flushSharedSessionQueue() {
		SyncFuture future;
		synchronized (directSyncStatesByNextCheckTime) {
			long now = provider.getTime();
			nextSharedSessionsCheckTime = now;
			future = wakeMessageLoopThread();
		}
		waitFor(future);
	}

	public void waitForMessageLoop() {
		SyncFuture future = wakeMessageLoopThread();
		waitFor(future);
	}

	public List<CommSessionService.ReceivedMessage> immediateCommSessionRead(byte[] id) {
		return immediateCommSessionRead(Collections.singletonList(id));
	}

	public List<CommSessionService.ReceivedMessage> immediateCommSessionRead(List<byte[]> ids) {
		SyncFuture future = requestImmediateCommSessionRead(ids);
		waitFor(future);
		List<CommSessionService.ReceivedMessage> messages = new ArrayList<>();
		for (byte[] id : ids) {
			messages.addAll(commSessionService.fetchNewMessages(id, true));
		}
		return messages;
	}

	public List<TypedPayload> immediateSharedObjectRead(byte[] id) {
		SyncFuture future = requestImmediateSharedObjectRead(id, null);
		waitFor(future);
		List<TypedPayload> updates = sharedObjectReadService.getUnconsumedUpdates(id, true);
		return updates;
	}

	private SyncFuture requestImmediateCommSessionRead(List<byte[]> ids) {
		synchronized (id2directSyncState) {
			long now = provider.getTime();
			for (byte[] id : ids) {
				if (Arrays.equals(id, ownId)) {
					continue;
				}
				ByteBuffer idWrapped = ByteBuffer.wrap(id);
				DirectSyncState syncState = id2directSyncState.computeIfAbsent(idWrapped, idWrappedParam -> newSyncState(id, idWrappedParam, CHECK_PERIOD_NEVER, CHECK_PERIOD_NEVER));
				setNextCheckTime(syncState, now);
			}

			SyncFuture future = wakeMessageLoopThread();
			return future;
		}
	}

	private SyncFuture requestImmediateSharedObjectRead(byte[] objectId, byte[] fromId) {
		if (fromId == null) {
			SharedObjectReadService.ObjectMetadata objectMetadata = sharedObjectReadService.getObjectMetadata(objectId);
			if (objectMetadata == null) {
				return getCompletedFuture();
			}
			fromId = objectMetadata.fromId;
		}
		boolean identityMissingOrStale = isIdentityMissingOrStale(fromId);
		if (identityMissingOrStale) {
			log.info(tracePrefix + " Requested shared object sync from stale identity " + Utils.getDebugStringFromId(fromId) + ", skipping");
			return getCompletedFuture();
		}
		requestImmediateCommSessionRead(Collections.singletonList(fromId));
		synchronized (sharedObjectReadStatesByNextCheckTime) {
			long now = provider.getTime();
			ByteBuffer idWrapped = ByteBuffer.wrap(objectId);
			SharedObjectReadState sharedObjectReadState = findObject(sharedObjectReadStatesByNextCheckTime, idWrapped);
			if (sharedObjectReadState == null) {
				sharedObjectReadState = new SharedObjectReadState(idWrapped, CHECK_PERIOD_NEVER);
			}
			setNextCheckTime(sharedObjectReadState, now);
			SyncFuture future = wakeMessageLoopThread();
			return future;
		}
	}

	private SyncFuture getCompletedFuture() {
		SyncFuture future = new SyncFuture();
		future.setDone();
		return future;
	}

	private void waitFor(SyncFuture future) {
		// log.info(tracePrefix + " Future wait requested " + future);
		synchronized (future) {
			// log.info(tracePrefix + " Future wait entered " + future);
			if (future.isDone()) {
				return;
			}
			Threads.waitChecked(future);
		}
		// log.info(tracePrefix + " Future wait completed " + future);
	}

	public void checkForNewHostedProfiles(boolean force) {
		long now = System.currentTimeMillis();
		if (!force && now - lastHostedProfileCheckTime < HOSTED_PROFILE_CHECK_INTERVAL) {
			return;
		}
		lastHostedProfileCheckTime = now;
		List<byte[]> newIds = remoteAdminHostService.checkForNewProfiles();
		for (byte[] id : newIds) {
			commSessionService.establishSessionWith(id);
		}
	}
}
