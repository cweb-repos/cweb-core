package org.cweb.communication;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.payload.GenericPayloadTypePredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.session.CommSessionSeed;
import org.cweb.schemas.comm.session.CommSessionSeedCryptoEnvelope;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.List;

public class CommSessionPushService {
    private static final Logger log = LoggerFactory.getLogger(CommSessionPushService.class);
    private static final String SERVICE_NAME = "CommSessionPushService";
    private final String tracePrefix;

    private final CryptoHelper cryptoHelper;
    private final RemoteIdentityFetcher remoteIdentityFetcher;
    private final SharedSessionService sharedSessionService;

    private CommSessionPushCallback callback;

    interface CommSessionPushCallback {
        void processSessionSeed(byte[] fromId, CommSessionSeed commSessionSeed);
    }

    public CommSessionPushService(String tracePrefix, CryptoHelper cryptoHelper, RemoteIdentityFetcher remoteIdentityFetcher, SharedSessionService sharedSessionService) {
        this.tracePrefix = tracePrefix;
        this.cryptoHelper = cryptoHelper;
        this.remoteIdentityFetcher = remoteIdentityFetcher;
        this.sharedSessionService = sharedSessionService;

        PayloadTypePredicate predicate = new GenericPayloadTypePredicate(PayloadType.COMM_SESSION_SEED_CRYPTO_ENVELOPE, SERVICE_NAME, null, null);
        sharedSessionService.addMessageProcessor(predicate, this::processReceivedEnvelope);
    }

    public void setCallback(CommSessionPushCallback callback) {
        this.callback = callback;
    }

    private MessageProcessor.Result processReceivedEnvelope(SessionId sessionId, TypedPayload typedPayload) {
        Pair<CommSessionSeedCryptoEnvelope, String> envelopeUnwrapped = TypedPayloadUtils.unwrap(typedPayload, CommSessionSeedCryptoEnvelope.class, SERVICE_NAME);
        if (envelopeUnwrapped.getRight() != null) {
            log.trace(tracePrefix + " Failed to extract seed envelope: " + envelopeUnwrapped.getRight());
            return MessageProcessor.Result.INVALID;
        }
        CommSessionSeedCryptoEnvelope sessionSeedCryptoEnvelope = envelopeUnwrapped.getLeft();

        Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(sessionSeedCryptoEnvelope.getCryptoEnvelope(), CryptoEnvelopeDecodingParams.create().setFetchSignerIfNeeded(true).setIdForSignatureTargetVerification(cryptoHelper.getOwnId()), CryptoContext.create().setCryptoHelper(cryptoHelper).setRemoteIdentityFetcher(remoteIdentityFetcher));
        if (decodedTypedPayloadWrapper.getError() != null) {
            if (decodedTypedPayloadWrapper.getError() != Decoded.Error.PK_KEY_DECRYPTION) {
                log.trace(tracePrefix + " Failed to decode seed envelope received on session " + Utils.getDebugStringFromId(sessionId.getId()) + " : " + decodedTypedPayloadWrapper.getError());
            }
            return MessageProcessor.Result.SUCCESS;
        }
        DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
        SignatureMetadata signatureMetadata = decodedTypedPayload.getSignatureMetadata();
        if (signatureMetadata == null) {
            log.debug(tracePrefix + " Missing signature in seed envelope received on session " + Utils.getDebugStringFromId(sessionId.getId()));
            return MessageProcessor.Result.SUCCESS;
        }

        byte[] fromId = signatureMetadata.getSignerId();
        log.trace(tracePrefix + " Received session seed on session " + Utils.getDebugStringFromId(sessionId.getId()) + " from " + Utils.getDebugStringFromId(fromId));

        Pair<CommSessionSeed, String> seedUnwrapped = TypedPayloadUtils.unwrap(decodedTypedPayload.getPayload(), CommSessionSeed.class, SERVICE_NAME);
        if (seedUnwrapped.getRight() != null) {
            log.info(tracePrefix + " Failed to extract seed from " + Utils.getDebugStringFromId(fromId) + " : " + seedUnwrapped.getRight());
            return MessageProcessor.Result.SUCCESS;
        }
        CommSessionSeed sessionSeed = seedUnwrapped.getLeft();

        callback.processSessionSeed(fromId, sessionSeed);

        return MessageProcessor.Result.SUCCESS;
    }

    public boolean tryPushCommSessionSeed(IdentityDescriptor destIdentity, CommSessionSeed commSessionSeed, long validUntil) {
        byte[] destId = destIdentity.getId();
        byte[] mostRecentCommonSessionId = getMostRecentCommonSharedSession(destId);
        if (mostRecentCommonSessionId == null) {
            return false;
        }

        TypedPayload sessionSeedPayload = TypedPayloadUtils.wrap(commSessionSeed, SERVICE_NAME, null, null);
        CryptoEnvelope cryptoEnvelope = cryptoHelper.signAndEncryptFor(sessionSeedPayload, destIdentity.getId(), destIdentity.getEcPublicKey(), validUntil);
        CommSessionSeedCryptoEnvelope sessionSeedCryptoEnvelope = new CommSessionSeedCryptoEnvelope(ByteBuffer.wrap(ThriftUtils.serialize(cryptoEnvelope)));
        TypedPayload cryptoEnvelopePayload = TypedPayloadUtils.wrap(sessionSeedCryptoEnvelope, SERVICE_NAME, null, null);
        log.trace(tracePrefix + " Sending session seed on session " + Utils.getDebugStringFromId(mostRecentCommonSessionId) + " to " + Utils.getDebugStringFromId(destId));
        return sharedSessionService.sendMessage(mostRecentCommonSessionId, cryptoEnvelopePayload);
    }

    private byte[] getMostRecentCommonSharedSession(byte[] destId) {
        List<byte[]> sessionIds = sharedSessionService.getActiveSessionIds();
        byte[] mostRecentCommonSessionId = null;
        long mostRecentCommonSessionTime = 0;
        for (byte[] sessionId : sessionIds) {
            SharedSessionService.SessionMetadata sessionMetadata = sharedSessionService.getSessionMetadata(sessionId);
            if (mostRecentCommonSessionTime > 0 && sessionMetadata.lastReceivedTime <= mostRecentCommonSessionTime) {
                continue;
            }
            if (!Utils.contains(sessionMetadata.participantIds, destId)) {
                continue;
            }
            mostRecentCommonSessionId = sessionId;
            mostRecentCommonSessionTime = sessionMetadata.lastReceivedTime;
        }
        return mostRecentCommonSessionId;
    }
}
