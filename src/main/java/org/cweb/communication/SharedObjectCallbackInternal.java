package org.cweb.communication;

interface SharedObjectCallbackInternal {
	void requestImmediateFetch(byte[] objectId, byte[] fromId);
}
