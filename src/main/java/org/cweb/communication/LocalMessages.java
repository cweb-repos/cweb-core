package org.cweb.communication;

import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.schemas.comm.session.LocalCommSessionMessage;
import org.cweb.storage.local.LocalDataWithDir;
import org.cweb.storage.local.LocalStorageInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class LocalMessages extends LocalDataWithDir<LocalCommSessionMessage> {
	LocalMessages(String tracePrefix, LocalStorageInterface localStorageInterface, String dirName, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(dirName, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	private byte[] serialToBytes(int serial) {
		byte[] hash = BinaryUtils.intToBytes(0xabcdef ^ (serial * 100000 + serial * serial + serial));
		return BinaryUtils.concat(BinaryUtils.intToBytes(serial), hash);
	}

	private int serialFromBytes(byte[] serialBinary) {
		return BinaryUtils.bytesToInt(Arrays.copyOfRange(serialBinary, 0, 4));
	}

	public void put(byte[] counterpartId, LocalCommSessionMessage message) {
		super.put(counterpartId, serialToBytes(message.getMessage().getSerial()), message);
	}

	public LocalCommSessionMessage get(byte[] counterpartId, int serial) {
		LocalCommSessionMessage result = super.get(counterpartId, serialToBytes(serial), LocalCommSessionMessage.class);
		// TODO: remove after migration
		if (result == null) {
			result = super.get(counterpartId, BinaryUtils.intToBytes(serial), LocalCommSessionMessage.class);
		}
		return result;
	}

	public boolean delete(byte[] counterpartId, LocalCommSessionMessage message) {
		boolean success = super.delete(counterpartId, serialToBytes(message.getMessage().getSerial()));
		// TODO: remove after migration
		if (!success) {
			success = super.delete(counterpartId, BinaryUtils.intToBytes(message.getMessage().getSerial()));
		}
		return success;
	}

	public List<Integer> listSerials(byte[] counterpartId) {
		List<byte[]> serialsBinary = super.list(counterpartId);
		List<Integer> serials = new ArrayList<>(serialsBinary.size());
	    for (byte[] serialBinary : serialsBinary) {
		    serials.add(serialFromBytes(serialBinary));
	    }
	    return serials;
	}
}
