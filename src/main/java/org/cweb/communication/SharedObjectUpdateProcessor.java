package org.cweb.communication;

import org.cweb.schemas.wire.TypedPayload;

public interface SharedObjectUpdateProcessor {
	Result processUpdate(byte[] objectId, TypedPayload typedPayload);
	void processUnsubscribe(byte[] objectId);

	enum Result {
		NOT_MATCHING,
		PROCESSED,
	}
}
