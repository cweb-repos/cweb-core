package org.cweb.communication;

import org.cweb.Config;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.crypto.lib.SequenceGenerator;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.object.LocalSharedObjectStateOwn;
import org.cweb.schemas.comm.object.SharedObject;
import org.cweb.schemas.comm.object.SharedObjectDeliveryType;
import org.cweb.schemas.comm.object.SharedObjectReference;
import org.cweb.schemas.comm.object.SharedObjectSubscriberInfo;
import org.cweb.schemas.comm.object.SharedObjectSyncMessage;
import org.cweb.schemas.comm.object.SharedObjectUnsubscribedMessage;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.utils.Threads;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedObjectPostService {
	private static final Logger log = LoggerFactory.getLogger(SharedObjectPostService.class);
	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;
	private final CommSessionService commSessionService;

	private final RemoteFileHandler remoteFileHandler;

	private final SharedObjectsOwn localObjects;

	public SharedObjectPostService(String tracePrefix, CryptoHelper cryptoHelper, CommSessionService commSessionService, RemoteFileHandler remoteFileHandler, LocalStorageInterface localStorageInterface) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.commSessionService = commSessionService;
		this.remoteFileHandler = remoteFileHandler;

		this.localObjects = new SharedObjectsOwn(tracePrefix, localStorageInterface, 10, 10);

		// logObjectsDbg();

		Threads.submitBackgroundTaskPeriodically(this::republishObjects, 0, Config.SHARED_OBJECT_REPUBLISH_CHECK_PERIOD);
	}

	private void logObjectsDbg() {
		for (byte[] objectId : localObjects.list()) {
			LocalSharedObjectStateOwn localState = localObjects.get(objectId);
			log.info(tracePrefix + " DUMP_OWN_OBJECT: " + toDebugString(localState));
		}
	}

	private synchronized void republishObjects() {
		long now = System.currentTimeMillis();
		for (byte[] objectId : localObjects.list()) {
			LocalSharedObjectStateOwn localState = localObjects.get(objectId);
			if (localState == null) {
				log.info(tracePrefix + " Failed to fetch local state for : " + Utils.getDebugStringFromId(objectId));
				continue;
			}
			long republishAfter = localState.getCurrentObjectPublishedAt() + localState.getObjectTtl() / 10 * 9;
			if (now > republishAfter) {
				log.trace(tracePrefix + " Republishing object " + toDebugString(localState));
				publishNextVersionOfObject(localState);
				localObjects.put(localState);
			}
		}
	}

	private String toDebugString(LocalSharedObjectStateOwn localState) {
		return Utils.getDebugStringFromId(localState.getObject().getObjectId()) + ":" + Utils.getDebugStringFromBytes(HashingUtils.SHA256(localState.getCurrentKey()), 4) + ":v" + localState.getNextVersionForCurrentKey() + " currentKeyCreatedAt " + Utils.formatDateTime(localState.getCurrentKeyCreatedAt()) + " currentObjectPublishedAt " + Utils.formatDateTime(localState.getCurrentObjectPublishedAt());
	}

	private static byte[] generateKey() {
		return CryptoHelper.generateAEADKey();
	}

	/**
	 * Does not actually publishes the object - have to add subscribers in a following step
	 */
	public byte[] create(SharedObjectDeliveryType type, long pollInterval, long objectTtl, long keyTtl, TypedPayload payload) {
		long now = System.currentTimeMillis();
		byte[] objectIdSeed = cryptoHelper.generateRandomBytes(SharedObjectCommon.OBJECT_ID_SIZE);
		byte[] objectId = SharedObjectCommon.generateObjectId(cryptoHelper.getOwnId(), objectIdSeed);
		SharedObject object = new SharedObject(ByteBuffer.wrap(cryptoHelper.getOwnId()), ByteBuffer.wrap(objectId), type, pollInterval, now, payload);
		byte[] key = generateKey();
		LocalSharedObjectStateOwn localState = new LocalSharedObjectStateOwn(ByteBuffer.wrap(objectIdSeed), objectTtl, keyTtl, now, ByteBuffer.wrap(key), 0, 0, object, new ArrayList<>());
		localObjects.put(localState);
		return objectId;
	}

	private void writeObject(LocalSharedObjectStateOwn localState, int version) {
		SharedObject object = localState.getObject();
		byte[] key = localState.getCurrentKey();
		TypedPayload objectPayload = TypedPayloadUtils.wrap(object, SharedObjectCommon.SERVICE_NAME, null, null);
		CryptoEnvelope objectEnvelope = cryptoHelper.signAndEncryptSymmetric(objectPayload, null, localState.getObjectTtl(), key, SharedObjectCommon.ASSOCIATED_DATA);
		long now = System.currentTimeMillis();
		OutboundDataWrapperRaw wrapper = new OutboundDataWrapperRaw(ThriftUtils.serialize(objectEnvelope), null, now + localState.getObjectTtl());
		byte[] fileName = SequenceGenerator.encode(object.getObjectId(), key, version, 32);
		remoteFileHandler.write(fileName, wrapper);
		localState.setCurrentObjectPublishedAt(now);
		log.trace(tracePrefix + " Published object " + Utils.getDebugStringFromBytes(object.getObjectId()) + Utils.getDebugStringFromBytes(HashingUtils.SHA256(key), 4) + ":v" + version);
	}

	private void publishNextVersionOfObject(LocalSharedObjectStateOwn localState) {
		int version = localState.getNextVersionForCurrentKey();
		writeObject(localState, version);
		localState.setNextVersionForCurrentKey(version + 1);
	}

	private SharedObjectSubscriberInfo findSubscriber(List<SharedObjectSubscriberInfo> subscribers, byte[] id) {
		for (SharedObjectSubscriberInfo subscriber : subscribers) {
			if (Arrays.equals(subscriber.getId(), id)) {
				return subscriber;
			}
		}
		return null;
	}

	public TypedPayload getCurrent(byte[] objectId) {
		LocalSharedObjectStateOwn localState = localObjects.get(objectId);
		if (localState == null) {
			return null;
		}
		return localState.getObject().getPayload();
	}

	public boolean deleteObject(byte[] objectId) {
		// TODO: implement
		return false;
	}

	public enum SubscriberUpdateType {
		ADD,
		REMOVE,
		SET
	}

	public synchronized boolean updatePayload(byte[] objectId, TypedPayload payload) {
		LocalSharedObjectStateOwn localState = localObjects.get(objectId);
		if (localState == null) {
			log.debug(tracePrefix + " Object not found updating payload on " + Utils.getDebugStringFromId(objectId));
			return false;
		}

		SharedObject object = localState.getObject();
		object.setPayload(payload);

		rotateKeyAndPublish(localState, false, true);

		localObjects.put(localState);

		return true;
	}

	public synchronized boolean updateSubscribers(byte[] objectId, SubscriberUpdateType type, List<byte[]> subscribers, TypedPayload newPayload) {
		LocalSharedObjectStateOwn localState = localObjects.get(objectId);
		if (localState == null) {
			log.debug(tracePrefix + " Object not found updating subscribers on " + Utils.getDebugStringFromId(objectId));
			return false;
		}
		SharedObject object = localState.getObject();

		boolean updatedSubscribers = false;
		List<SharedObjectSubscriberInfo> currentSubscribers = localState.getSubscribers();
		List<SharedObjectSubscriberInfo> newSubscribers;
		List<SharedObjectSubscriberInfo> unsubscribed = new ArrayList<>();
		if (type == SubscriberUpdateType.SET) {
			newSubscribers = new ArrayList<>();
			for (byte[] subscriberId : subscribers) {
				if (!cryptoHelper.isOwnId(subscriberId)) {
					newSubscribers.add(new SharedObjectSubscriberInfo(ByteBuffer.wrap(subscriberId)));
				}
			}
			updatedSubscribers = !newSubscribers.containsAll(currentSubscribers) || !currentSubscribers.containsAll(newSubscribers);
			if (updatedSubscribers) {
				for (SharedObjectSubscriberInfo subscriber : currentSubscribers) {
					if (!Utils.contains(subscribers, subscriber.getId())) {
						unsubscribed.add(subscriber);
					}
				}
			}
		} else {
			newSubscribers = new ArrayList<>(currentSubscribers);
			for (byte[] subscriberId : subscribers) {
				if (cryptoHelper.isOwnId(subscriberId)) {
					continue;
				}
				if (type == SubscriberUpdateType.ADD) {
					if (findSubscriber(newSubscribers, subscriberId) != null) {
						continue;
					}
					updatedSubscribers = true;
					newSubscribers.add(new SharedObjectSubscriberInfo(ByteBuffer.wrap(subscriberId)));
				} else {
					SharedObjectSubscriberInfo subscriberInfo = findSubscriber(newSubscribers, subscriberId);
					if (subscriberInfo != null) {
						updatedSubscribers = true;
						newSubscribers.remove(subscriberInfo);
						unsubscribed.add(subscriberInfo);
					}
				}
			}
		}

		boolean payloadChanged = false;
		if (newPayload != null) {
			object.setPayload(newPayload);
			payloadChanged = true;
		}

		if (!updatedSubscribers && !payloadChanged) {
			return true;
		}

		localState.setSubscribers(newSubscribers);

		for (SharedObjectSubscriberInfo subscriber : unsubscribed) {
			sendSyncMessageUnsubscribed(localState, subscriber.getId());
		}

		rotateKeyAndPublish(localState, updatedSubscribers, payloadChanged || type == SubscriberUpdateType.ADD || type == SubscriberUpdateType.SET);

		localObjects.put(localState);

		return true;
	}

	private static boolean shouldRotateKey(LocalSharedObjectStateOwn localState, long now) {
		return now - localState.getCurrentKeyCreatedAt() >= localState.getKeyTtl();
	}

	private void rotateKeyAndPublish(LocalSharedObjectStateOwn localState, boolean rotateKey, boolean publishPayload) {
		long now = System.currentTimeMillis();
		SharedObjectReference reference = null;
		if (rotateKey || shouldRotateKey(localState, now)) {
			reference = updateKey(localState, now);
		}

		if (publishPayload) {
			publishNextVersionOfObject(localState);
		}
		if (reference != null) {
			sendSyncMessageToAllSubscribers(localState, reference);
		}
	}

	private SharedObjectReference updateKey(LocalSharedObjectStateOwn localState, long now) {
		int lastVersionOfPreviousKey = localState.getNextVersionForCurrentKey() - 1;
		byte[] newKey = generateKey();
		localState.setCurrentKey(newKey);
		localState.setCurrentKeyCreatedAt(now);
		localState.setNextVersionForCurrentKey(0);
		return buildSharedObjectReference(localState, lastVersionOfPreviousKey);
	}

	private SharedObjectReference buildSharedObjectReference(LocalSharedObjectStateOwn localState, int lastVersionOfPreviousKey) {
		SharedObject object = localState.getObject();
		SharedObjectReference reference = new SharedObjectReference(ByteBuffer.wrap(object.getFromId()), ByteBuffer.wrap(localState.getObjectIdSeed()), ByteBuffer.wrap(object.getObjectId()), ByteBuffer.wrap(localState.getCurrentKey()));
		if (object.getType() == SharedObjectDeliveryType.DELIVER_ALL) {
			reference.setLastVersionOfPreviousKey(lastVersionOfPreviousKey);
		}
		return reference;
	}

	private void sendSyncMessageUnsubscribed(LocalSharedObjectStateOwn localState, byte[] toId) {
		if (!commSessionService.haveSessionWith(toId)) {
			return;
		}
		SharedObjectUnsubscribedMessage unsubscribedMessage = new SharedObjectUnsubscribedMessage(ByteBuffer.wrap(localState.getObject().getObjectId()));
		SharedObjectSyncMessage syncMessage = new SharedObjectSyncMessage();
		syncMessage.setUnsubscribedMessage(unsubscribedMessage);
		sendSyncMessage(localState, syncMessage, toId, "unsubscribedMessage");
	}

	private void sendSyncMessageToAllSubscribers(LocalSharedObjectStateOwn localState, SharedObjectReference reference) {
		SharedObjectSyncMessage syncMessage = new SharedObjectSyncMessage();
		syncMessage.setReference(reference);
		for (SharedObjectSubscriberInfo subscriber : localState.getSubscribers()) {
			byte[] toId = subscriber.getId();
			if (cryptoHelper.isOwnId(toId)) {
				continue;
			}
			if (!commSessionService.haveSessionWith(toId)) {
				commSessionService.establishSessionWith(toId);
			}

			sendSyncMessage(localState, syncMessage, toId, "syncMessage");
		}
	}

	private void sendSyncMessage(LocalSharedObjectStateOwn localState, SharedObjectSyncMessage syncMessage, byte[] toId, String messageTypeStr) {
		TypedPayload syncMessageWrapped = TypedPayloadUtils.wrap(syncMessage, SharedObjectCommon.SERVICE_NAME, null, null);
		boolean success = commSessionService.sendMessage(toId, syncMessageWrapped);
		log.trace(tracePrefix + (success ? " Sent" : " Failed to send") + " " + messageTypeStr + " to " + Utils.getDebugStringFromId(toId) + " for " + toDebugString(localState));
	}
}
