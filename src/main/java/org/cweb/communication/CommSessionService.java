package org.cweb.communication;

import com.google.gson.Gson;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.Config;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.CryptoThriftUtils;
import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.crypto.lib.DoubleRatchet;
import org.cweb.crypto.lib.DoubleRatchetMessageKeyExtension;
import org.cweb.crypto.lib.ECKeyPair;
import org.cweb.identity.IdentityService;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.payload.PayloadTypeLocalMetadataPredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.comm.session.CommSessionMessage;
import org.cweb.schemas.comm.session.CommSessionSeed;
import org.cweb.schemas.comm.session.CommSessionSeedLocalMetadata;
import org.cweb.schemas.comm.session.LocalCommSessionMessage;
import org.cweb.schemas.comm.session.LocalCommSessionOutstandingMessage;
import org.cweb.schemas.comm.session.LocalCommSessionState;
import org.cweb.schemas.crypto.DoubleRatchetMessage;
import org.cweb.schemas.crypto.X3DHInitialMessage;
import org.cweb.schemas.crypto.X3DHPreKeyBundle;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.storage.LocalMetadataEnvelope;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.schemas.wire.TypedPayloadMetadata;
import org.cweb.storage.local.LocalPreKeyService;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.local.SecretStorageService;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.Threads;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommSessionService {
	private static final Logger log = LoggerFactory.getLogger(CommSessionService.class);
	private static final String SERVICE_NAME = "CommSession2";
	private static final String ACK_MESSAGE_TYPE = "AckMessage";
	private static final long SESSION_SEED_TTL = 1000L * 60 * 60 * 24 * 14;
	private static final long MESSAGE_TTL = 1000L * 60 * 60 * 24 * 14;
	private static final long LOCAL_MESSAGE_TTL = 1000L * 60 * 60 * 24 * 7;
	private static final String REMOTE_MESSAGE_NAME_SUFFIX = "-sessionFrame";
	private static final int SERIALS_TO_CHECK_AND_DELETE_SEED = 4;
	private static final int SERIALS_TO_CHECK_SESSION_CONFLICT = 2;
	private static final byte[] INITIAL_DR_MESSAGE = {10, 20, 21};
	private static final int MAX_OUTSTANDING_MESSAGES = 200;
	private static final int MAX_UNCONSUMED_ACKED_SERIALS = MAX_OUTSTANDING_MESSAGES;
	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;
	private final PrivateBroadcastService privateBroadcastService;
	private final RemoteIdentityFetcher remoteIdentityFetcher;
	private final RemoteFileHandler remoteMessageHandler;
	private CommSessionPushService commSessionPushService;

	private final LocalCommSessions localSessions;
	private final LocalPreKeyService localPreKeyService;
	private final LocalMessages localMessagesReceived;
	private final LocalMessages localMessagesSent;

	private final List<Pair<PayloadTypePredicate, MessageProcessor>> messageProcessors = new ArrayList<>();
	private final List<CommSessionEstablishmentCallback> sessionEstablishmentCallbacks = new ArrayList<>();
	private final Map<ByteBuffer, ByteBuffer> sessionLocks = new ConcurrentHashMap<>();

	public static class ReceivedMessage {
		public final byte[] fromId;
		public final long createdAt;
		public final TypedPayload payload;

		public ReceivedMessage(byte[] fromId, long createdAt, TypedPayload payload) {
			this.fromId = fromId;
			this.createdAt = createdAt;
			this.payload = payload;
		}
	}

	public static class SessionMetadata {
		public final long lastReceivedTime;
		public final long lastForwardCheckTime;
		public final long lastLostCheckTime;
		public final int outstandingMessages;

		SessionMetadata(long lastReceivedTime, long lastForwardCheckTime, long lastLostCheckTime, int outstandingMessages) {
			this.lastReceivedTime = lastReceivedTime;
			this.lastForwardCheckTime = lastForwardCheckTime;
			this.lastLostCheckTime = lastLostCheckTime;
			this.outstandingMessages = outstandingMessages;
		}
	}

	public CommSessionService(String tracePrefix, CryptoHelper cryptoHelper, PrivateBroadcastService privateBroadcastService, RemoteWriteService remoteWriteService, RemoteReadService remoteReadService, RemoteIdentityFetcher remoteIdentityFetcher, LocalStorageInterface localStorageInterface, SecretStorageService secretStorageService) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.privateBroadcastService = privateBroadcastService;
		this.remoteIdentityFetcher = remoteIdentityFetcher;
		this.remoteMessageHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, REMOTE_MESSAGE_NAME_SUFFIX);
		this.localSessions = new LocalCommSessions(tracePrefix, localStorageInterface, 5000, 1440);
		this.localPreKeyService = new LocalPreKeyService(secretStorageService);
		this.localMessagesReceived = new LocalMessages(tracePrefix, localStorageInterface, "commSessionMsgReceived", 30, 5);
		this.localMessagesSent = new LocalMessages(tracePrefix, localStorageInterface, "commSessionMsgSent", 30, 5);

		addMessageProcessor(new PayloadTypePredicate() {
			@Override
			public boolean match(TypedPayloadMetadata payload) {
				return payload.getType() == PayloadType.CUSTOM && SERVICE_NAME.equals(payload.getServiceName()) && ACK_MESSAGE_TYPE.equals(payload.getCustomType());
			}
		}, new MessageProcessor() {
			@Override
			public Result process(SessionId sessionId, TypedPayload typedPayload) {
				return Result.SUCCESS;
			}
		});

		Threads.submitBackgroundTaskPeriodically(this::garbageCollectMessages, 0, Config.LOCAL_CACHE_GC_PERIOD);
	}

	public SessionMetadata getSessionMetadata(byte[] counterpartId) {
		LocalCommSessionState localCommSessionState = localSessions.get(counterpartId);
		if (localCommSessionState == null) {
			return null;
		}
		return new SessionMetadata(localCommSessionState.getLastReceivedAt(), localCommSessionState.getLastForwardCheckTime(), localCommSessionState.getLastLostCheckTime(), localCommSessionState.getOutstandingMessages().size());
	}

	public void setCommSessionPushService(CommSessionPushService commSessionPushService) {
		this.commSessionPushService = commSessionPushService;
		this.commSessionPushService.setCallback(this::onSessionSeedPushReceived);
	}

	private void onSessionSeedPushReceived(byte[] fromId, CommSessionSeed commSessionSeed) {
		LocalIdentityDescriptorState fromIdentityDescriptorState = remoteIdentityFetcher.fetch(fromId);
		if (fromIdentityDescriptorState == null) {
			log.debug(tracePrefix + " Failed to fetch identity descriptor on session seed push " + Utils.getDebugStringFromId(fromId));
			return;
		}
		ByteBuffer lock = getSessionLock(fromId);
		synchronized (lock) {
			LocalCommSessionState existingSessionState = localSessions.get(fromId);
			LocalCommSessionState updatedSessionState = null;
			if (existingSessionState != null) {
				LocalCommSessionState deconflictedSessionState = checkSessionConflict(fromIdentityDescriptorState.getDescriptor(), existingSessionState, commSessionSeed);
				if (deconflictedSessionState != existingSessionState) {
					updatedSessionState = deconflictedSessionState;
				}
			} else {
				updatedSessionState = createSessionFromSeed(fromIdentityDescriptorState.getDescriptor(), commSessionSeed);
			}
			if (updatedSessionState != null) {
				saveLocalSessionState(fromId, updatedSessionState);
				notifyNewSessionEstablished(fromId);
			}
		}
	}

	public void addMessageProcessor(PayloadTypePredicate predicate, MessageProcessor processor) {
		messageProcessors.add(Pair.of(predicate, processor));
	}

	public void addCommSessionEstablishmentCallback(CommSessionEstablishmentCallback callback) {
		sessionEstablishmentCallbacks.add(callback);
	}

	private void garbageCollectMessages() {
		long now = System.currentTimeMillis();
		final List<byte[]> counterpartIds = localSessions.list();
		for (byte[] counterpartId : counterpartIds) {
			{
				List<Integer> serialsReceived = localMessagesReceived.listSerials(counterpartId);
				for (int serial : serialsReceived) {
					LocalCommSessionMessage message = localMessagesReceived.get(counterpartId, serial);
					if (message.isSetConsumedAt() && now - message.getConsumedAt() > LOCAL_MESSAGE_TTL) {
						localMessagesReceived.delete(counterpartId, message);
					}
				}
			}
			{
				List<Integer> serialsSent = localMessagesSent.listSerials(counterpartId);
				for (int serial : serialsSent) {
					LocalCommSessionMessage message = localMessagesSent.get(counterpartId, serial);
					if (now - message.getCreatedAt() > LOCAL_MESSAGE_TTL) {
						localMessagesSent.delete(counterpartId, message);
					}
				}
			}
		}
		long elapsed = System.currentTimeMillis() - now;
		log.trace(tracePrefix + " CommSessionService.garbageCollectMessages took " + elapsed + " ms");

		// logReceivedMessagesDbg(Utils.decodeBase64Safe(""));
		// logSentMessagesDbg(Utils.decodeBase64Safe(""));
	}

	private void logReceivedMessagesDbg(byte[] fromId) {
		List<Integer> messagesSerials = localMessagesReceived.listSerials(fromId);
		List<LocalCommSessionMessage> messages = new ArrayList<>();
		for (int messageSerial : messagesSerials) {
			LocalCommSessionMessage message = localMessagesReceived.get(fromId, messageSerial);
			messages.add(message);
		}
		logDebugMessages("DUMP_MESSAGE_RECEIVED", fromId, messages);
	}

	private void logSentMessagesDbg(byte[] toId) {
		List<Integer> messagesSerials = localMessagesSent.listSerials(toId);
		List<LocalCommSessionMessage> messages = new ArrayList<>();
		for (int messageSerial : messagesSerials) {
			LocalCommSessionMessage message = localMessagesSent.get(toId, messageSerial);
			messages.add(message);
		}
		logDebugMessages("DUMP_MESSAGE_SENT", toId, messages);
	}

	private void logDebugMessages(String prefix, byte[] fromOrToId, List<LocalCommSessionMessage> messages) {
		Collections.sort(messages, ((o1, o2) -> Long.signum(o1.getMessage().getTimestamp() - o2.getMessage().getTimestamp())));
		Gson gson = new Gson();
		for (LocalCommSessionMessage message : messages) {
			log.info(tracePrefix + " " + prefix + ": " + toDebugString(fromOrToId, message) + " " + gson.toJson(TypedPayloadDecoder.decodePayload(message.getMessage().getPayload())));
		}
	}

	private String toDebugString(byte[] fromId, LocalCommSessionMessage localMessage) {
		CommSessionMessage message = localMessage.getMessage();
		return Utils.getDebugStringFromId(fromId) + ":" + message.getSerial() + ":" + Utils.formatDateTime(message.getTimestamp()) + ", createdAt=" + Utils.formatDateTime(localMessage.getCreatedAt());
	}

	private void saveLocalSessionState(byte[] id, LocalCommSessionState localCommSessionState) {
		localSessions.put(id, localCommSessionState);
		log.trace(tracePrefix + " Saved session state for " + Utils.getDebugStringFromId(id));
	}

	private DoubleRatchetMessage encrypt(DoubleRatchet.State drState, IdentityDescriptor destIdentity, byte[] dataToEncrypt) {
		byte[] associatedData = BinaryUtils.concat(cryptoHelper.getOwnECPublicKey().getPublicKey(), destIdentity.getEcPublicKey().getPublicKey());
		DoubleRatchet.Message message = DoubleRatchet.ratchetEncrypt(drState, dataToEncrypt, associatedData);
		return CryptoThriftUtils.toThrift(message);
	}

	private byte[] decrypt(DoubleRatchet.State drState, IdentityDescriptor identityDescriptor, DoubleRatchetMessage message) {
		byte[] associatedData = BinaryUtils.concat(identityDescriptor.getEcPublicKey().getPublicKey(), cryptoHelper.getOwnECPublicKey().getPublicKey());
		return DoubleRatchet.ratchetDecrypt(drState, CryptoThriftUtils.fromThrift(message), associatedData);
	}

	public boolean haveSessionWith(byte[] id) {
		return localSessions.get(id) != null;
	}

	private ByteBuffer getSessionLock(byte[] id) {
		ByteBuffer idWrapped = ByteBuffer.wrap(id);
		ByteBuffer lock = sessionLocks.get(idWrapped);
		if (lock == null) {
			lock = idWrapped;
			sessionLocks.put(idWrapped, lock);
		}
		return lock;
	}

	public boolean establishSessionWith(byte[] destId) {
		if (cryptoHelper.isOwnId(destId)) {
			return false;
		}
		LocalIdentityDescriptorState destIdentityState = remoteIdentityFetcher.fetch(destId);
		if (destIdentityState == null) {
			return false;
		}

		ByteBuffer lock = getSessionLock(destId);
		synchronized (lock) {
			LocalCommSessionState localCommSessionState = getOrCreateSession(destIdentityState.getDescriptor(), true, true);
			return localCommSessionState != null;
		}
	}

	public boolean sendMessage(byte[] destId, TypedPayload payload) {
		if (cryptoHelper.isOwnId(destId)) {
			return false;
		}
		LocalIdentityDescriptorState destIdentityState = remoteIdentityFetcher.fetch(destId);
		if (destIdentityState == null) {
			return false;
		}

		ByteBuffer lock = getSessionLock(destId);
		synchronized (lock) {
			LocalCommSessionState localCommSessionState = getOrCreateSession(destIdentityState.getDescriptor(), false, false);
			if (localCommSessionState == null) {
				return false;
			}
			int serial = localCommSessionState.getNextSerialOwn();
			if (localCommSessionState.getOutstandingMessages().size() >= MAX_OUTSTANDING_MESSAGES) {
				return false;
			}

			DoubleRatchet.State drState = CryptoThriftUtils.fromThrift(localCommSessionState.getDoubleRatchetState());

			long now = System.currentTimeMillis();
			byte[] nextMessageName = DoubleRatchetMessageKeyExtension.getMessageKeySelf(drState, 1);
			CommSessionMessage commSessionMessage = new CommSessionMessage(serial, now, localCommSessionState.getNextSerialRemote() - 1, payload);
			commSessionMessage.setRefId(Utils.generateRandomBytes(4));
			commSessionMessage.setNextMessageName(nextMessageName);

			LocalCommSessionMessage localMessage = new LocalCommSessionMessage(now, commSessionMessage);
			localMessagesSent.put(destId, localMessage);

			byte[] messageName = localCommSessionState.getNextMessageNameOwn() != null ? localCommSessionState.getNextMessageNameOwn() : DoubleRatchetMessageKeyExtension.getMessageKeySelf(drState, 0);
			// log.trace(tracePrefix + " DR message to " + Utils.getDebugStringFromId(destIdentityState.getId()) + "/" + localCommSessionState.getNextSerialOwn() + ": " + Utils.getDebugStringFromBytes(messageName));

			byte[] dataToEncrypt = ThriftUtils.serialize(commSessionMessage);
			DoubleRatchetMessage message = encrypt(drState, destIdentityState.getDescriptor(), dataToEncrypt);

			localCommSessionState.setDoubleRatchetState(CryptoThriftUtils.toThrift(drState));

			localCommSessionState.setNextSerialOwn(serial + 1);
			localCommSessionState.setNextRemoteSerialToAck(localCommSessionState.getNextSerialRemote());
			localCommSessionState.setNextMessageNameOwn(nextMessageName);

			updateOutstandingMessages(localCommSessionState, destId, serial, messageName);

			saveLocalSessionState(destId, localCommSessionState);
			OutboundDataWrapperRaw dataWrapper = new OutboundDataWrapperRaw(ThriftUtils.serialize(message), null, now + MESSAGE_TTL);
			boolean overwritten = remoteMessageHandler.write(messageName, dataWrapper);
			if (overwritten) {
				log.warn(tracePrefix + " Overwritten message " + serial + " to " + Utils.getDebugStringFromId(destId) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()));
			}
			log.trace(tracePrefix + " Sent message " + serial + " to " + Utils.getDebugStringFromId(destId) + ", outstandingMessages=" + localCommSessionState.getOutstandingMessages().size());
			return true;
		}
	}

	private void updateOutstandingMessages(LocalCommSessionState localCommSessionState, byte[] destId, int serial, byte[] messageName) {
		List<LocalCommSessionOutstandingMessage> outstandingMessages = new ArrayList<>(localCommSessionState.getOutstandingMessages());
		outstandingMessages.add(new LocalCommSessionOutstandingMessage(serial, ByteBuffer.wrap(messageName), 0));
		while(outstandingMessages.size() > MAX_OUTSTANDING_MESSAGES) {
			LocalCommSessionOutstandingMessage message = outstandingMessages.get(0);
			log.warn(tracePrefix + " Deleting outstanding message " + message.getSerial() + " to " + Utils.getDebugStringFromId(destId) + ", " + Utils.getDebugStringFromBytes(message.getName()));
			boolean success = remoteMessageHandler.delete(message.getName());
			if (!success) {
				log.warn(tracePrefix + " Failed to delete outstanding message " + message.getSerial() + " to " + Utils.getDebugStringFromId(destId) + ", " + Utils.getDebugStringFromBytes(message.getName()));
			}
			outstandingMessages.remove(0);
		}
		localCommSessionState.setOutstandingMessages(outstandingMessages);
	}

	private void deleteOutstandingMessages(LocalCommSessionState localCommSessionState, byte[] destId, int ackSerialStart, int ackSerialEnd, long now) {
		List<LocalCommSessionOutstandingMessage> newOutstandingMessages = new ArrayList<>(localCommSessionState.getOutstandingMessages());
		ArrayList<LocalCommSessionOutstandingMessage> toRemove = new ArrayList<>();

		for (LocalCommSessionOutstandingMessage message : localCommSessionState.getOutstandingMessages()) {
			boolean shouldDelete = false;
			if (message.getDeleteAt() == 0) {
				if (message.getSerial() >= ackSerialStart && message.getSerial() < ackSerialEnd) {
					shouldDelete = true;
				}
				if (message.getSerial() < ackSerialStart) {
					message.setDeleteAt(now + Config.COMM_SESSION_REMOTE_MESSAGE_DELETION_DELAY);
				}
			} else if (message.getDeleteAt() < now) {
				shouldDelete = true;
			}

			if (shouldDelete) {
				boolean success = remoteMessageHandler.delete(message.getName());
				if (!success) {
					log.info(tracePrefix + " Failed to delete message " + message.getSerial() + " to " + Utils.getDebugStringFromId(destId) + ", " + Utils.getDebugStringFromBytes(message.getName()) + ", deleteAt=" + Utils.formatDateTime(message.getDeleteAt()));
				}
				toRemove.add(message);
			}
		}

		newOutstandingMessages.removeAll(toRemove);
		localCommSessionState.setOutstandingMessages(newOutstandingMessages);
	}

	private CommSessionMessage readNewMessageFrom(IdentityDescriptor identityDescriptor, LocalCommSessionState localCommSessionState, int checkForwardOrLostMessages) {
		byte[] id = identityDescriptor.getId();
		DoubleRatchet.State drState = CryptoThriftUtils.fromThrift(localCommSessionState.getDoubleRatchetState());
		List<byte[]> messageNames = new ArrayList<>();
		if (localCommSessionState.getNextMessageNameRemote() != null) {
			messageNames.add(localCommSessionState.getNextMessageNameRemote());
		}
		if (checkForwardOrLostMessages > 0) {
			messageNames.addAll(Arrays.asList(DoubleRatchetMessageKeyExtension.getMessageKeysRemote(drState, checkForwardOrLostMessages)));
		} else if (checkForwardOrLostMessages < 0) {
			messageNames.addAll(Arrays.asList(DoubleRatchetMessageKeyExtension.getMessageKeysRemoteSkipped(drState)));
		} else if (messageNames.isEmpty()) {
			messageNames.addAll(Arrays.asList(DoubleRatchetMessageKeyExtension.getMessageKeysRemote(drState, 0)));
		}
		for (int messageNameIdx = 0; messageNameIdx < messageNames.size(); messageNameIdx++) {
			byte[] messageName = messageNames.get(messageNameIdx);
			RemoteFetchResultRaw fetchedEncryptedMessage = remoteMessageHandler.read(id, messageName);

			String messageFetchResultStr = getFetchResultStr(fetchedEncryptedMessage);

			log.trace(tracePrefix + " Fetching message " + (localCommSessionState.getNextSerialRemote() + (checkForwardOrLostMessages > 0 ? messageNameIdx : 0)) + " from " + Utils.getDebugStringFromId(id) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()) + ":" + Utils.getDebugStringFromBytesShort(messageName) + ": " + getFetchResultStr(fetchedEncryptedMessage));

			if (fetchedEncryptedMessage.getError() != null) {
				return null;
			}
			if (fetchedEncryptedMessage.getData() == null) {
				continue;
			}
			byte[] encryptedMessage = fetchedEncryptedMessage.getData();

			DoubleRatchetMessage message = ThriftUtils.deserializeSafe(encryptedMessage, DoubleRatchetMessage.class);
			if (message == null) {
				log.info("Failed to deserialize DR message from " + Utils.getDebugStringFromBytes(id) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()));
				continue;
			}

			byte[] messageData = decrypt(drState, identityDescriptor, message);

			localCommSessionState.setDoubleRatchetState(CryptoThriftUtils.toThrift(drState));

			if (messageData == null) {
				log.info("Failed to decrypt message from " + Utils.getDebugStringFromBytes(id) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()));
				continue;
			}

			CommSessionMessage commSessionMessage = ThriftUtils.deserializeSafe(messageData, CommSessionMessage.class);
			if (commSessionMessage == null) {
				log.info("Failed to deserialize message from " + Utils.getDebugStringFromBytes(id) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()));
				continue;
			}

			log.trace(tracePrefix + " Fetched message " + commSessionMessage.getSerial() + " from " + Utils.getDebugStringFromId(id) + ":" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()) + ": " + messageFetchResultStr);

			if (localCommSessionState.getNextToAckByRemote() <= SERIALS_TO_CHECK_AND_DELETE_SEED) {
				checkAndDeleteOutboundSessionSeed(identityDescriptor);
			}

			if (commSessionMessage.getSerial() >= localCommSessionState.getNextSerialRemote()) {
				localCommSessionState.setNextSerialRemote(commSessionMessage.getSerial() + 1);
				localCommSessionState.setNextMessageNameRemote(commSessionMessage.getNextMessageName());
			}

			int newNextToAckByRemote = commSessionMessage.getAckSerial() + 1;
			log.trace(tracePrefix + " Received message from " + Utils.getDebugStringFromId(id) + " newNextToAckByRemote=" + newNextToAckByRemote);
			if (newNextToAckByRemote < localCommSessionState.getNextToAckByRemote()) {
				log.warn(tracePrefix + " Ignoring decrementing ack from " + Utils.getDebugStringFromId(id) + " from " + localCommSessionState.getNextToAckByRemote() + " to " + newNextToAckByRemote);
			} else if (newNextToAckByRemote > localCommSessionState.getNextSerialOwn()) {
				log.warn(tracePrefix + " Ignoring overflowing ack from " + Utils.getDebugStringFromId(id) + " from " + localCommSessionState.getNextSerialRemote() + " to " + newNextToAckByRemote);
			} else if (newNextToAckByRemote > localCommSessionState.getNextToAckByRemote()) {
				localCommSessionState.setNextToAckByRemote(newNextToAckByRemote);
			}

			if (checkForwardOrLostMessages > 0) {
				log.warn(tracePrefix + " Skipped " + messageNameIdx + " messages from " + Utils.getDebugStringFromId(id));
			}

			return commSessionMessage;
		}
		return null;
	}

	private static String getFetchResultStr(RemoteFetchResultRaw fetchedEncryptedMessage) {
		String messageFetchResultStr;
		if (fetchedEncryptedMessage.getError() != null) {
			messageFetchResultStr = "error " + fetchedEncryptedMessage.getError().getMessage();
		} else if (fetchedEncryptedMessage.getData() == null) {
			messageFetchResultStr = "not found";
		} else {
			messageFetchResultStr = "fetched " + fetchedEncryptedMessage.getData().length + " bytes";
		}
		return messageFetchResultStr;
	}

	public List<ReceivedMessage> fetchAckedMessages(byte[] counterpartId, boolean consume) {
		List<ReceivedMessage> result = new ArrayList<>();
		if (cryptoHelper.isOwnId(counterpartId)) {
			return result;
		}
		ByteBuffer lock = getSessionLock(counterpartId);
		synchronized (lock) {
			LocalCommSessionState localCommSessionState = localSessions.get(counterpartId);
			if (localCommSessionState == null || localCommSessionState.getUnconsumedAckedMessageSerials().isEmpty()) {
				return result;
			}
			for (Integer serial : localCommSessionState.getUnconsumedAckedMessageSerials()) {
				LocalCommSessionMessage localMessage = localMessagesSent.get(counterpartId, serial);
				if (localMessage == null) {
					continue;
				}
				if (!localMessage.isSetAckedAt()) {
					log.debug(tracePrefix + " Message not acked: " + Utils.getDebugStringFromId(counterpartId) + ":" + serial);
					continue;
				}
				ReceivedMessage receivedMessage = new ReceivedMessage(counterpartId, localMessage.getMessage().getTimestamp(), localMessage.getMessage().getPayload());
				result.add(receivedMessage);
			}
			if (consume) {
				localCommSessionState.setUnconsumedAckedMessageSerials(new ArrayList<Integer>());
				saveLocalSessionState(counterpartId, localCommSessionState);
			}
			return result;
		}
	}

	public List<ReceivedMessage> fetchNewMessages(byte[] fromId, boolean consume) {
		List<ReceivedMessage> result = new ArrayList<>();
		if (cryptoHelper.isOwnId(fromId)) {
			return result;
		}
		ByteBuffer lock = getSessionLock(fromId);
		synchronized (lock) {
			LocalCommSessionState localCommSessionState = localSessions.get(fromId);
			if (localCommSessionState == null || localCommSessionState.getUnconsumedMessageSerials().isEmpty()) {
				return result;
			}
			long now = System.currentTimeMillis();
			for (Integer serial : localCommSessionState.getUnconsumedMessageSerials()) {
				LocalCommSessionMessage localMessage = localMessagesReceived.get(fromId, serial);
				if (localMessage == null) {
					continue;
				}
				if (localMessage.isSetConsumedAt()) {
					log.debug(tracePrefix + " Message already consumed: " + Utils.getDebugStringFromId(fromId) + ":" + serial);
					continue;
				}
				ReceivedMessage receivedMessage = new ReceivedMessage(fromId, localMessage.getMessage().getTimestamp(), localMessage.getMessage().getPayload());
				result.add(receivedMessage);
				if (consume) {
					localMessage.setConsumedAt(now);
					localMessagesReceived.put(fromId, localMessage);
				}
			}
			if (consume) {
				localCommSessionState.setUnconsumedMessageSerials(new ArrayList<>());
				saveLocalSessionState(fromId, localCommSessionState);
			}
			return result;
		}
	}

	static class ReceivedMessagesStats {
		final int receivedDirect;
		final int processed;

		ReceivedMessagesStats() {
			this.receivedDirect = 0;
			this.processed = 0;
		}

		ReceivedMessagesStats(int receivedDirect, int processed) {
			this.receivedDirect = receivedDirect;
			this.processed = processed;
		}
	}

	ReceivedMessagesStats readMessages(byte[] fromId, int checkForwardOrLostMessages) {
		if (cryptoHelper.isOwnId(fromId)) {
			return new ReceivedMessagesStats();
		}
		LocalIdentityDescriptorState identityDescriptorState = remoteIdentityFetcher.fetch(fromId);
		if (identityDescriptorState == null) {
			return new ReceivedMessagesStats();
		}

		ByteBuffer lock = getSessionLock(fromId);
		synchronized (lock) {
			LocalCommSessionState localCommSessionState = getOrCreateSession(identityDescriptorState.getDescriptor(), true, false);
			if (localCommSessionState == null) {
				return new ReceivedMessagesStats();
			}

			long now = System.currentTimeMillis();
			int currentCheckForwardOrLostMessages = checkForwardOrLostMessages;
			int prevNextToAckByRemote = localCommSessionState.getNextToAckByRemote();
			ArrayList<Integer> newUnconsumedMessageSerials = new ArrayList<>(localCommSessionState.getUnconsumedMessageSerials());
			boolean received = false;
			int receivedDirect = 0;
			int processedNonRedundant = 0;
			while (true) {
				CommSessionMessage message = readNewMessageFrom(identityDescriptorState.getDescriptor(), localCommSessionState, currentCheckForwardOrLostMessages);
				if (message == null) {
					break;
				}
				received = true;
				if (currentCheckForwardOrLostMessages > 0) {
					currentCheckForwardOrLostMessages = 0;
				}
				TypedPayload payload = message.getPayload();
				boolean consumed = false;
				for (Pair<PayloadTypePredicate, MessageProcessor> pair : messageProcessors) {
					if (pair.getLeft().match(payload.getMetadata())) {
						SessionId sessionId = new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(identityDescriptorState.getDescriptor().getId()));
						MessageProcessor.Result result = pair.getRight().process(sessionId, payload);
						if (result == MessageProcessor.Result.SUCCESS || result == MessageProcessor.Result.SUCCESS_REDUNDANT) {
							consumed = true;
							if (result != MessageProcessor.Result.SUCCESS_REDUNDANT) {
								processedNonRedundant++;
							}
							break;
						} else {
							log.debug(tracePrefix + " Invalid message passed to " + pair.getRight().getClass().getName() + ": " + payload.getMetadata().toString());
						}
					}
				}
				if (!consumed) {
					LocalCommSessionMessage localMessage = new LocalCommSessionMessage(now, message);
					localMessagesReceived.put(fromId, localMessage);
					newUnconsumedMessageSerials.add(message.getSerial());
					receivedDirect++;
				}
			}

			if (received) {
				localCommSessionState.setLastReceivedAt(now);
			}

			localCommSessionState.setUnconsumedMessageSerials(newUnconsumedMessageSerials);

			int newNextToAckByRemote = localCommSessionState.getNextToAckByRemote();
			if (newNextToAckByRemote > prevNextToAckByRemote) {
				recordAcks(fromId, localCommSessionState, now, prevNextToAckByRemote, newNextToAckByRemote);
				deleteOutstandingMessages(localCommSessionState, fromId, prevNextToAckByRemote, newNextToAckByRemote, now);
			}

			if (checkForwardOrLostMessages > 0) {
				localCommSessionState.setLastForwardCheckTime(now);
			}
			if (checkForwardOrLostMessages < 0) {
				localCommSessionState.setLastLostCheckTime(now);
			}

			saveLocalSessionState(fromId, localCommSessionState);

			if (localCommSessionState.getNextSerialRemote() - localCommSessionState.getNextRemoteSerialToAck() > MAX_OUTSTANDING_MESSAGES / 2) {
				sendAckMessage(fromId);
			}

			return new ReceivedMessagesStats(receivedDirect, processedNonRedundant);
		}
	}

	private void recordAcks(byte[] counterpartId, LocalCommSessionState localCommSessionState, long now, int fromSerial, int toSerial) {
		List<Integer> unconsumedAckedMessageSerials = new ArrayList<>(localCommSessionState.getUnconsumedAckedMessageSerials());
		for (int serial = fromSerial; serial < toSerial; serial++) {
			unconsumedAckedMessageSerials.add(serial);
			LocalCommSessionMessage sentMessage = localMessagesSent.get(counterpartId, serial);
			if (sentMessage == null) {
				continue;
			}
			sentMessage.setAckedAt(now);
			localMessagesSent.put(counterpartId, sentMessage);
		}
		if (unconsumedAckedMessageSerials.size() > MAX_UNCONSUMED_ACKED_SERIALS) {
			unconsumedAckedMessageSerials = unconsumedAckedMessageSerials.subList(unconsumedAckedMessageSerials.size() - MAX_UNCONSUMED_ACKED_SERIALS, unconsumedAckedMessageSerials.size());
		}
		localCommSessionState.setUnconsumedAckedMessageSerials(unconsumedAckedMessageSerials);
	}

	private boolean sendAckMessage(byte[] destId) {
		TypedPayload payload = TypedPayloadUtils.wrapCustom(new byte[0], SERVICE_NAME, ACK_MESSAGE_TYPE, null);
		return sendMessage(destId, payload);
	}

	private LocalCommSessionState fetchAndCheckSessionConflict(IdentityDescriptor identityDescriptor, LocalCommSessionState localCommSessionState) {
		Pair<CommSessionSeed, SeedCheckState> otherSessionSeedFetchResult = checkForSessionSeedFrom(identityDescriptor);
		CommSessionSeed otherCommSessionSeed = otherSessionSeedFetchResult.getLeft();
		if (otherCommSessionSeed == null) {
			return localCommSessionState;
		}
		return checkSessionConflict(identityDescriptor, localCommSessionState, otherCommSessionSeed);
	}

	private LocalCommSessionState checkSessionConflict(IdentityDescriptor identityDescriptor, LocalCommSessionState localCommSessionState, CommSessionSeed otherCommSessionSeed) {
		if (Arrays.equals(localCommSessionState.getSessionId(), otherCommSessionSeed.getSessionId())) {
			return localCommSessionState;
		}

		byte[] id = identityDescriptor.getId();
		log.debug(tracePrefix + " Conflicting session from " + Utils.getDebugStringFromId(id) +
				" local=" + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()) + ":" + localCommSessionState.getGeneratedAt() +
				" remote=" + Utils.getDebugStringFromBytesShort(otherCommSessionSeed.getSessionId()) + ":" + otherCommSessionSeed.getGeneratedAt());
		ByteBuffer sessionIdLocal = ByteBuffer.wrap(localCommSessionState.getSessionId());
		sessionIdLocal.order(ByteOrder.BIG_ENDIAN);
		ByteBuffer sessionIdOther = ByteBuffer.wrap(otherCommSessionSeed.getSessionId());
		sessionIdOther.order(ByteOrder.BIG_ENDIAN);
		if (sessionIdOther.compareTo(sessionIdLocal) < 0) {
			log.debug(tracePrefix + " Switching to new session " + Utils.getDebugStringFromBytesShort(otherCommSessionSeed.getSessionId()) + " from " + Utils.getDebugStringFromId(id) + ", old session was " + Utils.getDebugStringFromBytesShort(localCommSessionState.getSessionId()));
			LocalCommSessionState otherCommSessionState = createSessionFromSeed(identityDescriptor, otherCommSessionSeed);
			if (otherCommSessionState != null) {
				saveLocalSessionState(id, localCommSessionState);
				return otherCommSessionState;
			}
		}
		return localCommSessionState;
	}

	private LocalCommSessionState getOrCreateSession(IdentityDescriptor identityDescriptor, boolean createRemoteIfMissing, boolean initiateIfMissing) {
		byte[] id = identityDescriptor.getId();
		LocalCommSessionState localCommSessionState = localSessions.get(id);
		if (localCommSessionState != null) {
			if (localCommSessionState.getNextToAckByRemote() < SERIALS_TO_CHECK_SESSION_CONFLICT) {
				localCommSessionState = fetchAndCheckSessionConflict(identityDescriptor, localCommSessionState);
			}
			return localCommSessionState;
		}

		if (!createRemoteIfMissing && !initiateIfMissing) {
			return null;
		}

		log.trace(tracePrefix + " No local session, checking remote for " + Utils.getDebugStringFromId(id));
		Pair<CommSessionSeed, SeedCheckState> sessionSeedFetchResult = checkForSessionSeedFrom(identityDescriptor);
		if (sessionSeedFetchResult.getRight() == SeedCheckState.READ_ERROR) {
			return null;
		}
		CommSessionSeed commSessionSeed = sessionSeedFetchResult.getLeft();
		if (commSessionSeed != null) {
			log.trace(tracePrefix + " Found session seed, creating session for " + Utils.getDebugStringFromId(id));
			localCommSessionState = createSessionFromSeed(identityDescriptor, commSessionSeed);
		}
		if (localCommSessionState != null) {
			saveLocalSessionState(id, localCommSessionState);
			notifyNewSessionEstablished(id);
			return localCommSessionState;
		}

		if (!initiateIfMissing) {
			return null;
		}

		log.trace(tracePrefix + " No remote session, creating new for " + Utils.getDebugStringFromId(id));
		localCommSessionState = startNewSession(identityDescriptor);
		saveLocalSessionState(id, localCommSessionState);
		notifyNewSessionEstablished(id);
		return localCommSessionState;
	}

	private void notifyNewSessionEstablished(byte[] withId) {
		for (CommSessionEstablishmentCallback callback : sessionEstablishmentCallbacks) {
			callback.onSessionEstablished(withId);
		}
	}

	private LocalCommSessionState createSessionFromSeed(IdentityDescriptor identityDescriptor, CommSessionSeed commSessionSeed) {
		long now = System.currentTimeMillis();
		LocalCommSessionState localCommSessionState = null;
		X3DHInitialMessage initialMessage = commSessionSeed.getX3dhInitialMessage();
		KeyPair preKeyPair = localPreKeyService.findKeyPairByHash(initialMessage.getPreKeyHash());
		if (preKeyPair != null) {
			ECKeyPair preKeyPairRaw = CryptoThriftUtils.fromThrift(preKeyPair);
			byte[] secret = cryptoHelper.processX3DHInitialMessage(identityDescriptor, preKeyPairRaw, initialMessage);
			if (secret != null) {
				DoubleRatchet.InitialSharedSecrets initialSharedSecrets = DoubleRatchet.generateInitialSharedSecrets(secret);
				DoubleRatchet.State state = DoubleRatchet.initStateSecond(preKeyPairRaw, initialSharedSecrets);

				byte[] initialMessageData = decrypt(state, identityDescriptor, commSessionSeed.getInitialDoubleRatchetMessage());
				if (Arrays.equals(initialMessageData, INITIAL_DR_MESSAGE)) {
					localCommSessionState = new LocalCommSessionState(ByteBuffer.wrap(commSessionSeed.getSessionId()), commSessionSeed.getGeneratedAt(), CryptoThriftUtils.toThrift(state), 0, 0, 0, 0, 0, now, now, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
				} else {
					log.error(tracePrefix + " Invalid initial data from " + Utils.getDebugStringFromId(identityDescriptor.getId()));
				}
			} else {
				log.debug(tracePrefix + " Failed to process initial message from " + Utils.getDebugStringFromId(identityDescriptor.getId()));
			}
		} else {
			log.debug(tracePrefix + " Couldn't find pre-key, on message from " + Utils.getDebugStringFromId(identityDescriptor.getId()));
		}
		return localCommSessionState;
	}

	private LocalCommSessionState startNewSession(IdentityDescriptor destIdentity) {
		long now = System.currentTimeMillis();
		if (destIdentity.getX3dhPreKeyBundles().isEmpty()) {
			log.trace(tracePrefix + " Failed to generate session for " + IdentityService.toString(destIdentity) + ": no pre-keys");
			return null;
		}
		X3DHPreKeyBundle bundle = destIdentity.getX3dhPreKeyBundles().get(0);
		Pair<byte[], X3DHInitialMessage> initialMessageData = cryptoHelper.createX3DHInitialMessage(destIdentity, bundle);
		if (initialMessageData == null) {
			log.trace(tracePrefix + " Failed to generate session for " + IdentityService.toString(destIdentity) + ": failed to create initial message");
			return null;
		}
		byte[] sessionId = cryptoHelper.generateRandomBytes(8);

		DoubleRatchet.InitialSharedSecrets initialSharedSecrets = DoubleRatchet.generateInitialSharedSecrets(initialMessageData.getLeft());
		DoubleRatchet.State state = DoubleRatchet.initStateFirst(initialSharedSecrets, bundle.getPreKey());

		DoubleRatchetMessage initialDrMessage = encrypt(state, destIdentity, INITIAL_DR_MESSAGE);

		LocalCommSessionState localCommSessionState = new LocalCommSessionState(ByteBuffer.wrap(sessionId), now, CryptoThriftUtils.toThrift(state), 0, 0, 0, 0, now, now, now, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
		CommSessionSeed commSessionSeed = new CommSessionSeed(ByteBuffer.wrap(sessionId), now, initialMessageData.getRight(), initialDrMessage);
		CommSessionSeedLocalMetadata localMetadata = new CommSessionSeedLocalMetadata(ByteBuffer.wrap(destIdentity.getId()), ByteBuffer.wrap(sessionId), now);

		TypedPayload sessionSeedPayload = TypedPayloadUtils.wrap(commSessionSeed, SERVICE_NAME, null, null);
		long validUntil = now + SESSION_SEED_TTL;
		privateBroadcastService.publish(destIdentity, sessionSeedPayload, ThriftUtils.serialize(localMetadata), validUntil);
		commSessionPushService.tryPushCommSessionSeed(destIdentity, commSessionSeed, validUntil);

		return localCommSessionState;
	}

	private boolean checkAndDeleteOutboundSessionSeed(IdentityDescriptor identityDescriptor) {
		final byte[] destId = identityDescriptor.getId();
		int deleted = privateBroadcastService.deleteBroadcasts(identityDescriptor, new PayloadTypeLocalMetadataPredicate(TypedPayloadUtils.getGenericPayloadPredicate(PayloadType.COMM_SESSION_SEED, SERVICE_NAME, null, null)) {
			@Override
			public boolean match(LocalMetadataEnvelope envelope) {
				if (!super.match(envelope)) {
					return false;
				}
				CommSessionSeedLocalMetadata metadata = ThriftUtils.deserializeSafe(envelope.getCustomMetadata(), CommSessionSeedLocalMetadata.class);
				if (metadata == null) {
					log.trace(tracePrefix + " Failed to deserialize metadata, deleting");
					return true;
				}
				return Arrays.equals(destId, metadata.getDestId());
			}
		});
		if (deleted > 0) {
			log.trace(tracePrefix + " Deleted " + deleted + " session seeds for " + Utils.getDebugStringFromId(identityDescriptor.getId()));
		}
		return deleted > 0;
	}

	enum SeedCheckState {
		READ_ERROR,
		READ_SUCCESS
	}

	private Pair<CommSessionSeed, SeedCheckState> checkForSessionSeedFrom(IdentityDescriptor identityDescriptor) {
		PayloadTypePredicate predicate = TypedPayloadUtils.getGenericPayloadPredicate(PayloadType.COMM_SESSION_SEED, SERVICE_NAME, null, null);
		List<TypedPayload> broadcasts = privateBroadcastService.readBroadcastsFrom(identityDescriptor, predicate);
		if (broadcasts == null) {
			return Pair.of(null, SeedCheckState.READ_ERROR);
		}
		if (broadcasts.isEmpty()) {
			return Pair.of(null, SeedCheckState.READ_SUCCESS);
		}
		TypedPayload broadcast = broadcasts.get(0);
		Pair<CommSessionSeed, String> seedUnwrapped = TypedPayloadUtils.unwrap(broadcast, CommSessionSeed.class, SERVICE_NAME);
		if (seedUnwrapped.getRight() != null) {
			log.info(tracePrefix + " Failed to extract seed from " + Utils.getDebugStringFromId(identityDescriptor.getId()) + " : " + seedUnwrapped.getRight());
			return Pair.of(null, SeedCheckState.READ_SUCCESS);
		}
		return Pair.of(seedUnwrapped.getLeft(), null);
	}
}
