package org.cweb.communication;

public interface NativeSchedulingProvider {
	long getTime();
	long getNoConnectionRetryInterval();
	boolean hasInternetConnection();
	void scheduleWakeup(long time);
	void cancelWakeup();
	void setCallbacks(Callbacks callbacks);

	interface Callbacks {
		void wakeUp();
	}
}
