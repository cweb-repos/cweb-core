package org.cweb.communication;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

class SharedSessionWorkState {
	private static final Logger log = LoggerFactory.getLogger(SharedSessionWorkState.class);
	private final String tracePrefix;

	private Set<ByteBuffer> referenceReceivedSessions = new LinkedHashSet<>();
	private Set<ByteBuffer> messageReceivedSessions = new LinkedHashSet<>();
	private Set<ByteBuffer> messageSentSessions = new LinkedHashSet<>();
	private Set<ByteBuffer> messageAckedSessions = new LinkedHashSet<>();
	private Map<ByteBuffer, Set<ByteBuffer>> session2UnconsumedMessages = new LinkedHashMap<>();

	SharedSessionWorkState(String tracePrefix) {
		this.tracePrefix = tracePrefix;
	}

	synchronized boolean isEmpty() {
		return referenceReceivedSessions.isEmpty() && messageReceivedSessions.isEmpty() && messageSentSessions.isEmpty() && messageAckedSessions.isEmpty() && session2UnconsumedMessages.isEmpty();
	}

	synchronized Set<ByteBuffer> drainReferenceReceivedSessions() {
		return Utils.drain(referenceReceivedSessions);
	}

	synchronized Pair<Set<ByteBuffer>, Set<ByteBuffer>> drainReceivedAndSentSessions() {
		if (messageReceivedSessions.isEmpty() && messageSentSessions.isEmpty()) {
			return null;
		}
		return Pair.of(Utils.drain(messageReceivedSessions), Utils.drain(messageSentSessions));
	}

	synchronized Set<ByteBuffer> drainAckedSessions() {
		return Utils.drain(messageAckedSessions);
	}

	synchronized Map<ByteBuffer, Set<ByteBuffer>> drainUnconsumedSessions() {
		return Utils.drain(session2UnconsumedMessages);
	}

	synchronized void addReferenceReceivedSession(byte[] sessionId) {
		referenceReceivedSessions.add(ByteBuffer.wrap(sessionId));
	}

	private static Set<ByteBuffer> getOrCreateEntry(Map<ByteBuffer, Set<ByteBuffer>> map, ByteBuffer key) {
		Set<ByteBuffer> entry = map.computeIfAbsent(key, (key1) -> new LinkedHashSet<>());
		return entry;
	}

	synchronized void addMessageReceived(byte[] sessionId, byte[] messageId, boolean consumed) {
		ByteBuffer sessionIdWrapped = ByteBuffer.wrap(sessionId);
		messageReceivedSessions.add(sessionIdWrapped);
		if (!consumed) {
			Set<ByteBuffer> unconsumedEntry = getOrCreateEntry(session2UnconsumedMessages, sessionIdWrapped);
			unconsumedEntry.add(ByteBuffer.wrap(messageId));
		}
	}

	synchronized void addMessageSentSession(byte[] sessionId) {
		messageSentSessions.add(ByteBuffer.wrap(sessionId));
	}

	synchronized void addMessageAckedSession(byte[] sessionId) {
		messageAckedSessions.add(ByteBuffer.wrap(sessionId));
	}
}
