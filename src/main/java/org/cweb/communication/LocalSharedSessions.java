package org.cweb.communication;

import org.cweb.schemas.comm.shared.LocalSharedSessionState;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class LocalSharedSessions extends LocalDataSingleKey<LocalSharedSessionState> {
	private static final String NAME_SUFFIX = "-sharedSessionState";

	public LocalSharedSessions(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public void put(LocalSharedSessionState value) {
		super.put(value.getSessionId(), value);
	}

	public LocalSharedSessionState get(byte[] id) {
		return super.get(id, LocalSharedSessionState.class);
	}
}
