package org.cweb.communication;

import org.cweb.utils.Threads;

import java.util.concurrent.ScheduledFuture;

public class DefaultSchedulingProvider implements NativeSchedulingProvider {
	private Callbacks callbacks;
	private ScheduledFuture<?> wakeupTask;

	@Override
	public long getTime() {
		return System.currentTimeMillis();
	}

	@Override
	public long getNoConnectionRetryInterval() {
		return 1000L * 60 * 10;
	}

	@Override
	public boolean hasInternetConnection() {
		return true;
	}

	@Override
	public synchronized void scheduleWakeup(long time) {
		cancelWakeup();
		long now = getTime();
		long delay = Math.max(0, time - now);
		this.wakeupTask = Threads.submitTask(new Runnable() {
			@Override
			public void run() {
				wakeupTask = null;
				callbacks.wakeUp();
			}
		}, delay);
	}

	@Override
	public synchronized void cancelWakeup() {
		if (wakeupTask != null && !wakeupTask.isDone() && !wakeupTask.isCancelled()) {
			wakeupTask.cancel(false);
		}
	}

	@Override
	public void setCallbacks(Callbacks callbacks) {
		this.callbacks = callbacks;
	}
}
