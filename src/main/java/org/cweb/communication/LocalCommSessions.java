package org.cweb.communication;

import org.cweb.schemas.comm.session.LocalCommSessionState;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class LocalCommSessions extends LocalDataSingleKey<LocalCommSessionState> {
	private static final String NAME_SUFFIX = "-localSession2State";

	public LocalCommSessions(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalCommSessionState get(byte[] id) {
		return super.get(id, LocalCommSessionState.class);
	}
}
