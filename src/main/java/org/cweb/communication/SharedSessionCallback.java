package org.cweb.communication;

public interface SharedSessionCallback {
	void onMessagesReceived(byte[] sessionId);
	void onMessagesAcked(byte[] sessionId);
	void onDescriptorUpdated(byte[] sessionId);
}
