package org.cweb.communication;

import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.wire.TypedPayload;

public interface MessageProcessor {
	Result process(SessionId sessionId, TypedPayload typedPayload);

	enum Result {
		INVALID,
		SUCCESS,
		SUCCESS_REDUNDANT
	}
}
