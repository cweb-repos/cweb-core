package org.cweb.communication;

interface SharedSessionCallbackInternal {
	void onMessagesReceived(byte[] sessionId);
	void onMessagesSent(byte[] sessionId);
	void onMessagesAcked(byte[] sessionId);
	void onDescriptorUpdated(byte[] sessionId);
}
