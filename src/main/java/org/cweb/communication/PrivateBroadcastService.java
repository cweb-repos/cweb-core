package org.cweb.communication;

import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.identity.IdentityService;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.storage.LocalMetadataEnvelope;
import org.cweb.schemas.storage.PrivateBroadcastConfig;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.LocalMetadataPredicate;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrivateBroadcastService {
	private static final Logger log = LoggerFactory.getLogger(PrivateBroadcastService.class);
	private static final String NAME_SUFFIX = "-privateBroadcast";
	private static final PrivateBroadcastConfig privateBroadcastConfigDefault = new PrivateBroadcastConfig(2, 10);

	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;

	private final IdentityDescriptor ownIdentityDescriptor;
	private final RemoteFileHandler remoteBroadcastHandler;

	public PrivateBroadcastService(String tracePrefix, CryptoHelper cryptoHelper, IdentityService identityService, RemoteWriteService remoteWriteService, RemoteReadService remoteReadService) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;

		this.ownIdentityDescriptor = identityService.getIdentityDescriptor();
		this.remoteBroadcastHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, NAME_SUFFIX);
	}

	private byte[] getFileName(PrivateBroadcastConfig config, byte[] ownId, byte[] toId, int slot) {
		return BinaryUtils.concat(ownId, Arrays.copyOf(HashingUtils.SHA256(toId), config.getBucketNameBytes()), BinaryUtils.intToBytes(slot));
	}

	public boolean publish(IdentityDescriptor identityDescriptor, TypedPayload payload, byte[] customLocalMetadata, Long validUntil) {
		PrivateBroadcastConfig config = ownIdentityDescriptor.isSetPrivateBroadcastConfig() ? identityDescriptor.getPrivateBroadcastConfig() : privateBroadcastConfigDefault;
		for (int slot = 0; slot < config.getSlotsPerBucket(); slot++) {
			byte[] fileName = getFileName(config, ownIdentityDescriptor.getId(), identityDescriptor.getId(), slot);
			OutboundDataWrapperRaw existingDataWrapper = remoteBroadcastHandler.getFromLocalCache(fileName);
			if (existingDataWrapper != null) {
				continue;
			}
			CryptoEnvelope envelope = cryptoHelper.signAndEncryptFor(payload, identityDescriptor.getId(), identityDescriptor.getEcPublicKey(), validUntil);
			LocalMetadataEnvelope localMetadataEnvelope = new LocalMetadataEnvelope(payload.getMetadata());
			if (customLocalMetadata != null) {
				localMetadataEnvelope.setCustomMetadata(customLocalMetadata);
			}
			OutboundDataWrapperRaw dataWrapper = new OutboundDataWrapperRaw(ThriftUtils.serialize(envelope), localMetadataEnvelope, validUntil);
			remoteBroadcastHandler.write(fileName, dataWrapper);
			log.trace(tracePrefix + " Created private broadcast, slot " + slot + " for " + Utils.getDebugStringFromId(identityDescriptor.getId()));
			return true;
		}
		log.error(tracePrefix + " Too many broadcasts in bucket for " + Utils.getDebugStringFromId(identityDescriptor.getId()));
		return false;
	}

	/**
	 * @return null if there was read failure for one of the broadcasts
	 */
	public List<TypedPayload> readBroadcastsFrom(IdentityDescriptor identityDescriptor, PayloadTypePredicate predicate) {
		byte[] id = identityDescriptor.getId();
		PrivateBroadcastConfig config = identityDescriptor.isSetPrivateBroadcastConfig() ? identityDescriptor.getPrivateBroadcastConfig() : privateBroadcastConfigDefault;
		List<TypedPayload> result = new ArrayList<>();
		for (int slot = 0; slot < config.getSlotsPerBucket(); slot++) {
			byte[] fileName = getFileName(config, identityDescriptor.getId(), ownIdentityDescriptor.getId(), slot);
			RemoteFetchResultRaw fetchResult = remoteBroadcastHandler.read(id, fileName);
			if (fetchResult.getError() != null) {
				return null;
			}
			if (fetchResult.getData() == null) {
				continue;
			}
			Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(fetchResult.getData(), CryptoEnvelopeDecodingParams.create().setSignerIdentityDescriptor(identityDescriptor).setIdForSignatureTargetVerification(ownIdentityDescriptor.getId()), CryptoContext.create().setCryptoHelper(cryptoHelper));
			if (decodedTypedPayloadWrapper.getError() != null) {
				if (decodedTypedPayloadWrapper.getError() != Decoded.Error.PK_KEY_DECRYPTION) {
					log.trace(tracePrefix + " Failed to decode broadcast " + slot + " from " + Utils.getDebugStringFromId(id) + " : " + decodedTypedPayloadWrapper.getError());
				}
				continue;
			}
			DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
			if (decodedTypedPayload.getSignatureMetadata() == null || !Arrays.equals(id, decodedTypedPayload.getSignatureMetadata().getSignerId())) {
				log.trace(tracePrefix + " Mismatching id in broadcast " + slot + " from " + Utils.getDebugStringFromId(id));
				continue;
			}
			log.trace(tracePrefix + " Found broadcast on slot " + slot + " from " + Utils.getDebugStringFromId(id));
			TypedPayload typedPayload = decodedTypedPayload.getPayload();
			if (typedPayload == null) {
				log.trace(tracePrefix + " Failed to deserialize broadcast " + slot + " from " + Utils.getDebugStringFromId(id));
				continue;
			}
			if (predicate.match(typedPayload.getMetadata())) {
				result.add(typedPayload);
			}
		}
		return result;
	}

	public List<OutboundDataWrapperRaw> getOutboundBroadcastsTo(IdentityDescriptor identityDescriptor, LocalMetadataPredicate predicate) {
		PrivateBroadcastConfig config = ownIdentityDescriptor.isSetPrivateBroadcastConfig() ? identityDescriptor.getPrivateBroadcastConfig() : privateBroadcastConfigDefault;
		List<OutboundDataWrapperRaw> result = new ArrayList<>();
		for (int slot = 0; slot < config.getSlotsPerBucket(); slot++) {
			byte[] fileName = getFileName(config, ownIdentityDescriptor.getId(), identityDescriptor.getId(), slot);
			OutboundDataWrapperRaw dataWrapper = remoteBroadcastHandler.getFromLocalCache(fileName);
			if (dataWrapper == null || dataWrapper.getLocalMetadataEnvelope() == null) {
				continue;
			}
			if (!predicate.match(dataWrapper.getLocalMetadataEnvelope())) {
				continue;
			}
			result.add(dataWrapper);
		}
		return result;
	}

	public int deleteBroadcasts(IdentityDescriptor identityDescriptor, LocalMetadataPredicate predicate) {
		PrivateBroadcastConfig config = ownIdentityDescriptor.isSetPrivateBroadcastConfig() ? identityDescriptor.getPrivateBroadcastConfig() : privateBroadcastConfigDefault;
		int deleted = 0;
		for (int slot = 0; slot < config.getSlotsPerBucket(); slot++) {
			byte[] fileName = getFileName(config, ownIdentityDescriptor.getId(), identityDescriptor.getId(), slot);
			OutboundDataWrapperRaw dataWrapper = remoteBroadcastHandler.getFromLocalCache(fileName);
			if (dataWrapper == null || !predicate.match(dataWrapper.getLocalMetadataEnvelope())) {
				continue;
			}
			remoteBroadcastHandler.delete(fileName);
			deleted++;
		}
		return deleted;
	}
}
