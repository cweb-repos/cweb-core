package org.cweb.crypto;

import com.google.common.base.Preconditions;
import org.cweb.crypto.lib.AEAD;
import org.cweb.crypto.lib.ECAEAD;
import org.cweb.crypto.lib.ECKeyPair;
import org.cweb.crypto.lib.ECUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.crypto.lib.RSAUtils;
import org.cweb.crypto.lib.X3DH;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.keys.KeyType;
import org.cweb.schemas.keys.PublicKey;
import org.cweb.schemas.local.IdentityKeys;
import org.cweb.schemas.wire.PKEncryptedEnvelope;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.SignedEnvelope;
import org.cweb.schemas.wire.SignedPayload;
import org.cweb.schemas.wire.SymmetricEncryptedEnvelope;
import org.cweb.storage.local.SecretStorageService;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.security.PrivateKey;

public class IdentityCryptoService {
	private static final Logger log = LoggerFactory.getLogger(IdentityCryptoService.class);
	private static final String IDENTITY_KEY_FILENAME = "masterKey";
	private static final byte[] EC_AD_KEY = new byte[]{1};
	private static final byte[] EC_AD_DATA = new byte[]{1};

	private final IdentityKeys identityKeys;
	private final java.security.KeyPair rsaKeyPair;
	private final ECKeyPair ecKeyPair;
	private final PublicKey rsaPublicKey;
	private final PublicKey ecPublicKey;
	private final byte[] ownId;
	private final String tracePrefix;

	public enum SignerType {
		RSA,
		EC
	}

	private IdentityCryptoService(IdentityKeys identityKeys) {
		this.identityKeys = identityKeys;
		this.rsaKeyPair = constructRSAKeyPair(identityKeys);
		this.ecKeyPair = CryptoThriftUtils.fromThrift(identityKeys.getEcKey());
		this.rsaPublicKey = new PublicKey(KeyType.RSA_2048, ByteBuffer.wrap(RSAUtils.getPublicKeyX509Encoded(rsaKeyPair.getPublic().getEncoded().clone())));
		this.ecPublicKey = new PublicKey(KeyType.EC25519_256, ByteBuffer.wrap(ecKeyPair.publicKey.clone()));
		Preconditions.checkNotNull(this.rsaKeyPair);
		this.ownId = identityKeys.getId();
		this.tracePrefix = Utils.getDebugStringFromId(ownId);
	}

	public static IdentityCryptoService loadFromFile(SecretStorageService secretStorageService) {
		IdentityKeys identityKeys = loadIdentityKey(secretStorageService);
		return new IdentityCryptoService(identityKeys);
	}

	private static IdentityKeys loadIdentityKey(SecretStorageService secretStorageService) {
		byte[] data = secretStorageService.read(IDENTITY_KEY_FILENAME);
		if (data == null)
			return null;
		try {
			IdentityKeys identityKeys = ThriftUtils.deserialize(data, IdentityKeys.class);
			log.trace(Utils.getDebugStringFromId(identityKeys.getId()) + " Loaded identity keys");
			return identityKeys;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void saveIdentityKeys(SecretStorageService secretStorageService, IdentityKeys identityKeys) {
		secretStorageService.write(IDENTITY_KEY_FILENAME, ThriftUtils.serialize(identityKeys));
		log.trace(Utils.getDebugStringFromKey(identityKeys.getId()) + " Saved identity keys");
	}

	public void saveIdentityKeys(SecretStorageService secretStorageService) {
		saveIdentityKeys(secretStorageService, identityKeys);
	}

	public static IdentityCryptoService generateNewKeys() {
		IdentityKeys key = createNewKey();
		return new IdentityCryptoService(key);
	}

	private static IdentityKeys createNewKey() {
		try {
			IdentityKeys identityKeys = new IdentityKeys();

			java.security.KeyPair rsaKeyPair = RSAUtils.generateKeyPair();
			KeyPair rsaKey = new KeyPair(KeyType.RSA_2048, ByteBuffer.wrap(RSAUtils.getPublicKeyX509Encoded(rsaKeyPair.getPublic().getEncoded())), ByteBuffer.wrap(RSAUtils.getPrivateKeyPKCS8Encoded(rsaKeyPair.getPrivate().getEncoded())));
			identityKeys.setRsaKey(rsaKey);

			ECKeyPair ecKeyPair = ECUtils.generateKeyPair();
			KeyPair ecKey = new KeyPair(KeyType.EC25519_256, ByteBuffer.wrap(ecKeyPair.publicKey), ByteBuffer.wrap(ecKeyPair.privateKey));
			identityKeys.setEcKey(ecKey);

			identityKeys.setId(idFromKey(identityKeys.getEcKey().getPublicKey()));

			log.trace(Utils.getDebugStringFromId(identityKeys.getId()) + " Generated identity keys");
			return identityKeys;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static java.security.KeyPair constructRSAKeyPair(IdentityKeys identityKeys) {
		KeyPair rsaKey = identityKeys.getRsaKey();
		java.security.PublicKey publicKey = RSAUtils.constructPublicKey(rsaKey.getPublicKey());
		PrivateKey privateKey = RSAUtils.constructPrivateKey(rsaKey.getPrivateKey());
		return new java.security.KeyPair(publicKey, privateKey);
	}

	public PublicKey getRSAPublicKey() {
		return rsaPublicKey;
	}

	private PrivateKey getRSAPrivateKey() {
		return rsaKeyPair.getPrivate();
	}

	public PublicKey getECPublicKey() {
		return ecPublicKey;
	}

	private byte[] getECPrivateKey() {
		return ecKeyPair.privateKey;
	}

	public static byte[] idFromKey(byte[] key) {
		return HashingUtils.SHA512_256(key);
	}

	public static byte[] idFromPublicKey(PublicKey publicKey) {
		return idFromKey(publicKey.getPublicKey());
	}

	public byte[] getOwnId() {
		return ownId;
	}

	byte[] signWithIdentityKeyRSA(byte[] data) {
		log.trace(tracePrefix + " Signed with identity RSA keys");
		return RSAUtils.sign(getRSAPrivateKey(), data);
	}

	byte[] signWithIdentityKeyEC(byte[] data) {
		log.trace(tracePrefix + " Signed with identity EC keys");
		return ECUtils.sign(getECPrivateKey(), data);
	}

	public SignedEnvelope createSignedEnvelope(byte[] recipientId, byte[] data, Long ttl, SignerType signerType) {
		long now = System.currentTimeMillis();

		SignatureMetadata signatureMetadata = new SignatureMetadata(ByteBuffer.wrap(getOwnId()), signerType == SignerType.RSA ? RSAUtils.getSignerName() : ECUtils.getSignerName());
		if (recipientId != null) {
			signatureMetadata.setRecipientId(ByteBuffer.wrap(recipientId));
		}
		signatureMetadata.setGeneratedAt(now);
		if (ttl != null) {
			long validUntil = now + ttl;
			signatureMetadata.setValidUntil(validUntil);
		}

		SignedPayload signedPayload = new SignedPayload(signatureMetadata, ByteBuffer.wrap(data));

		byte[] signedPayloadSerialized = ThriftUtils.serialize(signedPayload);
		byte[] signature = signerType == SignerType.RSA ? signWithIdentityKeyRSA(signedPayloadSerialized) : signWithIdentityKeyEC(signedPayloadSerialized);
		return new SignedEnvelope(ByteBuffer.wrap(signature), ByteBuffer.wrap(signedPayloadSerialized));
	}

	private static boolean verifySignature(PublicKey publicKey, byte[] data, byte[] signature) {
		switch (publicKey.getType()) {
			case RSA_2048: {
				java.security.PublicKey publicKeyExtracted = RSAUtils.constructPublicKey(publicKey.getPublicKey());
				if (publicKeyExtracted == null) {
					return false;
				}
				boolean result = RSAUtils.verifySignature(publicKeyExtracted, data, signature);
				log.trace("Verified RSA signature from " + Utils.getDebugStringFromKey(publicKey) + ", result=" + result);
				return result;
			}
			case EC25519_256: {
				boolean result = ECUtils.checkSignature(publicKey.getPublicKey(), data, signature);
				log.trace("Verified EC signature from " + Utils.getDebugStringFromKey(publicKey) + ", result=" + result);
				return result;
			}
			default:
				log.error("Unsupported key type " + publicKey.getType());
				return false;
		}
	}

	public static boolean verifySignature(IdentityDescriptor identityDescriptor, String signerName, SignedEnvelope envelope) {
		PublicKey publicKey;
		if (RSAUtils.getSignerName().equals(signerName)) {
			publicKey = identityDescriptor.getRsaPublicKey();
		} else if (ECUtils.getSignerName().equals(signerName)) {
			publicKey = identityDescriptor.getEcPublicKey();
		} else {
			log.error("Unrecognized signer name " + signerName);
			return false;
		}
		return verifySignature(publicKey, envelope.getSignedPayload(), envelope.getSignature());
	}

	PKEncryptedEnvelope encryptFor(PublicKey publicKey, byte[] data) {
		byte[] key = AEAD.generateCompositeKey();
		byte[] encryptedData = ThriftUtils.serialize(CryptoHelper.encryptSymmetricRaw(key, data, EC_AD_DATA));
		log.trace(tracePrefix + " Generated sym key and encrypted envelope for " + Utils.getDebugStringFromKey(publicKey));
		switch (publicKey.getType()) {
			case RSA_2048: {
				byte[] encryptedKey = RSAUtils.encrypt(RSAUtils.constructPublicKey(publicKey.getPublicKey()), key);
				PKEncryptedEnvelope envelope = new PKEncryptedEnvelope(ByteBuffer.wrap(encryptedKey), RSAUtils.getCipherName(), AEAD.getCipherName(), ByteBuffer.wrap(encryptedData));
				return envelope;
			}
			case EC25519_256: {
				byte[] encryptedKey = ECAEAD.encrypt(publicKey.getPublicKey(), key, EC_AD_KEY);
				PKEncryptedEnvelope envelope = new PKEncryptedEnvelope(ByteBuffer.wrap(encryptedKey), ECAEAD.getCipherName(), AEAD.getCipherName(), ByteBuffer.wrap(encryptedData));
				return envelope;
			}
			default:
				log.error("Unsupported key type " + publicKey.getType());
				return null;
		}
	}

	public Decoded<byte[]> decrypt(PKEncryptedEnvelope pkEncryptedEnvelope) {
		byte[] key;
		if (RSAUtils.getCipherName().equals(pkEncryptedEnvelope.getKeyEncryptionAlgorithm())) {
			key = RSAUtils.decrypt(getRSAPrivateKey(), pkEncryptedEnvelope.getPkEncryptedKey());
		} else if (ECAEAD.getCipherName().equals(pkEncryptedEnvelope.getKeyEncryptionAlgorithm())) {
			key = ECAEAD.decrypt(getECPrivateKey(), pkEncryptedEnvelope.getPkEncryptedKey(), EC_AD_KEY);
		} else {
			return new Decoded<>(Decoded.Error.PK_KEY_UNKNOWN_CIPHER);
		}
		if (key == null) {
			return new Decoded<>(Decoded.Error.PK_KEY_DECRYPTION);
		}

		SymmetricEncryptedEnvelope symmetricEncryptedEnvelope = ThriftUtils.deserializeSafe(pkEncryptedEnvelope.getPayload(), SymmetricEncryptedEnvelope.class);
		if (symmetricEncryptedEnvelope == null) {
			return new Decoded<>(Decoded.Error.CRYPTO_ENVELOPE_DESERIALIZATION);
		}

		Decoded<byte[]> decryptedData = CryptoHelper.decryptSymmetric(key, symmetricEncryptedEnvelope, EC_AD_DATA);
		if (decryptedData.getError() != null) {
			return new Decoded<>(decryptedData.getError());
		}

		return new Decoded<>(decryptedData.getData());
	}

	public X3DH.PreKeyBundle generateX3DHPreKeyBundle(byte[] ephemeralPublicKey) {
		return X3DH.generatePreKeyBundle(ecKeyPair.privateKey, ecKeyPair.publicKey, ephemeralPublicKey);
	}

	public X3DH.InitialMessageGenerationResult generateX3DHSessionFirst(ECKeyPair ephemeralKeyPair, X3DH.PreKeyBundle preKeyBundle) {
		return X3DH.generateInitialMessage(ecKeyPair, ephemeralKeyPair, preKeyBundle);
	}

	public X3DH.InitialMessageProcessingResult generateX3DHSessionSecond(ECKeyPair preKeyPair, X3DH.InitialMessage initialMessage) {
		return X3DH.processInitialMessage(ecKeyPair, preKeyPair, initialMessage);
	}
}
