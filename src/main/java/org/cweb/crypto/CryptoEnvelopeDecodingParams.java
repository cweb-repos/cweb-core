package org.cweb.crypto;

import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.wire.TypedPayload;

import java.util.function.Function;

public class CryptoEnvelopeDecodingParams {
	boolean skipAllSignatureChecks;
	byte[] idForSignatureTargetVerification;
	IdentityDescriptor signerIdentityDescriptor;
	boolean fetchSignerIfNeeded;
	boolean isSelfSignedIdentityDescriptor;
	Function<TypedPayload, IdentityReference> payloadSignerExtractor;

	byte[] symmetricDecryptionKey;
	byte[] symmetricAssociatedData;

	public static CryptoEnvelopeDecodingParams create() {
		return new CryptoEnvelopeDecodingParams();
	}

	public CryptoEnvelopeDecodingParams setSkipAllSignatureChecks(boolean skipAllSignatureChecks) {
		this.skipAllSignatureChecks = skipAllSignatureChecks;
		return this;
	}

	public CryptoEnvelopeDecodingParams setIdForSignatureTargetVerification(byte[] idForSignatureTargetVerification) {
		this.idForSignatureTargetVerification = idForSignatureTargetVerification;
		return this;
	}

	public CryptoEnvelopeDecodingParams setSignerIdentityDescriptor(IdentityDescriptor signerIdentityDescriptor) {
		this.signerIdentityDescriptor = signerIdentityDescriptor;
		return this;
	}

	public CryptoEnvelopeDecodingParams setFetchSignerIfNeeded(boolean fetchSignerIfNeeded) {
		this.fetchSignerIfNeeded = fetchSignerIfNeeded;
		return this;
	}

	public CryptoEnvelopeDecodingParams setSelfSignedIdentityDescriptor(boolean selfSignedIdentityDescriptor) {
		isSelfSignedIdentityDescriptor = selfSignedIdentityDescriptor;
		return this;
	}

	public CryptoEnvelopeDecodingParams setPayloadSignerExtractor(Function<TypedPayload, IdentityReference> payloadSignerExtractor) {
		this.payloadSignerExtractor = payloadSignerExtractor;
		return this;
	}

	public CryptoEnvelopeDecodingParams setSymmetricDecryptionKey(byte[] symmetricDecryptionKey) {
		this.symmetricDecryptionKey = symmetricDecryptionKey;
		return this;
	}

	public CryptoEnvelopeDecodingParams setSymmetricAssociatedData(byte[] symmetricAssociatedData) {
		this.symmetricAssociatedData = symmetricAssociatedData;
		return this;
	}
}
