package org.cweb.crypto.lib;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESCipher {
	private static final String ENCRYPTION_KEY_TYPE = "AES";
	private static final String CIPHER_TYPE = "AES/CBC/PKCS5Padding";
	public static final int IV_BYTES = 16;

	public static byte[] getIv() {
		return SecureRandomUtils.generateRandomBytes(IV_BYTES);
	}

	public static Cipher getCipher(int mode, byte[] iv, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
		cipher.init(mode, new SecretKeySpec(key, ENCRYPTION_KEY_TYPE), new IvParameterSpec(iv));
		return cipher;
	}

}
