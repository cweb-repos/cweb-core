package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;

public class AEAD {
	private static final Logger log = LoggerFactory.getLogger(AEAD.class);
	private static final String CIPHER_NAME = "CwebAeadV1";
	private static final String ENCRYPTION_KEY_TYPE = "AES";
	private static final String CIPHER_TYPE = "AES/CBC/PKCS5Padding";
	private static final String HMAC_TYPE = "HmacSHA256";
	private static final int KEY_BYTES = 32;
	private static final int IV_BYTES = 16;
	private static final int HMAC_BYTES = 32;

	public static int getCompositeKeyLengthInBytes() {
		return KEY_BYTES * 2;
	}

	public static byte[] generateCompositeKey() {
		return SecureRandomUtils.generateRandomBytes(getCompositeKeyLengthInBytes());
	}

	public static String getCipherName() {
		return CIPHER_NAME;
	}

	public static byte[] encrypt(byte[] compositeKey, byte[] data, byte[] associatedData) {
		byte[] encryptionKey = Arrays.copyOfRange(compositeKey, 0, KEY_BYTES);
		byte[] authKey = Arrays.copyOfRange(compositeKey, KEY_BYTES, KEY_BYTES * 2);
		return encrypt(encryptionKey, authKey, data, associatedData);
	}

	public static byte[] encrypt(byte[] encryptionKey, byte[] authKey, byte[] data, byte[] associatedData) {
		try {
			byte[] iv = SecureRandomUtils.generateRandomBytes(IV_BYTES);

			Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptionKey, ENCRYPTION_KEY_TYPE), new IvParameterSpec(iv));
			byte[] encrypted = cipher.doFinal(data);

			byte[] dataToMac = BinaryUtils.concat(iv, encrypted);

			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(authKey, HMAC_TYPE));
			mac.update(associatedData);
			byte[] hmac = mac.doFinal(dataToMac);
			if (hmac.length != HMAC_BYTES) {
				throw new RuntimeException("Invalid hmac length=" + hmac.length);
			}

			return BinaryUtils.concat(dataToMac, hmac);
		} catch (Exception e) {
			throw new RuntimeException("Error encrypting", e);
		}
	}

	public static byte[] decrypt(byte[] key, byte[] encryptedMessage, byte[] associatedData) {
		byte[] encryptionKey = Arrays.copyOfRange(key, 0, KEY_BYTES);
		byte[] authKey = Arrays.copyOfRange(key, KEY_BYTES, KEY_BYTES * 2);
		return decrypt(encryptionKey, authKey, encryptedMessage, associatedData);
	}

	public static byte[] decrypt(byte[] encryptionKey, byte[] authKey, byte[] encryptedMessage, byte[] associatedData) {
		if (encryptedMessage.length <= HMAC_BYTES + IV_BYTES) {
			return null;
		}

		byte[] dataToMac = Arrays.copyOfRange(encryptedMessage, 0, encryptedMessage.length - HMAC_BYTES);
		byte[] hmac = Arrays.copyOfRange(encryptedMessage, encryptedMessage.length - HMAC_BYTES, encryptedMessage.length);

		try {
			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(authKey, HMAC_TYPE));
			mac.update(associatedData);
			byte[] hmacCalculated = mac.doFinal(dataToMac);

			if (!MessageDigest.isEqual(hmac, hmacCalculated)) {
				return null;
			}

			Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(encryptionKey, ENCRYPTION_KEY_TYPE), new IvParameterSpec(dataToMac, 0, IV_BYTES));
			byte[] decrypted = cipher.doFinal(dataToMac, IV_BYTES, dataToMac.length - IV_BYTES);

			return decrypted;
		} catch (Exception e) {
			log.debug("Error decrypting: " + e.getMessage());
			return null;
		}
	}
}
