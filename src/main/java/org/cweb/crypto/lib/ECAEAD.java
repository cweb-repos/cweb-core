package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;

/**
 * ECIES implementation based on EC and AEAD
 */
public class ECAEAD {
	private static final Logger log = LoggerFactory.getLogger(ECAEAD.class);
	private static final String CIPHER_NAME = "CwebEcAeadV1";
	private static final String ENCRYPTION_KEY_TYPE = "AES";
	private static final String SYM_CIPHER_TYPE = "AES/CBC/PKCS5Padding";
	private static final String HMAC_TYPE = "HmacSHA256";
	private static final int EC_KEY_BYTES = 32;
	private static final int KEY_BYTES = 32;
	private static final int AUTH_KEY_BYTES = 32;
	private static final int IV_BYTES = 16;
	private static final int HMAC_BYTES = 32;

	public static String getCipherName() {
		return CIPHER_NAME;
	}

	public static byte[] encrypt(byte[] publicKey, byte[] data, byte[] associatedData) {
		if (publicKey.length != EC_KEY_BYTES) {
			throw new RuntimeException("Invalid key length=" + publicKey.length);
		}
		ECKeyPair ephemeralKeyPair = ECUtils.generateKeyPair();
		byte[] ephemeralPrivateKey = ephemeralKeyPair.privateKey;
		byte[] ephemeralPublicKey = ephemeralKeyPair.publicKey;
		if (ephemeralPrivateKey.length != EC_KEY_BYTES || ephemeralPublicKey.length != EC_KEY_BYTES) {
			throw new RuntimeException("Invalid ephemeral key length:" + ephemeralPrivateKey.length + ", " + ephemeralPublicKey.length);
		}

		byte[] dh = ECUtils.dh(ephemeralPrivateKey, publicKey);
		byte[] dhSha512 = HashingUtils.SHA512(dh);
		byte[] encryptionKey = Arrays.copyOfRange(dhSha512, 0, KEY_BYTES);
		byte[] authKey = Arrays.copyOfRange(dhSha512, KEY_BYTES, KEY_BYTES + AUTH_KEY_BYTES);

		byte[] iv = SecureRandomUtils.generateRandomBytes(IV_BYTES);

		try {
			Cipher cipher = Cipher.getInstance(SYM_CIPHER_TYPE);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptionKey, ENCRYPTION_KEY_TYPE), new IvParameterSpec(iv));
			byte[] encrypted = cipher.doFinal(data);

			byte[] dataToMac = BinaryUtils.concat(iv, ephemeralPublicKey, encrypted);

			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(authKey, HMAC_TYPE));
			mac.update(associatedData);
			byte[] hmac = mac.doFinal(dataToMac);
			if (hmac.length != HMAC_BYTES) {
				throw new RuntimeException("Invalid hmac length=" + hmac.length);
			}

			return BinaryUtils.concat(dataToMac, hmac);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] decrypt(byte[] privateKey, byte[] encryptedMessage, byte[] associatedData) {
		if (privateKey.length != EC_KEY_BYTES) {
			return null;
		}
		if (encryptedMessage.length <= HMAC_BYTES + IV_BYTES + EC_KEY_BYTES) {
			return null;
		}

		byte[] dataToMac = Arrays.copyOfRange(encryptedMessage, 0, encryptedMessage.length - HMAC_BYTES);
		byte[] hmac = Arrays.copyOfRange(encryptedMessage, encryptedMessage.length - HMAC_BYTES, encryptedMessage.length);

		byte[] ephemeralPublicKey = Arrays.copyOfRange(dataToMac, IV_BYTES, IV_BYTES + EC_KEY_BYTES);
		byte[] dh = ECUtils.dh(privateKey, ephemeralPublicKey);
		if (dh == null) {
			return null;
		}
		byte[] dhSha512 = HashingUtils.SHA512(dh);
		byte[] encryptionKey = Arrays.copyOfRange(dhSha512, 0, KEY_BYTES);
		byte[] authKey = Arrays.copyOfRange(dhSha512, KEY_BYTES, KEY_BYTES + AUTH_KEY_BYTES);

		try {
			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(authKey, HMAC_TYPE));
			mac.update(associatedData);
			byte[] hmacCalculated = mac.doFinal(dataToMac);

			if (!MessageDigest.isEqual(hmac, hmacCalculated)) {
				return null;
			}

			Cipher cipher = Cipher.getInstance(SYM_CIPHER_TYPE);
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(encryptionKey, ENCRYPTION_KEY_TYPE), new IvParameterSpec(dataToMac, 0, IV_BYTES));
			byte[] decrypted = cipher.doFinal(dataToMac, IV_BYTES + EC_KEY_BYTES, dataToMac.length - (IV_BYTES + EC_KEY_BYTES));

			return decrypted;
		} catch (Exception e) {
			log.debug("Error decrypting: " + e.getMessage());
			return null;
		}
	}
}
