package org.cweb.crypto.lib;

import com.google.common.base.Preconditions;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class BinaryUtils {

	public static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

	public static byte[] concat(byte[] first, byte[] second) {
        byte[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

	public static byte[] concat(byte[] first, byte[] second, byte[] third) {
		byte[] result = Arrays.copyOf(first, first.length + second.length + third.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		System.arraycopy(third, 0, result, first.length + second.length, third.length);
		return result;
	}

	public static byte[] intToBytes(final int i) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.putInt(i);
		return bb.array();
	}

	public static void intToBytes(final int i, ByteBuffer bb) {
		bb.putInt(0, i);
	}

	public static int bytesToInt(byte[] bytes) {
		Preconditions.checkArgument(bytes.length == 4);
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		return bb.getInt();
	}

	public static long bytesToLong(byte[] bytes) {
		Preconditions.checkArgument(bytes.length == 8);
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		return bb.getLong();
	}

	public static byte[][] split(byte[] data, int[] sizes) {
		byte[][] result = new byte[sizes.length][];

		int currPos = 0;
		for (int i = 0; i < sizes.length; i++) {
			int size = sizes[i];
			if (currPos + size > data.length) {
				return null;
			}
			byte[] chunk = new byte[size];
			System.arraycopy(data, currPos, chunk, 0, size);
			result[i] = chunk;
			currPos += size;
		}

		return result;
	}
}
