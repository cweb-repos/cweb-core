package org.cweb.crypto.lib;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class DoubleRatchetMessageKeyExtension {
	public static byte[] getMessageKeySelf(DoubleRatchet.State state, int messageOffset) {
		byte[] salt = BinaryUtils.intToBytes(state.messageSerialSelf + messageOffset);
		byte[][] keys = HKDF.computeKdf(salt, state.headerKeySelf, DoubleRatchetInfoConstants.INFO_MESSAGE_KEY, 1, "keyEncr");
		return keys[0];
	}

	public static byte[][] getMessageKeysRemote(DoubleRatchet.State state, int numToGenerateAhead) {
		List<byte[]> keys = new ArrayList<>(state.skippedMessageKeys.size());
		byte[] salt = new byte[DoubleRatchet.KEY_SIZE];
		ByteBuffer saltByteBuffer = ByteBuffer.wrap(salt);
		for (int i = 0; i <= numToGenerateAhead; i++) {
			BinaryUtils.intToBytes(state.messageSerialRemote + i, saltByteBuffer);
			byte[][] keyCurrent = HKDF.computeKdf(salt, state.headerKeyRemote, DoubleRatchetInfoConstants.INFO_MESSAGE_KEY, 1, "keyEncr");
			keys.add(keyCurrent[0]);
			BinaryUtils.intToBytes(i, saltByteBuffer);
			byte[][] keyNext = HKDF.computeKdf(salt, state.nextHeaderKeyRemote, DoubleRatchetInfoConstants.INFO_MESSAGE_KEY, 1, "keyEncr");
			keys.add(keyNext[0]);
		}
		return keys.toArray(new byte[0][]);
	}

	public static byte[][] getMessageKeysRemoteSkipped(DoubleRatchet.State state) {
		List<byte[]> keys = new ArrayList<>(state.skippedMessageKeys.size());
		byte[] salt = new byte[DoubleRatchet.KEY_SIZE];
		ByteBuffer saltByteBuffer = ByteBuffer.wrap(salt);
		for (DoubleRatchet.SkippedMessageKey skippedMessageKey : state.skippedMessageKeys) {
			BinaryUtils.intToBytes(skippedMessageKey.messageSerialRemote, saltByteBuffer);
			byte[][] key = HKDF.computeKdf(salt, skippedMessageKey.headerKeyRemote, DoubleRatchetInfoConstants.INFO_MESSAGE_KEY, 1, "keyEncr");
			keys.add(key[0]);
		}
		return keys.toArray(new byte[0][]);
	}
}
