package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.Arrays;

public class FernetBinary {
    private static final Logger log = LoggerFactory.getLogger(FernetBinary.class);
    private static final String CIPHER_NAME = "Fernet";
    private static final String CIPHER_TYPE = "AES/CBC/PKCS5Padding";
    private static final String HMAC_SHA_256 = "HmacSHA256";
    private static final String KEY_TYPE = "AES";
    private static final int KEY_PART_BYTES = 16;
    private static final int TIME_BYTES = 8;
    private static final int IV_BYTES = 16;
    private static final int HMAC_BYTES = 32;
    private static final byte SIGNATURE_BYTE = (byte) 0x80;

    public static byte[] generateKey() {
        return SecureRandomUtils.generateRandomBytes(KEY_PART_BYTES * 2);
    }

    public static String getCipherName() {
        return CIPHER_NAME;
    }

    public static byte[] encrypt(byte[] key, byte[] data) {
        long now = System.currentTimeMillis();
        byte[] iv = SecureRandomUtils.generateRandomBytes(IV_BYTES);
        return encryptFromParts(key, data, now, iv);
    }

    private static byte[] encryptFromParts(byte[] key, byte[] data, long time, byte[] iv) {
        try {
            SecretKeySpec eks = new SecretKeySpec(key, KEY_PART_BYTES, KEY_PART_BYTES, KEY_TYPE);
            Cipher c = Cipher.getInstance(CIPHER_TYPE);
            c.init(Cipher.ENCRYPT_MODE, eks, new IvParameterSpec(iv));
            byte[] es = c.doFinal(data);

            ByteBuffer bb = ByteBuffer.allocate(1 + TIME_BYTES + iv.length + es.length);
            bb.order(ByteOrder.BIG_ENDIAN);
            bb.put(SIGNATURE_BYTE);
            bb.putLong(time / 1000);
            bb.put(iv);
            bb.put(es);
            byte[] basicParts = bb.array();

            SecretKeySpec hks = new SecretKeySpec(key, 0, KEY_PART_BYTES, HMAC_SHA_256);
            Mac m = Mac.getInstance(HMAC_SHA_256);
            m.init(hks);
            byte[] hmac = m.doFinal(basicParts);
            if (hmac.length != HMAC_BYTES) {
                throw new RuntimeException("Invalid hmac length=" + hmac.length);
            }

            byte[] result = BinaryUtils.concat(basicParts, hmac);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting", e);
        }
    }

    public static byte[] decrypt(byte[] key, byte[] data, Long ttl) {
        if (data.length <= 1 + TIME_BYTES + IV_BYTES + HMAC_BYTES) {
            log.error("Invalid data length " + data.length);
            return null;
        }
        if (data[0] != SIGNATURE_BYTE) {
            log.error("Invalid data header " + data.length);
            return null;
        }
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        long time = bb.getLong(1) * 1000;

        long now = System.currentTimeMillis();
        if (ttl != null && time + ttl < now) {
            return null;
        }

        try {
            byte[] basicParts = Arrays.copyOfRange(data, 0, data.length - HMAC_BYTES);
            byte[] hmac = Arrays.copyOfRange(data, data.length - HMAC_BYTES, data.length);

            SecretKeySpec hks = new SecretKeySpec(key, 0, KEY_PART_BYTES, HMAC_SHA_256);
            Mac m = Mac.getInstance(HMAC_SHA_256);
            m.init(hks);
            byte[] chmac = m.doFinal(basicParts);

            if (!MessageDigest.isEqual(hmac, chmac)) {
                log.error("Hmac mismatch");
                return null;
            }

            SecretKeySpec eks = new SecretKeySpec(key, KEY_PART_BYTES, KEY_PART_BYTES, KEY_TYPE);
            Cipher c = Cipher.getInstance(CIPHER_TYPE);
            c.init(Cipher.DECRYPT_MODE, eks, new IvParameterSpec(data, 1 + TIME_BYTES, IV_BYTES));
            byte[] s = c.doFinal(data, 1 + TIME_BYTES + IV_BYTES, data.length - (1 + TIME_BYTES + IV_BYTES + HMAC_BYTES));

            return s;
        } catch (Exception e) {
            log.info("Error decrypting", e);
            return null;
        }
    }
}
