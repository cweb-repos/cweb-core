package org.cweb.crypto.lib;

import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

public class ECUtils {
	private static final Curve25519 ecCipher = Curve25519.getInstance(Curve25519.BEST);
	private static final String EXTERNAL_SIGNER_NAME = "CwebEcV1";

	public static ECKeyPair generateKeyPair() {
		Curve25519KeyPair pair = ecCipher.generateKeyPair();
		return new ECKeyPair(pair.getPublicKey(), pair.getPrivateKey());
	}

	public static byte[] dh(byte[] ownPrivateKey, byte[] otherPublicKey) {
		try {
			return ecCipher.calculateAgreement(otherPublicKey, ownPrivateKey);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	public static String getSignerName() {
		return EXTERNAL_SIGNER_NAME;
	}

	public static byte[] sign(byte[] privateKey, byte[] message) {
		try {
			return ecCipher.calculateSignature(privateKey, message);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	public static boolean checkSignature(byte[] publicKey, byte[] message, byte[] signature) {
		try {
			return ecCipher.verifySignature(publicKey, message, signature);
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
}
