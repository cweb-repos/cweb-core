package org.cweb.crypto.lib;

import java.util.Arrays;

/**
 * Based on https://whispersystems.org/docs/specifications/x3dh/x3dh.pdf
 * as of June 2017
 */

public class X3DH {
	private static final int KEY_HASH_LENGTH = 8;

	public static class PreKeyBundle {
		public final byte[] idPublicKey;
		public final byte[] preKeyPublic;
		public final byte[] preKeyPublicSignature;

		public PreKeyBundle(byte[] idPublicKey, byte[] preKeyPublic, byte[] preKeyPublicSignature) {
			this.idPublicKey = idPublicKey;
			this.preKeyPublic = preKeyPublic;
			this.preKeyPublicSignature = preKeyPublicSignature;
		}
	}

	public static class InitialMessage {
		public final byte[] idPublicKey;
		public final byte[] ephemeralPublicKey;
		public final byte[] preKeyHash;
		public final byte[] secretKeyHash;

		public InitialMessage(byte[] idPublicKey, byte[] preKeyHash, byte[] ephemeralPublicKey, byte[] secretKeyHash) {
			this.idPublicKey = idPublicKey;
			this.preKeyHash = preKeyHash;
			this.ephemeralPublicKey = ephemeralPublicKey;
			this.secretKeyHash = secretKeyHash;
		}
	}

	public enum Error {
		PRE_KEY_SIGNATURE_MISMATCH,
		INVALID_DATA,
		INVALID_PRE_KEY_HASH,
		SECRET_KEY_HASH_MISMATCH
	}

	public static class InitialMessageGenerationResult {
		public final InitialMessage initialMessage;
		public final byte[] masterSecret;
		public final Error error;

		public InitialMessageGenerationResult(InitialMessage initialMessage, byte[] masterSecret, Error error) {
			this.initialMessage = initialMessage;
			this.masterSecret = masterSecret;
			this.error = error;
		}
	}

	public static class InitialMessageProcessingResult {
		public final byte[] masterSecret;
		public final Error error;

		public InitialMessageProcessingResult(byte[] masterSecret, Error error) {
			this.masterSecret = masterSecret;
			this.error = error;
		}
	}

	public static byte[] hashKey(byte[] key) {
		return Arrays.copyOf(HashingUtils.SHA256(key), KEY_HASH_LENGTH);
	}

	public static PreKeyBundle generatePreKeyBundle(byte[] idPrivateKey, byte[] idPublicKey, byte[] preKeyPublic) {
		return new PreKeyBundle(idPublicKey, preKeyPublic, ECUtils.sign(idPrivateKey, preKeyPublic));
	}

	public static InitialMessageGenerationResult generateInitialMessage(ECKeyPair idKeyPair, ECKeyPair ephemeralKeyPair, PreKeyBundle preKeyBundle) {
		boolean signatureMatches = ECUtils.checkSignature(preKeyBundle.idPublicKey, preKeyBundle.preKeyPublic, preKeyBundle.preKeyPublicSignature);
		if (!signatureMatches) {
			return new InitialMessageGenerationResult(null, null, Error.PRE_KEY_SIGNATURE_MISMATCH);
		}
		byte[] dh1 = ECUtils.dh(idKeyPair.privateKey, preKeyBundle.preKeyPublic);
		byte[] dh2 = ECUtils.dh(ephemeralKeyPair.privateKey, preKeyBundle.idPublicKey);
		byte[] dh3 = ECUtils.dh(ephemeralKeyPair.privateKey, preKeyBundle.preKeyPublic);
		if (dh1 == null || dh2 == null || dh3 == null) {
			return new InitialMessageGenerationResult(null, null, Error.INVALID_DATA);
		}

		byte[] masterSecret = BinaryUtils.concat(dh1, dh2, dh3);

		InitialMessage initialMessage = new InitialMessage(idKeyPair.publicKey, hashKey(preKeyBundle.preKeyPublic), ephemeralKeyPair.publicKey, hashKey(masterSecret));

		return new InitialMessageGenerationResult(initialMessage, masterSecret, null);
	}

	public static InitialMessageProcessingResult processInitialMessage(ECKeyPair idKeyPair, ECKeyPair preKeyPair, InitialMessage initialMessage) {
		if (!Arrays.equals(initialMessage.preKeyHash, hashKey(preKeyPair.publicKey))) {
			return new InitialMessageProcessingResult(null, Error.INVALID_PRE_KEY_HASH);
		}
		byte[] dh1 = ECUtils.dh(preKeyPair.privateKey, initialMessage.idPublicKey);
		byte[] dh2 = ECUtils.dh(idKeyPair.privateKey, initialMessage.ephemeralPublicKey);
		byte[] dh3 = ECUtils.dh(preKeyPair.privateKey, initialMessage.ephemeralPublicKey);
		if (dh1 == null || dh2 == null || dh3 == null) {
			return new InitialMessageProcessingResult(null, Error.INVALID_DATA);
		}

		byte[] masterSecret = BinaryUtils.concat(dh1, dh2, dh3);

		if (!Arrays.equals(hashKey(masterSecret), initialMessage.secretKeyHash)) {
			return new InitialMessageProcessingResult(null, Error.SECRET_KEY_HASH_MISMATCH);
		}

		return new InitialMessageProcessingResult(masterSecret, null);
	}
}
