package org.cweb.crypto.lib;

public class ECKeyPair {
	public final byte[] publicKey;
	public final byte[] privateKey;

	public ECKeyPair(byte[] publicKey, byte[] privateKey) {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}
}
