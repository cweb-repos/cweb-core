package org.cweb.crypto.lib;

import java.security.MessageDigest;
import java.util.Arrays;

public class HashingUtils {
	public static byte[] SHA256(byte[] data) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(data);
			byte[] digest = md.digest();
			return digest;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] SHA512(byte[] data) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(data);
			byte[] digest = md.digest();
			return digest;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] SHA512_256(byte[] data) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(data);
			byte[] digest = md.digest();
			return Arrays.copyOf(digest, 32);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
