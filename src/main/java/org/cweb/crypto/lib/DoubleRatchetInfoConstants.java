package org.cweb.crypto.lib;

import java.nio.charset.StandardCharsets;

public class DoubleRatchetInfoConstants {
	static final byte[] INFO_SESSION = "CwebSession".getBytes(StandardCharsets.UTF_8);
	static final byte[] INFO_MESSAGE = "CwebMessage".getBytes(StandardCharsets.UTF_8);
	static final byte[] INFO_HEADERS = "CwebMessageHeader".getBytes(StandardCharsets.UTF_8);
	static final byte[] INFO_MESSAGE_KEY = "CwebFrameName".getBytes(StandardCharsets.UTF_8);
}
