package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * NOT thread-safe
 */
public class RSAUtils {
	private static final Logger log = LoggerFactory.getLogger(RSAUtils.class);

	private static final String EXTERNAL_CIPHER_NAME = "CwebRsaV1";
	private static final String EXTERNAL_SIGNER_NAME = "CwebRsaV1";
	private static final int KEY_SIZE_BITS = 2048;
	private static final String KEY_FACTORY_TYPE = "RSA";
	private static final String CIPHER_TYPE = "RSA/ECB/OAEPWithSHA1AndMGF1Padding";
	private static final String SIGNER_TYPE = "SHA256withRSA";

	private static final KeyFactory keyFactory;
	private static final Cipher cipher;

	static {
		try {
			keyFactory = KeyFactory.getInstance(KEY_FACTORY_TYPE);
			cipher = Cipher.getInstance(CIPHER_TYPE);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static KeyPair generateKeyPair() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(KEY_SIZE_BITS);
			KeyPair key = keyGen.genKeyPair();
			return key;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] getPublicKeyX509Encoded(byte[] key) {
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(key);
		return x509EncodedKeySpec.getEncoded();
	}

	public static byte[] getPrivateKeyPKCS8Encoded(byte[] key) {
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(key);
		return pkcs8EncodedKeySpec.getEncoded();
	}

	public static PublicKey constructPublicKey(byte[] publicKey) {
		try {
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKey);
			return keyFactory.generatePublic(publicKeySpec);
		} catch (Exception e) {
			log.error("Exception constructing key", e);
			return null;
		}
	}

	public static PrivateKey constructPrivateKey(byte[] privateKey) {
		try {
			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKey);
			return keyFactory.generatePrivate(privateKeySpec);
		} catch (Exception e) {
			log.error("Exception constructing key", e);
			return null;
		}
	}

	public static String getSignerName() {
		return EXTERNAL_SIGNER_NAME;
	}

	public static byte[] sign(PrivateKey privateKey, byte[] data) {
		try {
			Signature signer = Signature.getInstance(SIGNER_TYPE);
			signer.initSign(privateKey);
			signer.update(data);
			byte[] signature = signer.sign();
			return signature;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean verifySignature(PublicKey publicKey, byte[] data, byte[] signature) {
		try {
			Signature signer = Signature.getInstance(SIGNER_TYPE);
			signer.initVerify(publicKey);
			signer.update(data);
			return signer.verify(signature);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getCipherName() {
		return EXTERNAL_CIPHER_NAME;
	}

	public static synchronized byte[] encrypt(PublicKey publicKey, byte[] plaintext) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(plaintext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static synchronized byte[] decrypt(PrivateKey privateKey, byte[] ciphertext) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return cipher.doFinal(ciphertext);
		} catch (Exception e) {
			return null;
		}
	}
}
