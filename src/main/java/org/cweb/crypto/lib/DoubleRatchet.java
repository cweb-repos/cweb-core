package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Based on https://whispersystems.org/docs/specifications/doubleratchet/doubleratchet.pdf
 * as of June 2017
 */

public class DoubleRatchet {
	private static final Logger log = LoggerFactory.getLogger(DoubleRatchet.class);

	private static final String HMAC_TYPE = "HmacSHA256";
	static final int KEY_SIZE = 32;
	private static final byte[] DEFAULT_SALT = new byte[KEY_SIZE];

	private static final byte[] TYPE_MESSAGE_KEY = new byte[]{0x1};
	private static final byte[] TYPE_CHAIN_KEY = new byte[]{0x2};
	private static final int MAX_SKIPPED_KEYS = 100;

	public static class SkippedMessageKey {
		public final byte[] headerKeyRemote;  // HKr
		public final int messageSerialRemote;  // Nr
		public final byte[] messageKeyRemote;  // MKr

		public SkippedMessageKey(byte[] headerKeyRemote, int messageSerialRemote, byte[] messageKeyRemote) {
			this.headerKeyRemote = headerKeyRemote;
			this.messageSerialRemote = messageSerialRemote;
			this.messageKeyRemote = messageKeyRemote;
		}
	}

	public static class State {
		public ECKeyPair dhKeyPairSelf;  // DHs: DH Ratchet key pair (the "sending" or "self" ratchet key)
		public byte[] dhPublicKeyRemote;  // DHr: DH Ratchet public key (the "received" or "remote" key)
		public byte[] rootKey;  // RK: 32-byte Root Key
		public byte[] chainKeySelf, chainKeyRemote;  // CKs, CKr: 32-byte Chain Keys for sending and receiving

		public int messageSerialSelf, messageSerialRemote;  // Ns, Nr: Message numbers for sending and receiving
		public int messageSerialPrevChainSelf;  // PN: Number of messages in previous sending chain

		public byte[] headerKeySelf, headerKeyRemote;  // HKs, HKr: 32-byte Header Keys for sending and receiving
		public byte[] nextHeaderKeySelf, nextHeaderKeyRemote;  // NHKs, NHKr: 32-byte Next Header Keys for sending and receiving
		public List<SkippedMessageKey> skippedMessageKeys;  // MKSKIPPED: Dictionary of skipped-over message keys, indexed by header key and message number. Raises an exception if too many elements are stored.
	}

	public static class InitialSharedSecrets {
		final byte[] sharedKey;  // SK: 32-byte Shared Key
		final byte[] headerKey, nextHeaderKey;  // HK, NHK: 32-byte Header Key and Next Header Key

		public InitialSharedSecrets(byte[] sharedKey, byte[] headerKey, byte[] nextHeaderKey) {
			this.sharedKey = sharedKey;
			this.headerKey = headerKey;
			this.nextHeaderKey = nextHeaderKey;
		}
	}

	private static class Header {
		final byte[] publicKey;
		final int messageSerial;
		final int messageSerialPrevChain;

		public Header(byte[] publicKey, int messageSerial, int messageSerialPrevChain) {
			this.publicKey = publicKey;
			this.messageSerial = messageSerial;
			this.messageSerialPrevChain = messageSerialPrevChain;
		}

		public static Header deserialize(byte[] serialized) {
			if (serialized.length != 4 + 4 + KEY_SIZE) {
				return null;
			}
			ByteBuffer bb = ByteBuffer.wrap(serialized);
			bb.order(ByteOrder.BIG_ENDIAN);
			int messageSerial = bb.getInt();
			int messageSerialPrevChain = bb.getInt();
			byte[] publicKey = Arrays.copyOfRange(serialized, 4 + 4, serialized.length);
			return new Header(publicKey, messageSerial, messageSerialPrevChain);
		}

		public byte[] serialize() {
			ByteBuffer bb = ByteBuffer.allocate(4 + 4 + publicKey.length);
			bb.order(ByteOrder.BIG_ENDIAN);
			bb.putInt(messageSerial);
			bb.putInt(messageSerialPrevChain);
			bb.put(publicKey);
			return bb.array();
		}
	}

	public static class Message {
		public final byte[] encryptedHeader;
		public final byte[] encryptedData;

		public Message(byte[] encryptedHeader, byte[] encryptedData) {
			this.encryptedHeader = encryptedHeader;
			this.encryptedData = encryptedData;
		}
	}

	private static byte[] kdfChainKey(byte[] key, byte[] type) {
		try {
			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(key, HMAC_TYPE));
			return mac.doFinal(type);
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}

	public static InitialSharedSecrets generateInitialSharedSecrets(byte[] seedSecret) {
		byte[][] derivedKeys = HKDF.computeKdf(DEFAULT_SALT, seedSecret, DoubleRatchetInfoConstants.INFO_SESSION, 3, "initSS");
		return new InitialSharedSecrets(derivedKeys[0], derivedKeys[1], derivedKeys[2]);
	}

	public static State initStateFirst(InitialSharedSecrets initialSharedSecrets, byte[] otherPublicKey) {
		State state = new State();

		state.dhKeyPairSelf = ECUtils.generateKeyPair();
		state.dhPublicKeyRemote = otherPublicKey;

		byte[] dhOut = ECUtils.dh(state.dhKeyPairSelf.privateKey, state.dhPublicKeyRemote);
		byte[][] derivedKeys = HKDF.computeKdf(initialSharedSecrets.sharedKey, dhOut, DoubleRatchetInfoConstants.INFO_SESSION, 3, "initFirst");
		state.rootKey = derivedKeys[0];
		state.chainKeySelf = derivedKeys[1];
		state.nextHeaderKeySelf = derivedKeys[2];

		state.chainKeyRemote = null;
		state.messageSerialSelf = 0;
		state.messageSerialRemote = 0;
		state.messageSerialPrevChainSelf = 0;

		state.headerKeySelf = initialSharedSecrets.headerKey;
		state.headerKeyRemote = null;
		state.nextHeaderKeyRemote = initialSharedSecrets.nextHeaderKey;

		state.skippedMessageKeys = new ArrayList<>();

		return state;
	}

	public static State initStateSecond(ECKeyPair ownKeyPair, InitialSharedSecrets initialSharedSecrets) {
		State state = new State();

		state.dhKeyPairSelf = ownKeyPair;
		state.dhPublicKeyRemote = null;

		state.rootKey = initialSharedSecrets.sharedKey;
		state.chainKeySelf = null;
		state.chainKeyRemote = null;

		state.messageSerialSelf = 0;
		state.messageSerialRemote = 0;
		state.messageSerialPrevChainSelf = 0;

		state.headerKeySelf = null;
		state.headerKeyRemote = null;
		state.nextHeaderKeySelf = initialSharedSecrets.nextHeaderKey;
		state.nextHeaderKeyRemote = initialSharedSecrets.headerKey;

		state.skippedMessageKeys = new ArrayList<>();

		return state;
	}

	public static Message ratchetEncrypt(State state, byte[] data, byte[] associatedData) {
		Header header = new Header(state.dhKeyPairSelf.publicKey, state.messageSerialSelf, state.messageSerialPrevChainSelf);

		byte[][] headerKeys = HKDF.computeKdf(DEFAULT_SALT, state.headerKeySelf, DoubleRatchetInfoConstants.INFO_HEADERS, 2, "headerEncr");
		byte[] headerSerialized = header.serialize();
		byte[] encryptedHeader = AEAD.encrypt(headerKeys[0], headerKeys[1], headerSerialized, associatedData);

		// Updating state
		state.chainKeySelf = kdfChainKey(state.chainKeySelf, TYPE_CHAIN_KEY);
		state.messageSerialSelf += 1;

		byte[] messageKey = kdfChainKey(state.chainKeySelf, TYPE_MESSAGE_KEY);
		byte[][] derivedKeys = HKDF.computeKdf(DEFAULT_SALT, messageKey, DoubleRatchetInfoConstants.INFO_MESSAGE, 2, "messageEncr");
		byte[] encryptedData = AEAD.encrypt(derivedKeys[0], derivedKeys[1], data, BinaryUtils.concat(associatedData, encryptedHeader));

		return new Message(encryptedHeader, encryptedData);
	}

	private static Header tryDecryptHeader(byte[] key, byte[] encryptedHeader, byte[] associatedData) {
		byte[][] headerKeys = HKDF.computeKdf(DEFAULT_SALT, key, DoubleRatchetInfoConstants.INFO_HEADERS, 2, "headerDecr");
		byte[] headerSerialized = AEAD.decrypt(headerKeys[0], headerKeys[1], encryptedHeader, associatedData);
		if (headerSerialized == null) {
			return null;
		}
		Header header = Header.deserialize(headerSerialized);
		return header;
	}

	private static SkippedMessageKey findKey(List<SkippedMessageKey> skippedMessageKeys, byte[] headerKey, int messageSerial) {
		for (SkippedMessageKey skippedKey : skippedMessageKeys) {
			if (skippedKey.messageSerialRemote == messageSerial && Arrays.equals(skippedKey.headerKeyRemote, headerKey)) {
				return skippedKey;
			}
		}
		return null;
	}

	private static void addSkippedKey(List<SkippedMessageKey> messageKeys, byte[] headerKeyRemote, int messageSerialRemote, byte[] messageKeyRemote) {
		SkippedMessageKey existingEntry = findKey(messageKeys, headerKeyRemote, messageSerialRemote);
		if (existingEntry != null) {
			if (!Arrays.equals(existingEntry.messageKeyRemote, messageKeyRemote)) {
				log.error("Mismatching key: " + messageSerialRemote);
			} else {
				return;
			}
		}
		SkippedMessageKey newEntry = new SkippedMessageKey(headerKeyRemote, messageSerialRemote, messageKeyRemote);
		messageKeys.add(newEntry);
	}

	private static void skipMessageKeys(State state, int until) {
		if (state.messageSerialRemote + MAX_SKIPPED_KEYS < until) {
			log.warn("Exceeded MAX_SKIPPED_KEYS: " + state.messageSerialRemote + ":" + until);
			return;
		}
		if (state.chainKeyRemote != null) {
			while (state.messageSerialRemote < until) {
				state.chainKeyRemote = kdfChainKey(state.chainKeyRemote, TYPE_CHAIN_KEY);
				byte[] messageKeyRemote = kdfChainKey(state.chainKeyRemote, TYPE_MESSAGE_KEY);
				addSkippedKey(state.skippedMessageKeys, state.headerKeyRemote, state.messageSerialRemote, messageKeyRemote);
				state.messageSerialRemote++;
			}
		}
		while (state.skippedMessageKeys.size() > MAX_SKIPPED_KEYS) {
			log.warn("Deleting skipped key");
			state.skippedMessageKeys.remove(0);
		}
	}

	private static void dhRatchet(State state, Header header, byte[] info) {
		state.messageSerialPrevChainSelf = state.messageSerialSelf;
		state.messageSerialSelf = 0;
		state.messageSerialRemote = 0;
		state.headerKeySelf = state.nextHeaderKeySelf;
		state.headerKeyRemote = state.nextHeaderKeyRemote;
		state.dhPublicKeyRemote = header.publicKey;

		byte[] dhOutReceiving = ECUtils.dh(state.dhKeyPairSelf.privateKey, state.dhPublicKeyRemote);
		byte[][] derivedKeysReceiving = HKDF.computeKdf(state.rootKey, dhOutReceiving, info, 3, "dhRatchetRecv");
		state.rootKey = derivedKeysReceiving[0];
		state.chainKeyRemote = derivedKeysReceiving[1];
		state.nextHeaderKeyRemote = derivedKeysReceiving[2];

		state.dhKeyPairSelf = ECUtils.generateKeyPair();

		byte[] dhOutSending = ECUtils.dh(state.dhKeyPairSelf.privateKey, state.dhPublicKeyRemote);
		byte[][] derivedKeysSending = HKDF.computeKdf(state.rootKey, dhOutSending, info, 3, "dhRatchetSend");
		state.rootKey = derivedKeysSending[0];
		state.chainKeySelf = derivedKeysSending[1];
		state.nextHeaderKeySelf = derivedKeysSending[2];
	}

	public static byte[] ratchetDecrypt(State state, Message message, byte[] associatedData) {
		boolean dhRatchetStep = false;
		Header header = null;
		if (state.headerKeyRemote != null) {
			header = tryDecryptHeader(state.headerKeyRemote, message.encryptedHeader, associatedData);
			if (header != null && header.messageSerial < state.messageSerialRemote) {
				header = null;
			}
		}
		if (header == null && state.nextHeaderKeyRemote != null) {
			header = tryDecryptHeader(state.nextHeaderKeyRemote, message.encryptedHeader, associatedData);
			if (header != null) {
				dhRatchetStep = true;
			}
		}
		SkippedMessageKey usingSkippedKey = null;
		if (header == null) {
			for (SkippedMessageKey skippedKey : state.skippedMessageKeys) {
				header = tryDecryptHeader(skippedKey.headerKeyRemote, message.encryptedHeader, associatedData);
				if (header != null) {
					if (header.messageSerial == skippedKey.messageSerialRemote) {
						usingSkippedKey = skippedKey;
						break;
					} else {
						header = null;
					}
				}
			}
		}

		if (header == null) {
			return null;
		}

		byte[] messageKey;

		// Updating state
		if (usingSkippedKey == null) {
			if (dhRatchetStep) {
				skipMessageKeys(state, header.messageSerialPrevChain);
				dhRatchet(state, header, DoubleRatchetInfoConstants.INFO_SESSION);
			}
			skipMessageKeys(state, header.messageSerial);
			state.chainKeyRemote = kdfChainKey(state.chainKeyRemote, TYPE_CHAIN_KEY);
			state.messageSerialRemote++;
			messageKey = kdfChainKey(state.chainKeyRemote, TYPE_MESSAGE_KEY);
		} else {
			state.skippedMessageKeys.remove(usingSkippedKey);
			messageKey = usingSkippedKey.messageKeyRemote;
		}

		byte[][] derivedKeys = HKDF.computeKdf(DEFAULT_SALT, messageKey, DoubleRatchetInfoConstants.INFO_MESSAGE, 2, "messageDecr");
		byte[] decryptedData = AEAD.decrypt(derivedKeys[0], derivedKeys[1], message.encryptedData, BinaryUtils.concat(associatedData, message.encryptedHeader));

		return decryptedData;
	}
}
