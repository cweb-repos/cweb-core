package org.cweb.crypto.lib;

import org.cweb.schemas.keys.PublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class SequenceGenerator {
	private static final Logger log = LoggerFactory.getLogger(SequenceGenerator.class);

	public static byte[] encodeWithPublicKey(PublicKey publicKey, byte[] seed, int elementSerial, int hashLengthBytes) {
		byte[] data = BinaryUtils.concat(publicKey.getPublicKey(), seed, BinaryUtils.intToBytes(elementSerial));
		return encode(data, hashLengthBytes);
	}

	private static byte[] encode(byte[] seed, int hashLengthBytes) {
		if (hashLengthBytes > 512/8) {
			return null;
		}
		byte[] encrypted = HashingUtils.SHA512(seed);
		byte[] sequenceNumberHash = Arrays.copyOf(encrypted, hashLengthBytes);
		return sequenceNumberHash;
	}

	public static byte[] encode(byte[] seed, int elementSerial, int hashLengthBytes) {
		byte[] data = BinaryUtils.concat(seed, BinaryUtils.intToBytes(elementSerial));
		return encode(data, hashLengthBytes);
	}

	public static byte[] encode(byte[] seed1, byte[] seed2, int hashLengthBytes) {
		byte[] data = BinaryUtils.concat(seed1, seed2);
		return encode(data, hashLengthBytes);
	}

	public static byte[] encode(byte[] seed1, byte[] seed2, int elementSerial, int hashLengthBytes) {
		byte[] data = BinaryUtils.concat(seed1, seed2, BinaryUtils.intToBytes(elementSerial));
		return encode(data, hashLengthBytes);
	}
}
