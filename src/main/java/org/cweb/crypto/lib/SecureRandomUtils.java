package org.cweb.crypto.lib;

import java.security.SecureRandom;

public class SecureRandomUtils {
	private static final String SECURE_RANDOM_TYPE = "SHA1PRNG";

	private static final SecureRandom secureRandom;

	static {
		try {
			secureRandom = SecureRandom.getInstance(SECURE_RANDOM_TYPE);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void nextBytes(byte[] dest) {
		secureRandom.nextBytes(dest);
	}

	public static byte[] generateRandomBytes(int length) {
		byte[] data = new byte[length];
		SecureRandomUtils.nextBytes(data);
		return data;
	}
}
