package org.cweb.crypto.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HKDF {
	private static final Logger log = LoggerFactory.getLogger(HKDF.class);
	private static final String HMAC_TYPE = "HmacSHA256";

	public static byte[][] computeKdf(byte[] salt, byte[] inputKey, byte[] info, int outputChunks, String debugStr) {
		try {
			Mac mac = Mac.getInstance(HMAC_TYPE);
			mac.init(new SecretKeySpec(salt, HMAC_TYPE));
			byte[] prk = mac.doFinal(inputKey);

			byte[][] result = new byte[outputChunks][];
			for (int i = 0; i < outputChunks; i++) {
				mac.init(new SecretKeySpec(prk, HMAC_TYPE));
				if (i > 0) {
					mac.update(result[i - 1]);
				}
				mac.update(info);
				mac.update((byte) (i + 1));
				byte[] chunk = mac.doFinal();
				result[i] = chunk;
			}
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
