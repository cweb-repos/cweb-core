package org.cweb.crypto;

import org.cweb.identity.RemoteIdentityFetcher;

public class CryptoContext {
	CryptoHelper cryptoHelper;
	RemoteIdentityFetcher remoteIdentityFetcher;

	public static CryptoContext create() {
		return new CryptoContext();
	}

	public CryptoContext setCryptoHelper(CryptoHelper cryptoHelper) {
		this.cryptoHelper = cryptoHelper;
		return this;
	}

	public CryptoContext setRemoteIdentityFetcher(RemoteIdentityFetcher remoteIdentityFetcher) {
		this.remoteIdentityFetcher = remoteIdentityFetcher;
		return this;
	}
}
