package org.cweb.crypto;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.lib.AEAD;
import org.cweb.crypto.lib.ECKeyPair;
import org.cweb.crypto.lib.ECUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.crypto.lib.RSAUtils;
import org.cweb.crypto.lib.SecureRandomUtils;
import org.cweb.crypto.lib.X3DH;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.crypto.X3DHInitialMessage;
import org.cweb.schemas.crypto.X3DHPreKeyBundle;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.keys.KeyType;
import org.cweb.schemas.keys.PublicKey;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.CryptoEnvelopeContent;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.SignedEnvelope;
import org.cweb.schemas.wire.SignedPayload;
import org.cweb.schemas.wire.SymmetricEncryptedEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class CryptoHelper {
	private static final Logger log = LoggerFactory.getLogger(CryptoHelper.class);
	private static final int KEY_HASH_LENGTH = 8;
	private static final int DATA_HASH_LENGTH = 32;
	private static final String SERVICE_NAME = "Core";

	private final String tracePrefix;
	private final IdentityCryptoService identityCryptoService;

	public CryptoHelper(String tracePrefix, IdentityCryptoService identityCryptoService) {
		this.identityCryptoService = identityCryptoService;
		this.tracePrefix = tracePrefix;
	}

	public byte[] getOwnId() {
		return identityCryptoService.getOwnId();
	}

	public boolean isOwnId(byte[] id) {
		return Arrays.equals(id, identityCryptoService.getOwnId());
	}

	public PublicKey getOwnRSAPublicKey() {
		return identityCryptoService.getRSAPublicKey();
	}

	public PublicKey getOwnECPublicKey() {
		return identityCryptoService.getECPublicKey();
	}

	public CryptoEnvelope signTypedPayload(TypedPayload typedPayload, byte[] recipientId, Long ttl) {
		byte[] typedPayloadEnvelopeSerialized = ThriftUtils.serialize(new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(typedPayload)));
		SignedEnvelope signedEnvelope = identityCryptoService.createSignedEnvelope(recipientId, typedPayloadEnvelopeSerialized, ttl, IdentityCryptoService.SignerType.EC);
		log.trace(tracePrefix + " Created signed envelope");
		return new CryptoEnvelope(CryptoEnvelopeContent.signedEnvelope(signedEnvelope));
	}

	public CryptoEnvelope createIdProofEnvelope() {
		TypedPayload idProofPayload = TypedPayloadUtils.wrapCustom(getOwnId(), SERVICE_NAME, "idProof", null);
		byte[] typedPayloadEnvelopeSerialized = ThriftUtils.serialize(new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(idProofPayload)));
		SignedEnvelope signedEnvelope;
		if (Arrays.equals(getOwnId(), IdentityCryptoService.idFromPublicKey(getOwnECPublicKey()))) {
			signedEnvelope = identityCryptoService.createSignedEnvelope(null, typedPayloadEnvelopeSerialized, null, IdentityCryptoService.SignerType.EC);
		} else if (Arrays.equals(getOwnId(), IdentityCryptoService.idFromPublicKey(getOwnRSAPublicKey()))) {
			signedEnvelope = identityCryptoService.createSignedEnvelope(null, typedPayloadEnvelopeSerialized, null, IdentityCryptoService.SignerType.RSA);
		} else {
			return null;
		}
		return new CryptoEnvelope(CryptoEnvelopeContent.signedEnvelope(signedEnvelope));
	}

	public static boolean verifyIdProofEnvelope(IdentityDescriptor identityDescriptor, byte[] envelopeData) {
		byte[] id = identityDescriptor.getId();
		Decoded<DecodedTypedPayload> idProofData = CryptoHelper.decodeCryptoEnvelope(envelopeData, CryptoEnvelopeDecodingParams.create().setSignerIdentityDescriptor(identityDescriptor), CryptoContext.create());
		if (idProofData.getError() != null || idProofData.getData() == null) {
			log.trace("Error verifying identity proof envelope for " + Utils.getDebugStringFromId(id) + ": " + idProofData.getError());
			return false;
		}
		DecodedTypedPayload decodedPayload = idProofData.getData();
		TypedPayload payload = decodedPayload.getPayload();
		SignatureMetadata signatureMetadata = decodedPayload.getSignatureMetadata();
		if (!Arrays.equals(signatureMetadata.getSignerId(), id)) {
			return false;
		}
		String expectedSignerType;
		if (identityDescriptor.getEcPublicKey() != null && Arrays.equals(id, IdentityCryptoService.idFromPublicKey(identityDescriptor.getEcPublicKey()))) {
			expectedSignerType = ECUtils.getSignerName();
		} else if (identityDescriptor.getRsaPublicKey() != null && Arrays.equals(id, IdentityCryptoService.idFromPublicKey(identityDescriptor.getRsaPublicKey()))) {
			expectedSignerType = RSAUtils.getSignerName();
		} else {
			return false;
		}
		if (!expectedSignerType.equals(signatureMetadata.getSigningAlgorightm())) {
			return false;
		}
		if (!Arrays.equals(payload.getData(), id)) {
			return false;
		}
		return true;
	}

	public CryptoEnvelope signAndEncryptFor(TypedPayload typedPayload, byte[] recipientId, PublicKey recipientPublicKey, Long ttl) {
		byte[] typedPayloadEnvelopeSerialized = ThriftUtils.serialize(new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(typedPayload)));
		CryptoEnvelope signedEnvelope = new CryptoEnvelope(CryptoEnvelopeContent.signedEnvelope(identityCryptoService.createSignedEnvelope(recipientId, typedPayloadEnvelopeSerialized, ttl, IdentityCryptoService.SignerType.EC)));
		byte[] signedEnvelopeSerialized = ThriftUtils.serialize(signedEnvelope);
		log.trace(tracePrefix + " Signed for " + Utils.getDebugStringFromKey(recipientPublicKey));
		return new CryptoEnvelope(CryptoEnvelopeContent.pkEncryptedEnvelope(identityCryptoService.encryptFor(recipientPublicKey, signedEnvelopeSerialized)));
	}

	public static Decoded<DecodedTypedPayload> decodeCryptoEnvelope(byte[] cryptoEnvelopeSerialized, CryptoEnvelopeDecodingParams params, CryptoContext context) {
		byte[] nextEnvelopeSerialized = cryptoEnvelopeSerialized;
		SignatureMetadata signatureMetadata = null;
		while (true) {
			CryptoEnvelope currentEnvelope = ThriftUtils.deserializeSafe(nextEnvelopeSerialized, CryptoEnvelope.class);
			if (currentEnvelope == null) {
				return new Decoded<>(Decoded.Error.CRYPTO_ENVELOPE_DESERIALIZATION);
			}

			CryptoEnvelopeContent content = currentEnvelope.getContent();
			if (content.isSetTypedPayload()) {
				return new Decoded<>(new DecodedTypedPayload(signatureMetadata, content.getTypedPayload()));
			}

			if (content.isSetSignedEnvelope()) {
				Decoded<Pair<SignatureMetadata, byte[]>> innerPayload = extractSignedPayload(content.getSignedEnvelope(), params, context);
				if (innerPayload.getError() != null) {
					return new Decoded<>(innerPayload.getError());
				}
				signatureMetadata = innerPayload.getData().getLeft();
				nextEnvelopeSerialized = innerPayload.getData().getRight();
			} else if (content.isSetPkEncryptedEnvelope()) {
				if (context.cryptoHelper == null) {
					return new Decoded<>(Decoded.Error.INVALID_CONTEXT);
				}
				Decoded<byte[]> innerPayload = context.cryptoHelper.identityCryptoService.decrypt(content.getPkEncryptedEnvelope());
				if (innerPayload.getError() != null) {
					return new Decoded<>(innerPayload.getError());
				}
				nextEnvelopeSerialized = innerPayload.getData();
			} else if (content.isSetSymmetricEncryptedEnvelope()) {
				if (params.symmetricDecryptionKey == null) {
					return new Decoded<>(Decoded.Error.SYMMETRIC_KEY_PARAM_MISSING);
				}
				Decoded<byte[]> innerPayload = decryptSymmetric(params.symmetricDecryptionKey, content.getSymmetricEncryptedEnvelope(), params.symmetricAssociatedData);
				if (innerPayload.getError() != null) {
					return new Decoded<>(innerPayload.getError());
				}
				nextEnvelopeSerialized = innerPayload.getData();
			} else {
				return new Decoded<>(Decoded.Error.CRYPTO_ENVELOPE_TYPE);
			}
		}
	}

	private static Decoded<Pair<SignatureMetadata, byte[]>> extractSignedPayload(SignedEnvelope signedEnvelope, CryptoEnvelopeDecodingParams params, CryptoContext context) {
		SignedPayload signedPayload = ThriftUtils.deserializeSafe(signedEnvelope.getSignedPayload(), SignedPayload.class);
		if (signedPayload == null) {
			return new Decoded<>(Decoded.Error.SIGNED_PAYLOAD_DESERIALIZATION);
		}

		SignatureMetadata signatureMetadata = signedPayload.getMetadata();

		if (params.skipAllSignatureChecks) {
			return new Decoded<>(Pair.of(signatureMetadata, signedPayload.getPayload()));
		}

		IdentityDescriptor signerIdentityDescriptor = params.signerIdentityDescriptor;
		if (signerIdentityDescriptor == null && params.fetchSignerIfNeeded) {
			if (context.remoteIdentityFetcher == null) {
				return new Decoded<>(Decoded.Error.INVALID_CONTEXT);
			}
			LocalIdentityDescriptorState signerIdentityDescriptorState = null;
			if (params.payloadSignerExtractor != null) {
				CryptoEnvelope payloadEnvelope = ThriftUtils.deserializeSafe(signedPayload.getPayload(), CryptoEnvelope.class);
				if (payloadEnvelope != null && payloadEnvelope.getContent().isSetTypedPayload()) {
					TypedPayload typedPayload = payloadEnvelope.getContent().getTypedPayload();
					IdentityReference signerIdentityReference = params.payloadSignerExtractor.apply(typedPayload);
					if (Arrays.equals(signatureMetadata.getSignerId(), signerIdentityReference.getId())) {
						signerIdentityDescriptorState = context.remoteIdentityFetcher.fetch(signerIdentityReference);
					} else {
						log.info("Mismatching id extracted from payload " + Utils.getDebugStringFromId(signerIdentityReference.getId()) + " while signer is " + Utils.getDebugStringFromId(signatureMetadata.getSignerId()));
					}
				}
			}
			if (signerIdentityDescriptorState == null) {
				signerIdentityDescriptorState = context.remoteIdentityFetcher.fetch(signatureMetadata.getSignerId());
			}
			if (signerIdentityDescriptorState == null) {
				return new Decoded<>(Decoded.Error.SIGNER_IDENTITY_FETCH_FAILED);
			}
			signerIdentityDescriptor = signerIdentityDescriptorState.getDescriptor();
		}
		if (signerIdentityDescriptor == null && params.isSelfSignedIdentityDescriptor) {
			CryptoEnvelope payloadEnvelope = ThriftUtils.deserializeSafe(signedPayload.getPayload(), CryptoEnvelope.class);
			if (payloadEnvelope != null && payloadEnvelope.getContent().isSetTypedPayload()) {
				TypedPayload typedPayload = payloadEnvelope.getContent().getTypedPayload();
				Pair<IdentityDescriptor, String> signerIdentityDescriptorUnwrapped = TypedPayloadUtils.unwrap(typedPayload, IdentityDescriptor.class, null);
				signerIdentityDescriptor = signerIdentityDescriptorUnwrapped.getLeft();
			}
		}
		if (signerIdentityDescriptor == null) {
			return new Decoded<>(Decoded.Error.SIGNER_IDENTITY_NOT_FOUND);
		}

		if (!Arrays.equals(signatureMetadata.getRecipientId(), params.idForSignatureTargetVerification)) {
			return new Decoded<>(Decoded.Error.SIGNATURE_RECIPIENT_ID_MISMATCH);
		}

		if (!Arrays.equals(signerIdentityDescriptor.getId(), signatureMetadata.getSignerId())) {
			return new Decoded<>(Decoded.Error.SIGNER_IDENTITY_MISMATCH);
		}

		if (!IdentityCryptoService.verifySignature(signerIdentityDescriptor, signatureMetadata.getSigningAlgorightm(), signedEnvelope)) {
			return new Decoded<>(Decoded.Error.SIGNATURE_VALIDATION);
		}

		// TODO: check validUntil
		return new Decoded<>(Pair.of(signatureMetadata, signedPayload.getPayload()));
	}

	public byte[] generateRandomBytes(int length) {
		log.trace(tracePrefix + " Generated " + length + " random bytes");
		return SecureRandomUtils.generateRandomBytes(length);
	}

	public KeyPair generateNewECKeyPair() {
		log.trace(tracePrefix + " Generated EC key pair");
		ECKeyPair ecKeyPair = ECUtils.generateKeyPair();
		KeyPair keyPair = new KeyPair(KeyType.EC25519_256, ByteBuffer.wrap(ecKeyPair.publicKey), ByteBuffer.wrap(ecKeyPair.privateKey));
		return keyPair;
	}

	private static byte[] hashKey(byte[] key) {
		return Arrays.copyOf(HashingUtils.SHA256(key), KEY_HASH_LENGTH);
	}

	public static byte[] hashData(byte[] data) {
		return Arrays.copyOf(HashingUtils.SHA256(data), DATA_HASH_LENGTH);
	}

	public static int getAEADKeyLengthInBytes() {
		return AEAD.getCompositeKeyLengthInBytes();
	}

	public static byte[] generateAEADKey() {
		return AEAD.generateCompositeKey();
	}

	static SymmetricEncryptedEnvelope encryptSymmetricRaw(byte[] key, byte[] data, byte[] associatedData) {
		byte[] encryptedData = AEAD.encrypt(key, data, associatedData);
		return new SymmetricEncryptedEnvelope(AEAD.getCipherName(), ByteBuffer.wrap(hashKey(key)), ByteBuffer.wrap(encryptedData));
	}

	public static CryptoEnvelope encryptSymmetric(byte[] key, TypedPayload typedPayload, byte[] associatedData) {
		byte[] typedPayloadEnvelopeSerialized = ThriftUtils.serialize(new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(typedPayload)));
		SymmetricEncryptedEnvelope envelope = encryptSymmetricRaw(key, typedPayloadEnvelopeSerialized, associatedData);
		return new CryptoEnvelope(CryptoEnvelopeContent.symmetricEncryptedEnvelope(envelope));
	}

	static Decoded<byte[]> decryptSymmetric(byte[] key, SymmetricEncryptedEnvelope envelope, byte[] associatedData) {
		if (!AEAD.getCipherName().equals(envelope.getPayloadEncryptionAlgorightm())) {
			return new Decoded<>(Decoded.Error.SYMMETRIC_UNKNOWN_CIPHER);
		}
		if (envelope.getKeyHash() != null && !Arrays.equals(envelope.getKeyHash(), hashKey(key))) {
			return new Decoded<>(Decoded.Error.SYMMETRIC_KEY_HASH_MISMATCH);
		}
		byte[] decrypted = AEAD.decrypt(key, envelope.getPayload(), associatedData);
		if (decrypted == null) {
			return new Decoded<>(Decoded.Error.SYMMETRIC_DECRYPTION);
		}
		return new Decoded<>(decrypted);
	}

	public CryptoEnvelope signAndEncryptSymmetric(TypedPayload typedPayload, byte[] recipientId, Long ttl, byte[] key, byte[] associatedData) {
		byte[] typedPayloadEnvelopeSerialized = ThriftUtils.serialize(new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(typedPayload)));
		SignedEnvelope signedEnvelope = identityCryptoService.createSignedEnvelope(recipientId, typedPayloadEnvelopeSerialized, ttl, IdentityCryptoService.SignerType.EC);
		CryptoEnvelope signedCryptoEnvelope = new CryptoEnvelope(CryptoEnvelopeContent.signedEnvelope(signedEnvelope));
		SymmetricEncryptedEnvelope encryptedEnvelope = encryptSymmetricRaw(key, ThriftUtils.serialize(signedCryptoEnvelope), associatedData);
		log.trace(tracePrefix + " Created signed and sym-encrypted envelope");
		return new CryptoEnvelope(CryptoEnvelopeContent.symmetricEncryptedEnvelope(encryptedEnvelope));
	}

	public X3DHPreKeyBundle generatePreKeyBundle(byte[] ephemeralPublicKey) {
		X3DH.PreKeyBundle preKeyBundle = identityCryptoService.generateX3DHPreKeyBundle(ephemeralPublicKey);
		return CryptoThriftUtils.toThrift(preKeyBundle);
	}

	public Pair<byte[], X3DHInitialMessage> createX3DHInitialMessage(IdentityDescriptor destIdentity, X3DHPreKeyBundle bundle) {
		ECKeyPair ephemeralKeyPair = ECUtils.generateKeyPair();
		X3DH.PreKeyBundle preKeyBundle = CryptoThriftUtils.fromThrift(bundle, destIdentity.getEcPublicKey().getPublicKey());
		X3DH.InitialMessageGenerationResult result = identityCryptoService.generateX3DHSessionFirst(ephemeralKeyPair, preKeyBundle);
		if (result.error != null) {
			log.trace(tracePrefix + " Failed to generate first session for " + Utils.getDebugStringFromId(destIdentity.getId()) + ": " + result.error);
			return null;
		}
		return Pair.of(result.masterSecret, CryptoThriftUtils.toThrift(result.initialMessage));
	}

	public byte[] processX3DHInitialMessage(IdentityDescriptor srcIdentity, ECKeyPair preKeyPair, X3DHInitialMessage initialMessage) {
		X3DH.InitialMessage initialMessageRaw = CryptoThriftUtils.fromThrift(initialMessage, srcIdentity.getEcPublicKey().getPublicKey());
		X3DH.InitialMessageProcessingResult result = identityCryptoService.generateX3DHSessionSecond(preKeyPair, initialMessageRaw);
		if (result.error != null) {
			log.trace(tracePrefix + " Failed to generate second session from " + Utils.getDebugStringFromId(srcIdentity.getId()) + ": " + result.error);
			return null;
		}
		return result.masterSecret;
	}
}
