package org.cweb.crypto;

import com.google.common.base.Preconditions;
import org.cweb.crypto.lib.DoubleRatchet;
import org.cweb.crypto.lib.ECKeyPair;
import org.cweb.crypto.lib.X3DH;
import org.cweb.schemas.crypto.DoubleRatchetMessage;
import org.cweb.schemas.crypto.DoubleRatchetSkippedMessageKey;
import org.cweb.schemas.crypto.DoubleRatchetState;
import org.cweb.schemas.crypto.X3DHInitialMessage;
import org.cweb.schemas.crypto.X3DHPreKeyBundle;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.keys.KeyType;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CryptoThriftUtils {

	private static ByteBuffer wrapNullable(byte[] data) {
		if (data == null) {
			return null;
		}
		return ByteBuffer.wrap(data);
	}

	public static KeyPair toThrift(ECKeyPair keyPair) {
		return new KeyPair(KeyType.EC25519_256, ByteBuffer.wrap(keyPair.publicKey), ByteBuffer.wrap(keyPair.privateKey));
	}

	public static ECKeyPair fromThrift(KeyPair keyPair) {
		Preconditions.checkArgument(keyPair.getType() == KeyType.EC25519_256);
		return new ECKeyPair(keyPair.getPublicKey(), keyPair.getPrivateKey());
	}

	public static X3DHPreKeyBundle toThrift(X3DH.PreKeyBundle preKeyBundle) {
		X3DHPreKeyBundle bundle = new X3DHPreKeyBundle(ByteBuffer.wrap(preKeyBundle.preKeyPublic), ByteBuffer.wrap(preKeyBundle.preKeyPublicSignature));
		bundle.setIdPublicKey(preKeyBundle.idPublicKey);
		return bundle;
	}

	public static X3DH.PreKeyBundle fromThrift(X3DHPreKeyBundle bundle, byte[] idPublicKeyOverride) {
		Preconditions.checkArgument(((idPublicKeyOverride == null) != (bundle.getIdPublicKey() == null) || Arrays.equals(idPublicKeyOverride, bundle.getIdPublicKey())));
		return new X3DH.PreKeyBundle(idPublicKeyOverride != null ? idPublicKeyOverride : bundle.getIdPublicKey(), bundle.getPreKey(), bundle.getSignature());
	}

	public static X3DHInitialMessage toThrift(X3DH.InitialMessage initialMessage) {
		X3DHInitialMessage message = new X3DHInitialMessage(ByteBuffer.wrap(initialMessage.preKeyHash), ByteBuffer.wrap(initialMessage.ephemeralPublicKey), ByteBuffer.wrap(initialMessage.secretKeyHash));
		message.setIdPublicKey(initialMessage.idPublicKey);
		return message;
	}

	public static X3DH.InitialMessage fromThrift(X3DHInitialMessage message, byte[] idPublicKeyOverride) {
		Preconditions.checkArgument(((idPublicKeyOverride == null) != (message.getIdPublicKey() == null)) || Arrays.equals(idPublicKeyOverride, message.getIdPublicKey()));
		return new X3DH.InitialMessage(idPublicKeyOverride != null ? idPublicKeyOverride : message.getIdPublicKey(), message.getPreKeyHash(), message.getEphemeralPublicKey(), message.getSecretKeyHash());
	}

	public static DoubleRatchetSkippedMessageKey toThrift(DoubleRatchet.SkippedMessageKey skippedMessageKey) {
		return new DoubleRatchetSkippedMessageKey(ByteBuffer.wrap(skippedMessageKey.headerKeyRemote), skippedMessageKey.messageSerialRemote, ByteBuffer.wrap(skippedMessageKey.messageKeyRemote));
	}

	public static DoubleRatchet.SkippedMessageKey fromThrift(DoubleRatchetSkippedMessageKey skippedKey) {
		return new DoubleRatchet.SkippedMessageKey(skippedKey.getHeaderKeyRemote(), skippedKey.getMessageSerialRemote(), skippedKey.getMessageKeyRemote());
	}

	public static DoubleRatchetState toThrift(DoubleRatchet.State state) {
		List<DoubleRatchetSkippedMessageKey> skippedKeys = new ArrayList<>(state.skippedMessageKeys.size());
		for (DoubleRatchet.SkippedMessageKey skippedMessageKey : state.skippedMessageKeys) {
			skippedKeys.add(toThrift(skippedMessageKey));
		}

		return new DoubleRatchetState(
				toThrift(state.dhKeyPairSelf),
				wrapNullable(state.dhPublicKeyRemote),
				ByteBuffer.wrap(state.rootKey),
				wrapNullable(state.chainKeySelf),
				wrapNullable(state.chainKeyRemote),

				state.messageSerialSelf,
				state.messageSerialRemote,
				state.messageSerialPrevChainSelf,

				wrapNullable(state.headerKeySelf),
				wrapNullable(state.headerKeyRemote),
				ByteBuffer.wrap(state.nextHeaderKeySelf),
				ByteBuffer.wrap(state.nextHeaderKeyRemote),

				skippedKeys);
	}

	public static DoubleRatchet.State fromThrift(DoubleRatchetState state) {
		List<DoubleRatchet.SkippedMessageKey> skippedMessageKeys = new ArrayList<>(state.getSkippedMessageKeysSize());
		for (DoubleRatchetSkippedMessageKey skippedKey : state.getSkippedMessageKeys()) {
			skippedMessageKeys.add(fromThrift(skippedKey));
		}

		DoubleRatchet.State result = new DoubleRatchet.State();

		result.dhKeyPairSelf = fromThrift(state.getDhKeyPairSelf());
		result.dhPublicKeyRemote = state.getDhPublicKeyRemote();
		result.rootKey = state.getRootKey();
		result.chainKeySelf = state.getChainKeySelf();
		result.chainKeyRemote = state.getChainKeyRemote();

		result.messageSerialSelf = state.getMessageSerialSelf();
		result.messageSerialRemote = state.getMessageSerialRemote();
		result.messageSerialPrevChainSelf = state.getMessageSerialPrevChainSelf();

		result.headerKeySelf = state.getHeaderKeySelf();
		result.headerKeyRemote = state.getHeaderKeyRemote();
		result.nextHeaderKeySelf = state.getNextHeaderKeySelf();
		result.nextHeaderKeyRemote = state.getNextHeaderKeyRemote();

		result.skippedMessageKeys = skippedMessageKeys;

		return result;
	}

	public static DoubleRatchetMessage toThrift(DoubleRatchet.Message message) {
		return new DoubleRatchetMessage(ByteBuffer.wrap(message.encryptedHeader), ByteBuffer.wrap(message.encryptedData));
	}

	public static DoubleRatchet.Message fromThrift(DoubleRatchetMessage message) {
		return new DoubleRatchet.Message(message.getEncryptedHeader(), message.getEncryptedData());
	}
}
