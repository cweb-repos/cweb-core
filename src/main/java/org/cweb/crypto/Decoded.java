package org.cweb.crypto;

public class Decoded<T> {
	private final T data;
	private final Error error;

	public enum Error {
		INVALID_CONTEXT,
		CRYPTO_ENVELOPE_TYPE,
		CRYPTO_ENVELOPE_DESERIALIZATION,
		PK_KEY_DECRYPTION,
		PK_KEY_UNKNOWN_CIPHER,
		SIGNED_PAYLOAD_DESERIALIZATION,
		SIGNATURE_RECIPIENT_ID_MISMATCH,
		SIGNER_IDENTITY_FETCH_FAILED,
		SIGNER_IDENTITY_NOT_FOUND,
		SIGNER_IDENTITY_MISMATCH,
		SIGNATURE_VALIDATION,
		SYMMETRIC_KEY_PARAM_MISSING,
		SYMMETRIC_KEY_HASH_MISMATCH,
		SYMMETRIC_DECRYPTION,
		SYMMETRIC_UNKNOWN_CIPHER
	}

	public Decoded(T data) {
		this.data = data;
		this.error = null;
	}

	public Decoded(Error error) {
		this.data = null;
		this.error = error;
	}

	public T getData() {
		return data;
	}

	public Error getError() {
		return error;
	}
}
