package org.cweb.crypto;

import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.TypedPayload;

public class DecodedTypedPayload {
	private final SignatureMetadata signatureMetadata;
	private final TypedPayload payload;

	public DecodedTypedPayload(SignatureMetadata signatureMetadata, TypedPayload payload) {
		this.signatureMetadata = signatureMetadata;
		this.payload = payload;
	}

	public SignatureMetadata getSignatureMetadata() {
		return signatureMetadata;
	}

	public TypedPayload getPayload() {
		return payload;
	}
}
