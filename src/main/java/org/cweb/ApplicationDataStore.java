package org.cweb;

import org.apache.thrift.TBase;
import org.cweb.storage.local.LocalDataBase;
import org.cweb.storage.local.LocalStorageInterface;

public class ApplicationDataStore<T extends TBase> extends LocalDataBase<T> {
	private static final String APP_DATA_PATH_PREFIX = "ad/";
	private final Class<T> klass;
	private final String name;

	ApplicationDataStore(String tracePrefix, Class<T> klass, String name, LocalStorageInterface localStorageInterface, int memoryCacheExpirationMinutes) {
		super(tracePrefix, localStorageInterface, 1, memoryCacheExpirationMinutes);
		this.klass = klass;
		this.name = name;
	}

	private String getName() {
		return APP_DATA_PATH_PREFIX + name;
	}

	public T get() {
		return super.get(getName(), klass);
	}

	public void put(T value) {
		super.put(getName(), value);
	}
}
