package org.cweb.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Constants {
	public static final String PROTOCOL_VERSION = "0.1.5";
	public static final String ENCODING_UTF8 = "UTF-8";
	public static final Charset CHARSET_UTF8 = StandardCharsets.UTF_8;
}
