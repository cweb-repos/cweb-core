package org.cweb.utils;

import com.google.common.base.Objects;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.properties.Property;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PropertyUtils {
	public static final String PROPERTY_KEY_NICK_NAME = "nickName";
	public static final String PROPERTY_KEY_PROFILE_PICTURE = "profilePic";

	public static String getStringProperty(List<Property> properties, String key) {
		Property property = findProperty(properties, key);
		return property != null && property.isSetValue() && property.getValue().isSetStr() ? property.getValue().getStr() : null;
	}

	public static byte[] getBinaryProperty(List<Property> properties, String key) {
		Property property = findProperty(properties, key);
		return property != null && property.isSetValue() && property.getValue().isSetBin() ? property.getValue().getBin() : null;
	}

	public static FileReference getFileReferenceProperty(List<Property> properties, String key) {
		Property property = findProperty(properties, key);
		return property != null && property.isSetValue() && property.getValue().isSetTypedPayload() ? TypedPayloadUtils.unwrap(property.getValue().getTypedPayload()) : null;
	}

	static HashMap<String, Property> toMap(List<Property> properties) {
		HashMap<String, Property> key2property = new LinkedHashMap<>();
		if (properties != null) {
			for (Property property : properties) {
				key2property.put(property.getKey(), property);
			}
		}
		return key2property;
	}

	public static Property findProperty(List<Property> properties, String key) {
		if (properties == null) {
			return null;
		}
		for (Property property : properties) {
			if (key.equals(property.getKey())){
				return property;
			}
		}
		return null;
	}

	public static List<Property> updateProperties(List<Property> currentProperties, List<Property> updates) {
		HashMap<String, Property> key2propertyCurrent = toMap(currentProperties);
		HashMap<String, Property> key2propertyNew = toMap(updates);

		boolean changed = false;
		for (Map.Entry<String, Property> entry : key2propertyNew.entrySet()) {
			String key = entry.getKey();
			Property newProperty = entry.getValue();
			Property oldProperty = key2propertyCurrent.get(key);
			if (oldProperty == null) {
				if (newProperty.isSetValue()) {
					key2propertyCurrent.put(key, newProperty);
				}
				changed = true;
			} else {
				if (!Objects.equal(oldProperty, newProperty)) {
					if (newProperty.isSetValue()) {
						key2propertyCurrent.put(key, newProperty);
					} else {
						key2propertyCurrent.remove(key);
					}
					changed = true;
				}
			}
		}

		if (!changed) {
			return null;
		}
		return new ArrayList<>(key2propertyCurrent.values());
	}
}
