package org.cweb.utils;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
	public static void addToZip(String filePath, byte[] content, ZipOutputStream zipOutputStream) throws IOException {
		final ZipEntry zipEntry = new ZipEntry(filePath);
		zipOutputStream.putNextEntry(zipEntry);
		zipOutputStream.write(content);
	}

	public static byte[] unzipByName(final ZipInputStream zipInputStream, String fileName) throws IOException {
		for (ZipEntry zipEntry = zipInputStream.getNextEntry(); zipEntry != null; zipEntry = zipInputStream.getNextEntry()) {
			if (!zipEntry.getName().equals(fileName)) {
				continue;
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.copy(zipInputStream, out);
			return out.toByteArray();
		}
		return null;
	}
}
