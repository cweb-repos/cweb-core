package org.cweb.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.schemas.wire.CompressionType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;

public class CompressionUtils {
	private static final int MIN_BYTES_TO_COMPRESS = 40;
	private static final double MIN_COMPRESSION_RATIO = 0.97;

	private static byte[] compressGzip(byte[] data) {
		try {
			ByteArrayOutputStream obj = new ByteArrayOutputStream();
			GZIPOutputStream compressor = new GZIPOutputStream(obj);
			compressor.write(data);
			compressor.close();
			return obj.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	private static byte[] decompressGzip(byte[] bytes) {
		try {
			ByteArrayOutputStream decompressedData = new ByteArrayOutputStream();
			GZIPInputStream decompressor = new GZIPInputStream(new ByteArrayInputStream(bytes));
			IOUtils.copy(decompressor, decompressedData);
			decompressor.close();
			return decompressedData.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	public static byte[] compressDeflate(byte[] data) {
		try {
			ByteArrayOutputStream compressedData = new ByteArrayOutputStream();
			DeflaterOutputStream compressor = new DeflaterOutputStream(compressedData);
			compressor.write(data);
			compressor.close();
			return compressedData.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	public static byte[] decompressDeflate(byte[] bytes) {
		try {
			ByteArrayOutputStream decompressedData = new ByteArrayOutputStream();
			InflaterInputStream decompressor = new InflaterInputStream(new ByteArrayInputStream(bytes));
			IOUtils.copy(decompressor, decompressedData);
			decompressor.close();
			return decompressedData.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	public static byte[] compressString(String str) {
		return compressGzip(str.getBytes(Constants.CHARSET_UTF8));
	}

	public static String decompressString(byte[] bytes) {
		return new String(decompressGzip(bytes), Constants.CHARSET_UTF8);
	}

	private static CompressionType getCompressionTypeForData(byte[] data, CompressionType compressionType) {
		return data.length >= MIN_BYTES_TO_COMPRESS ? compressionType : CompressionType.NONE;
	}

	public static Pair<CompressionType, byte[]> tryCompress(byte[] data, CompressionType compressionType) {
		CompressionType actualCompressionType = getCompressionTypeForData(data, compressionType);
		if (actualCompressionType == CompressionType.NONE) {
			return Pair.of(actualCompressionType, data);
		}
		byte[] compressed = compress(data, actualCompressionType);
		if ((double)compressed.length / data.length > MIN_COMPRESSION_RATIO) {
			return Pair.of(CompressionType.NONE, data);
		}
		return Pair.of(actualCompressionType, compressed);
	}

	private static byte[] compress(byte[] rawData, CompressionType compressionType) {
		byte[] data;
		switch (compressionType) {
			case GZIP:
				data = CompressionUtils.compressGzip(rawData);
				break;
			case DEFLATE:
				data = CompressionUtils.compressDeflate(rawData);
				break;
			case NONE:
				data = rawData;
				break;
			default:
				throw new RuntimeException("Invalid compressionType=" + compressionType);
		}
		return data;
	}

	public static byte[] decompress(byte[] rawData, CompressionType compressionType) {
		byte[] data;
		switch (compressionType) {
			case GZIP:
				data = CompressionUtils.decompressGzip(rawData);
				break;
			case DEFLATE:
				data = CompressionUtils.decompressDeflate(rawData);
				break;
			case NONE:
				data = rawData;
				break;
			default:
				throw new RuntimeException("Invalid compressionType=" + compressionType);
		}
		return data;
	}
}
