package org.cweb.utils;

import org.cweb.schemas.storage.LocalMetadataEnvelope;

public interface LocalMetadataPredicate {
	boolean match(LocalMetadataEnvelope envelope);
}
