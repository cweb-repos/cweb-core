package org.cweb.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TBase;
import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.schemas.wire.CompressionType;

import java.util.Arrays;
import java.util.regex.Pattern;

public class ThriftTextUtils {
    private static final String SEPARATOR =  "-------";
    private static final Pattern SEPARATOR_FIRST_REGEX = Pattern.compile("^\\-{5,}\\w*$");
    private static final Pattern SEPARATOR_LAST_REGEX = Pattern.compile("^\\-{5,}$");

    public static <T extends TBase> String toUrlSafeString(T object) {
        byte[] data = ThriftUtils.serialize(object);
        Pair<CompressionType, byte[]> compressed = CompressionUtils.tryCompress(data, CompressionType.DEFLATE);
        byte[] header = new byte[] {(byte) compressed.getLeft().getValue()};
        byte[] compressedWithHeader = BinaryUtils.concat(header, compressed.getRight());
        return Utils.toBase32String(compressedWithHeader);
    }

    public static byte[] fromUrlSafeString(String dataStr) {
        byte[] compressedWithHeader = Utils.fromBase32String(dataStr);
        if (compressedWithHeader == null || compressedWithHeader.length < 1) {
            return null;
        }
        CompressionType compressionType = CompressionType.findByValue(compressedWithHeader[0]);
        if (compressionType == null) {
            return null;
        }
        byte[] compressed = Arrays.copyOfRange(compressedWithHeader, 1, compressedWithHeader.length);
        return CompressionUtils.decompress(compressed, compressionType);
    }

    public static <T extends TBase> T fromUrlSafeString(String dataStr, Class<T> klass) {
        byte[] data = fromUrlSafeString(dataStr);
        if (data == null) {
            return null;
        }
        return ThriftUtils.deserializeSafe(data, klass);
    }

    private static String wrap(String str, int width) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i+=width) {
            if (i > 0) {
                result.append('\n');
            }
            result.append(str, i, Math.min(i + width, str.length()));
        }
        return result.toString();
    }

    public static String formatShareableString(String data, String header, int width) {
        return SEPARATOR + header + "\n" + wrap(data, width) + "\n" + SEPARATOR;
    }

    public static String parseShareableString(String rawStr) {
        String[] lines = StringUtils.split(rawStr, "\n\r");
        boolean foundStart = false;
        StringBuilder dataStr = new StringBuilder();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i].trim();
            if (SEPARATOR_FIRST_REGEX.matcher(line).matches()) {
                foundStart = true;
                continue;
            }
            if (!foundStart) {
                continue;
            }
            if (SEPARATOR_LAST_REGEX.matcher(line).matches()) {
                break;
            }
            dataStr.append(line);
        }
        if (dataStr.length() == 0) {
            return null;
        }
        return dataStr.toString();
    }
}
