package org.cweb.utils;

import org.apache.thrift.TBase;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

public class ThriftUtils {
	private static final Logger log = LoggerFactory.getLogger(ThriftUtils.class);
	private static ErrorMode defaultErrorMode = ErrorMode.FAIL_ALL;

	public enum ErrorMode {
		FAIL_ALL,
		IGNORE_ALL
	}

	private static void handleException(String msg, Exception e) {
		if (defaultErrorMode != ErrorMode.IGNORE_ALL) {
			throw new RuntimeException(e);
		} else {
			log.error(msg, e);
		}
	}

	public static <T extends TBase> byte[] serialize(T object) {
		try {
			TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
			byte[] bytes = serializer.serialize(object);
			return bytes;
		} catch (Exception e) {
			throw new RuntimeException("Exception while binary encoding object of type " + object.getClass().getName() + ": " + object.toString(), e);
		}
	}

	private static <T extends TBase> T deserializeInternal(byte[] data, Class<T> klass) throws InstantiationException, IllegalAccessException, TException, NoSuchMethodException, InvocationTargetException {
		TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		T object = klass.getDeclaredConstructor().newInstance();
		deserializer.deserialize(object, data);
		return object;
	}

	public static <T extends TBase> T deserialize(byte[] data, Class<T> klass) {
		try {
			return deserializeInternal(data, klass);
		} catch (Exception e) {
			handleException("Exception while binary encoding object of type " + klass.getName(), e);
		}
		return null;
	}

	public static <T extends TBase> T deserializeSafe(byte[] data, Class<T> klass) {
		try {
			return deserializeInternal(data, klass);
		} catch (Exception e) {
			log.trace("Error deserializing " + klass.getName(), e);
		}
		return null;
	}
}
