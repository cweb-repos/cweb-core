package org.cweb.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class Threads {
	private static final Logger log = LoggerFactory.getLogger(Threads.class);

	private static class ExceptionCatchingScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {
		public ExceptionCatchingScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory) {
			super(corePoolSize, threadFactory);
		}

		public ExceptionCatchingScheduledThreadPoolExecutor(int corePoolSize) {
			super(corePoolSize);
		}

		@Override
		protected void afterExecute(Runnable r, Throwable t) {
			super.afterExecute(r, t);
			if (t == null && r instanceof Future<?> && (((Future) r).isDone() || ((Future) r).isCancelled())) {
				try {
					((Future<?>) r).get();
				} catch (CancellationException | InterruptedException ce) {
					// t = ce;
				} catch (ExecutionException ee) {
					t = ee.getCause();
				}
			}
			if (t != null) {
				log.error("Threads.afterExecute", t);
			}
		}
	}

	private static final ScheduledExecutorService lowPriorityExecutorService =
			new ExceptionCatchingScheduledThreadPoolExecutor(1, new ThreadFactory() {
				@Override
				public Thread newThread(Runnable r) {
					Thread thread = new Thread(r, "LowPriorityExecutor");
					thread.setPriority(Thread.NORM_PRIORITY - 1);
					return thread;
				}
			});

	private static final ScheduledExecutorService highPriorityExecutorService =
			new ExceptionCatchingScheduledThreadPoolExecutor(4);

	public static Future<?> submitBackgroundTask(Runnable task) {
		return lowPriorityExecutorService.submit(task);
	}

	public static void submitBackgroundTaskPeriodically(Runnable task, long initialDelay, long period) {
		lowPriorityExecutorService.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
	}

	public static Future<?> submitTask(Runnable task) {
		return highPriorityExecutorService.submit(task);
	}

	public static ScheduledFuture<?> submitTask(Runnable task, long delay) {
		return highPriorityExecutorService.schedule(task, delay, TimeUnit.MILLISECONDS);
	}

	public static void closeExecutors(int waitSeconds) {
		closeExecutor(lowPriorityExecutorService, waitSeconds);
		closeExecutor(highPriorityExecutorService, waitSeconds);
	}

	private static void closeExecutor(ScheduledExecutorService executorService, int waitSeconds) {
		executorService.shutdown();
		try {
			boolean success = executorService.awaitTermination(waitSeconds, TimeUnit.SECONDS);
			if (!success) {
				executorService.shutdownNow();
			}
		} catch (InterruptedException e) {
		}
	}

	public static Thread runAsNewThread(Runnable task) {
		Thread thread = new Thread(task, "NewThread");
		thread.start();
		return thread;
	}

	public static void sleepChecked(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ignored) {
		}
	}

	public static <T> T getChecked(Future<T> future) {
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException ignored) {
			return null;
		}
	}

	public static void waitChecked(Object obj) {
		try {
			obj.wait();
		} catch (InterruptedException ignored) {
		}
	}

	public static boolean waitChecked(Object obj, long time) {
		try {
			obj.wait(time);
			return false;
		} catch (InterruptedException ignored) {
			return true;
		}
	}

	public static void joinThreadSafe(Thread thread) {
		synchronized (thread) {
			thread.notify();
			sleepChecked(20);
			thread.interrupt();
		}
		try {
			thread.join();
		} catch (InterruptedException e) {
		}
	}
}
