package org.cweb.utils;

import net.iharder.Base64;
import org.apache.commons.codec.binary.Base32;
import org.cweb.crypto.IdentityCryptoService;
import org.cweb.schemas.keys.PublicKey;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

public class Utils {
	private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

	private static final Random random = new Random();
	private static final Base32 base32 = new Base32();

	public static byte[] toArray(ByteBuffer buffer) {
		final byte[] bbArray = buffer.array();
		final int bbArrayOffset = buffer.arrayOffset();
		if (bbArrayOffset == 0 && buffer.position() == 0 && bbArray.length == buffer.limit()) {
			return bbArray;
		}
		return Arrays.copyOfRange(bbArray, bbArrayOffset + buffer.position(), bbArrayOffset + buffer.limit());
	}

	public static List<byte[]> toArray(Set<ByteBuffer> idsWrapped) {
		List<byte[]> ids = new ArrayList<>(idsWrapped.size());
		for (ByteBuffer idWrapped : idsWrapped) {
			ids.add(toArray(idWrapped));
		}
		return ids;
	}

	public static byte[] copyOf(byte[] array) {
		return Arrays.copyOf(array, array.length);
	}

	public static boolean contains(List<byte[]> list, byte[] value) {
		for (byte[] arr : list) {
			if (Arrays.equals(arr, value)) {
				return true;
			}
		}
		return false;
	}

	public static byte[] generateRandomBytes(int length) {
		byte[] bytes = new byte[length];
		random.nextBytes(bytes);
		return bytes;
	}

	public static String getDebugStringFromKey(byte[] key) {
		return encodeBase64(IdentityCryptoService.idFromKey(key)).substring(0, 16);
	}

	public static String getDebugStringFromKey(PublicKey publicKey) {
		return encodeBase64(IdentityCryptoService.idFromPublicKey(publicKey)).substring(0, 16);
	}

	public static String getDebugStringFromId(byte[] id) {
		if (id == null) {
			return "null";
		}
		return encodeBase64(id).substring(0, 16);
		// return Hex.encodeHexString(id).substring(0, 32);
	}

	public static String getDebugStringFromBytes(byte[] data) {
		return getDebugStringFromBytes(data, 10000);
	}

	public static String getDebugStringFromBytesShort(byte[] data) {
		return getDebugStringFromBytes(data, 16);
	}

	public static String getDebugStringFromBytes(byte[] data, int length) {
		String str = encodeBase64(data);
		return str.substring(0, Math.min(length, str.length()));
	}

	public static String encodeBase64(byte[] data) {
		// return new String(Base64.encodeBase64(data), Constants.CHARSET_UTF8).replace('+', '-').replace('/', '_');
		// return StringUtils.stripEnd(new String(Base64.encodeBase64(data), Constants.CHARSET_UTF8).replace('+', '-').replace('/', '_'), "=");
		// return Base64.encodeBase64URLSafeString(data);
		try {
			return Base64.encodeBytes(data, Base64.URL_SAFE);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] decodeBase64(String base64Str) {
		// base64Str = base64Str.replace('-', '+').replace('_', '/');
		// int paddedSize = base64Str.length() + (4 - base64Str.length() % 3);
		// base64Str = StringUtils.rightPad(base64Str, paddedSize, 'A');
		// return Base64.decodeBase64(base64Str.getBytes(Constants.CHARSET_UTF8));
		// return Base64.decodeBase64(base64Str);
		try {
			return Base64.decode(base64Str, Base64.URL_SAFE);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] decodeBase64Safe(String base64Str) {
		try {
			return Base64.decode(base64Str, Base64.URL_SAFE);
		} catch (Exception e) {
			return null;
		}
	}

	public static String toBase32String(byte[] bytes) {
		return base32.encodeAsString(bytes).toLowerCase().replace('=', '9');
	}

	public static byte[] fromBase32String(String str) {
		try {
			return base32.decode(str.toUpperCase().replace('9', '='));
		} catch (Exception e) {
			return null;
		}
	}

	public static String getDebugString(PublicStorageProfile publicStorageProfile) {
		return publicStorageProfile.toString();
	}

	public static String getDebugString(PrivateStorageProfile privateStorageProfile) {
		return StorageProfileUtils.toPublicStorageProfile(privateStorageProfile).toString();
	}

	public static String getHost(String urlStr) {
		try {
			URL url = new URL(urlStr);
			return url.getHost();
		} catch (Exception e) {
			return null;
		}
	}

	public static String appendPath(String first, String second) {
		if (first.charAt(first.length() - 1) == '/') {
			return first + second;
		} else {
			return first + '/' + second;
		}
	}

	public static <T> Set<T> drainAndDedup(Queue<T> queue) {
		Set<T> result = new LinkedHashSet<>(queue.size());
		while (true) {
			T elem = queue.poll();
			if (elem == null) {
				break;
			}
			result.add(elem);
		}
		return result;
	}

	public static <T> Set<T> drain(Set<T> source) {
		LinkedHashSet copy = new LinkedHashSet<T>(source);
		source.clear();
		return copy;
	}

	public static <K, V> Map<K, V> drain(Map<K, V> source) {
		LinkedHashMap copy = new LinkedHashMap<K, V>(source);
		source.clear();
		return copy;
	}

	public static String formatDateTime(long timestamp) {
		if (timestamp == 0) {
			return "null";
		}
		LocalDateTime localDateTime = Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
		return formatDateTime(localDateTime);
	}

	public static String formatDateTime(LocalDateTime date) {
		return date.format(dateFormatter);
	}
}
