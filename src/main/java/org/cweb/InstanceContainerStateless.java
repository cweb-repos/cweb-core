package org.cweb;

import org.cweb.crypto.CryptoHelper;
import org.cweb.identity.PublicStorageProfiles;
import org.cweb.identity.RemoteIdentityService;
import org.cweb.storage.local.InMemoryLocalStorage;
import org.cweb.storage.remote.InboundCacheService;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteStorageClient;
import org.cweb.storage.remote.RemoteStorageFactory;

/**
 * Partial instance container without own remote storage
 */
public class InstanceContainerStateless {
	protected final String tracePrefix;
	protected final InboundCacheService inboundCacheService;
	protected final PublicStorageProfiles publicStorageProfiles;
	protected final RemoteStorageClient remoteStorageClient;
	protected final RemoteReadService remoteReadService;
	protected final RemoteIdentityService remoteIdentityService;

	public InstanceContainerStateless() {
		tracePrefix = "static";
		InMemoryLocalStorage localStorageInterface = new InMemoryLocalStorage();

		this.inboundCacheService = new InboundCacheService(localStorageInterface);
		this.publicStorageProfiles = new PublicStorageProfiles(tracePrefix, localStorageInterface, 1000, 1440);
		this.remoteStorageClient = RemoteStorageFactory.getRemoteStorageClient();
		this.remoteReadService = new RemoteReadService(tracePrefix, inboundCacheService, publicStorageProfiles, remoteStorageClient);
		CryptoHelper cryptoHelper = new CryptoHelper(tracePrefix, null);
		this.remoteIdentityService = new RemoteIdentityService(tracePrefix, localStorageInterface, remoteReadService, publicStorageProfiles, cryptoHelper);
	}

	public RemoteIdentityService getRemoteIdentityService() {
		return remoteIdentityService;
	}
}
