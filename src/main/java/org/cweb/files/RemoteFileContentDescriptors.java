package org.cweb.files;

import org.cweb.schemas.files.FileContentDescriptor;
import org.cweb.storage.local.LocalDataWithDir;
import org.cweb.storage.local.LocalStorageInterface;

public class RemoteFileContentDescriptors extends LocalDataWithDir<FileContentDescriptor> {
	private static final String DIR_NAME = "fcdRemote";

	public RemoteFileContentDescriptors(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(DIR_NAME, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public FileContentDescriptor get(byte[] id, byte[] fileId) {
		return super.get(id, fileId, FileContentDescriptor.class);
	}

	@Override
	public boolean delete(byte[] id, byte[] fileId) {
		return super.delete(id, fileId);
	}
}
