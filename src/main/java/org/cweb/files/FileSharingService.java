package org.cweb.files;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.communication.CommSessionService;
import org.cweb.communication.MessageProcessor;
import org.cweb.communication.SharedSessionService;
import org.cweb.payload.GenericPayloadTypePredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalSharedFileInfo;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.NameConversionUtils;
import org.cweb.storage.local.LocalStorageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileSharingService {
	private static final Logger log = LoggerFactory.getLogger(FileSharingService.class);
	private static final String SERVICE_NAME = "FileSharingService";

	private final String tracePrefix;
	private final CommSessionService commSessionService;
	private final SharedSessionService sharedSessionService;
	private final SharedFileInfos sharedFileInfos;

	private final MessageProcessor messageProcessor = new MessageProcessor() {
		@Override
		public MessageProcessor.Result process(SessionId sessionId, TypedPayload typedPayload) {
			return processIncomingMessage(typedPayload);
		}
	};

	public FileSharingService(String tracePrefix, LocalStorageInterface localStorageInterface, CommSessionService commSessionService, SharedSessionService sharedSessionService) {
		this.tracePrefix = tracePrefix;
		this.commSessionService = commSessionService;
		this.sharedSessionService = sharedSessionService;
		this.sharedFileInfos = new SharedFileInfos(tracePrefix, localStorageInterface, 10, 3);

		PayloadTypePredicate predicate = new GenericPayloadTypePredicate(PayloadType.FILE_REFERENCE, SERVICE_NAME, null, null);
		commSessionService.addMessageProcessor(predicate, messageProcessor);
		sharedSessionService.addMessageProcessor(predicate, messageProcessor);
	}

	boolean share(LocalUploadedFileInfo uploadedFileInfo, SessionId sessionId) {
		TypedPayload payload = TypedPayloadUtils.wrap(uploadedFileInfo.getFileReference(), SERVICE_NAME, null, null);

		boolean success = false;
		if (sessionId.getType() == SessionType.COMM_SESSION) {
			success = commSessionService.sendMessage(sessionId.getId(), payload);
		} else if (sessionId.getType() == SessionType.SHARED_SESSION) {
			success = sharedSessionService.sendMessage(sessionId.getId(), payload);
		}
		return success;
	}

	private MessageProcessor.Result processIncomingMessage(TypedPayload typedPayload) {
		Pair<FileReference, String> fileReferenceUnwrapped = TypedPayloadUtils.unwrap(typedPayload, FileReference.class, SERVICE_NAME);
		if (fileReferenceUnwrapped.getRight() != null) {
			log.trace(tracePrefix + " Failed to extract file reference: " + fileReferenceUnwrapped.getRight());
			return MessageProcessor.Result.INVALID;
		}
		FileReference fileReference = fileReferenceUnwrapped.getLeft();

		log.trace(tracePrefix + " Received file reference " + NameConversionUtils.toString(fileReference.getFileId()));
		putFileReference(fileReference);

		return MessageProcessor.Result.SUCCESS;
	}

	void putFileReference(FileReference fileReference) {
		LocalSharedFileInfo sharedFileInfo = new LocalSharedFileInfo(fileReference);
		sharedFileInfos.put(sharedFileInfo);
	}

	public LocalSharedFileInfo getSharedInfo(byte[] fromId, byte[] fileId) {
		return sharedFileInfos.get(fromId, fileId);
	}
}
