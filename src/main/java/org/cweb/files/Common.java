package org.cweb.files;

import org.cweb.schemas.files.FileContentDescriptor;

class Common {
	static final String FILE_CONTENT_DESCRIPTOR_NAME_SUFFIX = "-fcd";
	static final String FILE_PART_NAME_SUFFIX = "-fp";
	static final String SERVICE_NAME_FILE_SERVICE = "FileService";
	static final int FILE_PART_NAME_SIZE = 32;
	static final int FILE_ID_SIZE = 32;
	static final byte[] ASSOCIATED_DATA = new byte[]{93, 59, 118};

	static boolean isInlined(FileContentDescriptor fcd) {
		return fcd.getParts().size() == 1 && fcd.isSetInlineContent();
	}
}
