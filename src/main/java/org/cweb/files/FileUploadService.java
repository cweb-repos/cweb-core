package org.cweb.files;

import org.apache.commons.lang3.tuple.Triple;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.lib.AEAD;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.files.FileContentDescriptor;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FilePartDescriptor;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalFileSystemInterface;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class FileUploadService {
	private static final Logger log = LoggerFactory.getLogger(FileUploadService.class);
	private static final int MAX_SIZE_FOR_INLINING_CONTENT = 10000;

	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;
	private final LocalFileSystemInterface localFileSystemInterface;
	private final FileSharingService fileSharingService;

	private final UploadedFileContentDescriptors uploadedFileContentDescriptors;
	private final UploadedFileInfos uploadedFileInfos;
	private final RemoteFileHandler fileContentDescriptorHandler;
	private final RemoteFileHandler filePartHandler;

	public enum UploadError {
		FILE_DOES_NOT_EXIST,
		FAILED_TO_READ_PART,
	}

	public FileUploadService(String tracePrefix, CryptoHelper cryptoHelper, LocalStorageInterface localStorageInterface, LocalFileSystemInterface localFileSystemInterface, RemoteWriteService remoteWriteService, RemoteReadService remoteReadService, UploadedFileInfos uploadedFileInfos, FileSharingService fileSharingService) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.localFileSystemInterface = localFileSystemInterface;
		this.fileSharingService = fileSharingService;

		this.uploadedFileContentDescriptors = new UploadedFileContentDescriptors(tracePrefix, localStorageInterface, 10, 3);
		this.uploadedFileInfos = uploadedFileInfos;
		this.fileContentDescriptorHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, Common.FILE_CONTENT_DESCRIPTOR_NAME_SUFFIX);
		this.filePartHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, Common.FILE_PART_NAME_SUFFIX);
	}

	private static byte[] generateKey() {
		return CryptoHelper.generateAEADKey();
	}

	public static boolean isValidFileId(byte[] fileId) {
		return fileId.length == Common.FILE_ID_SIZE;
	}

	public Triple<LocalUploadedFileInfo, FileMetadata, UploadError> upload(String localFilePath, String externalName, List<Property> properties, int filePartSizeBytes, boolean persistUploadedData) {
		if (!localFileSystemInterface.checkIfExists(localFilePath)) {
			return Triple.of(null, null, UploadError.FILE_DOES_NOT_EXIST);
		}
		long fileLength = localFileSystemInterface.getLength(localFilePath);

		byte[] fileId = Utils.generateRandomBytes(Common.FILE_ID_SIZE);
		byte[] key = FileUploadService.generateKey();
		long now = System.currentTimeMillis();
		FileMetadata metadata = new FileMetadata(externalName, now, fileLength, properties);
		List<FilePartDescriptor> parts = createParts(fileLength, filePartSizeBytes);
		FileContentDescriptor fcd = new FileContentDescriptor(metadata, parts);

		boolean failed = false;
		for (FilePartDescriptor part : parts) {
			byte[] data = localFileSystemInterface.read(localFilePath, part.getStartOffset(), (int) part.getContentLength());
			if (data == null || data.length != part.getContentLength()) {
				log.info(tracePrefix + " Failed to read from " + localFilePath + ":" + part.getStartOffset() + ":" + part.getContentLength());
				failed = true;
				break;
			}
			part.setContentHash(CryptoHelper.hashData(data));
			TypedPayload partPayload = TypedPayloadUtils.wrapCustom(data, Common.SERVICE_NAME_FILE_SERVICE, "FilePart", fileId);
			CryptoEnvelope partCryptoEnvelope = CryptoHelper.encryptSymmetric(part.getEncryptionKey(), partPayload, Common.ASSOCIATED_DATA);
			byte[] partCryptoEnvelopeSerialized = ThriftUtils.serialize(partCryptoEnvelope);
			if (parts.size() == 1 && partCryptoEnvelopeSerialized.length <= MAX_SIZE_FOR_INLINING_CONTENT) {
				fcd.setInlineContent(partCryptoEnvelopeSerialized);
			} else {
				OutboundDataWrapperRaw partFileWrapper = new OutboundDataWrapperRaw(partCryptoEnvelopeSerialized, null, null, !persistUploadedData);
				filePartHandler.write(part.getFileName(), partFileWrapper);
			}
		}

		if (failed) {
			for (FilePartDescriptor part : parts) {
				filePartHandler.delete(part.getFileName());
			}
			return Triple.of(null, null, UploadError.FAILED_TO_READ_PART);
		}

		TypedPayload fcdPayload = TypedPayloadUtils.wrap(fcd, Common.SERVICE_NAME_FILE_SERVICE, null, null);
		CryptoEnvelope fcdCryptoEnvelope = CryptoHelper.encryptSymmetric(key, fcdPayload, Common.ASSOCIATED_DATA);
		byte[] fcdCryptoEnvelopeSerialized = ThriftUtils.serialize(fcdCryptoEnvelope);
		OutboundDataWrapperRaw fcdFileWrapper = new OutboundDataWrapperRaw(fcdCryptoEnvelopeSerialized, null, null, !persistUploadedData);
		FileReference fileReference = new FileReference(ByteBuffer.wrap(cryptoHelper.getOwnId()), ByteBuffer.wrap(fileId), ByteBuffer.wrap(key));
		fileReference.setMetadata(metadata);
		LocalUploadedFileInfo uploadedFileInfo = new LocalUploadedFileInfo(fileReference);

		uploadedFileContentDescriptors.put(fileId, fcd);
		saveUploadedFileInfo(uploadedFileInfo);
		fileContentDescriptorHandler.write(fileId, fcdFileWrapper);

		return Triple.of(uploadedFileInfo, metadata, null);
	}

	private List<FilePartDescriptor> createParts(long fileLength, int partSize) {
		List<FilePartDescriptor> parts = new ArrayList<>();
		long pos = 0;
		for (int i = 0; ; i++) {
			if (pos >= fileLength) {
				break;
			}

			byte[] filename = Utils.generateRandomBytes(Common.FILE_PART_NAME_SIZE);
			byte[] key = generateKey();
			long partLength = Math.min(partSize, fileLength - pos);
			FilePartDescriptor filePartDescriptor = new FilePartDescriptor(ByteBuffer.wrap(filename), pos, partLength, ByteBuffer.wrap(key), AEAD.getCipherName());
			parts.add(filePartDescriptor);

			pos += partSize;
		}
		return parts;
	}

	public LocalUploadedFileInfo getUploadedFileInfo(byte[] fileId) {
		return uploadedFileInfos.get(fileId);
	}

	private void saveUploadedFileInfo(LocalUploadedFileInfo uploadedFileInfo) {
		uploadedFileInfos.put(uploadedFileInfo.getFileReference().getFileId(), uploadedFileInfo);
	}

	public static class UploadState {
		public Double uploadedFraction = 0.0;
		public boolean corrupted = false;
	}

	public UploadState getUploadState(byte[] fileId) {
		UploadState result = new UploadState();

		FileContentDescriptor fcd = uploadedFileContentDescriptors.get(fileId);
		if (fcd == null) {
			return null;
		}

		List<RemoteWriteService.UploadState> uploadStates = new ArrayList<>();
		if (!Common.isInlined(fcd)) {
			for (FilePartDescriptor part : fcd.getParts()) {
				uploadStates.add(filePartHandler.getUploadState(part.getFileName()));
			}
		}
		uploadStates.add(fileContentDescriptorHandler.getUploadState(fileId));

		int numCompleted = 0;
		for (RemoteWriteService.UploadState uploadState : uploadStates) {
			if (uploadState == null) {
				result.corrupted = true;
				continue;
			}
			if (!uploadState.deleted && uploadState.completedAt != null) {
				numCompleted++;
			}
		}

		result.uploadedFraction = (double)numCompleted / uploadStates.size();

		return result;
	}

	public List<byte[]> list() {
		List<byte[]> fileIds = uploadedFileContentDescriptors.list();
		return fileIds;
	}

	public FileMetadata getMetadata(byte[] fileId) {
		FileContentDescriptor fcd = uploadedFileContentDescriptors.get(fileId);
		if (fcd == null) {
			return null;
		}
		return fcd.getMetadata();
	}

	public boolean delete(byte[] fileId, boolean force) {
		FileContentDescriptor fcd = uploadedFileContentDescriptors.get(fileId);
		if (fcd == null) {
			return false;
		}
		boolean allSuccess = true;
		if (!Common.isInlined(fcd)) {
			for (FilePartDescriptor part : fcd.getParts()) {
				boolean success = filePartHandler.delete(part.getFileName());
				allSuccess = allSuccess && success;
			}
		}
		if (allSuccess || force) {
			boolean success = fileContentDescriptorHandler.delete(fileId);
			allSuccess = allSuccess && success;
		}
		if (allSuccess || force) {
			boolean success = uploadedFileContentDescriptors.delete(fileId);
			allSuccess = allSuccess && success;
		}
		return allSuccess;
	}

	public synchronized boolean share(byte[] fileId, SessionId sessionId) {
		LocalUploadedFileInfo uploadedFileInfo = getUploadedFileInfo(fileId);
		if (uploadedFileInfo == null) {
			return false;
		}

		boolean success = fileSharingService.share(uploadedFileInfo, sessionId);
		if (!success) {
			return false;
		}

		saveUploadedFileInfo(uploadedFileInfo);
		return true;
	}
}
