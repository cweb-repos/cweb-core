package org.cweb.files;

import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalSharedFileInfo;
import org.cweb.storage.local.LocalDataWithDir;
import org.cweb.storage.local.LocalStorageInterface;

class SharedFileInfos extends LocalDataWithDir<LocalSharedFileInfo> {
	private static final String DIR_NAME = "sharedFileInfo";

	SharedFileInfos(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(DIR_NAME, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public void put(LocalSharedFileInfo sharedFileInfo) {
		FileReference fileReference = sharedFileInfo.getFileReference();
		super.put(fileReference.getFromId(), fileReference.getFileId(), sharedFileInfo);
	}

	public LocalSharedFileInfo get(byte[] fromId, byte[] fileId) {
		return super.get(fromId, fileId, LocalSharedFileInfo.class);
	}
}
