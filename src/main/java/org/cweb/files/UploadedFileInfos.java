package org.cweb.files;

import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class UploadedFileInfos extends LocalDataSingleKey<LocalUploadedFileInfo> {
	private static final String NAME_SUFFIX = "-uploadedFileInfo";

	public UploadedFileInfos(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalUploadedFileInfo get(byte[] fileId) {
		return super.get(fileId, LocalUploadedFileInfo.class);
	}

	@Override
	public boolean delete(byte[] fileId) {
		return super.delete(fileId);
	}
}
