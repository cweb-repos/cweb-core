package org.cweb.files;

import org.cweb.schemas.files.DownloadQueueState;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class DownloadQueueStateLD extends LocalDataSingleKey<DownloadQueueState> {
	private static final String NAME_SUFFIX = "-downloadQueue";
	private final byte[] ownId;

	public DownloadQueueStateLD(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes, byte[] ownId) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
		this.ownId = ownId;
	}

	public void put(DownloadQueueState value) {
		super.put(ownId, value);
	}

	public DownloadQueueState get() {
		return super.get(ownId, DownloadQueueState.class);
	}
}
