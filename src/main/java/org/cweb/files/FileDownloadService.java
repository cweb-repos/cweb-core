package org.cweb.files;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.files.DownloadQueueState;
import org.cweb.schemas.files.EnqueuedDownload;
import org.cweb.schemas.files.FileContentDescriptor;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FilePartDescriptor;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalFileDownloadState;
import org.cweb.schemas.files.LocalSharedFileInfo;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.storage.local.LocalFileSystemInterface;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

public class FileDownloadService {
	private static final Logger log = LoggerFactory.getLogger(FileDownloadService.class);
	private static final long MAX_FILE_SIZE = 1000000000000L;
	private static final long WAIT_TIME_ON_DOWNLOAD_FAILURE = 1000L * 60 * 2;

	private final String tracePrefix;
	private final CryptoHelper cryptoHelper;
	private final LocalFileSystemInterface localFileSystemInterface;
	private final UploadedFileInfos uploadedFileInfos;
	private final FileSharingService fileSharingService;

	private final RemoteFileContentDescriptors remoteFileContentDescriptors;
	private final DownloadStates downloadStates;
	private final DownloadQueueStateLD downloadQueueState;
	private final RemoteFileHandler fileContentDescriptorHandler;
	private final RemoteFileHandler filePartHandler;

	private boolean closed;
	private final LinkedBlockingDeque<EnqueuedDownload> downloadQueue = new LinkedBlockingDeque<>();

	public enum DownloadError {
		FILE_REFERENCE_NOT_FOUND,
		NETWORK_OR_ACCESS_ERROR,
		FILE_DOES_NOT_EXIST,
		FCD_DATA_CORRUPT,
		DECRYPTION_ERROR,
		FILE_SIZE_EXCEEDED_THRESHOLD,
		FILE_CHANGED_WHILE_DOWNLOAD_IN_PROGRESS,
		FAILED_TO_CREATE_LOCAL_FILE
	}

	public FileDownloadService(String tracePrefix, CryptoHelper cryptoHelper, LocalFileSystemInterface localFileSystemInterface, RemoteWriteService remoteWriteService, RemoteReadService remoteReadService, LocalStorageInterface localStorageInterface, UploadedFileInfos uploadedFileInfos, FileSharingService fileSharingService) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.localFileSystemInterface = localFileSystemInterface;
		this.uploadedFileInfos = uploadedFileInfos;
		this.fileSharingService = fileSharingService;

		this.remoteFileContentDescriptors = new RemoteFileContentDescriptors(tracePrefix, localStorageInterface, 10, 3);
		this.downloadStates = new DownloadStates(tracePrefix, localStorageInterface, 10, 10);
		this.downloadQueueState = new DownloadQueueStateLD(tracePrefix, localStorageInterface, 1, 10, cryptoHelper.getOwnId());
		this.fileContentDescriptorHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, Common.FILE_CONTENT_DESCRIPTOR_NAME_SUFFIX);
		this.filePartHandler = new RemoteFileHandler(remoteReadService, remoteWriteService, Common.FILE_PART_NAME_SUFFIX);

		DownloadQueueState downloadQueueState = this.downloadQueueState.get();
		if (downloadQueueState != null) {
			this.downloadQueue.addAll(downloadQueueState.getQueue());
		}

		downloadThread.setDaemon(true);
		downloadThread.start();

		// TODO: GC old downloadStates
	}

	public void close() {
		synchronized (downloadThread) {
			closed = true;
			Threads.joinThreadSafe(downloadThread);
		}
	}

	private final Thread downloadThread = new Thread("FileDownloadThread") {
		@Override
		public void run() {
			while (!closed) {
				Boolean success = dequeueAndDownloadFile();
				synchronized (this) {
					if (closed) {
						break;
					}
					if (success == null) {
						Threads.waitChecked(this);
					} else if (!success) {
						Threads.waitChecked(this, WAIT_TIME_ON_DOWNLOAD_FAILURE);
					}
				}
			}
		}
	};

	private Boolean dequeueAndDownloadFile() {
		EnqueuedDownload download = downloadQueue.peek();
		if (download == null) {
			return null;
		}

		byte[] fromId = download.getFromId();
		byte[] fileId = download.getFileId();

		FileContentDescriptor fcd = remoteFileContentDescriptors.get(fromId, fileId);
		LocalFileDownloadState downloadState = downloadStates.get(fromId, fileId);

		if (fcd == null || downloadState == null) {
			log.warn(tracePrefix + " Failed to download state for " + Utils.getDebugStringFromId(fromId) + ":" + Utils.getDebugStringFromBytesShort(fileId));
			return false;
		}

		long downloadStartTime = System.currentTimeMillis();

		List<FilePartDescriptor> parts = fcd.getParts();
		int partToDownload = -1;
		for (int partIndex = 0; partIndex < parts.size(); partIndex++) {
			if (!downloadState.getDownloadedParts().contains(partIndex)) {
				partToDownload = partIndex;
				break;
			}
		}

		boolean downloadSuccess = true;
		boolean decryptionSuccess = true;
		boolean fileWriteSuccess = true;
		if (partToDownload >= 0) {
			FilePartDescriptor part = parts.get(partToDownload);
			byte[] encryptedData;
			if (partToDownload == 0 && Common.isInlined(fcd)) {
				encryptedData = fcd.getInlineContent();
			} else {
				RemoteFetchResultRaw file = filePartHandler.read(fromId, part.getFileName());
				if (file.getError() != null) {
					downloadState.setLastError("Error downloading part " + partToDownload + ": " + file.getError());
					log.trace(tracePrefix + downloadState.getLastError());
					downloadSuccess = false;
				}
				if (file.getData() == null) {
					downloadState.setLastError("Part not found " + partToDownload);
					log.trace(tracePrefix + downloadState.getLastError());
					downloadSuccess = false;
				}
				encryptedData = file.getData();
			}

			long downloadCompletionTime = System.currentTimeMillis();
			if (downloadSuccess) {
				log.trace(tracePrefix + " Downloaded part " + partToDownload + " of " + downloadState.getLocalFileName() + ", " + encryptedData.length + " bytes in " + (downloadCompletionTime - downloadStartTime) + " ms");

				Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(encryptedData, CryptoEnvelopeDecodingParams.create().setSymmetricDecryptionKey(part.getEncryptionKey()).setSymmetricAssociatedData(Common.ASSOCIATED_DATA), CryptoContext.create());
				DecodedTypedPayload data = decodedTypedPayloadWrapper.getData();
				if (data != null && data.getPayload().getMetadata().getType() == PayloadType.CUSTOM) {
					byte[] partData = TypedPayloadUtils.unwrapCustom(data.getPayload());
					fileWriteSuccess = writeFilePart(downloadState, part, partData);
					if (!fileWriteSuccess) {
						downloadState.setLastError("Failed to write part " + partToDownload);
					}
				} else {
					downloadState.setLastError("Failed to decrypt part " + partToDownload);
					decryptionSuccess = false;
				}
			}
		}

		long downloadCompletionTime = System.currentTimeMillis();

		if (downloadSuccess && decryptionSuccess && fileWriteSuccess) {
			downloadState.setLastError(null);
			downloadState.setNumConsecutiveErrors(0);
			if (partToDownload >= 0) {
				downloadState.getDownloadedParts().add(partToDownload);
			}
		} else {
			downloadState.setNumConsecutiveErrors(downloadState.getNumConsecutiveErrors() + 1);
			if (!decryptionSuccess) {
				downloadState.setAbortedAt(downloadCompletionTime);
			}
		}

		if (downloadState.getDownloadedParts().size() == parts.size()) {
			downloadState.getDownloadedParts().clear();
			downloadState.setCompletedAt(downloadCompletionTime);
		}

		downloadStates.put(fromId, fileId, downloadState);

		if (downloadState.isSetCompletedAt() || downloadState.isSetAbortedAt()) {
			boolean removed = downloadQueue.remove(download);
			if (!removed) {
				log.error(tracePrefix + " Download queue inconsistency");
			}
			remoteFileContentDescriptors.delete(fromId, fileId);
			flushQueueState();
		}

		if (!downloadSuccess) {
			boolean removed = downloadQueue.remove(download);
			if (!removed) {
				log.error(tracePrefix + " Download queue inconsistency");
			}
			downloadQueue.add(download);
			// Do not flush queue since only the order is changing
		}

		return downloadSuccess;
	}

	private void flushQueueState() {
		DownloadQueueState downloadQueueState = new DownloadQueueState(new ArrayList<>(downloadQueue));
		this.downloadQueueState.put(downloadQueueState);
	}

	private void enqueueDownload(byte[] fromId, byte[] fileId) {
		EnqueuedDownload download = new EnqueuedDownload(ByteBuffer.wrap(fromId), ByteBuffer.wrap(fileId));
		if (!downloadQueue.contains(download)) {
			downloadQueue.add(download);
			flushQueueState();
		}
		synchronized (downloadThread) {
			downloadThread.notify();
		}
	}

	public void addSharedFileReference(FileReference fileReference) {
		fileSharingService.putFileReference(fileReference);
	}

	private Pair<FileReference, DownloadError> getFileReference(byte[] fromId, byte[] fileId) {
		FileReference fileReference;
		if (cryptoHelper.isOwnId(fromId)) {
			LocalUploadedFileInfo uploadedFileInfo = uploadedFileInfos.get(fileId);
			if (uploadedFileInfo == null) {
				return Pair.of(null, DownloadError.FILE_REFERENCE_NOT_FOUND);
			}
			fileReference = uploadedFileInfo.getFileReference();
		} else {
			LocalSharedFileInfo sharedInfo = fileSharingService.getSharedInfo(fromId, fileId);
			if (sharedInfo == null) {
				return Pair.of(null, DownloadError.FILE_REFERENCE_NOT_FOUND);
			}
			fileReference = sharedInfo.getFileReference();
		}
		return Pair.of(fileReference, null);
	}

	private Pair<FileContentDescriptor, DownloadError> readFileContentDescriptor(FileReference fileReference) {
		RemoteFetchResultRaw fetchResult = fileContentDescriptorHandler.read(fileReference.getFromId(), fileReference.getFileId());
		if (fetchResult.getError() != null) {
			return Pair.of(null, DownloadError.NETWORK_OR_ACCESS_ERROR);
		}
		if (fetchResult.getData() == null) {
			return Pair.of(null, DownloadError.FILE_DOES_NOT_EXIST);
		}
		Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(fetchResult.getData(), CryptoEnvelopeDecodingParams.create().setSymmetricDecryptionKey(fileReference.getKey()).setSymmetricAssociatedData(Common.ASSOCIATED_DATA), CryptoContext.create());
		if (decodedTypedPayloadWrapper.getError() != null) {
			return Pair.of(null, DownloadError.DECRYPTION_ERROR);
		}
		DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
		Pair<FileContentDescriptor, String> fcdUnwrapped = TypedPayloadUtils.unwrap(decodedTypedPayload.getPayload(), FileContentDescriptor.class, Common.SERVICE_NAME_FILE_SERVICE);
		if (fcdUnwrapped.getRight() != null) {
			return Pair.of(null, DownloadError.FCD_DATA_CORRUPT);
		}
		FileContentDescriptor fcd = fcdUnwrapped.getLeft();
		return Pair.of(fcd, null);
	}

	public Pair<FileMetadata, DownloadError> readFileMeta(byte[] fromId, byte[] fileId) {
		Pair<FileReference, DownloadError> fileReferenceResult = getFileReference(fromId, fileId);
		if (fileReferenceResult.getRight() != null) {
			return Pair.of(null, fileReferenceResult.getRight());
		}
		Pair<FileContentDescriptor, DownloadError> fcdResult = readFileContentDescriptor(fileReferenceResult.getLeft());
		if (fcdResult.getRight() != null) {
			return Pair.of(null, fcdResult.getRight());
		}
		return Pair.of(fcdResult.getLeft().getMetadata(), null);
	}

	private long getFileContentSize(FileContentDescriptor fcd) {
		long size = 0;
		for (FilePartDescriptor part : fcd.getParts()) {
			size += part.getContentLength();
		}
		return size;
	}

	private long getFileContentHash(FileContentDescriptor fcd) {
		long hash = 0;
		int i = 0;
		for (FilePartDescriptor part : fcd.getParts()) {
			hash ^= part.getContentLength();
			hash ^= Arrays.hashCode(part.getContentHash());
			hash = hash << i;
			i++;
		}
		return hash;
	}

	private long hashFileContentDescriptor(FileMetadata metadata) {
		long hash = metadata.getName().hashCode() + (int) metadata.getCreatedAt();
		return hash;
	}

	public Pair<FileMetadata, DownloadError> startDownload(byte[] fromId, byte[] fileId, String localFileName) {
		Pair<FileReference, DownloadError> fileReferenceResult = getFileReference(fromId, fileId);
		if (fileReferenceResult.getRight() != null) {
			return Pair.of(null, fileReferenceResult.getRight());
		}
		return startDownload(fileReferenceResult.getLeft(), localFileName);
	}

	public Pair<FileMetadata, DownloadError> startDownload(FileReference fileReference, String localFileName) {
		Pair<FileContentDescriptor, DownloadError> fcdResult = readFileContentDescriptor(fileReference);
		if (fcdResult.getRight() != null) {
			return Pair.of(null, fcdResult.getRight());
		}
		FileContentDescriptor fcd = fcdResult.getLeft();

		/*long origHash = hashFileContentDescriptor(origFcd);
		long currentHash = hashFileContentDescriptor(fcd);
		if (origHash != currentHash) {
			log.debug(tracePrefix + " Mismatching hash for " + Utils.getDebugStringFromId(fromId) + ":" + Utils.getDebugStringFromBytesShort(fileId));
			return null;
		}*/

		return startDownload(fileReference.getFromId(), fileReference.getFileId(), fcd, localFileName);
	}

	private synchronized Pair<FileMetadata, DownloadError> startDownload(byte[] fromId, byte[] fileId, FileContentDescriptor fcd, String localFileName) {
		long fileSize = getFileContentSize(fcd);
		if (fileSize <= 0 || fileSize > MAX_FILE_SIZE) {
			log.trace(tracePrefix + " Invalid file size " + localFileName + ":" + fileSize);
			return Pair.of(null, DownloadError.FILE_SIZE_EXCEEDED_THRESHOLD);
		}

		LocalFileDownloadState downloadState = downloadStates.get(fromId, fileId);
		FileContentDescriptor fcdFromCache = remoteFileContentDescriptors.get(fromId, fileId);
		if (downloadState != null && !downloadState.isSetAbortedAt() && !downloadState.isSetCompletedAt() && fcdFromCache != null) {
			if (fcd.equals(fcdFromCache)) {
				return Pair.of(fcd.getMetadata(), null);
			} else {
				log.trace(tracePrefix + " Repeated download for " + Utils.getDebugStringFromId(fromId) + ":" + Utils.getDebugStringFromBytesShort(fileId) + " to " + localFileName);
				return Pair.of(null, DownloadError.FILE_CHANGED_WHILE_DOWNLOAD_IN_PROGRESS);
			}
		}

		boolean createdFile = localFileSystemInterface.createFile(localFileName, fileSize);
		if (!createdFile) {
			log.trace(tracePrefix + " Failed to create " + localFileName);
			return Pair.of(null, DownloadError.FAILED_TO_CREATE_LOCAL_FILE);
		}

		remoteFileContentDescriptors.put(fromId, fileId, fcd);

		downloadState = new LocalFileDownloadState(ByteBuffer.wrap(fromId), ByteBuffer.wrap(fileId), localFileName, 0, new ArrayList<Integer>());
		long now = System.currentTimeMillis();
		downloadState.setStartedAt(now);

		downloadStates.put(fromId, fileId, downloadState);

		enqueueDownload(fromId, fileId);

		return Pair.of(fcd.getMetadata(), null);
	}

	private boolean writeFilePart(LocalFileDownloadState downloadState, FilePartDescriptor part, byte[] data) {
		byte[] dataHash = CryptoHelper.hashData(data);
		if (!Arrays.equals(dataHash, part.getContentHash())) {
			log.info(tracePrefix + " invalid content hash writing to " + downloadState.getLocalFileName() + ": " + part.getStartOffset());
			return false;
		}
		if (data.length != part.getContentLength()) {
			log.info(tracePrefix + " invalid content length writing to " + downloadState.getLocalFileName() + ": " + part.getStartOffset() + ":" + part.getContentLength() + ":" + data.length);
			return false;
		}
		return localFileSystemInterface.write(downloadState.getLocalFileName(), part.getStartOffset(), data);
	}

	public static class DownloadState {
		public Long startedAt;
		public Long completedAt;
		public Long abortedAt;
		public Double downloadedFraction = 0.0;
	}

	public DownloadState getDownloadState(FileReference fileReference) {
		return getDownloadState(fileReference.getFromId(), fileReference.getFileId());
	}

	public DownloadState getDownloadState(byte[] fromId, byte[] fileId) {
		DownloadState result = new DownloadState();

		LocalFileDownloadState downloadState = downloadStates.get(fromId, fileId);

		if (downloadState == null) {
			return null;
		}

		if (downloadState.isSetStartedAt()) {
			result.startedAt = downloadState.getStartedAt();
		}
		if (downloadState.isSetAbortedAt()) {
			result.abortedAt = downloadState.getAbortedAt();
		}
		if (downloadState.isSetCompletedAt()) {
			result.completedAt = downloadState.getCompletedAt();
			result.downloadedFraction = 1.0;
		} else {
			FileContentDescriptor fcd = remoteFileContentDescriptors.get(fromId, fileId);
			if (fcd != null) {
				result.downloadedFraction = (double) downloadState.getDownloadedParts().size() / fcd.getParts().size();
			} else {
				result.downloadedFraction = -1.0;
			}
		}

		return result;
	}

	public List<Pair<byte[], byte[]>> listAllDownloads() {
		return downloadStates.listFiles();
	}

	public List<Pair<byte[], byte[]>> listCurrentDownloads() {
		ArrayList<EnqueuedDownload> queueSnapshot = new ArrayList<>(downloadQueue);
		List<Pair<byte[], byte[]>> result = new ArrayList<>(queueSnapshot.size());
		for (EnqueuedDownload download : queueSnapshot) {
			result.add(Pair.of(download.getFromId(), download.getFileId()));
		}
		return result;
	}

	public FileMetadata getMetadataOfCurrentDownload(byte[] fromId, byte[] fileId) {
		FileContentDescriptor fcd = remoteFileContentDescriptors.get(fromId, fileId);
		if (fcd == null) {
			return null;
		}
		return fcd.getMetadata();
	}
}
