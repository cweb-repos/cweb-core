package org.cweb.files;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.schemas.files.LocalFileDownloadState;
import org.cweb.storage.local.LocalDataWithDir;
import org.cweb.storage.local.LocalStorageInterface;

import java.util.ArrayList;
import java.util.List;

class DownloadStates extends LocalDataWithDir<LocalFileDownloadState> {
	private static final String DIR_NAME = "fileDownloadState";

	DownloadStates(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(DIR_NAME, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalFileDownloadState get(byte[] id, byte[] fileId) {
		return super.get(id, fileId, LocalFileDownloadState.class);
	}

	public List<Pair<byte[], byte[]>> listFiles() {
		List<Pair<byte[], byte[]>> result = new ArrayList<>();
		List<byte[]> fromIds = listDirs();
		for (byte[] fromId : fromIds) {
			List<byte[]> fileIds = list(fromId);
			for (byte[] fileId : fileIds) {
				result.add(Pair.of(fromId, fileId));
			}
		}
		return result;
	}
}
