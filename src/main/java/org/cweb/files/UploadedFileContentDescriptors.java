package org.cweb.files;

import org.cweb.schemas.files.FileContentDescriptor;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class UploadedFileContentDescriptors extends LocalDataSingleKey<FileContentDescriptor> {
	private static final String NAME_SUFFIX = "-fcdLocal";

	public UploadedFileContentDescriptors(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public FileContentDescriptor get(byte[] fileId) {
		return super.get(fileId, FileContentDescriptor.class);
	}

	@Override
	public boolean delete(byte[] fileId) {
		return super.delete(fileId);
	}
}
