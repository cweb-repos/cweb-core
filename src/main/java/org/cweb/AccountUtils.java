package org.cweb;

import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TBase;
import org.cweb.crypto.lib.AESCipher;
import org.cweb.schemas.backup.BackupMetadata;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.local.LocalStorageInterfaceFactory;
import org.cweb.storage.remote.RemoteStorageClient;
import org.cweb.storage.remote.RemoteStorageFactory;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.ZipUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class AccountUtils {
	private static final Logger log = LoggerFactory.getLogger(AccountUtils.class);
	private static final String BACKUP_METADATA_NAME = "backupMetadata";

	public static <T extends TBase> ApplicationDataStore<T> getApplicationDataStore(String localRootPath, Class<T> klass, String name) {
		LocalStorageInterface localStorageInterface = LocalStorageInterfaceFactory.getLocalStorageInterface(localRootPath);
		return new ApplicationDataStore<>("static", klass, name, localStorageInterface, 1);
	}

	public static boolean testStorageProfile(PrivateStorageProfile privateStorageProfile) {
		RemoteStorageClient remoteClient = RemoteStorageFactory.getRemoteStorageClient();
		return RemoteWriteService.testPrivateStorageProfile(privateStorageProfile, remoteClient);
	}

	public static InstanceContainer attachPrivateStorageProfile(InstanceContainerBase instanceContainerBase, PrivateStorageProfile privateStorageProfile) {
		boolean success = testStorageProfile(privateStorageProfile);
		if (!success) {
			return null;
		}
		return instanceContainerBase.attachPrivateStorageProfile(privateStorageProfile);
	}

	public static InstanceContainerBase createNewIdentity(String localRootPath, byte[] secretStorageKey) {
		return new InstanceContainerBase(localRootPath, null, secretStorageKey);
	}

	public static InstanceContainer createNewIdentity(String localRootPath, PrivateStorageProfile privateStorageProfile, byte[] secretStorageKey) {
		InstanceContainerBase instanceContainerBase = createNewIdentity(localRootPath, secretStorageKey);
		return attachPrivateStorageProfile(instanceContainerBase, privateStorageProfile);
	}

	public static boolean backup(InstanceContainer instanceContainer, OutputStream out, byte[] key, byte[] applicationData) {
		byte[] iv = AESCipher.getIv();
		try (CipherOutputStream cipherOutputStream = new CipherOutputStream(out, AESCipher.getCipher(Cipher.ENCRYPT_MODE, iv, key));
		     ZipOutputStream zipOutputStream = new ZipOutputStream(cipherOutputStream)) {
			out.write(iv);

			writeMetadata(instanceContainer, zipOutputStream, applicationData);

			backup(instanceContainer.localStorageInterface, zipOutputStream);

			return true;
		} catch (Exception e) {
			log.error("Failed to create backup at", e);
			return false;
		}
	}

	private static void writeMetadata(InstanceContainer instanceContainer, ZipOutputStream zipOutputStream, byte[] applicationData) throws IOException {
		BackupMetadata metadata = new BackupMetadata(ByteBuffer.wrap(instanceContainer.getOwnId()));
		metadata.setApplicationData(applicationData);
		ZipUtils.addToZip(BACKUP_METADATA_NAME, ThriftUtils.serialize(metadata), zipOutputStream);
	}

	private static void backup(LocalStorageInterface localStorageInterface, ZipOutputStream zipOutputStream) throws IOException {
		List<String> files = localStorageInterface.listAllFiles();
		for (String file: files) {
			byte[] content = localStorageInterface.read(file);
			ZipUtils.addToZip(file, content, zipOutputStream);
		}
	}

	private static ZipInputStream getRestoreZipInputStream(InputStream in, byte[] key) throws Exception {
		byte[] iv = new byte[AESCipher.IV_BYTES];
		int len = in.read(iv);
		Preconditions.checkArgument(len == AESCipher.IV_BYTES, "Failed to read header");
		CipherInputStream cipherInputStream = new CipherInputStream(in, AESCipher.getCipher(Cipher.DECRYPT_MODE, iv, key));
		return new ZipInputStream(cipherInputStream);
	}

	public static Pair<byte[], byte[]> restore(String localRootPath, InputStream in, byte[] key) {
		in.mark(Integer.MAX_VALUE);
		BackupMetadata metadata = extractBackupMetadata(in, key);
		if (metadata == null) {
			return null;
		}
		byte[] id = metadata.getId();
		LocalStorageInterface localStorageInterface = LocalStorageInterfaceFactory.getLocalStorageInterface(localRootPath, id);
		try {
			in.reset();
			try (ZipInputStream zipInputStream = getRestoreZipInputStream(in, key)) {
				restore(localStorageInterface, zipInputStream, BACKUP_METADATA_NAME);
			}
		} catch (Exception e) {
			log.error("Failed to extract backup", e);
			return null;
		}
		return Pair.of(id, metadata.getApplicationData());
	}

	private static BackupMetadata extractBackupMetadata(InputStream in, byte[] key) {
		BackupMetadata metadata;
		try (ZipInputStream zipInputStream = getRestoreZipInputStream(in, key)) {
			byte[] backupMetadataContent = ZipUtils.unzipByName(zipInputStream, BACKUP_METADATA_NAME);
			if (backupMetadataContent == null) {
				log.error("Corrupt backup or invalid key");
				return null;
			}
			metadata = ThriftUtils.deserialize(backupMetadataContent, BackupMetadata.class);
		} catch (Exception e) {
			log.error("Failed to extract backup metadata", e);
			return null;
		}
		return metadata;
	}

	private static void restore(LocalStorageInterface localStorageInterface, ZipInputStream zipInputStream, String excludeName) throws IOException {
		if (!localStorageInterface.listAllFiles().isEmpty()) {
			throw new RuntimeException("Can't restore into an existing destination");
		}
		for (ZipEntry entry = zipInputStream.getNextEntry(); entry != null; entry = zipInputStream.getNextEntry()) {
			if (entry.getName().equals(excludeName)) {
				continue;
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.copy(zipInputStream, out);
			localStorageInterface.write(entry.getName(), out.toByteArray());
		}
	}
}
