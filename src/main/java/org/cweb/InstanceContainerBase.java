package org.cweb;

import com.google.common.base.Preconditions;
import org.cweb.admin.RemoteAdminClientService;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.IdentityCryptoService;
import org.cweb.identity.PublicStorageProfiles;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.identity.RemoteIdentityService;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.local.LocalStorageInterfaceFactory;
import org.cweb.storage.local.SecretStorageService;
import org.cweb.storage.remote.InboundCacheService;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.RemoteStorageClient;
import org.cweb.storage.remote.RemoteStorageFactory;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Partial instance container without own remote storage
 */
public class InstanceContainerBase {
	private static final Logger log = LoggerFactory.getLogger(InstanceContainerBase.class);
	protected static final String PRIVATE_STORAGE_PROFILE_FILENAME = "privateStorageProfile";

	protected final String tracePrefix;
	protected final String localRootPath;
	protected final byte[] secretStorageKey;
	protected final LocalStorageInterface localStorageInterface;
	protected final SecretStorageService secretStorageService;
	protected final IdentityCryptoService identityCryptoService;
	protected final CryptoHelper cryptoHelper;
	protected final RemoteAdminClientService remoteAdminClientService;
	protected final InboundCacheService inboundCacheService;
	protected final PublicStorageProfiles publicStorageProfiles;
	protected final RemoteStorageClient remoteStorageClient;
	protected final RemoteReadService remoteReadService;
	protected final RemoteIdentityService remoteIdentityService;

	public InstanceContainerBase(String localRootPath, byte[] idOrNullToCreate, byte[] secretStorageKey) {
		byte[] id;
		IdentityCryptoService newIdentityCryptoService = null;
		if (idOrNullToCreate == null) {
			newIdentityCryptoService = IdentityCryptoService.generateNewKeys();
			id = newIdentityCryptoService.getOwnId();
			tracePrefix = Utils.getDebugStringFromId(id);
			log.debug(tracePrefix + " created at " + localRootPath);
		} else {
			id = idOrNullToCreate;
			tracePrefix = Utils.getDebugStringFromId(id);
			log.debug(tracePrefix + " starting at " + localRootPath);
		}

		this.localRootPath = localRootPath;
		this.secretStorageKey = secretStorageKey;
		this.localStorageInterface = LocalStorageInterfaceFactory.getLocalStorageInterface(localRootPath, id);
		this.secretStorageService = new SecretStorageService(localStorageInterface, secretStorageKey);

		if (newIdentityCryptoService != null) {
			newIdentityCryptoService.saveIdentityKeys(secretStorageService);
			this.identityCryptoService = newIdentityCryptoService;
		} else {
			this.identityCryptoService = IdentityCryptoService.loadFromFile(secretStorageService);
		}

		this.cryptoHelper = new CryptoHelper(tracePrefix, identityCryptoService);
		this.remoteAdminClientService = new RemoteAdminClientService(tracePrefix, cryptoHelper);

		this.inboundCacheService = new InboundCacheService(localStorageInterface);
		this.publicStorageProfiles = new PublicStorageProfiles(tracePrefix, localStorageInterface, 1000, 1440);
		this.remoteStorageClient = RemoteStorageFactory.getRemoteStorageClient();
		this.remoteReadService = new RemoteReadService(tracePrefix, inboundCacheService, publicStorageProfiles, remoteStorageClient);
		this.remoteIdentityService = new RemoteIdentityService(tracePrefix, localStorageInterface, remoteReadService, publicStorageProfiles, cryptoHelper);
	}

	public byte[] getOwnId() {
		return cryptoHelper.getOwnId();
	}

	public RemoteAdminClientService getRemoteAdminClientService() {
		return remoteAdminClientService;
	}

	public RemoteIdentityService getRemoteIdentityService() {
		return remoteIdentityService;
	}

	public RemoteIdentityFetcher getRemoteIdentityFetcher() {
		return remoteIdentityService.getFetcher();
	}

	public void savePrivateStorageProfile(PrivateStorageProfile privateStorageProfile) {
		secretStorageService.write(PRIVATE_STORAGE_PROFILE_FILENAME, ThriftUtils.serialize(privateStorageProfile));
		log.trace(tracePrefix + " Saved private storage profile");
	}

	public InstanceContainer attachPrivateStorageProfile(PrivateStorageProfile privateStorageProfile) {
		Preconditions.checkArgument(secretStorageService.read(PRIVATE_STORAGE_PROFILE_FILENAME) == null);
		savePrivateStorageProfile(privateStorageProfile);
		return new InstanceContainer(localRootPath, getOwnId(), secretStorageKey);
	}

	public void deleteInboundStorageCache() {
		remoteReadService.deleteLocalCache();
		remoteIdentityService.deleteLocalCache();
	}
}
