package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.communication.SharedObjectReadService;
import org.cweb.communication.SharedObjectUpdateProcessor;
import org.cweb.payload.GenericPayloadTypePredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.identity.LocalIdentityProfileRemote;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IdentityProfileReadService {
	private static final Logger log = LoggerFactory.getLogger(IdentityProfileReadService.class);
	private static final String SERVICE_NAME = "IdentityProfileService";
	private final String tracePrefix;
	private final SharedObjectReadService sharedObjectReadService;
	private final IdentityProfilesRemote localProfiles;

	private final List<IdentityProfileUpdateCallback> updateCallbacks = new ArrayList<>();

	public IdentityProfileReadService(String tracePrefix, LocalStorageInterface localStorageInterface, SharedObjectReadService sharedObjectReadService) {
		this.tracePrefix = tracePrefix;
		this.sharedObjectReadService = sharedObjectReadService;

		this.localProfiles = new IdentityProfilesRemote(tracePrefix, localStorageInterface, 5, 5);

		PayloadTypePredicate descriptorPredicate = new GenericPayloadTypePredicate(PayloadType.IDENTITY_PROFILE, SERVICE_NAME, null, null);
		sharedObjectReadService.addUpdateProcessor(descriptorPredicate, new SharedObjectUpdateProcessor() {
			@Override
			public Result processUpdate(byte[] objectId, TypedPayload typedPayload) {
				return processProfileUpdate(objectId, typedPayload);
			}

			@Override
			public void processUnsubscribe(byte[] objectId) {
				onUnsubscribedMessageReceived(objectId);
			}
		});
	}

	private SharedObjectUpdateProcessor.Result processProfileUpdate(byte[] objectId, TypedPayload typedPayload) {
		IdentityProfile profile = extractProfile(typedPayload);
		if (profile == null) {
			return SharedObjectUpdateProcessor.Result.NOT_MATCHING;
		}
		SharedObjectReadService.ObjectMetadata objectMetadata = sharedObjectReadService.getObjectMetadata(objectId);
		byte[] fromId = objectMetadata.fromId;
		LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
		if (localIdentityProfileRemote != null && !Arrays.equals(objectId, localIdentityProfileRemote.getProfileObjectId())) {
			log.debug(tracePrefix + " Conflicting profile objectId " + Utils.getDebugStringFromId(objectId) + " != " + Utils.getDebugStringFromId(localIdentityProfileRemote.getProfileObjectId()));
			localIdentityProfileRemote = null;
		}

		if (localIdentityProfileRemote == null) {
			localIdentityProfileRemote = new LocalIdentityProfileRemote(ByteBuffer.wrap(fromId), ByteBuffer.wrap(objectId));
			localProfiles.put(localIdentityProfileRemote);
		}

		for (IdentityProfileUpdateCallback callback : updateCallbacks) {
			callback.processUpdate(fromId, profile);
		}

		return SharedObjectUpdateProcessor.Result.PROCESSED;
	}

	private void onUnsubscribedMessageReceived(byte[] objectId) {
		SharedObjectReadService.ObjectMetadata metadata = sharedObjectReadService.getObjectMetadata(objectId);
		byte[] fromId = metadata.fromId;
		LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
		if (localIdentityProfileRemote == null || !Arrays.equals(objectId, localIdentityProfileRemote.getProfileObjectId())) {
			log.debug(tracePrefix + " Invalid un-subscription message on objectId" + Utils.getDebugStringFromId(objectId) + ", from " + Utils.getDebugStringFromId(fromId));
			return;
		}
		// Keep the last profile around
		// localProfiles.delete(fromId);
	}

	public synchronized void addUpdateCallback(IdentityProfileUpdateCallback updateCallback) {
		updateCallbacks.add(updateCallback);
	}

	public byte[] getObjectId(byte[] fromId) {
		LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
		if (localIdentityProfileRemote == null) {
			return null;
		}
		return localIdentityProfileRemote.getProfileObjectId();
	}

	public List<byte[]> getObjectIds(List<byte[]> fromIds) {
		List<byte[]> result = new ArrayList<>();
		for (byte[] fromId : fromIds) {
			LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
			if (localIdentityProfileRemote != null) {
				result.add(localIdentityProfileRemote.getProfileObjectId());
			}
		}
		return result;
	}

	public IdentityProfile getProfile(byte[] fromId) {
		LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
		if (localIdentityProfileRemote == null) {
			return null;
		}
		TypedPayload typedPayload = sharedObjectReadService.getCurrent(localIdentityProfileRemote.getProfileObjectId());
		if (typedPayload == null) {
			return null;
		}

		return extractProfile(typedPayload);
	}

	private IdentityProfile extractProfile(TypedPayload typedPayload) {
		Pair<IdentityProfile, String> profileUnwrapped = TypedPayloadUtils.unwrap(typedPayload, IdentityProfile.class, SERVICE_NAME);
		if (profileUnwrapped.getRight() != null) {
			log.trace(tracePrefix + " Failed to extract profile: " + profileUnwrapped.getRight());
			return null;
		}

		return profileUnwrapped.getLeft();
	}

	public void requestProfileFetch(byte[] fromId) {
		LocalIdentityProfileRemote localIdentityProfileRemote = localProfiles.get(fromId);
		if (localIdentityProfileRemote == null) {
			return;
		}
		sharedObjectReadService.requestObjectFetch(localIdentityProfileRemote.getProfileObjectId(), fromId);
	}
}
