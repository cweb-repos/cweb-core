package org.cweb.identity;

import org.cweb.schemas.identity.LocalIdentityProfileRemote;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

class IdentityProfilesRemote extends LocalDataSingleKey<LocalIdentityProfileRemote> {
	private static final String NAME_SUFFIX = "-identityProfileRemote";

	IdentityProfilesRemote(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalIdentityProfileRemote get(byte[] fromId) {
		return super.get(fromId, LocalIdentityProfileRemote.class);
	}

	public void put(LocalIdentityProfileRemote localState) {
		super.put(localState.getFromId(), localState);
	}
}
