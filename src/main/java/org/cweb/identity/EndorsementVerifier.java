package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;

import java.util.List;

public interface EndorsementVerifier {
	enum Result {
		SUCCESS,
		FAIL,
		IGNORE_ONCE,
		IGNORE_PERMANENTLY,
	}

	Pair<Result, String> verify(IdentityDescriptor identityDescriptor, List<Property> propertiesToEndorse);
}
