package org.cweb.identity;

import org.cweb.Migrations;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class PublicStorageProfiles extends LocalDataSingleKey<PublicStorageProfile> {
	private static final String NAME_SUFFIX = "-storageProfile";

	public PublicStorageProfiles(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	@Override
	public void put(byte[] id, PublicStorageProfile value) {
		super.put(id, value);
	}

	public PublicStorageProfile get(byte[] id) {
		PublicStorageProfile publicStorageProfile = super.get(id, PublicStorageProfile.class);
		if (publicStorageProfile != null) {
			boolean changed = Migrations.migratePublicStorageProfile(publicStorageProfile);
			if (changed) {
				put(id, publicStorageProfile);
			}
		}
		return publicStorageProfile;
	}

	@Override
	public boolean delete(byte[] id) {
		return super.delete(id);
	}
}
