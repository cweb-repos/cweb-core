package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.cweb.Migrations;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.crypto.lib.X3DH;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.crypto.X3DHPreKeyBundle;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.storage.PrivateBroadcastConfig;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.NameConversionUtils;
import org.cweb.storage.local.LocalPreKeyService;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.OutboundDataWrapperRaw;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.utils.Constants;
import org.cweb.utils.PropertyUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.ThriftUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class IdentityService {
	private static final Logger log = LoggerFactory.getLogger(IdentityService.class);
	public static final int ID_SIZE = 32;
	private static final long IDENTITY_UPLOAD_PERIOD = 1000L * 60 * 60 * 24 * 2;
	private static final long IDENTITY_TTL = 1000L * 60 * 60 * 24 * 60;
	private static final long PRE_KEY_REFRESH_PERIOD = 1000L * 60 * 60 * 24;
	static final String REMOTE_IDENTITY_NAME_SUFFIX = "-identity";
	private static final String LOCAL_IDENTITY_FILENAME = "id/identity";

	private final LocalStorageInterface localStorageInterface;
	private final CryptoHelper cryptoHelper;
	private final RemoteWriteService remoteWriteService;

	private final LocalPreKeyService localPreKeyService;

	private final IdentityDescriptor identityDescriptor;
	private final String tracePrefix;

	public IdentityService(String tracePrefix, PublicStorageProfile publicStorageProfile, LocalStorageInterface localStorageInterface, CryptoHelper cryptoHelper, RemoteWriteService remoteWriteService, LocalPreKeyService localPreKeyService) {
		this.tracePrefix = tracePrefix;
		this.localStorageInterface = localStorageInterface;
		this.cryptoHelper = cryptoHelper;
		this.remoteWriteService = remoteWriteService;
		IdentityDescriptor restoredIdentityDescriptor = loadOwnIdentity(localStorageInterface, cryptoHelper);
		if (restoredIdentityDescriptor != null) {
			this.identityDescriptor = restoredIdentityDescriptor;
			this.identityDescriptor.setStorageProfile(publicStorageProfile);
		} else {
			this.identityDescriptor = createEmptyIdentity(publicStorageProfile);
		}
		this.localPreKeyService = localPreKeyService;

		updatePreKeys();
		saveAndUploadIdentity();

		Threads.submitBackgroundTaskPeriodically(() -> {
			updatePreKeys();
			saveAndUploadIdentity();
		}, IDENTITY_UPLOAD_PERIOD, IDENTITY_UPLOAD_PERIOD);
	}

	private void updatePreKeys() {
		Long currentPreKeyTime = null;
		if (!identityDescriptor.getX3dhPreKeyBundles().isEmpty()) {
			X3DHPreKeyBundle currentPreKey = identityDescriptor.getX3dhPreKeyBundles().get(0);
			currentPreKeyTime = localPreKeyService.getCreationTime(currentPreKey.getPreKey());
		}

		long now = System.currentTimeMillis();
		if (currentPreKeyTime != null && now - currentPreKeyTime < PRE_KEY_REFRESH_PERIOD) {
			return;
		}

		KeyPair keyPair = cryptoHelper.generateNewECKeyPair();
		localPreKeyService.addKeyPair(X3DH.hashKey(keyPair.getPublicKey()), keyPair, now);
		X3DHPreKeyBundle bundle = cryptoHelper.generatePreKeyBundle(keyPair.getPublicKey());
		identityDescriptor.setX3dhPreKeyBundles(Collections.singletonList(bundle));
	}

	private static IdentityDescriptor loadOwnIdentity(LocalStorageInterface localStorageInterface, CryptoHelper cryptoHelper) {
		try {
			if (!localStorageInterface.checkIfExists(LOCAL_IDENTITY_FILENAME)) {
				return null;
			}
			byte[] cryptoEnvelopeSerialized = localStorageInterface.read(LOCAL_IDENTITY_FILENAME);
			Triple<IdentityDescriptor, SignatureMetadata, String> identityDescriptorResult = extractIdentityDescriptor(cryptoEnvelopeSerialized);
			if (identityDescriptorResult.getRight() != null) {
				throw new RuntimeException(identityDescriptorResult.getRight());
			}

			IdentityDescriptor identityDescriptor = identityDescriptorResult.getLeft();

			if (!Arrays.equals(identityDescriptor.getRsaPublicKey().getPublicKey(), cryptoHelper.getOwnRSAPublicKey().getPublicKey())) {
				throw new RuntimeException("RSA public key mismatch with local identityDescriptor for " + toString(identityDescriptor));
			}
			if (identityDescriptor.getEcPublicKey() == null) {
				identityDescriptor.setEcPublicKey(cryptoHelper.getOwnECPublicKey());
			}
			if (!Arrays.equals(identityDescriptor.getEcPublicKey().getPublicKey(), cryptoHelper.getOwnECPublicKey().getPublicKey())) {
				throw new RuntimeException("EC public key mismatch with local identityDescriptor for " + toString(identityDescriptor));
			}
			if (!cryptoHelper.isOwnId(identityDescriptor.getId())) {
				throw new RuntimeException("Id mismatch with local identityDescriptor for " + toString(identityDescriptor));
			}
			if (identityDescriptor.getIdProofEnvelope() == null) {
				CryptoEnvelope idProofEnvelope = cryptoHelper.createIdProofEnvelope();
				identityDescriptor.setIdProofEnvelope(ByteBuffer.wrap(ThriftUtils.serialize(idProofEnvelope)));
			}

			Migrations.migrateIdentityDescriptor(identityDescriptor);

			identityDescriptor.setProtocolVersion(Constants.PROTOCOL_VERSION);

			return identityDescriptor;
		} catch (Exception e) {
			log.error("Failed to load identityDescriptor", e);
			return null;
			// throw new RuntimeException(e);
		}
	}

	static Triple<IdentityDescriptor, SignatureMetadata, String> extractIdentityDescriptor(byte[] cryptoEnvelopeSerialized) {
		Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(cryptoEnvelopeSerialized, CryptoEnvelopeDecodingParams.create().setSelfSignedIdentityDescriptor(true), CryptoContext.create());
		if (decodedTypedPayloadWrapper.getError() != null) {
			 return Triple.of(null, null, "Error extracting identity envelope");
		}
		DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
		TypedPayload identityDescriptorTypedPayload = decodedTypedPayload.getPayload();

		Pair<IdentityDescriptor, String> identityDescriptorUnwrapped = TypedPayloadUtils.unwrap(identityDescriptorTypedPayload, IdentityDescriptor.class, null);
		if (identityDescriptorUnwrapped.getRight() != null) {
			return Triple.of(null, null, "Failed to extract descriptor: " + identityDescriptorUnwrapped.getRight());
		}
		IdentityDescriptor identityDescriptor = identityDescriptorUnwrapped.getLeft();

		byte[] id = identityDescriptor.getId();
		SignatureMetadata signatureMetadata = decodedTypedPayload.getSignatureMetadata();
		if (signatureMetadata == null || !Arrays.equals(signatureMetadata.getSignerId(), id)) {
			return Triple.of(null, null, "IdentityDescriptor signature verification failed for " + toString(identityDescriptor));
		}

		Migrations.migrateIdentityDescriptor(identityDescriptor);

		if (!CryptoHelper.verifyIdProofEnvelope(identityDescriptor, identityDescriptor.getIdProofEnvelope())) {
			return Triple.of(null, null, "Id Proof verification failed for " + toString(identityDescriptor));
		}

		return Triple.of(identityDescriptor, signatureMetadata, null);
	}

	public static boolean isValidId(byte[] id) {
		return id != null && id.length == ID_SIZE;
	}

	private IdentityDescriptor createEmptyIdentity(PublicStorageProfile publicStorageProfile) {
		CryptoEnvelope idProofEnvelope = cryptoHelper.createIdProofEnvelope();
		PrivateBroadcastConfig privateBroadcastConfigDefault = new PrivateBroadcastConfig(2, 10);
		IdentityDescriptor identityDescriptor = new IdentityDescriptor(ByteBuffer.wrap(cryptoHelper.getOwnId()), Constants.PROTOCOL_VERSION, cryptoHelper.getOwnRSAPublicKey(), cryptoHelper.getOwnECPublicKey(), ByteBuffer.wrap(ThriftUtils.serialize(idProofEnvelope)), new ArrayList<>(), new ArrayList<>(), publicStorageProfile, privateBroadcastConfigDefault, new ArrayList<>());
		identityDescriptor.setPrivateBroadcastConfig(privateBroadcastConfigDefault);
		return identityDescriptor;
	}

	public static String toString(byte[] id) {
		return NameConversionUtils.toString(id);
	}

	public static String toString(IdentityDescriptor identityDescriptor) {
		return toString(identityDescriptor.getId());
	}

	public static byte[] idFromString(String idStr) {
		byte[] id = NameConversionUtils.fromString(idStr);
		return isValidId(id) ? id : null;
	}

	public IdentityDescriptor getIdentityDescriptor() {
		return identityDescriptor;
	}

	public IdentityReference getOwnIdentityReference() {
		return new IdentityReference(ByteBuffer.wrap(cryptoHelper.getOwnId()), identityDescriptor.getStorageProfile());
	}

	private CryptoEnvelope createSignedIdentity() {
		CryptoEnvelope envelope = cryptoHelper.signTypedPayload(TypedPayloadUtils.wrap(getIdentityDescriptor(), null, null, null), null, IDENTITY_TTL);
		log.trace(tracePrefix + " Signed identityDescriptor");
		return envelope;
	}

	synchronized void saveAndUploadIdentity() {
		saveIdentity();
		uploadIdentity();
	}

	private void saveIdentity() {
		try {
			CryptoEnvelope signedIdentityEnvelope = createSignedIdentity();
			localStorageInterface.write(LOCAL_IDENTITY_FILENAME, ThriftUtils.serialize(signedIdentityEnvelope));
			log.trace(tracePrefix + " Saved local identityDescriptor");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void uploadIdentity() {
		try {
			CryptoEnvelope signedIdentityEnvelope = createSignedIdentity();
			remoteWriteService.write(identityDescriptor.getId(), REMOTE_IDENTITY_NAME_SUFFIX, new OutboundDataWrapperRaw(ThriftUtils.serialize(signedIdentityEnvelope), null, null));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public boolean updateProperties(List<Property> updates) {
		if (updates.isEmpty()) {
			return false;
		}

		IdentityDescriptor identityDescriptor = getIdentityDescriptor();
		List<Property> updatedProperties = PropertyUtils.updateProperties(identityDescriptor.getOwnProperties(), updates);
		if (updatedProperties == null) {
			return false;
		}

		identityDescriptor.setOwnProperties(updatedProperties);
		saveAndUploadIdentity();
		return true;
	}

	public void updatePublicStorageProfile(PublicStorageProfile publicStorageProfile) {
		this.identityDescriptor.setStorageProfile(publicStorageProfile);
		saveAndUploadIdentity();
	}
}
