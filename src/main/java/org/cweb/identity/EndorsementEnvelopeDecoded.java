package org.cweb.identity;

import java.util.ArrayList;
import java.util.List;

public class EndorsementEnvelopeDecoded {
	public String generatedAt;
	public String validUntil;
	public String byIdStr;
	public String toIdStr;
	public List<IdentityPropertyDecoded> payload = new ArrayList<>();
}
