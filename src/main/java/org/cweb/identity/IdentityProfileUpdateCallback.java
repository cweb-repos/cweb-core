package org.cweb.identity;

import org.cweb.schemas.identity.IdentityProfile;

public interface IdentityProfileUpdateCallback {
	void processUpdate(byte[] id, IdentityProfile identityProfile);
}
