package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.communication.SharedObjectPostService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.object.SharedObjectDeliveryType;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.identity.LocalIdentityProfileStateOwn;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.PropertyUtils;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class IdentityProfilePostService {
	private static final Logger log = LoggerFactory.getLogger(IdentityProfilePostService.class);
	private static final String SERVICE_NAME = "IdentityProfileService";
	private static final String OBJECT_ID_FILE_NAME = "identityProfileObjectId";
	private static final long OBJECT_POLL_INTERVAL = 1000L * 60 * 60 * 24;
	private static final long OBJECT_TTL = 1000L * 60 * 60 * 24 * 30 * 6;
	private static final long OBJECT_KEY_TTL = 1000L * 60 * 60 * 24 * 7;
	private final String tracePrefix;
	private final LocalStorageInterface localStorageInterface;
	private final SharedObjectPostService sharedObjectPostService;
	private byte[] objectId;

	public IdentityProfilePostService(String tracePrefix, LocalStorageInterface localStorageInterface, SharedObjectPostService sharedObjectPostService) {
		this.tracePrefix = tracePrefix;
		this.localStorageInterface = localStorageInterface;
		this.sharedObjectPostService = sharedObjectPostService;
	}

	private static byte[] getOrCreateSharedObject(LocalStorageInterface localStorageInterface, SharedObjectPostService sharedObjectPostService) {
		byte[] objectId = null;
		LocalIdentityProfileStateOwn localState = null;
		byte[] localStateSerialized = localStorageInterface.read(OBJECT_ID_FILE_NAME);
		if (localStateSerialized != null) {
			localState = ThriftUtils.deserializeSafe(localStateSerialized, LocalIdentityProfileStateOwn.class);
			objectId = localState != null ? localState.getObjectId() : null;
		}
		if (objectId == null) {
			IdentityProfile identityProfile = new IdentityProfile();
			TypedPayload identityProfilePayload = TypedPayloadUtils.wrap(identityProfile, SERVICE_NAME, null, null);
			objectId = sharedObjectPostService.create(SharedObjectDeliveryType.DELIVER_LAST, OBJECT_POLL_INTERVAL, OBJECT_TTL, OBJECT_KEY_TTL, identityProfilePayload);
			localState = new LocalIdentityProfileStateOwn();
			localState.setObjectId(objectId);
			localStorageInterface.write(OBJECT_ID_FILE_NAME, ThriftUtils.serialize(localState));
		}
		return objectId;
	}

	private boolean publishProfile(IdentityProfile profile) {
		TypedPayload profilePayload = TypedPayloadUtils.wrap(profile, SERVICE_NAME, null, null);
		boolean result = sharedObjectPostService.updatePayload(objectId, profilePayload);
		log.trace(tracePrefix + " Published new profile " + Utils.getDebugStringFromId(objectId) + ", success=" + result);
		return result;
	}

	private void createObjectIfNeeded() {
		if (objectId != null) {
			return;
		}
		objectId = getOrCreateSharedObject(localStorageInterface, sharedObjectPostService);
	}

	public void setSubscribers(List<byte[]> subscriberIds) {
		createObjectIfNeeded();
		sharedObjectPostService.updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.SET, subscriberIds, null);
	}

	public void addSubscriber(byte[] subscriberId) {
		createObjectIfNeeded();
		sharedObjectPostService.updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.ADD, Collections.singletonList(subscriberId), null);
	}

	public void removeSubscriber(byte[] subscriberId) {
		createObjectIfNeeded();
		sharedObjectPostService.updateSubscribers(objectId, SharedObjectPostService.SubscriberUpdateType.REMOVE, Collections.singletonList(subscriberId), null);
	}

	public IdentityProfile getProfile() {
		createObjectIfNeeded();
		if (objectId == null) {
			return null;
		}
		TypedPayload payload = sharedObjectPostService.getCurrent(objectId);
		if (payload == null) {
			return null;
		}
		Pair<IdentityProfile, String> identityProfileUnwrapped = TypedPayloadUtils.unwrap(payload, IdentityProfile.class, SERVICE_NAME);
		return identityProfileUnwrapped.getLeft();
	}

	public boolean updateProperties(List<Property> updates) {
		IdentityProfile profile = getProfile();
		if (profile == null) {
			return false;
		}

		List<Property> updatedProperties = PropertyUtils.updateProperties(profile.getProperties(), updates);
		if (updatedProperties == null) {
			return false;
		}

		IdentityProfile newProfile = new IdentityProfile(profile);
		newProfile.setProperties(updatedProperties);
		publishProfile(newProfile);
		return true;
	}
}
