package org.cweb.identity;

import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;

public class RemoteIdentityFetcher {
	private final RemoteIdentityService remoteIdentityService;

	RemoteIdentityFetcher(RemoteIdentityService remoteIdentityService) {
		this.remoteIdentityService = remoteIdentityService;
	}

	public LocalIdentityDescriptorState fetch(byte[] id) {
		return remoteIdentityService.get(id);
	}

	public LocalIdentityDescriptorState fetch(IdentityReference identityReference) {
		remoteIdentityService.setRemoteStorageProfile(identityReference);
		return remoteIdentityService.get(identityReference.getId());
	}
}
