package org.cweb.identity;

import org.apache.commons.lang3.tuple.Triple;
import org.cweb.Migrations;
import org.cweb.crypto.CryptoHelper;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.RemoteFetchResultRaw;
import org.cweb.storage.remote.RemoteReadService;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

public class RemoteIdentityService {
	private static final Logger log = LoggerFactory.getLogger(RemoteIdentityService.class);
	private static final long REFRESH_INTERVAL = 1000L * 60 * 60 * 24 * 2;
	private static final long REFRESH_INTERVAL_MIN = 1000L * 60 * 5;
	private static final int MAX_STORAGE_PROFILE_CHAIN_LENGTH = 20;

    private final String tracePrefix;
	private final PublicStorageProfiles publicStorageProfiles;
	private final CryptoHelper cryptoHelper;
	private final RemoteReadService remoteReadService;

	private final RemoteIdentityDescriptors remoteIdentityDescriptors;
	private final RemoteIdentityFetcher remoteIdentityFetcher;
	private final Map<ByteBuffer, Long> descriptorsFetchTimes = new ConcurrentHashMap<>();

	public RemoteIdentityService(String tracePrefix, LocalStorageInterface localStorageInterface, RemoteReadService remoteReadService, PublicStorageProfiles publicStorageProfiles, CryptoHelper cryptoHelper) {
		this.tracePrefix = tracePrefix;
		this.publicStorageProfiles = publicStorageProfiles;
		this.remoteReadService = remoteReadService;
		this.cryptoHelper = cryptoHelper;

		this.remoteIdentityDescriptors = new RemoteIdentityDescriptors(tracePrefix, localStorageInterface, 1000, 30);
		this.remoteIdentityFetcher = new RemoteIdentityFetcher(this);
	}

	public RemoteIdentityFetcher getFetcher() {
		return remoteIdentityFetcher;
	}

	public boolean setRemoteStorageProfile(IdentityReference identityRef) {
		return setRemoteStorageProfile(identityRef.getId(), identityRef.getStorageProfile());
	}

	public synchronized boolean setRemoteStorageProfile(byte[] id, PublicStorageProfile publicStorageProfile) {
		if (cryptoHelper.isOwnId(id)) {
			return false;
		}
		PublicStorageProfile currentProfile = publicStorageProfiles.get(id);
		if (publicStorageProfile.equals(currentProfile)) {
			return true;
		}
		IdentityDescriptor identityDescriptor = fetchIdentityUncached(id, publicStorageProfile);
		return identityDescriptor != null;
	}

	public void setOwnRemoteStorageProfile(PublicStorageProfile publicStorageProfile) {
		byte[] id = cryptoHelper.getOwnId();
		deleteLocalCache(id);
		publicStorageProfiles.put(id, publicStorageProfile);
	}

	public PublicStorageProfile getPublicStorageProfile(byte[] id) {
		return publicStorageProfiles.get(id);
	}

	public void deleteLocalCache(byte[] id) {
		remoteIdentityDescriptors.delete(id);
		remoteReadService.deleteLocalCache(id, id, IdentityService.REMOTE_IDENTITY_NAME_SUFFIX);
	}

	public void deleteLocalCache() {
		List<byte[]> allIds = remoteIdentityDescriptors.list();
		for (byte[] id: allIds) {
			deleteLocalCache(id);
		}
	}

	public LocalIdentityDescriptorState get(byte[] id) {
		LocalIdentityDescriptorState descriptorState = remoteIdentityDescriptors.get(id);
		Future<?> future = backgroundFetchDescriptor(id, descriptorState);
		if (descriptorState == null && future != null) {
			Threads.getChecked(future);
			descriptorState = remoteIdentityDescriptors.get(id);
		}
		return descriptorState;
	}

	private Future<?> backgroundFetchDescriptor(byte[] id, LocalIdentityDescriptorState descriptorState) {
		long now = System.currentTimeMillis();
		if (descriptorState != null && now - descriptorState.getFetchedAt() < REFRESH_INTERVAL) {
			return null;
		}
		ByteBuffer idWrapped = ByteBuffer.wrap(id);
		Long previousFetchTime = descriptorsFetchTimes.get(idWrapped);
		if (previousFetchTime != null && now - previousFetchTime < REFRESH_INTERVAL_MIN) {
			return null;
		}
		Long previousFetchTime2 = descriptorsFetchTimes.put(idWrapped, now);
		if (!Objects.equals(previousFetchTime, previousFetchTime2)) {
			return null;
		}
		return Threads.submitBackgroundTask(() -> fetchIdentityUncached(id, null));
	}

	private void onDescriptorFetchSuccess(LocalIdentityDescriptorState identityDescriptorState) {
		IdentityDescriptor identityDescriptor = identityDescriptorState.getDescriptor();
		publicStorageProfiles.put(identityDescriptor.getId(), identityDescriptor.getStorageProfile());
		remoteIdentityDescriptors.put(identityDescriptor.getId(), identityDescriptorState);
	}

	public IdentityDescriptor fetchIdentityUncached(byte[] id, PublicStorageProfile publicStorageProfileOverride) {
		PublicStorageProfile currentStorageProfileOverride = publicStorageProfileOverride;
		for (int i = 0; i < MAX_STORAGE_PROFILE_CHAIN_LENGTH; i++) {
			LocalIdentityDescriptorState identityDescriptorState = fetchRemoteIdentity(id, currentStorageProfileOverride);
			if (identityDescriptorState == null) {
				return null;
			}
			IdentityDescriptor identityDescriptor = identityDescriptorState.getDescriptor();
			if (identityDescriptor.getStorageProfile() == null) {
				log.trace(tracePrefix + " Invalid identityDescriptor, storageProfile missing " + Utils.getDebugStringFromId(id));
				return null;
			}
			Migrations.migrateIdentityDescriptor(identityDescriptor);
			log.trace(tracePrefix + " Fetched remote identityDescriptor " + Utils.getDebugStringFromId(id));

			PublicStorageProfile currentPublicStorageProfile = currentStorageProfileOverride != null ? currentStorageProfileOverride : publicStorageProfiles.get(id);
			if (currentPublicStorageProfile.equals(identityDescriptor.getStorageProfile())) {
				onDescriptorFetchSuccess(identityDescriptorState);
				return identityDescriptor;
			}
			log.trace(tracePrefix + " Forwarding storage profile for " + Utils.getDebugStringFromId(id) + " from " + StorageProfileUtils.toHumanReadableString(currentPublicStorageProfile) + " to " + StorageProfileUtils.toHumanReadableString(identityDescriptor.getStorageProfile()));
			currentStorageProfileOverride = identityDescriptor.getStorageProfile();
		}
		log.warn(tracePrefix + " Too many iterations fetching storage profile for " + Utils.getDebugStringFromId(id));
		return null;
	}

	private LocalIdentityDescriptorState fetchRemoteIdentity(byte[] id, PublicStorageProfile publicStorageProfileOverride) {
		RemoteFetchResultRaw fetchResult;
		if (publicStorageProfileOverride != null) {
			fetchResult = remoteReadService.readNonCached(id, publicStorageProfileOverride, id, IdentityService.REMOTE_IDENTITY_NAME_SUFFIX);
		} else {
			fetchResult = remoteReadService.read(id, id, IdentityService.REMOTE_IDENTITY_NAME_SUFFIX);
		}
		if (fetchResult.getError() != null || fetchResult.getData() == null) {
			log.info(tracePrefix + " Error fetching identityDescriptor of " + Utils.getDebugStringFromId(id) + ": " + fetchResult.getError());
			return null;
		}

		Triple<IdentityDescriptor, SignatureMetadata, String> identityDescriptorResult = IdentityService.extractIdentityDescriptor(fetchResult.getData());
		if (identityDescriptorResult.getRight() != null) {
			log.info(tracePrefix + " Error extracting identityDescriptor of " + Utils.getDebugStringFromId(id) + ": " + identityDescriptorResult.getRight());
			return null;
		}

		IdentityDescriptor identityDescriptor = identityDescriptorResult.getLeft();
		SignatureMetadata signatureMetadata = identityDescriptorResult.getMiddle();

		if (!Arrays.equals(id, identityDescriptor.getId())) {
			log.info(tracePrefix + " Mismatching id when fetching identityDescriptor of " + Utils.getDebugStringFromId(id) + " : " + Utils.getDebugStringFromId(identityDescriptor.getId()));
			return null;
		}

		long now = System.currentTimeMillis();
		LocalIdentityDescriptorState identityDescriptorState = new LocalIdentityDescriptorState(now, identityDescriptor);
		identityDescriptorState.setSignedAt(signatureMetadata.getGeneratedAt());
		identityDescriptorState.setValidUntil(signatureMetadata.getValidUntil());
		return identityDescriptorState;
	}
}
