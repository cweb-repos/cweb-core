package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.storage.NameConversionUtils;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class IdentityDescriptorDecoder {

	public static String formatIdentityShort(IdentityDescriptor identityDescriptor) {
		StringBuilder str = new StringBuilder();
		str.append(Utils.getDebugStringFromId(identityDescriptor.getId()));
		for (Property property : identityDescriptor.getOwnProperties()) {
			IdentityPropertyDecoded decodedProperty = decode(property);
			str.append(", ").append(decodedProperty).append(':').append(decodedProperty.value);
		}
		return str.toString();
	}

	public static List<IdentityPropertyDecoded> decode(List<Property> properties) {
		List<IdentityPropertyDecoded> result = new ArrayList<>();
		for (Property property : properties) {
			result.add(decode(property));
		}
		return result;
	}

	public static IdentityPropertyDecoded decode(Property property) {
		IdentityPropertyDecoded converted = new IdentityPropertyDecoded();
		converted.key = property.getKey();
		PropertyValue value = property.getValue();
		if (value.isSetStr()) {
			converted.value = value.getStr();
		} else if (value.isSetBin()) {
			converted.value = Utils.getDebugStringFromBytesShort(value.getBin());
		} else if (value.isSetTypedPayload()) {
			converted.value = "TypedPayload";
			converted.valueData = TypedPayloadDecoder.decodePayload(value.getTypedPayload());
		}
		return converted;
	}

	public static IdentityDescriptorDecoded decode(IdentityDescriptor identityDescriptor) {
		IdentityDescriptorDecoded result = new IdentityDescriptorDecoded();

		result.idStr = NameConversionUtils.toString(identityDescriptor.getId());
		result.publicStorageProfileDecoded = StorageProfileUtils.toHumanReadableString(identityDescriptor.getStorageProfile());
		result.protocolVersion = identityDescriptor.getProtocolVersion();

		result.ownProperties.addAll(decode(identityDescriptor.getOwnProperties()));

		List<Pair<SignatureMetadata, EndorsementPayload>> endorsements = EndorsementService.getEndorsementsWithoutVerification(identityDescriptor);

		for (Pair<SignatureMetadata, EndorsementPayload> endorsementPair : endorsements) {
			SignatureMetadata signatureMetadata = endorsementPair.getLeft();
			EndorsementPayload endorsementPayload = endorsementPair.getRight();
			EndorsementEnvelopeDecoded envelope = new EndorsementEnvelopeDecoded();
			if (signatureMetadata.isSetGeneratedAt()) {
				envelope.generatedAt = Utils.formatDateTime(signatureMetadata.getGeneratedAt());
			}
			if (signatureMetadata.isSetValidUntil()) {
				envelope.validUntil = Utils.formatDateTime(signatureMetadata.getValidUntil());
			}
			envelope.byIdStr = NameConversionUtils.toString(signatureMetadata.getSignerId());
			if (signatureMetadata.isSetRecipientId()) {
				envelope.toIdStr = NameConversionUtils.toString(signatureMetadata.getRecipientId());
			}

			envelope.payload.addAll(decode(endorsementPayload.getProperties()));

			result.endorsements.add(envelope);
		}
		return result;
	}

	public static IdentityDescriptorDecoded decode(LocalIdentityDescriptorState identityDescriptorState) {
		IdentityDescriptorDecoded result = decode(identityDescriptorState.getDescriptor());
		if (identityDescriptorState.isSetFetchedAt()) {
			result.fetchedAt = Utils.formatDateTime(identityDescriptorState.getFetchedAt());
		}
		if (identityDescriptorState.isSetSignedAt()) {
			result.signedAt = Utils.formatDateTime(identityDescriptorState.getSignedAt());
		}
		if (identityDescriptorState.isSetValidUntil()) {
			result.validUntil = Utils.formatDateTime(identityDescriptorState.getValidUntil());
		}
		return result;
	}
}
