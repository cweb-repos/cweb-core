package org.cweb.identity;

import java.util.ArrayList;
import java.util.List;

public class IdentityDescriptorDecoded {
	public String idStr;
	public String publicStorageProfileDecoded;
	public String protocolVersion;
	public List<IdentityPropertyDecoded> ownProperties = new ArrayList<>();
	public List<EndorsementEnvelopeDecoded> endorsements = new ArrayList<>();
	public String fetchedAt;
	public String signedAt;
	public String validUntil;
}
