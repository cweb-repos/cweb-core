package org.cweb.identity;

import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.schemas.local.LocalEndorsementProcessingTimestamp;
import org.cweb.schemas.local.LocalEndorsementState;
import org.cweb.schemas.local.LocalEndorsementTimestampType;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.ThriftUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class LocalEndorsementStateService {
	private static final String FILE_NAME = "ld/localEndorsementState";
	private static final long TIMESTAMP_TTL = 1000L * 60 * 60 * 24 * 30;
	private final LocalStorageInterface localStorageInterface;

	private final Map<Long, LocalEndorsementProcessingTimestamp> hash2timestamp = new LinkedHashMap<>();

	LocalEndorsementStateService(LocalStorageInterface localStorageInterface) {
		this.localStorageInterface = localStorageInterface;

		LocalEndorsementState state = loadAndPurgeExpired();
		for (LocalEndorsementProcessingTimestamp processingTimestamp : state.getTimestamps()) {
			hash2timestamp.put(processingTimestamp.getHash(), processingTimestamp);
		}
		save();
	}

	private LocalEndorsementState loadAndPurgeExpired() {
		long now = System.currentTimeMillis();
		LocalEndorsementState state = load();
		List<LocalEndorsementProcessingTimestamp> currentList = state.getTimestamps();
		List<LocalEndorsementProcessingTimestamp> newList = new ArrayList<>(currentList.size());
		for (LocalEndorsementProcessingTimestamp processingTimestamp : currentList) {
			if (now - processingTimestamp.getTime() < TIMESTAMP_TTL) {
				newList.add(processingTimestamp);
			}
		}
		state.setTimestamps(newList);
		return state;
	}

	private LocalEndorsementState load() {
		byte[] data = localStorageInterface.read(FILE_NAME);
		LocalEndorsementState state = data != null ? ThriftUtils.deserializeSafe(data, LocalEndorsementState.class) : null;
		if (state == null) {
			state = new LocalEndorsementState();
		}
		return state;
	}

	private void save() {
		LocalEndorsementState state = new LocalEndorsementState();
		state.setTimestamps(new ArrayList<>(hash2timestamp.values()));
		byte[] data = ThriftUtils.serialize(state);
		localStorageInterface.write(FILE_NAME, data);
	}

	private long hash(byte[] id, byte[] refId) {
		return BinaryUtils.bytesToLong(Arrays.copyOf(HashingUtils.SHA256(BinaryUtils.concat(id, refId)), 8));
	}

	private LocalEndorsementTimestampType getType(boolean request) {
		return request ? LocalEndorsementTimestampType.REQUEST : LocalEndorsementTimestampType.RESPONSE;
	}

	Long getProcessingTime(byte[] id, byte[] refId, boolean request) {
		LocalEndorsementProcessingTimestamp timestamp = hash2timestamp.get(hash(id, refId));
		if (timestamp == null || getType(request) != timestamp.getType()) {
			return null;
		}
		return timestamp.getTime();
	}

	void addProcessingTime(byte[] id, byte[] refId, boolean request, long time) {
		long hash = BinaryUtils.bytesToLong(Arrays.copyOf(HashingUtils.SHA256(BinaryUtils.concat(id, refId)), 8));
		hash2timestamp.put(hash, new LocalEndorsementProcessingTimestamp(getType(request), hash, time));
		save();
	}
}
