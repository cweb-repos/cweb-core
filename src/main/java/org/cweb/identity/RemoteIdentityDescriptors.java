package org.cweb.identity;

import org.cweb.Migrations;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class RemoteIdentityDescriptors extends LocalDataSingleKey<LocalIdentityDescriptorState> {
	private static final String NAME_SUFFIX = "-remoteIdentity";

	public RemoteIdentityDescriptors(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	@Override
	public void put(byte[] id, LocalIdentityDescriptorState value) {
		super.put(id, value);
	}

	public LocalIdentityDescriptorState get(byte[] id) {
		LocalIdentityDescriptorState descriptorState = super.get(id, LocalIdentityDescriptorState.class);
		if (descriptorState != null) {
			boolean changed = Migrations.migrateIdentityDescriptor(descriptorState.getDescriptor());
			if (changed) {
				put(id, descriptorState);
			}
		}
		return descriptorState;
	}

	@Override
	public boolean delete(byte[] id) {
		return super.delete(id);
	}
}
