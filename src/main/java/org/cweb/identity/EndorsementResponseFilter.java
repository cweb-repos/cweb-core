package org.cweb.identity;

import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.identity.IdentityDescriptor;

public interface EndorsementResponseFilter {
	enum Action {
		APPLY,
		IGNORE_ONCE,
		IGNORE_PERMANENTLY,
	}

	Action getAction(IdentityDescriptor identityDescriptor, EndorsementPayload endorsementPayload);
}
