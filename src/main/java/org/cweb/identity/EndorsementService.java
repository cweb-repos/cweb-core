package org.cweb.identity;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.communication.PrivateBroadcastService;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.crypto.lib.BinaryUtils;
import org.cweb.payload.PayloadTypeLocalMetadataPredicate;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.endorsements.EndorsementRequest;
import org.cweb.schemas.endorsements.EndorsementRequestLocalMetadata;
import org.cweb.schemas.endorsements.EndorsementResponse;
import org.cweb.schemas.endorsements.EndorsementResponseLocalMetadata;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.storage.LocalMetadataEnvelope;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.SignatureMetadata;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class EndorsementService {
	private static final Logger log = LoggerFactory.getLogger(EndorsementService.class);
	private static final String SERVICE_NAME = "EndorsementService";
	private static final long REQUEST_TTL = 1000L * 60 * 60 * 24;
	private static final long RESPONSE_TTL = 1000L * 60 * 60 * 24;
	private final CryptoHelper cryptoHelper;
	private final RemoteIdentityFetcher remoteIdentityFetcher;
	private final IdentityService identityService;
	private final PrivateBroadcastService privateBroadcastService;
	private final LocalEndorsementStateService localEndorsementStateService;
	private final String tracePrefix;

	public EndorsementService(String tracePrefix, CryptoHelper cryptoHelper, LocalStorageInterface localStorageInterface, RemoteIdentityFetcher remoteIdentityFetcher, IdentityService identityService, PrivateBroadcastService privateBroadcastService) {
		this.tracePrefix = tracePrefix;
		this.cryptoHelper = cryptoHelper;
		this.remoteIdentityFetcher = remoteIdentityFetcher;
		this.identityService = identityService;
		this.privateBroadcastService = privateBroadcastService;
		this.localEndorsementStateService = new LocalEndorsementStateService(localStorageInterface);
	}

	public boolean publishEndorsementRequestFor(IdentityDescriptor targetIdentityDescriptor, List<Property> properties) {
		long now = System.currentTimeMillis();
		EndorsementPayload payload = new EndorsementPayload();
		payload.setProperties(properties);
		byte[] payloadSerialized = ThriftUtils.serialize(payload);
		byte[] requestId = cryptoHelper.generateRandomBytes(8);
		EndorsementRequest request = new EndorsementRequest(ByteBuffer.wrap(identityService.getIdentityDescriptor().getId()), ByteBuffer.wrap(requestId), ByteBuffer.wrap(payloadSerialized));
		log.trace(tracePrefix + " Created endorsement request to " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()));
		TypedPayload typedPayload = TypedPayloadUtils.wrap(request, SERVICE_NAME, null, null);
		EndorsementRequestLocalMetadata localMetadata = new EndorsementRequestLocalMetadata(ByteBuffer.wrap(targetIdentityDescriptor.getId()), ByteBuffer.wrap(requestId), ByteBuffer.wrap(BinaryUtils.intToBytes(request.hashCode())));
		return privateBroadcastService.publish(targetIdentityDescriptor, typedPayload, ThriftUtils.serialize(localMetadata), now + REQUEST_TTL);
	}

	public void checkAndProcessEndorsementRequestsFrom(IdentityDescriptor targetIdentityDescriptor, EndorsementVerifier verifier) {
		PayloadTypePredicate predicate = TypedPayloadUtils.getGenericPayloadPredicate(PayloadType.ENDORSEMENT_REQUEST, SERVICE_NAME, null, null);
		List<TypedPayload> requests = privateBroadcastService.readBroadcastsFrom(targetIdentityDescriptor, predicate);
		if (requests == null) {
			return;
		}
		long now = System.currentTimeMillis();
		for (TypedPayload wrappedRequest : requests) {
			Pair<EndorsementRequest, String> requestUnwrapped = TypedPayloadUtils.unwrap(wrappedRequest, EndorsementRequest.class, SERVICE_NAME);
			if (requestUnwrapped.getRight() != null) {
				log.trace(tracePrefix + " Failed to extract request: " + requestUnwrapped.getRight());
				continue;
			}
			EndorsementRequest request = requestUnwrapped.getLeft();

			if (!Arrays.equals(request.getRequesterId(), targetIdentityDescriptor.getId())) {
				log.trace(tracePrefix + " Mismatching requester id from " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()) + ": " + Utils.getDebugStringFromId(request.getRequesterId()));
				continue;
			}

			EndorsementPayload requestPayload = ThriftUtils.deserializeSafe(request.getEndorsementRequestPayload(), EndorsementPayload.class);

			if (requestPayload == null) {
				log.trace(tracePrefix + " Failed to deserialize request from " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()) + ", " + Utils.getDebugStringFromBytesShort(request.getRequestId()));
				continue;
			}

			Long lastProcessingTime = localEndorsementStateService.getProcessingTime(targetIdentityDescriptor.getId(), request.getRequestId(), true);
			if (lastProcessingTime != null) {
				log.trace(tracePrefix + " Skipping request from " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()) + ", " + Utils.getDebugStringFromBytesShort(request.getRequestId()) + ", lastProcessingTime=" + lastProcessingTime);
				continue;
			}

			boolean processed = true;
			Pair<EndorsementVerifier.Result, EndorsementResponse> result = checkAndEndorse(targetIdentityDescriptor, request.getRequestId(), requestPayload, verifier);
			EndorsementResponse response = result.getRight();
			if (response != null) {
				TypedPayload wrappedResponse = TypedPayloadUtils.wrap(response, SERVICE_NAME, null, null);
				EndorsementResponseLocalMetadata localMetadata = new EndorsementResponseLocalMetadata(ByteBuffer.wrap(targetIdentityDescriptor.getId()), ByteBuffer.wrap(request.getRequestId()), ByteBuffer.wrap(BinaryUtils.intToBytes(response.hashCode())));
				processed = privateBroadcastService.publish(targetIdentityDescriptor, wrappedResponse, ThriftUtils.serialize(localMetadata), now + RESPONSE_TTL);
			}

			if (processed && result.getLeft() != EndorsementVerifier.Result.IGNORE_ONCE) {
				localEndorsementStateService.addProcessingTime(targetIdentityDescriptor.getId(), request.getRequestId(), true, now);
			}

			log.trace(tracePrefix + (processed ? "successfully processed" : "failed to process") + " endorsement request and generted response to " + Utils.getDebugStringFromId(request.getRequesterId()));
		}
	}

	private Pair<EndorsementVerifier.Result, EndorsementResponse> checkAndEndorse(IdentityDescriptor requesterIdentityDescriptor, byte[] requestId, EndorsementPayload requestPayload, EndorsementVerifier verifier) {
		List<Property> sanitizedProperties = new ArrayList<>();

		// Clone to get rid of potential extra data due to schema differences
		for (Property property : requestPayload.getProperties()) {
			Property sanitizedProperty = new Property(property);
			sanitizedProperties.add(sanitizedProperty);
		}

		Pair<EndorsementVerifier.Result, String> verificationResultPair = verifier.verify(requesterIdentityDescriptor, sanitizedProperties);

		EndorsementVerifier.Result verificationResult = verificationResultPair.getLeft();
		if (verificationResult == EndorsementVerifier.Result.IGNORE_ONCE || verificationResult == EndorsementVerifier.Result.IGNORE_PERMANENTLY) {
			return Pair.of(verificationResult, null);
		}

		EndorsementResponse response = new EndorsementResponse(ByteBuffer.wrap(identityService.getIdentityDescriptor().getId()), ByteBuffer.wrap(requestId));
		if (verificationResult == EndorsementVerifier.Result.SUCCESS) {
			EndorsementPayload responsePayload = new EndorsementPayload();
			responsePayload.setProperties(sanitizedProperties);
			CryptoEnvelope endorsementEnvelope = cryptoHelper.signTypedPayload(TypedPayloadUtils.wrap(responsePayload, SERVICE_NAME, null, null), requesterIdentityDescriptor.getId(), null);
			response.setEndorsementEnvelope(ThriftUtils.serialize(endorsementEnvelope));
		} else {
			response.setErrorStr(verificationResultPair.getRight());
		}

		return Pair.of(verificationResult, response);
	}

	public void checkAndProcessEndorsementResponsesFrom(IdentityDescriptor targetIdentityDescriptor, EndorsementResponseFilter responseFilter) {
		PayloadTypePredicate predicate = TypedPayloadUtils.getGenericPayloadPredicate(PayloadType.ENDORSEMENT_RESPONSE, null, null, null);
		List<TypedPayload> responses = privateBroadcastService.readBroadcastsFrom(targetIdentityDescriptor, predicate);
		if (responses == null) {
			return;
		}

		long now = System.currentTimeMillis();
		byte[] ownId = cryptoHelper.getOwnId();
		for (TypedPayload wrappedResponse : responses) {
			Pair<EndorsementResponse, String> responseUnwrapped = TypedPayloadUtils.unwrap(wrappedResponse, EndorsementResponse.class, SERVICE_NAME);
			if (responseUnwrapped.getRight() != null) {
				log.trace(tracePrefix + " Failed to extract response: " + responseUnwrapped.getRight());
				continue;
			}
			EndorsementResponse response = responseUnwrapped.getLeft();

			Long lastProcessingTime = localEndorsementStateService.getProcessingTime(targetIdentityDescriptor.getId(), response.getRequestId(), false);
			if (lastProcessingTime != null) {
				log.trace(tracePrefix + " Skipping response from " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()) + ", " + Utils.getDebugStringFromBytesShort(response.getRequestId()) + ", lastProcessingTime=" + lastProcessingTime);
				continue;
			}

			String error = null;
			if (!Arrays.equals(targetIdentityDescriptor.getId(), response.getEndorserId())) {
				error = "Inconsistent ids in response to request " + Utils.getDebugStringFromBytes(response.getRequestId()) + " from " + IdentityDescriptorDecoder.formatIdentityShort(targetIdentityDescriptor) + " id2= " + Utils.getDebugStringFromBytes(response.getEndorserId());
			}
			if (error == null && response.getErrorStr() != null) {
				error = "Error " + response.getErrorStr() + " returned on request " + Utils.getDebugStringFromBytes(response.getRequestId()) + " from " + IdentityDescriptorDecoder.formatIdentityShort(targetIdentityDescriptor);
			}
			if (error == null && response.getEndorsementEnvelope() == null) {
				error = "No data returned on " + Utils.getDebugStringFromBytes(response.getRequestId()) + " from " + IdentityDescriptorDecoder.formatIdentityShort(targetIdentityDescriptor);
			}

			if (error != null) {
				log.info(tracePrefix + " " + error);
				localEndorsementStateService.addProcessingTime(targetIdentityDescriptor.getId(), response.getRequestId(), false, now);
				continue;
			}

			Decoded<DecodedTypedPayload> endorsementData = CryptoHelper.decodeCryptoEnvelope(response.getEndorsementEnvelope(), CryptoEnvelopeDecodingParams.create().setIdForSignatureTargetVerification(ownId).setSignerIdentityDescriptor(targetIdentityDescriptor), CryptoContext.create());
			if (endorsementData.getError() != null || endorsementData.getData().getSignatureMetadata() == null) {
				log.info(tracePrefix + " Failed to deserialize endorsement envelope: " + endorsementData.getError());
				continue;
			}
			if (!Arrays.equals(targetIdentityDescriptor.getId(), endorsementData.getData().getSignatureMetadata().getSignerId())) {
				log.info(tracePrefix + " Inconsistent ids in endorsement " + Utils.getDebugStringFromBytes(response.getRequestId()) + " from " + IdentityDescriptorDecoder.formatIdentityShort(targetIdentityDescriptor) + " id2= " + Utils.getDebugStringFromBytes(endorsementData.getData().getSignatureMetadata().getSignerId()));
				continue;
			}

			int deleted = privateBroadcastService.deleteBroadcasts(targetIdentityDescriptor, new PayloadTypeLocalMetadataPredicate(TypedPayloadUtils.getGenericPayloadPredicate(PayloadType.ENDORSEMENT_REQUEST, SERVICE_NAME, null, null)) {
				@Override
				public boolean match(LocalMetadataEnvelope envelope) {
					if (!super.match(envelope)) {
						return false;
					}
					EndorsementRequestLocalMetadata metadata = ThriftUtils.deserializeSafe(envelope.getCustomMetadata(), EndorsementRequestLocalMetadata.class);
					if (metadata == null) {
						log.trace(tracePrefix + " Failed to deserialize metadata, deleting");
						return true;
					}
					return Arrays.equals(response.getEndorserId(), metadata.getEndorserId()) && Arrays.equals(response.getRequestId(), metadata.getRequestId());
				}
			});
			log.trace(tracePrefix + " Deleted " + deleted + " endorsement requests to " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()));

			Pair<EndorsementPayload, String> endorsementPayloadUnwrapped = TypedPayloadUtils.unwrap(endorsementData.getData().getPayload(), EndorsementPayload.class, SERVICE_NAME);
			if (endorsementPayloadUnwrapped.getRight() != null) {
				log.debug(tracePrefix + " Failed to extract payload: " + endorsementPayloadUnwrapped.getRight());
				continue;
			}
			EndorsementPayload endorsementPayload = endorsementPayloadUnwrapped.getLeft();
			EndorsementResponseFilter.Action action = responseFilter.getAction(targetIdentityDescriptor, endorsementPayload);

			if (action == EndorsementResponseFilter.Action.IGNORE_ONCE) {
				continue;
			}
			localEndorsementStateService.addProcessingTime(targetIdentityDescriptor.getId(), response.getRequestId(), false, now);

			if (action == EndorsementResponseFilter.Action.APPLY) {
				addEndorsement(response.getEndorsementEnvelope());
			}

			log.trace(tracePrefix + " Processed endorsement response from " + Utils.getDebugStringFromId(targetIdentityDescriptor.getId()));
		}
	}

	private void addEndorsement(byte[] newEndorsementEnvelope) {
		IdentityDescriptor identityDescriptor = identityService.getIdentityDescriptor();
		List<ByteBuffer> endorsementEnvelopes = new ArrayList<>(identityDescriptor.getEndorsementSignedEnvelopes());
		endorsementEnvelopes.add(ByteBuffer.wrap(newEndorsementEnvelope));
		byte[] ownId = cryptoHelper.getOwnId();
		List<ByteBuffer> toRemove = new ArrayList<>();
		// TODO: consider endorser and validUntil in comparison
		HashSet<Property> seenProperties = new HashSet<>();
		for (int i = endorsementEnvelopes.size() - 1; i >= 0; i--) {
			ByteBuffer endorsementEnvelope = endorsementEnvelopes.get(i);
			Decoded<DecodedTypedPayload> endorsementData = CryptoHelper.decodeCryptoEnvelope(Utils.toArray(endorsementEnvelope), CryptoEnvelopeDecodingParams.create().setIdForSignatureTargetVerification(ownId).setFetchSignerIfNeeded(true), CryptoContext.create().setRemoteIdentityFetcher(remoteIdentityFetcher));
			if (endorsementData.getError() != null && endorsementData.getError() != Decoded.Error.SIGNER_IDENTITY_FETCH_FAILED) {
				log.warn(tracePrefix + " Failed to deserialize endorsement envelope: " + endorsementData.getError());
				toRemove.add(endorsementEnvelope);
				continue;
			}
			Pair<EndorsementPayload, String> endorsementPayloadUnwrapped = TypedPayloadUtils.unwrap(endorsementData.getData().getPayload(), EndorsementPayload.class, SERVICE_NAME);
			if (endorsementPayloadUnwrapped.getRight() != null) {
				log.warn(tracePrefix + " Failed to deserialize endorsementPayload, removing");
				toRemove.add(endorsementEnvelope);
				continue;
			}
			EndorsementPayload endorsementPayload = endorsementPayloadUnwrapped.getLeft();
			boolean allSeen = true;
			if (endorsementPayload.getProperties() != null) {
				for (Property property : endorsementPayload.getProperties()) {
					if (!seenProperties.contains(property)) {
						allSeen = false;
						seenProperties.add(property);
					}
				}
			}
			if (allSeen) {
				toRemove.add(endorsementEnvelope);
			}
		}

		endorsementEnvelopes.removeAll(toRemove);
		identityDescriptor.setEndorsementSignedEnvelopes(endorsementEnvelopes);
		identityService.saveAndUploadIdentity();
	}

	public List<Pair<SignatureMetadata, EndorsementPayload>> getEndorsementsWithoutVerification() {
		IdentityDescriptor identityDescriptor = identityService.getIdentityDescriptor();
		return getEndorsementsWithoutVerification(identityDescriptor);
	}

	public static List<Pair<SignatureMetadata, EndorsementPayload>> getEndorsementsWithoutVerification(IdentityDescriptor identityDescriptor) {
		List<Pair<SignatureMetadata, EndorsementPayload>> result = new ArrayList<>();
		for (ByteBuffer envelope : identityDescriptor.getEndorsementSignedEnvelopes()) {
			Decoded<DecodedTypedPayload> endorsementData = CryptoHelper.decodeCryptoEnvelope(Utils.toArray(envelope), CryptoEnvelopeDecodingParams.create().setSkipAllSignatureChecks(true), CryptoContext.create());
			if (endorsementData.getError() != null) {
				log.warn("Failed to deserialize endorsement envelope: " + endorsementData.getError());
				continue;
			}
			Pair<EndorsementPayload, String> endorsementPayloadUnwrapped = TypedPayloadUtils.unwrap(endorsementData.getData().getPayload(), EndorsementPayload.class, SERVICE_NAME);
			if (endorsementPayloadUnwrapped.getRight() != null) {
				log.warn("Failed to deserialize endorsementPayload");
				continue;
			}
			EndorsementPayload endorsementPayload = endorsementPayloadUnwrapped.getLeft();
			result.add(Pair.of(endorsementData.getData().getSignatureMetadata(), endorsementPayload));
		}
		return result;
	}
}
