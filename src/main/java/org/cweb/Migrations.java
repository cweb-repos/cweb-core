package org.cweb;

import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Migrations {
	private static final Logger log = LoggerFactory.getLogger(Migrations.class);

	public static boolean migratePublicStorageProfile(PublicStorageProfile publicStorageProfile) {
		if (publicStorageProfile.isSetPublicS3StorageProfile()) {
		}
		return false;
	}

	public static boolean migratePrivateStorageProfile(PrivateStorageProfile privateStorageProfile) {
		if (privateStorageProfile.isSetPrivateS3StorageProfile()) {
		}
		return false;
	}

	private static boolean migrateProperties(List<Property> properties) {
		List<Property> toRemove = new ArrayList<>();
		for (Property property : properties) {
			if (!property.isSetValue()) {
				toRemove.add(property);
			}
		}
		properties.removeAll(toRemove);
		return !toRemove.isEmpty();
	}

	public static boolean migrateIdentityDescriptor(IdentityDescriptor identityDescriptor) {
		boolean changed = false;
		if (migratePublicStorageProfile(identityDescriptor.getStorageProfile())) {
			changed = true;
		}
		if (migrateProperties(identityDescriptor.getOwnProperties())) {
			changed = true;
		}
		if (changed) {
			log.trace("Migrated identity " + Utils.getDebugStringFromId(identityDescriptor.getId()));
		}
		return changed;
	}
}
