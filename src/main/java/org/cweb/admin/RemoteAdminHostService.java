package org.cweb.admin;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.identity.RemoteIdentityService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.admin.LocalHostedProfile;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.remote.StorageProfileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class RemoteAdminHostService {
    private static final Logger log = LoggerFactory.getLogger(RemoteAdminHostService.class);

    private final String tracePrefix;
    private final CryptoHelper cryptoHelper;
    private final RemoteIdentityService remoteIdentityService;
    private final LocalHostedProfiles localHostedProfiles;

    public RemoteAdminHostService(String tracePrefix, LocalStorageInterface localStorageInterface, CryptoHelper cryptoHelper, RemoteIdentityService remoteIdentityService) {
        this.tracePrefix = tracePrefix;
        this.cryptoHelper = cryptoHelper;
        this.remoteIdentityService = remoteIdentityService;
        this.localHostedProfiles = new LocalHostedProfiles(tracePrefix, localStorageInterface, 200, 30);
    }

    public StorageProfileRequest decodeStorageProfileRequest(byte[] requestEnvelopeSerialized) {
        Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(requestEnvelopeSerialized, CryptoEnvelopeDecodingParams.create(), CryptoContext.create().setCryptoHelper(cryptoHelper));
        if (decodedTypedPayloadWrapper.getError() != null) {
            log.trace(tracePrefix + " Failed to decode request: " + decodedTypedPayloadWrapper.getError());
            return null;
        }
        DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
        TypedPayload typedPayload = decodedTypedPayload.getPayload();
        if (typedPayload == null) {
            log.trace(tracePrefix + " Failed to deserialize request");
            return null;
        }
        Pair<StorageProfileRequest, String> requestUnwrapped = TypedPayloadUtils.unwrap(typedPayload, StorageProfileRequest.class, Common.SERVICE_NAME);
        if (requestUnwrapped.getRight() != null) {
            log.trace(tracePrefix + " Failed to extract request: " + requestUnwrapped.getRight());
            return null;
        }
        return requestUnwrapped.getLeft();
    }

    public CryptoEnvelope generateStorageProfileResponse(StorageProfileRequest request, PrivateStorageProfile privateStorageProfile, IdentityReference adminIdentityRef) {
        byte[] id = request.getId();
        long now = System.currentTimeMillis();
        StorageProfileResponse response = new StorageProfileResponse(privateStorageProfile, adminIdentityRef);
        IdentityReference requesterIdentityRef = new IdentityReference(ByteBuffer.wrap(id), StorageProfileUtils.toPublicStorageProfile(privateStorageProfile));
        LocalHostedProfile localHostedProfile = new LocalHostedProfile(request, privateStorageProfile, requesterIdentityRef, now);
        localHostedProfiles.put(localHostedProfile);
        return cryptoHelper.signAndEncryptFor(TypedPayloadUtils.wrap(response, Common.SERVICE_NAME, null, null), id, request.getPublicKey(), null);
    }

    public List<LocalHostedProfile> getLocalHostedProfiles() {
        List<byte[]> ids = localHostedProfiles.list();
        List<LocalHostedProfile> result = new ArrayList<>(ids.size());
        for (byte[] id : ids) {
            LocalHostedProfile profile = localHostedProfiles.get(id);
            result.add(profile);
        }
        return result;
    }

    public List<byte[]> checkForNewProfiles() {
        long now = System.currentTimeMillis();
        List<byte[]> allHostedIds = localHostedProfiles.list();
        List<byte[]> newlyConnectedIds = new ArrayList<>();
        for (byte[] id : allHostedIds) {
            LocalHostedProfile profile = localHostedProfiles.get(id);
            if (!profile.isSetFirstSeenAt()) {
                boolean success = remoteIdentityService.setRemoteStorageProfile(profile.getIdentityRef());
                if (success) {
                    profile.setFirstSeenAt(now);
                    localHostedProfiles.put(profile);
                    newlyConnectedIds.add(id);
                }
            }
        }
        return newlyConnectedIds;
    }
}
