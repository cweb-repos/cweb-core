package org.cweb.admin;

import org.cweb.schemas.admin.LocalHostedProfile;
import org.cweb.storage.local.LocalDataSingleKey;
import org.cweb.storage.local.LocalStorageInterface;

public class LocalHostedProfiles extends LocalDataSingleKey<LocalHostedProfile> {
	private static final String NAME_SUFFIX = "-localHostedProfile";

	public LocalHostedProfiles(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(NAME_SUFFIX, tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
	}

	public LocalHostedProfile get(byte[] id) {
		return super.get(id, LocalHostedProfile.class);
	}

	public void put(LocalHostedProfile profile) {
		super.put(profile.getRequest().getId(), profile);
	}
}
