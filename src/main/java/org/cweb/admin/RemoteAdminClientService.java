package org.cweb.admin;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.crypto.CryptoContext;
import org.cweb.crypto.CryptoEnvelopeDecodingParams;
import org.cweb.crypto.CryptoHelper;
import org.cweb.crypto.Decoded;
import org.cweb.crypto.DecodedTypedPayload;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.CryptoEnvelopeContent;
import org.cweb.schemas.wire.TypedPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.function.Function;

public class RemoteAdminClientService {
    private static final Logger log = LoggerFactory.getLogger(RemoteAdminClientService.class);

    private final String tracePrefix;
    private final CryptoHelper cryptoHelper;

    public RemoteAdminClientService(String tracePrefix, CryptoHelper cryptoHelper) {
        this.tracePrefix = tracePrefix;
        this.cryptoHelper = cryptoHelper;
    }

    public CryptoEnvelope generateStorageProfileRequest(String fromNickname) {
        StorageProfileRequest request = new StorageProfileRequest(ByteBuffer.wrap(cryptoHelper.getOwnId()), cryptoHelper.getOwnECPublicKey());
        if (fromNickname != null) {
            request.setFromNickname(fromNickname);
        }
        return new CryptoEnvelope(CryptoEnvelopeContent.typedPayload(TypedPayloadUtils.wrap(request, Common.SERVICE_NAME, null, null)));
    }

    private final Function<TypedPayload, IdentityReference> responseSignerExtractor = typedPayload -> {
        Pair<StorageProfileResponse, String> requestUnwrapped = TypedPayloadUtils.unwrap(typedPayload, StorageProfileResponse.class, Common.SERVICE_NAME);
        StorageProfileResponse response = requestUnwrapped.getLeft();
        if (response == null) {
            return null;
        }
        return response.getAdmin();
    };

    private StorageProfileResponse extractDecodedStorageProfileResponse(Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper) {
        if (decodedTypedPayloadWrapper.getError() != null) {
            log.trace(tracePrefix + " Failed to decode response: " + decodedTypedPayloadWrapper.getError());
            return null;
        }
        DecodedTypedPayload decodedTypedPayload = decodedTypedPayloadWrapper.getData();
        TypedPayload typedPayload = decodedTypedPayload.getPayload();
        if (typedPayload == null) {
            log.trace(tracePrefix + " Failed to deserialize response");
            return null;
        }
        Pair<StorageProfileResponse, String> requestUnwrapped = TypedPayloadUtils.unwrap(typedPayload, StorageProfileResponse.class, Common.SERVICE_NAME);
        if (requestUnwrapped.getRight() != null) {
            log.trace(tracePrefix + " Failed to extract response: " + requestUnwrapped.getRight());
            return null;
        }
        return requestUnwrapped.getLeft();
    }

    public StorageProfileResponse decodeStorageProfileResponse(byte[] responsetEnvelopeSerialized, RemoteIdentityFetcher remoteIdentityFetcher) {
        Decoded<DecodedTypedPayload> decodedTypedPayloadWrapper = CryptoHelper.decodeCryptoEnvelope(responsetEnvelopeSerialized, CryptoEnvelopeDecodingParams.create().setFetchSignerIfNeeded(true).setIdForSignatureTargetVerification(cryptoHelper.getOwnId()).setPayloadSignerExtractor(responseSignerExtractor), CryptoContext.create().setCryptoHelper(cryptoHelper).setRemoteIdentityFetcher(remoteIdentityFetcher));
        return extractDecodedStorageProfileResponse(decodedTypedPayloadWrapper);
    }
}
