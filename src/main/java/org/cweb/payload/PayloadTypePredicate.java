package org.cweb.payload;

import org.cweb.schemas.wire.TypedPayloadMetadata;

public interface PayloadTypePredicate {
	boolean match(TypedPayloadMetadata payload);
}
