package org.cweb.payload;

import org.cweb.schemas.storage.LocalMetadataEnvelope;
import org.cweb.utils.LocalMetadataPredicate;

public class PayloadTypeLocalMetadataPredicate implements LocalMetadataPredicate {
	private final PayloadTypePredicate payloadTypePredicate;

	public PayloadTypeLocalMetadataPredicate(PayloadTypePredicate payloadTypePredicate) {
		this.payloadTypePredicate = payloadTypePredicate;
	}

	@Override
	public boolean match(LocalMetadataEnvelope envelope) {
		return payloadTypePredicate.match(envelope.getTypedPayloadMetadata());
	}
}
