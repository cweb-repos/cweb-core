package org.cweb.payload;

import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayloadMetadata;

import java.util.Arrays;

public class GenericPayloadTypePredicate implements PayloadTypePredicate {
	private final PayloadType type;
	private final String serviceName;
	private final String customType;
	private final byte[] refId;

	public GenericPayloadTypePredicate(PayloadType type, String serviceName, String customType, byte[] refId) {
		this.type = type;
		this.serviceName = serviceName;
		this.customType = customType;
		this.refId = refId;
	}

	@Override
	public boolean match(TypedPayloadMetadata metadata) {
		return (type == null || type == metadata.getType()) &&
				(serviceName == null || serviceName.equals(metadata.getServiceName())) &&
				(customType == null || customType.equals(metadata.getCustomType())) &&
				(refId == null || Arrays.equals(refId, metadata.getRefId()));
	}
}
