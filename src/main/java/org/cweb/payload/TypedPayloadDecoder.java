package org.cweb.payload;

import org.apache.thrift.TBase;
import org.cweb.crypto.lib.HashingUtils;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.schemas.comm.object.SharedObjectReference;
import org.cweb.schemas.comm.object.SharedObjectSyncMessage;
import org.cweb.schemas.comm.object.SharedObjectUnsubscribedMessage;
import org.cweb.schemas.comm.session.CommSessionSeed;
import org.cweb.schemas.comm.shared.LocalSharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionMessageMetadata;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.schemas.wire.TypedPayloadMetadata;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Constants;
import org.cweb.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class TypedPayloadDecoder {

	public static TypedPayloadDecoded decodePayload(TypedPayload payload) {
		TypedPayloadMetadata metadata = payload.getMetadata();
		TypedPayloadDecoded decoded = new TypedPayloadDecoded();
		decoded.type = metadata.getType().name();
		decoded.service = metadata.getServiceName();
		decoded.customType = metadata.getCustomType();
		decoded.data = decodeData(payload);
		return decoded;
	}

	private static Object decodeData(TypedPayload payload) {
		TypedPayloadMetadata metadata = payload.getMetadata();
		if (metadata.getType() == PayloadType.CUSTOM) {
			byte[] data = TypedPayloadUtils.unwrapCustom(payload);
			TypedPayloadDecoded.CustomData customData = new TypedPayloadDecoded.CustomData();
			customData.dataSize = data.length;
			customData.dataHash = Arrays.hashCode(data);
			customData.dataHead = new String(Arrays.copyOf(data, Math.min(1024, data.length)), Constants.CHARSET_UTF8);
			return customData;
		}
		return decodeSpecificData(TypedPayloadUtils.unwrap(payload));
	}

	private static Object decodeSpecificData(TBase payload) {
		if (payload instanceof IdentityDescriptor) {
			return IdentityDescriptorDecoder.decode((IdentityDescriptor) payload);
		} else if (payload instanceof CommSessionSeed) {
			return decodeCommSessionSeed((CommSessionSeed) payload);
		} else if (payload instanceof FileReference) {
			return decodeFileReference((FileReference) payload);
		} else if (payload instanceof SharedSessionMessageMetadata) {
			return decodeSharedSessionMessageMetadata((SharedSessionMessageMetadata) payload);
		} else if (payload instanceof LocalSharedSessionMessage) {
			return decodeLocalSharedSessionMessage((LocalSharedSessionMessage) payload);
		} else if (payload instanceof SharedObjectSyncMessage) {
			return decodeSharedObjectSyncMessage((SharedObjectSyncMessage) payload);
		} else {
			return "Unsupported type: " + payload.getClass().getSimpleName();
		}
	}

	private static TypedPayloadDecoded.CommSessionSeed decodeCommSessionSeed(CommSessionSeed seed) {
		TypedPayloadDecoded.CommSessionSeed decoded = new TypedPayloadDecoded.CommSessionSeed();
		decoded.sessionId = NameConversionUtils.toString(seed.getSessionId());
		return decoded;
	}

	public static TypedPayloadDecoded.FileReference decodeFileReference(FileReference payload) {
		TypedPayloadDecoded.FileReference decoded = new TypedPayloadDecoded.FileReference();
		decoded.fromId = NameConversionUtils.toString(payload.getFromId());
		decoded.fileId = NameConversionUtils.toString(payload.getFileId());
		decoded.key = NameConversionUtils.toString(payload.getKey());
		decoded.metadata = decodeFileMetadata(payload.getMetadata());
		return decoded;
	}

	public static TypedPayloadDecoded.FileMetadata decodeFileMetadata(FileMetadata metadata) {
		if (metadata == null) {
			return null;
		}
		TypedPayloadDecoded.FileMetadata decoded = new TypedPayloadDecoded.FileMetadata();
		decoded.name = metadata.getName();
		decoded.createdAt = Utils.formatDateTime(metadata.getCreatedAt());
		decoded.size = metadata.getSize();
		return decoded;
	}

	public static TypedPayloadDecoded.SharedSessionMessageMetadata decodeSharedSessionMessageMetadata(SharedSessionMessageMetadata metadata) {
		TypedPayloadDecoded.SharedSessionMessageMetadata decoded = new TypedPayloadDecoded.SharedSessionMessageMetadata();
		decoded.sessionId = NameConversionUtils.toString(metadata.getSessionId());
		decoded.descriptorVersion = metadata.getDescriptorVersion();
		decoded.fromId = NameConversionUtils.toString(metadata.getFromId());
		decoded.messageId = NameConversionUtils.toString(metadata.getMessageId());
		decoded.createdAt = Utils.formatDateTime(metadata.getCreatedAt());
		return decoded;
	}

	public static TypedPayloadDecoded.LocalSharedSessionMessage decodeLocalSharedSessionMessage(LocalSharedSessionMessage message) {
		TypedPayloadDecoded.LocalSharedSessionMessage decoded = new TypedPayloadDecoded.LocalSharedSessionMessage();
		decoded.fetchScheduledAt = Utils.formatDateTime(message.getFetchScheduledAt());
		decoded.messageReference = decodeSharedSessionMessageMetadata(message.getMessageReference());
		if (message.isSetSourceSyncFromId()) {
			decoded.sourceSyncFromId = NameConversionUtils.toString(message.getSourceSyncFromId());
		}
		if (message.isSetSourcePrevInMessageId()) {
			decoded.sourcePrevInMessageId = NameConversionUtils.toString(message.getSourcePrevInMessageId());
		}
		if (message.isSetFetchedAt()) {
			decoded.fetchedAt = Utils.formatDateTime(message.getFetchedAt());
		}
		if (message.isSetConsumedAt()) {
			decoded.consumedAt = Utils.formatDateTime(message.getConsumedAt());
		}
		if (message.isSetAckedAt()) {
			decoded.ackedAt = Utils.formatDateTime(message.getAckedAt());
		}
		if (message.isSetMessage()) {
			decoded.previousMessageMetadata = new ArrayList<>();
			SharedSessionMessage sharedSessionMessage = message.getMessage();
			for (SharedSessionMessageMetadata previousMessageMetadata : sharedSessionMessage.getPreviousMessageMetadata()) {
				decoded.previousMessageMetadata.add(decodeSharedSessionMessageMetadata(previousMessageMetadata));
			}
			if (sharedSessionMessage.isSetPayload()) {
				decoded.payload = TypedPayloadDecoder.decodePayload(sharedSessionMessage.getPayload());
			}
		}
		return decoded;
	}

	private static TypedPayloadDecoded.SharedObjectReference decodeSharedObjectReference(SharedObjectReference reference) {
		TypedPayloadDecoded.SharedObjectReference decoded = new TypedPayloadDecoded.SharedObjectReference();
		decoded.fromId = NameConversionUtils.toString(reference.getFromId());
		decoded.objectId = NameConversionUtils.toString(reference.getObjectId());
		decoded.keyHash = Utils.getDebugStringFromBytes(HashingUtils.SHA256(reference.getKey()), 4);
		reference.lastVersionOfPreviousKey = reference.getLastVersionOfPreviousKey();
		return decoded;
	}

	private static TypedPayloadDecoded.SharedObjectUnsubscribedMessage decodeSharedObjectUnsubscribedMessage(SharedObjectUnsubscribedMessage message) {
		TypedPayloadDecoded.SharedObjectUnsubscribedMessage decoded = new TypedPayloadDecoded.SharedObjectUnsubscribedMessage();
		decoded.objectId = NameConversionUtils.toString(message.getObjectId());
		return decoded;
	}

	private static TypedPayloadDecoded.SharedObjectSyncMessage decodeSharedObjectSyncMessage(SharedObjectSyncMessage message) {
		TypedPayloadDecoded.SharedObjectSyncMessage decoded = new TypedPayloadDecoded.SharedObjectSyncMessage();
		if (message.isSetReference()) {
			decoded.reference = decodeSharedObjectReference(message.getReference());
		}
		if (message.isSetUnsubscribedMessage()) {
			decoded.unsubscribedMessage = decodeSharedObjectUnsubscribedMessage(message.getUnsubscribedMessage());
		}
		return decoded;
	}
}
