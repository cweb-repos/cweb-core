package org.cweb.payload;

import java.util.List;

public class TypedPayloadDecoded {
	public String type;
	public String service;
	public String customType;

	public Object data;

	public static class CustomData {
		public int dataSize;
		public int dataHash;
		public String dataHead;
	}

	public static class FilePayload {
		public String name;
		public int dataSize;
		public int dataHash;
	}

	public static class CommSessionSeed {
		public String sessionId;
	}

	public static class FileReference {
		public String fromId;
		public String fileId;
		public String key;
		public FileMetadata metadata;
	}

	public static class FileMetadata {
		public String name;
		public String createdAt;
		public long size;
	}

	public static class SharedSessionMessageMetadata {
		public String sessionId;
		public int descriptorVersion;
		public String fromId;
		public String messageId;
		public String createdAt;
	}

	public static class LocalSharedSessionMessage {
		public SharedSessionMessageMetadata messageReference;
		public String fetchScheduledAt;
		public String sourceSyncFromId;
		public String sourcePrevInMessageId;
		public String fetchedAt;
		public String consumedAt;
		public String ackedAt;
		public List<SharedSessionMessageMetadata> previousMessageMetadata;
		public TypedPayloadDecoded payload;
	}

	public static class SharedObjectReference {
		public String fromId;
		public String objectId;
		public String keyHash;
		public int lastVersionOfPreviousKey;
	}

	public static class SharedObjectUnsubscribedMessage {
		public String objectId;
	}

	public static class SharedObjectSyncMessage {
		public SharedObjectReference reference;
		public SharedObjectUnsubscribedMessage unsubscribedMessage;
	}
}
