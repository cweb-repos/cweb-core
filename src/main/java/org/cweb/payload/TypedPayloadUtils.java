package org.cweb.payload;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TBase;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.comm.object.SharedObject;
import org.cweb.schemas.comm.object.SharedObjectSyncMessage;
import org.cweb.schemas.comm.session.CommSessionSeed;
import org.cweb.schemas.comm.session.CommSessionSeedCryptoEnvelope;
import org.cweb.schemas.comm.shared.SharedSessionDescriptor;
import org.cweb.schemas.comm.shared.SharedSessionMessage;
import org.cweb.schemas.comm.shared.SharedSessionSyncMessage;
import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.endorsements.EndorsementRequest;
import org.cweb.schemas.endorsements.EndorsementResponse;
import org.cweb.schemas.files.FileContentDescriptor;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.wire.CompressionType;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.schemas.wire.TypedPayloadMetadata;
import org.cweb.utils.CompressionUtils;
import org.cweb.utils.LocalMetadataPredicate;
import org.cweb.utils.ThriftUtils;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class TypedPayloadUtils {
	private static final Map<Class<? extends TBase>, PayloadType> class2type = ImmutableMap.<Class<? extends TBase>, PayloadType>builder()
			.put(IdentityDescriptor.class, PayloadType.IDENTITY_DESCRIPTOR)
			.put(IdentityProfile.class, PayloadType.IDENTITY_PROFILE)
			.put(EndorsementRequest.class, PayloadType.ENDORSEMENT_REQUEST)
			.put(EndorsementResponse.class, PayloadType.ENDORSEMENT_RESPONSE)
			.put(CommSessionSeed.class, PayloadType.COMM_SESSION_SEED)
			.put(CommSessionSeedCryptoEnvelope.class, PayloadType.COMM_SESSION_SEED_CRYPTO_ENVELOPE)
			.put(FileReference.class, PayloadType.FILE_REFERENCE)
			.put(EndorsementPayload.class, PayloadType.ENDORSEMENT_PAYLOAD)
			.put(FileContentDescriptor.class, PayloadType.FILE_CONTENT_DESCRIPTOR)
			.put(SharedSessionDescriptor.class, PayloadType.SHARED_SESSION_DESCRIPTOR)
			.put(SharedSessionMessage.class, PayloadType.SHARED_SESSION_MESSAGE)
			.put(SharedSessionSyncMessage.class, PayloadType.SHARED_SESSION_SYNC_MESSAGE)
			.put(SharedObject.class, PayloadType.SHARED_OBJECT)
			.put(SharedObjectSyncMessage.class, PayloadType.SHARED_OBJECT_SYNC_MESSAGE)
			.put(StorageProfileRequest.class, PayloadType.STORAGE_PROFILE_REQUEST)
			.put(StorageProfileResponse.class, PayloadType.STORAGE_PROFILE_RESPONSE)
			.build();
	private static final Map<PayloadType, Class<? extends TBase>> type2class;

	static {
		type2class = new LinkedHashMap<>();
		for (Map.Entry<Class<? extends TBase>, PayloadType> entry: class2type.entrySet()) {
			Class<? extends TBase> prev = type2class.put(entry.getValue(), entry.getKey());
			Preconditions.checkArgument(prev == null);
		}
	}

	public static PayloadTypePredicate getGenericPayloadPredicate(PayloadType type, String serviceName, String customType, byte[] refId) {
		return new GenericPayloadTypePredicate(type, serviceName, customType, refId);
	}

	public static LocalMetadataPredicate getGenericLocalMetadataPredicate(PayloadType type, String serviceName, String customType, byte[] refId) {
		return new PayloadTypeLocalMetadataPredicate(getGenericPayloadPredicate(type, serviceName, customType, refId));
	}

	public static <T extends TBase> TypedPayload wrap(T obj, String serviceName, String customType, byte[] refId) {
		PayloadType type = class2type.get(obj.getClass());
		Preconditions.checkNotNull(type);
		byte[] rawData = ThriftUtils.serialize(obj);
		return constructTypedPayload(type, rawData, serviceName, customType, refId);
	}

	public static TypedPayload wrapCustom(byte[] data, String serviceName, String customType, byte[] refId) {
		Preconditions.checkNotNull(serviceName);
		return constructTypedPayload(PayloadType.CUSTOM, data, serviceName, customType, refId);
	}

	private static TypedPayload constructTypedPayload(PayloadType type, byte[] rawData, String serviceName, String customType, byte[] refId) {
		Pair<CompressionType, byte[]> compressed = CompressionUtils.tryCompress(rawData, CompressionType.GZIP);
		TypedPayloadMetadata metadata = new TypedPayloadMetadata(type);
		metadata.setServiceName(serviceName);
		metadata.setCustomType(customType);
		metadata.setRefId(refId);
		TypedPayload typedPayload = new TypedPayload(metadata, compressed.getLeft(), ByteBuffer.wrap(compressed.getRight()));
		return typedPayload;
	}

	public static byte[] unwrapCustom(TypedPayload typedPayload) {
		Preconditions.checkArgument(typedPayload.getMetadata().getType() == PayloadType.CUSTOM, typedPayload.getMetadata().getType().name());
		return CompressionUtils.decompress(typedPayload.getData(), typedPayload.getCompression());
	}

	private static <T extends TBase> T unwrap(TypedPayload typedPayload, Class<T> klass) {
		byte[] data = CompressionUtils.decompress(typedPayload.getData(), typedPayload.getCompression());
		return ThriftUtils.deserializeSafe(data, klass);
	}

	@SuppressWarnings("unchecked")
	public static <T extends TBase> T unwrap(TypedPayload typedPayload) {
		Class<T> klass = (Class<T>) type2class.get(typedPayload.getMetadata().getType());
		T obj = unwrap(typedPayload, klass);
		return obj;
	}

	public static <T extends TBase> Pair<T, String> unwrap(TypedPayload typedPayload, Class<T> klass, String serviceName) {
		TypedPayloadMetadata metadata = typedPayload.getMetadata();
		Class payloadClass = type2class.get(metadata.getType());
		if (!klass.equals(payloadClass)) {
			return Pair.of(null, "Expected payload type " + klass.getSimpleName() + " got " + payloadClass.getSimpleName());
		}
		if (!Objects.equals(serviceName, metadata.getServiceName())) {
			return Pair.of(null, "Expected serviceName " + serviceName + " got " + metadata.getServiceName());
		}
		T payload = unwrap(typedPayload, klass);
		if (payload == null) {
			return Pair.of(null, "Failed to deserialize " + klass.getName() + " from " + serviceName);
		}

		return Pair.of(payload, null);
	}

	public static <T extends TBase> T unwrapNullable(TypedPayload typedPayload) {
		if (typedPayload == null) {
			return null;
		}
		return unwrap(typedPayload);
	}

}
