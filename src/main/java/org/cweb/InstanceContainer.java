package org.cweb;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.admin.RemoteAdminHostService;
import org.cweb.communication.CommScheduler;
import org.cweb.communication.CommSessionPushService;
import org.cweb.communication.CommSessionService;
import org.cweb.communication.PrivateBroadcastService;
import org.cweb.communication.SharedObjectCommon;
import org.cweb.communication.SharedObjectPostService;
import org.cweb.communication.SharedObjectReadService;
import org.cweb.communication.SharedSessionService;
import org.cweb.files.FileDownloadService;
import org.cweb.files.FileSharingService;
import org.cweb.files.FileUploadService;
import org.cweb.files.UploadedFileInfos;
import org.cweb.identity.EndorsementService;
import org.cweb.identity.IdentityProfilePostService;
import org.cweb.identity.IdentityProfileReadService;
import org.cweb.identity.IdentityService;
import org.cweb.identity.RemoteIdentityFetcher;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.local.LocalFileSystemInterface;
import org.cweb.storage.local.LocalFileSystemInterfaceFactory;
import org.cweb.storage.local.LocalPreKeyService;
import org.cweb.storage.remote.OutboundCacheService;
import org.cweb.storage.remote.RemoteFileHandler;
import org.cweb.storage.remote.RemoteWriteService;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstanceContainer extends InstanceContainerBase {
	private static final Logger log = LoggerFactory.getLogger(InstanceContainer.class);

	private final LocalFileSystemInterface localFileSystemInterface;
	private PrivateStorageProfile privateStorageProfile;
	private final LocalPreKeyService localPreKeyService;
	private final IdentityService identityService;
	private final IdentityProfilePostService identityProfilePostService;
	private final IdentityProfileReadService identityProfileReadService;
	private final EndorsementService endorsementService;
	private final RemoteWriteService remoteWriteService;
	private final RemoteAdminHostService remoteAdminHostService;
	private final PrivateBroadcastService privateBroadcastService;
	private final CommSessionService commSessionService;
	private final SharedObjectPostService sharedObjectPostService;
	private final SharedObjectReadService sharedObjectReadService;
	private final SharedSessionService sharedSessionService;
	private final CommSessionPushService commSessionPushService;
	private final CommScheduler commScheduler;
	private final FileSharingService fileSharingService;
	private final FileUploadService fileUploadService;
	private final FileDownloadService fileDownloadService;

	public InstanceContainer(String localRootPath, byte[] id, byte[] secretStorageKey) {
		super(localRootPath, id, secretStorageKey);

		this.localFileSystemInterface = LocalFileSystemInterfaceFactory.createLocalStorageInterface();

		this.privateStorageProfile = loadPrivateStorageProfile();

		if (privateStorageProfile == null) {
			throw new RuntimeException("Failed to load own storage profile");
		}

		this.localPreKeyService = new LocalPreKeyService(secretStorageService);
		PublicStorageProfile publicStorageProfile = StorageProfileUtils.toPublicStorageProfile(privateStorageProfile);

		OutboundCacheService outboundCacheService = new OutboundCacheService(localStorageInterface);
		this.remoteWriteService = new RemoteWriteService(tracePrefix, privateStorageProfile, outboundCacheService, remoteStorageClient);
		this.identityService = new IdentityService(tracePrefix, publicStorageProfile, localStorageInterface, cryptoHelper, remoteWriteService, localPreKeyService);
		this.remoteAdminHostService = new RemoteAdminHostService(tracePrefix, localStorageInterface, cryptoHelper, remoteIdentityService);
		this.privateBroadcastService = new PrivateBroadcastService(tracePrefix, cryptoHelper, identityService, remoteWriteService, remoteReadService);
		RemoteIdentityFetcher remoteIdentityFetcher = remoteIdentityService.getFetcher();
		this.endorsementService = new EndorsementService(tracePrefix, cryptoHelper, localStorageInterface, remoteIdentityFetcher, identityService, privateBroadcastService);
		this.commSessionService = new CommSessionService(tracePrefix, cryptoHelper, privateBroadcastService, remoteWriteService, remoteReadService, remoteIdentityFetcher, localStorageInterface, secretStorageService);
		RemoteFileHandler remoteSharedObjectHandler = SharedObjectCommon.createRemoteFileHandler(remoteReadService, remoteWriteService);
		this.sharedObjectPostService = new SharedObjectPostService(tracePrefix, cryptoHelper, commSessionService, remoteSharedObjectHandler, localStorageInterface);
		this.sharedObjectReadService = new SharedObjectReadService(tracePrefix, cryptoHelper, remoteIdentityService, commSessionService, remoteSharedObjectHandler, localStorageInterface);
		this.identityProfilePostService = new IdentityProfilePostService(tracePrefix, localStorageInterface, sharedObjectPostService);
		this.identityProfileReadService = new IdentityProfileReadService(tracePrefix, localStorageInterface, sharedObjectReadService);
		this.sharedSessionService = new SharedSessionService(tracePrefix, publicStorageProfile, cryptoHelper, remoteWriteService, remoteReadService, localStorageInterface, remoteIdentityService, commSessionService, sharedObjectPostService, sharedObjectReadService);
		this.commSessionPushService = new CommSessionPushService(tracePrefix, cryptoHelper, remoteIdentityFetcher, sharedSessionService);
		this.commSessionService.setCommSessionPushService(commSessionPushService);
		this.commScheduler = new CommScheduler(tracePrefix, id, remoteWriteService, remoteIdentityFetcher, remoteAdminHostService, commSessionService, sharedSessionService, sharedObjectReadService, identityProfileReadService);
		UploadedFileInfos uploadedFileInfos = new UploadedFileInfos(tracePrefix, localStorageInterface, 10, 3);
		this.fileSharingService = new FileSharingService(tracePrefix, localStorageInterface, commSessionService, sharedSessionService);
		this.fileUploadService = new FileUploadService(tracePrefix, cryptoHelper, localStorageInterface, localFileSystemInterface, remoteWriteService, remoteReadService, uploadedFileInfos, fileSharingService);
		this.fileDownloadService = new FileDownloadService(tracePrefix, cryptoHelper, localFileSystemInterface, remoteWriteService, remoteReadService, localStorageInterface, uploadedFileInfos, fileSharingService);

		this.remoteIdentityService.setOwnRemoteStorageProfile(identityService.getIdentityDescriptor().getStorageProfile());
	}

	public LocalFileSystemInterface getLocalFileSystemInterface() {
		return localFileSystemInterface;
	}

	public void close() {
		if (remoteWriteService != null) {
			remoteWriteService.close();
		}
		if (fileDownloadService != null) {
			fileDownloadService.close();
		}
		if (commScheduler != null) {
			commScheduler.close();
		}
	}

	public static void globalShutdown(int waitSeconds) {
		Threads.closeExecutors(waitSeconds);
	}

	public PrivateStorageProfile getPrivateStorageProfile() {
		return privateStorageProfile;
	}

	public PublicStorageProfile getPublicStorageProfile() {
		return new PublicStorageProfile(identityService.getIdentityDescriptor().getStorageProfile());
	}

	public Pair<Boolean, String> updatePrivateStorageProfile(PrivateStorageProfile privateStorageProfile) {
		if (privateStorageProfile.equals(this.privateStorageProfile)) {
			return Pair.of(false, null);
		}
		boolean success = RemoteWriteService.testPrivateStorageProfile(privateStorageProfile, this.remoteStorageClient);
		if (!success) {
			return Pair.of(false, "Failed to verify access");
		}
		identityService.updatePublicStorageProfile(StorageProfileUtils.toPublicStorageProfile(privateStorageProfile));
		flushUploadQueue(100);
		savePrivateStorageProfile(privateStorageProfile);
		this.privateStorageProfile = privateStorageProfile;
		remoteWriteService.updatePrivateStorageProfile(privateStorageProfile);
		return Pair.of(true, null);
	}

	@Override
	public byte[] getOwnId() {
		return getIdentityService().getIdentityDescriptor().getId();
	}

	public IdentityService getIdentityService() {
		return identityService;
	}

	public IdentityProfilePostService getIdentityProfilePostService() {
		return identityProfilePostService;
	}

	public IdentityProfileReadService getIdentityProfileReadService() {
		return identityProfileReadService;
	}

	public RemoteAdminHostService getRemoteAdminHostService() {
		return remoteAdminHostService;
	}

	public PrivateBroadcastService getPrivateBroadcastService() {
		return privateBroadcastService;
	}

	public EndorsementService getEndorsementService() {
		return endorsementService;
	}

	public CommSessionService getCommSessionService() {
		return commSessionService;
	}

	public SharedObjectPostService getSharedObjectPostService() {
		return sharedObjectPostService;
	}

	public SharedObjectReadService getSharedObjectReadService() {
		return sharedObjectReadService;
	}

	public SharedSessionService getSharedSessionService() {
		return sharedSessionService;
	}

	public CommScheduler getCommScheduler() {
		return commScheduler;
	}

	public FileUploadService getFileUploadService() {
		return fileUploadService;
	}

	public FileDownloadService getFileDownloadService() {
		return fileDownloadService;
	}

	public boolean isUploadQueueEmpty() {
		return remoteWriteService.isQueueEmpty();
	}

	public void flushUploadQueue(long sleepInterval) {
		while (!isUploadQueueEmpty()) {
			Threads.sleepChecked(sleepInterval);
		}
	}

	private PrivateStorageProfile loadPrivateStorageProfile() {
		byte[] data = secretStorageService.read(PRIVATE_STORAGE_PROFILE_FILENAME);
		if (data == null)
			return null;
		try {
			PrivateStorageProfile privateStorageProfile = ThriftUtils.deserialize(data, PrivateStorageProfile.class);
			if (Migrations.migratePrivateStorageProfile(privateStorageProfile)) {
				savePrivateStorageProfile(privateStorageProfile);
			}
			log.trace("Loaded private storage profile: " + Utils.getDebugString(privateStorageProfile));
			return privateStorageProfile;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void permanentlyDestroy() {
		close();
		localStorageInterface.deleteAll();
	}
}
