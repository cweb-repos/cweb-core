package org.cweb.storage;

import org.cweb.utils.Utils;

public class NameConversionUtils {

	public static String toString(byte[] name) {
		return Utils.encodeBase64(name);
	}

	public static byte[] fromString(String name) {
		return Utils.decodeBase64Safe(name);
	}

	public static String toString(byte[] name, String suffix) {
		return toString(name) + suffix;
	}

	public static byte[] fromString(String name, String suffix) {
		if (!name.endsWith(suffix)) {
			return null;
		}
		String nameSubstr = name.substring(0, name.length() - suffix.length());
		return Utils.decodeBase64Safe(nameSubstr);
	}
}
