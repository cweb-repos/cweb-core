package org.cweb.storage.remote;

public class RemoteStorageHandler {
	private RemoteStorageContainer.RemoteStorageType type;
	private SpecificStorageClient client;
	private StorageProfileConverter converter;

	public RemoteStorageHandler(RemoteStorageContainer.RemoteStorageType type, SpecificStorageClient client, StorageProfileConverter converter) {
		this.type = type;
		this.client = client;
		this.converter = converter;
	}

	public RemoteStorageContainer.RemoteStorageType getType() {
		return type;
	}

	public SpecificStorageClient getClient() {
		return client;
	}

	public StorageProfileConverter getConverter() {
		return converter;
	}
}
