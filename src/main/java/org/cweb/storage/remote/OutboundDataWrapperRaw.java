package org.cweb.storage.remote;

import org.cweb.schemas.storage.LocalMetadataEnvelope;

public class OutboundDataWrapperRaw extends OutboundDataWrapper<byte[]> {
	public OutboundDataWrapperRaw(byte[] data, LocalMetadataEnvelope localMetadataEnvelope, Long deleteAt, boolean deleteDataAfterUpload) {
		super(data, localMetadataEnvelope, deleteAt, deleteDataAfterUpload);
	}

	public OutboundDataWrapperRaw(byte[] data, LocalMetadataEnvelope localMetadataEnvelope, Long deleteAt) {
		super(data, localMetadataEnvelope, deleteAt, false);
	}
}
