package org.cweb.storage.remote;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.storage.InboundDataRecord;
import org.cweb.storage.NameConversionUtils;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.ThriftUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class InboundCacheService {
	private static final Logger log = LoggerFactory.getLogger(InboundCacheService.class);
	private static final String PATH_PREFIX = "ic/";

	private final LocalStorageInterface localStorageInterface;

	public InboundCacheService(LocalStorageInterface localStorageInterface) {
		this.localStorageInterface = localStorageInterface;
	}

	public static String getName(byte[] id, RemoteFileDescriptor file) {
		return PATH_PREFIX + NameConversionUtils.toString(id) + "/" + file.getName();
	}

	public boolean checkIfExists(byte[] id, RemoteFileDescriptor file) {
		return localStorageInterface.checkIfExists(getName(id, file));
	}

	public InboundDataRecord get(byte[] id, RemoteFileDescriptor file) {
		byte[] data = localStorageInterface.read(getName(id, file));
		if (data == null) {
			return null;
		}
		return ThriftUtils.deserializeSafe(data, InboundDataRecord.class);
	}

	public void put(byte[] id, RemoteFileDescriptor file, InboundDataRecord dataRecord) {
		if (!IdentityService.isValidId(id)) {
			log.warn("Invalid id " + NameConversionUtils.toString(id));
			return;
		}
		localStorageInterface.write(getName(id, file), ThriftUtils.serialize(dataRecord));
	}

	public List<Pair<byte[], RemoteFileDescriptor>> listFiles() {
		List<Pair<byte[], RemoteFileDescriptor>> results = new ArrayList<>();
		List<String> dirs = localStorageInterface.listDirectories(PATH_PREFIX);
		for (String dir : dirs) {
			byte[] id = IdentityService.idFromString(dir);
			if (id == null) {
				log.warn("Invalid directory " + dir);
				continue;
			}
			List<String> files = localStorageInterface.listFiles(PATH_PREFIX + dir);
			results.addAll(RemoteStorageUtils.convertToDescriptors(id, files));
		}
		return results;
	}

	public boolean delete(byte[] id, RemoteFileDescriptor file) {
		String name = getName(id, file);
		return localStorageInterface.delete(name);
	}

	public void deleteAll() {
		List<Pair<byte[], RemoteFileDescriptor>> files = listFiles();
		for (Pair<byte[], RemoteFileDescriptor> pair : files) {
			byte[] id = pair.getLeft();
			RemoteFileDescriptor file = pair.getRight();
			delete(id, file);
		}
	}
}
