package org.cweb.storage.remote;

public class RemoteFileHandler {
	private final RemoteReadService remoteReadService;
	private final RemoteWriteService remoteWriteService;
	private final String nameSuffix;

	public RemoteFileHandler(RemoteReadService remoteReadService, RemoteWriteService remoteWriteService, String nameSuffix) {
		this.remoteReadService = remoteReadService;
		this.remoteWriteService = remoteWriteService;
		this.nameSuffix = nameSuffix;
	}

	public RemoteFetchResultRaw read(byte[] id, byte[] name) {
		return remoteReadService.read(id, name, nameSuffix);
	}

	/**
	 *
	 * @return true if file with this name already exists
	 */
	public boolean write(byte[] id, OutboundDataWrapperRaw dataWrapper) {
		return remoteWriteService.write(id, nameSuffix, dataWrapper);
	}

	/**
	 *
	 * @return false if file with this name does not exist
	 */
	public boolean delete(byte[] name) {
		return remoteWriteService.delete(name, nameSuffix);
	}

	public OutboundDataWrapperRaw getFromLocalCache(byte[] name) {
		return remoteWriteService.getFromLocalCache(name, nameSuffix);
	}

	public RemoteWriteService.UploadState getUploadState(byte[] name) {
		return remoteWriteService.getUploadState(name, nameSuffix);
	}
}
