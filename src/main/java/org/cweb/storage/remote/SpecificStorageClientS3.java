package org.cweb.storage.remote;

import com.google.common.base.Preconditions;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.cweb.schemas.storage.PrivateS3StorageProfile;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicS3StorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.S3Credentials;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

public class SpecificStorageClientS3 implements SpecificStorageClient {
	private static final Logger log = LoggerFactory.getLogger(SpecificStorageClientS3.class);
	private static final MediaType BINARY = MediaType.parse("application/octet-stream");
	private static final String SERVICE_NAME_FOR_AUTHORIZATION = "s3";
	// S3 max key length is 1000 bytes in UTF8
	private static final int MAX_KEY_LENGTH = 800;
	private final OkHttpClient client;
	private final DateTimeFormatter dateTimeFormat;

	SpecificStorageClientS3() {
		client = new OkHttpClient.Builder()
				.retryOnConnectionFailure(true)
				.build();
		dateTimeFormat = DateTimeFormatter.ofPattern(AWSV4HeaderSigner.ISO_8601_BASIC_FORMAT).withZone(ZoneId.of("UTC"));
	}

	private String buildUrl(PublicS3StorageProfile profile, RemoteFileDescriptor file) {
		String pathPrefix = profile.getPathPrefix();
		return profile.getHost() +
				'/' + profile.getBucket() +
				(pathPrefix != null && !pathPrefix.isEmpty() ? '/' + pathPrefix : "") +
				'/' + file.getName();
	}

	private Map<String, String> getBaseHeaders(String host, String contentHashString) {
		Map<String, String> headers = new LinkedHashMap<>();
		headers.put("Host", host);
		// headers.put("x-amz-acl", "public-read");
		headers.put("x-amz-content-sha256", contentHashString);
		LocalDateTime now = LocalDateTime.now().atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
		String dateTimeStamp = dateTimeFormat.format(now);
		headers.put("x-amz-date", dateTimeStamp);
		return headers;
	}

	private void setAuthHeaders(S3Credentials credentials, String host, String region, String url, Request.Builder builder, String contentHashString, String httpMethod) throws MalformedURLException {
		Map<String, String> headers = getBaseHeaders(host, contentHashString);
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			builder.header(entry.getKey(), entry.getValue());
		}
		if (credentials != null) {
			String authorization = AWSV4HeaderSigner.signRequest(new URL(url), httpMethod, SERVICE_NAME_FOR_AUTHORIZATION, region, headers, null, contentHashString, credentials.getAccessKeyId(), credentials.getSecretAccessKey());
			builder.header("Authorization", authorization);
		}
	}

	@Override
	public void put(PrivateStorageProfile privateStorageProfile, RemoteFileDescriptor file, byte[] data) throws Exception {
		PrivateS3StorageProfile privateS3StorageProfile = privateStorageProfile.getPrivateS3StorageProfile();
		Preconditions.checkNotNull(privateS3StorageProfile);
		Preconditions.checkArgument(file.getName().length() <= MAX_KEY_LENGTH);
		PublicS3StorageProfile publicStorageProfile = privateS3StorageProfile.getPublicS3StorageProfile();

		String host = Utils.getHost(publicStorageProfile.getHost());
		String url = buildUrl(publicStorageProfile, file);
		Request.Builder builder = new Request.Builder()
				.url(url)
				.put(RequestBody.create(data, BINARY));

		byte[] contentHash = AWSV4HeaderSigner.hash(data);
		String contentHashString = AWSV4HeaderSigner.toHex(contentHash);
		String httpMethod = "PUT";
		setAuthHeaders(privateS3StorageProfile.getCredentials(), host, publicStorageProfile.getRegion(), url, builder, contentHashString, httpMethod);

		builder.header("content-length", String.valueOf(data.length));

		Request request = builder.build();
		try (Response response = client.newCall(request).execute()) {
			if (!response.isSuccessful()) {
				String bodyStr = response.body().source().readUtf8();
				if (response.code() == 403 && bodyStr.contains("<Error><Code>SignatureDoesNotMatch</Code>")) {
					log.debug("Exception 403 uploading file");
					throw new TemporaryRemoteException();
				}
				Exception e = new Exception(response.code() + ": " + response.message() + ": " + bodyStr);
				log.warn("Exception uploading file", e);
				throw e;
			}
		}
	}

	@Override
	public void delete(PrivateStorageProfile privateStorageProfile, RemoteFileDescriptor file) throws Exception {
		PrivateS3StorageProfile privateS3StorageProfile = privateStorageProfile.getPrivateS3StorageProfile();
		Preconditions.checkNotNull(privateS3StorageProfile);
		Preconditions.checkArgument(file.getName().length() <= MAX_KEY_LENGTH);
		PublicS3StorageProfile publicStorageProfile = privateS3StorageProfile.getPublicS3StorageProfile();

		String host = Utils.getHost(publicStorageProfile.getHost());
		String url = buildUrl(publicStorageProfile, file);
		Request.Builder builder = new Request.Builder()
				.url(url)
				.delete();

		String contentHashString = AWSV4HeaderSigner.EMPTY_BODY_SHA256;
		String httpMethod = "DELETE";
		setAuthHeaders(privateS3StorageProfile.getCredentials(), host, publicStorageProfile.getRegion(), url, builder, contentHashString, httpMethod);

		Request request = builder.build();
		try (Response response = client.newCall(request).execute()) {
			if (!response.isSuccessful()) {
				String bodyStr = response.body().source().readUtf8();
				if (response.code() == 403 && bodyStr.contains("<Error><Code>SignatureDoesNotMatch</Code><Message>")) {
					log.debug("Exception 403 deleting file");
					throw new TemporaryRemoteException();
				}
				Exception e = new Exception(response.code() + " : " + response.message() + " : " + response.body().source().readUtf8());
				log.warn("Exception deleting file", e);
				throw e;
			}
		}
	}

	@Override
	public byte[] get(PublicStorageProfile publicStorageProfile, RemoteFileDescriptor file) throws Exception {
		PublicS3StorageProfile publicS3StorageProfile = publicStorageProfile.getPublicS3StorageProfile();
		Preconditions.checkNotNull(publicS3StorageProfile);
		Preconditions.checkArgument(file.getName().length() <= MAX_KEY_LENGTH);

		String host = Utils.getHost(publicS3StorageProfile.getHost());
		String url = buildUrl(publicS3StorageProfile, file);
		Request.Builder builder = new Request.Builder()
				.url(url);

		S3Credentials readCredentials = publicS3StorageProfile.getReadCredentials();
		if (readCredentials != null) {
			setAuthHeaders(readCredentials, host, publicS3StorageProfile.getRegion(), url, builder, AWSV4HeaderSigner.EMPTY_BODY_SHA256, "GET");
		}

		Request request = builder.build();
		try (Response response = client.newCall(request).execute()) {
			if (!response.isSuccessful()) {
				if (response.code() == 403 || response.code() == 404) {
					return null;
				}

				throw new RemoteStorageException(response.code() + " : " + response.message() + " : " + response.body().source().readUtf8());
			}

			byte[] responseData = response.body().bytes();
			return responseData;
		} catch (IOException e) {
			throw new RemoteStorageException(e);
		}
	}
}
