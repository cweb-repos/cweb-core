package org.cweb.storage.remote;

import org.cweb.Config;
import org.cweb.schemas.storage.OutboundDataRecord;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.RemoteSyncStatus;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

public class RemoteWriteService {
	private static final Logger log = LoggerFactory.getLogger(RemoteWriteService.class);
	private static final long WAIT_TIME_ON_WRITE_FAILURE = 1000L * 60 * 2;
	private static final long FINAL_DELETION_DELAY = 1000L * 60 * 60 * 24 * 7;
	private static final int FINAL_DELETION_AFTER_CONSECUTIVE_ERRORS = 10;
	private PrivateStorageProfile privateStorageProfile;
	private final OutboundCacheService outboundCacheService;
	private final RemoteStorageClient remoteStorageClient;
	private final LinkedBlockingDeque<RemoteFileDescriptor> pendingWrites;
	private final Object pendingWritesLock = new Object();
	private final String tracePrefix;
	private boolean closed;

	public RemoteWriteService(String tracePrefix, PrivateStorageProfile privateStorageProfile, OutboundCacheService outboundCacheService, RemoteStorageClient remoteStorageClient) {
		this.tracePrefix = tracePrefix;
		this.privateStorageProfile = privateStorageProfile;
		this.outboundCacheService = outboundCacheService;
		this.remoteStorageClient = remoteStorageClient;
		this.pendingWrites = new LinkedBlockingDeque<>();

		this.uploadThread.setDaemon(true);
		this.uploadThread.start();

		List<RemoteFileDescriptor> deleted = deleteStale();

		List<RemoteFileDescriptor> pendingOutbound = this.outboundCacheService.listOutboundPending();
		for (RemoteFileDescriptor file : pendingOutbound) {
			// log.trace(tracePrefix + " pendingOutbound: " + file);
			if (!deleted.contains(file)) {
				enqueueLast(file);
			}
		}

		/*List<RemoteFileDescriptor> outbound = outboundCacheService.listUploaded();
		for (RemoteFileDescriptor file : outbound) {
			log.trace(tracePrefix + " outbound: " + file);
		}*/
	}

	public void updatePrivateStorageProfile(PrivateStorageProfile privateStorageProfile) {
		boolean isSameLocation = StorageProfileUtils.isLocationEqual(StorageProfileUtils.toPublicStorageProfile(this.privateStorageProfile), StorageProfileUtils.toPublicStorageProfile(privateStorageProfile));
		this.privateStorageProfile = privateStorageProfile;
		if (!isSameLocation) {
			log.trace(tracePrefix + " re-uploading files due to storage profile update");
			reUploadAll();
		}
	}

	private void reUploadAll() {
		synchronized (pendingWritesLock) {
			List<RemoteFileDescriptor> filesToUpload = outboundCacheService.listUploaded();
			filesToUpload.addAll(outboundCacheService.listOutboundPending());
			for (RemoteFileDescriptor file : filesToUpload) {
				OutboundDataRecord dataRecord = outboundCacheService.get(file);
				if (!dataRecord.isDeleted() && dataRecord.getData() != null) {
					dataRecord.setUploadStatus(new RemoteSyncStatus());
					outboundCacheService.put(file, dataRecord);
					enqueueLast(file);
				}
			}
		}
	}

	private boolean isExpired(OutboundDataRecord dataRecord, long now) {
		return dataRecord.isSetDeleteAt() && !dataRecord.isDeleted() && now >= dataRecord.getDeleteAt();
	}

	private List<RemoteFileDescriptor> deleteStale() {
		long now = System.currentTimeMillis();
		List<RemoteFileDescriptor> deleted = new ArrayList<>();
		List<RemoteFileDescriptor> outbound = outboundCacheService.listUploaded();
		outbound.addAll(this.outboundCacheService.listOutboundPending());
		for (RemoteFileDescriptor fileDescriptor : outbound) {
			OutboundDataRecord dataRecord = outboundCacheService.get(fileDescriptor);
			if (dataRecord == null) {
				log.trace(tracePrefix + " failed to read " + fileDescriptor.toString() + ", deleting");
				outboundCacheService.delete(fileDescriptor);
				continue;
			}
			if (isExpired(dataRecord, now)) {
				delete(fileDescriptor);
				deleted.add(fileDescriptor);
			} else if (dataRecord.isDeleted() &&
					((dataRecord.getDeletionStatus().isSetLastSuccessTime() && now - dataRecord.getDeletionStatus().getLastSuccessTime() >= FINAL_DELETION_DELAY) ||
							(dataRecord.getDeletionStatus().isSetNumConsecutiveErrors() && dataRecord.getDeletionStatus().getNumConsecutiveErrors() >= FINAL_DELETION_AFTER_CONSECUTIVE_ERRORS))) {
				outboundCacheService.delete(fileDescriptor);
				deleted.add(fileDescriptor);
			}
		}
		long elapsed = System.currentTimeMillis() - now;
		log.trace(tracePrefix + " RemoteWriteService.deleteStale took " + elapsed + " ms");
		return deleted;
	}

	private final Thread uploadThread = new Thread("RemoteWriteThread") {
		@Override
		public void run() {
			while (!closed) {
				Boolean success = dequeueAndUploadFile();
				synchronized (pendingWritesLock) {
					if (closed) {
						break;
					}
					if (success == null) {
						if (pendingWrites.isEmpty()) {
							Threads.waitChecked(pendingWritesLock);
						}
					} else if (!success) {
						Threads.waitChecked(pendingWritesLock, WAIT_TIME_ON_WRITE_FAILURE);
					}
				}
			}
			log.trace(tracePrefix + " RemoteWriteServiceUploadThread terminated");
		}
	};

	public void retryUploads() {
		synchronized (pendingWritesLock) {
			if (!pendingWrites.isEmpty()) {
				pendingWritesLock.notify();
			}
		}
	}

	private Boolean dequeueAndUploadFile() {
		RemoteFileDescriptor file = pendingWrites.peek();
		if (file == null) {
			return null;
		}

		long downloadStartTime = System.currentTimeMillis();
		boolean uploadSuccess;
		String error = null;

		OutboundDataRecord dataRecordInitial;
		synchronized (outboundCacheService) {
			dataRecordInitial = outboundCacheService.get(file);
		}
		if (dataRecordInitial == null) {
			log.warn(tracePrefix + " Failed to read file " + file);
			return false;
		}
		RemoteSyncStatus syncStatusInitial = dataRecordInitial.isDeleted() ? dataRecordInitial.getDeletionStatus() : dataRecordInitial.getUploadStatus();
		if (syncStatusInitial.isSetLastSuccessTime()) {
			log.warn(tracePrefix + " Redundant write for " + file + ", syncStatus=" + syncStatusInitial.toString(), ", downloadStartTime=" + downloadStartTime);
			dequeue(file);
			return true;
		}

		try {
			if (Config.SIMULATE_REMOTE_STORAGE_WRITE_LATENCY > 0) {
				Thread.sleep(Config.SIMULATE_REMOTE_STORAGE_WRITE_LATENCY);
			}
			if (dataRecordInitial.isDeleted()) {
				remoteStorageClient.delete(privateStorageProfile, file);
			} else {
				remoteStorageClient.put(privateStorageProfile, file, dataRecordInitial.getData());
			}

			long completionTime = System.currentTimeMillis();
			if (!dataRecordInitial.isDeleted()) {
				log.trace(tracePrefix + " Uploaded " + file + " of " + dataRecordInitial.getDataSize() + " bytes in " + (completionTime - downloadStartTime) + " ms");
			} else {
				log.trace(tracePrefix + " Deleted " + file + " in " + (completionTime - downloadStartTime) + " ms");
			}
			uploadSuccess = true;
		} catch (java.net.UnknownHostException | SocketTimeoutException | java.net.ConnectException e) {
			log.trace(tracePrefix + " Network error " + e.getClass().getSimpleName() + " uploading " + file);
			error = e.getMessage();
			uploadSuccess = false;
		} catch (TemporaryRemoteException e) {
			error = e.getMessage();
			uploadSuccess = false;
		} catch (Exception e) {
			log.warn(tracePrefix + " Failed to upload file " + file, e);
			error = e.getMessage();
			uploadSuccess = false;
		}

		synchronized (outboundCacheService) {
			OutboundDataRecord dataRecordFinal = outboundCacheService.get(file);
			if (dataRecordFinal == null) {
				log.warn(tracePrefix + " Failed to read file after IO " + file);
				return false;
			}

			// Concurrent modification
			boolean newData = !Arrays.equals(dataRecordInitial.getData(), dataRecordFinal.getData());
			boolean newDeletion = dataRecordInitial.isDeleted() != dataRecordFinal.isDeleted();

			RemoteSyncStatus syncStatusFinal = dataRecordFinal.isDeleted() ? dataRecordFinal.getDeletionStatus() : dataRecordFinal.getUploadStatus();
			syncStatusFinal.setLastAttemptTime(downloadStartTime);
			if (!newData && !newDeletion) {
				if (uploadSuccess) {
					if (dataRecordFinal.isDeleteDataAfterUpload()) {
						dataRecordFinal.unsetData();
					}
					syncStatusFinal.setLastSuccessTime(downloadStartTime);
					syncStatusFinal.setLastError(null);
					syncStatusFinal.setNumConsecutiveErrors(0);
				} else {
					syncStatusFinal.setLastError(error);
					syncStatusFinal.setNumConsecutiveErrors(syncStatusFinal.getNumConsecutiveErrors() + 1);
				}
			}

			outboundCacheService.put(file, dataRecordFinal);
			if (uploadSuccess && !newData && !newDeletion) {
				outboundCacheService.onOutboundUploadCompleted(file, dataRecordFinal);
				dequeue(file);
			}
		}

		return uploadSuccess;
	}

	private void dequeue(RemoteFileDescriptor file) {
		synchronized (pendingWritesLock) {
			boolean removed = pendingWrites.remove(file);
			if (!removed) {
				log.error(tracePrefix + " Queue inconsistency");
			}
		}
	}

	private void enqueueLast(RemoteFileDescriptor file) {
		synchronized (pendingWritesLock) {
			if (!pendingWrites.contains(file)) {
				pendingWrites.add(file);
				pendingWritesLock.notify();
			}
		}
	}

	public boolean write(byte[] name, OutboundDataWrapperRaw data) {
		return write(RemoteFileDescriptor.from(NameConversionUtils.toString(name)), data);
	}

	public boolean write(byte[] name, String nameSuffix, OutboundDataWrapperRaw data) {
		return write(RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)), data);
	}

	private boolean write(RemoteFileDescriptor file, OutboundDataWrapperRaw data) {
		synchronized (outboundCacheService) {
			boolean exists = outboundCacheService.checkIfExists(file);
			OutboundDataRecord dataRecord = new OutboundDataRecord(new RemoteSyncStatus(), false, data.getData().length, data.isDeleteDataAfterUpload());
			if (data.getDeleteAt() != null) {
				dataRecord.setDeleteAt(data.getDeleteAt());
			}
			dataRecord.setData(data.getData());
			dataRecord.setLocalMetadataEnvelope(data.getLocalMetadataEnvelope());
			try {
				outboundCacheService.put(file, dataRecord);
				enqueueLast(file);
				log.trace(tracePrefix + " Enqueued upload " + file);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			return exists;
		}
	}

	public boolean delete(byte[] name, String nameSuffix) {
		return delete(RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)));
	}

	public boolean delete(RemoteFileDescriptor file) {
		synchronized (outboundCacheService) {
			OutboundDataRecord dataRecord = outboundCacheService.get(file);
			if (dataRecord == null) {
				return false;
			}
			if (dataRecord.isDeleted()) {
				return true;
			}
			dataRecord.setDeleted(true);
			dataRecord.setDeletionStatus(new RemoteSyncStatus());
			try {
				outboundCacheService.put(file, dataRecord);
				enqueueLast(file);
				log.trace(tracePrefix + " Enqueued deletion " + file);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return true;
	}

	public static class UploadState {
		public boolean deleted = false;
		public Long completedAt;
	}

	public UploadState getUploadState(byte[] name, String nameSuffix) {
		RemoteFileDescriptor file = RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix));
		return getUploadState(file);
	}

	public UploadState getUploadState(RemoteFileDescriptor file) {
		synchronized (outboundCacheService) {
			OutboundDataRecord dataRecord = outboundCacheService.get(file);
			if (dataRecord == null) {
				return null;
			}

			UploadState result = new UploadState();
			result.deleted = dataRecord.isDeleted();

			RemoteSyncStatus status;
			if (dataRecord.isDeleted()) {
				status = dataRecord.getDeletionStatus();
			} else {
				status = dataRecord.getUploadStatus();
			}
			if (status.isSetLastSuccessTime()) {
				result.completedAt = status.getLastSuccessTime();
			}

			return result;
		}
	}

	public OutboundDataWrapperRaw getFromLocalCache(byte[] name) {
		return getFromLocalCache(RemoteFileDescriptor.from(NameConversionUtils.toString(name)));
	}

	public OutboundDataWrapperRaw getFromLocalCache(byte[] name, String nameSuffix) {
		return getFromLocalCache(RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)));
	}

	public OutboundDataWrapperRaw getFromLocalCache(RemoteFileDescriptor file) {
		synchronized (outboundCacheService) {
			OutboundDataRecord dataRecord = outboundCacheService.get(file);
			if (dataRecord == null) {
				return null;
			}
			long now = System.currentTimeMillis();
			if (isExpired(dataRecord, now)) {
				return null;
			}
			return new OutboundDataWrapperRaw(dataRecord.getData(), dataRecord.getLocalMetadataEnvelope(), dataRecord.isSetDeleteAt() ? dataRecord.getDeleteAt() : null, dataRecord.isDeleteDataAfterUpload());
		}
	}

	public static boolean testPrivateStorageProfile(PrivateStorageProfile privateStorageProfile, RemoteStorageClient remoteStorageClient) {
		byte[] name = Utils.generateRandomBytes(64);
		byte[] data = Utils.generateRandomBytes(1024);
		RemoteFileDescriptor file = RemoteFileDescriptor.from(NameConversionUtils.toString(name));
		return testFileWriteAndRead(privateStorageProfile, remoteStorageClient, file, data);
	}

	private static boolean testFileWriteAndRead(PrivateStorageProfile privateStorageProfile, RemoteStorageClient remoteStorageClient, RemoteFileDescriptor file, byte[] data) {
		try {
			remoteStorageClient.put(privateStorageProfile, file, data);
		} catch (Exception e) {
			log.trace("Error writing test file: " + e.getMessage());
			return false;
		}

		PublicStorageProfile publicStorageProfile = StorageProfileUtils.toPublicStorageProfile(privateStorageProfile);
		try {
			byte[] dataRead = remoteStorageClient.get(publicStorageProfile, file);
			if (dataRead == null) {
				return false;
			}
			if (!Arrays.equals(dataRead, data)) {
				log.trace("Data corrupted on testing account");
				return false;
			}
		} catch (Exception e) {
			log.trace("Error reading test file: " + e.getMessage());
			return false;
		}

		try {
			remoteStorageClient.delete(privateStorageProfile, file);
		} catch (Exception e) {
			log.trace("Error reading test file: " + e.getMessage());
			return false;
		}

		return true;
	}

	public boolean isQueueEmpty() {
		return pendingWrites.isEmpty();
	}

	public void close() {
		closed = true;
		Threads.joinThreadSafe(uploadThread);
	}
}
