package org.cweb.storage.remote;

class RemoteFileDescriptor {
	private final String name;

	private RemoteFileDescriptor(String name) {
		this.name = name;
	}

	public static RemoteFileDescriptor from(String name) {
		return new RemoteFileDescriptor(name);
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RemoteFileDescriptor that = (RemoteFileDescriptor) o;

		return name.equals(that.name);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
