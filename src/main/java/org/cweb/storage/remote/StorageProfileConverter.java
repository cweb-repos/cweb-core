package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;

import java.util.function.Function;

public interface StorageProfileConverter {
	boolean isOwnType(PublicStorageProfile publicStorageProfile);

	boolean isOwnType(PrivateStorageProfile privateStorageProfile);

	String getUniquePathPrefix(PublicStorageProfile publicStorageProfile);

	PublicStorageProfile toPublicStorageProfile(PrivateStorageProfile privateStorageProfile);

	PublicStorageProfile parsePublicStorageProfileHumanReadable(String publicStorageProfileStr);

	String toHumanReadableString(PublicStorageProfile publicStorageProfile);

	PrivateStorageProfile parsePrivateStorageProfileHumanReadable(String privateStorageProfileStr);

	String toHumanReadableString(PrivateStorageProfile privateStorageProfile);

	PrivateStorageProfile modifyPaths(PrivateStorageProfile privateStorageProfile, Function<String, String> updater);

	boolean isLocationEqual(PublicStorageProfile profile1, PublicStorageProfile profile2);
}
