package org.cweb.storage.remote;

import com.google.common.base.Preconditions;
import org.cweb.Config;

public class RemoteStorageFactory {
	private static RemoteStorageClient client;

	public static RemoteStorageClient getRemoteStorageClient() {
		if (client != null) {
			return client;
		}
		switch (Config.remoteStorageType) {
			case REAL_REMOTE:
				client = new RemoteStorageClientImpl();
				break;
			case LOCAL_FILE_SYSTEM:
				Preconditions.checkNotNull(Config.remoteStorageLocalFileSystemRootPath);
				client = new LocalFileMockClient(Config.remoteStorageLocalFileSystemRootPath + "/rdm");
				break;
			case LOCAL_MEMORY:
				client = new InMemoryMockClient();
				break;
		}
		return client;
	}
}
