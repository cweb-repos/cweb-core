package org.cweb.storage.remote;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

class RemoteStorageContainer {
	private static final Logger log = LoggerFactory.getLogger(RemoteStorageContainer.class);

	enum RemoteStorageType {
		S3,
	}

	private static Map<RemoteStorageType, RemoteStorageHandler> type2handler = ImmutableMap.<RemoteStorageType, RemoteStorageHandler>builder()
			.put(RemoteStorageType.S3, new RemoteStorageHandler(RemoteStorageType.S3, new SpecificStorageClientS3(), new StorageProfileConverterS3()))
			.build();

	private static List<RemoteStorageHandler> handlers = ImmutableList.copyOf(type2handler.values());

	static List<RemoteStorageHandler> getHandlers() {
		return handlers;
	}

	static RemoteStorageHandler getHandler(PublicStorageProfile publicStorageProfile) {
		for (RemoteStorageHandler handler : getHandlers()) {
			if (handler.getConverter().isOwnType(publicStorageProfile)) {
				return handler;
			}
		}
		log.error("Unsupported profile type");
		return null;
	}

	static RemoteStorageHandler getHandler(PrivateStorageProfile privateStorageProfile) {
		for (RemoteStorageHandler handler : getHandlers()) {
			if (handler.getConverter().isOwnType(privateStorageProfile)) {
				return handler;
			}
		}
		log.error("Unsupported profile type");
		return null;
	}

}
