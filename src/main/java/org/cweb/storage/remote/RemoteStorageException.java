package org.cweb.storage.remote;

public class RemoteStorageException extends Exception {
	public RemoteStorageException(Throwable cause) {
		super(cause);
	}

	public RemoteStorageException(String message) {
		super(message);
	}

	public RemoteStorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
