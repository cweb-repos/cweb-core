package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.storage.local.LocalStorageService;

public class LocalFileMockClient implements RemoteStorageClient {
	private final LocalStorageInterface localStorageInterface;

	public LocalFileMockClient(String localRootPath) {
		this.localStorageInterface = new LocalStorageService(localRootPath);
	}

	private String getFullName(PublicStorageProfile storageProfile, RemoteFileDescriptor file) {
		String pathPrefix = StorageProfileUtils.getLocalPathPrefix(storageProfile);
		return pathPrefix + "/" + file.getName();
	}

	@Override
	public void put(PrivateStorageProfile storageProfile, RemoteFileDescriptor file, byte[] data) {
		String fullName = getFullName(StorageProfileUtils.toPublicStorageProfile(storageProfile), file);
		localStorageInterface.write(fullName, data);
	}

	@Override
	public byte[] get(PublicStorageProfile storageProfile, RemoteFileDescriptor file) {
		String fullName = getFullName(storageProfile, file);
		return localStorageInterface.read(fullName);
	}

	@Override
	public void delete(PrivateStorageProfile storageProfile, RemoteFileDescriptor file) {
		String fullName = getFullName(StorageProfileUtils.toPublicStorageProfile(storageProfile), file);
		localStorageInterface.delete(fullName);
	}
}
