package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;

public interface RemoteStorageClient {
	void put(PrivateStorageProfile storageProfile, RemoteFileDescriptor file, byte[] data) throws Exception;
	byte[] get(PublicStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception;
	void delete(PrivateStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception;
}
