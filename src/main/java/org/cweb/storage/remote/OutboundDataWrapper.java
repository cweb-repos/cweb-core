package org.cweb.storage.remote;

import org.cweb.schemas.storage.LocalMetadataEnvelope;

public class OutboundDataWrapper<T> {
	private final T data;
	private final LocalMetadataEnvelope localMetadataEnvelope;
	private final Long deleteAt;
	private final boolean deleteDataAfterUpload;

	public OutboundDataWrapper(T data, LocalMetadataEnvelope localMetadataEnvelope, Long deleteAt, boolean deleteDataAfterUpload) {
		this.data = data;
		this.localMetadataEnvelope = localMetadataEnvelope;
		this.deleteAt = deleteAt;
		this.deleteDataAfterUpload = deleteDataAfterUpload;
	}

	public T getData() {
		return data;
	}

	public LocalMetadataEnvelope getLocalMetadataEnvelope() {
		return localMetadataEnvelope;
	}

	public Long getDeleteAt() {
		return deleteAt;
	}

	public boolean isDeleteDataAfterUpload() {
		return deleteDataAfterUpload;
	}
}
