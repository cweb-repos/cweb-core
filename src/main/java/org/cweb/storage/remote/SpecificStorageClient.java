package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;

public interface SpecificStorageClient {
	void put(PrivateStorageProfile privateStorageProfile, RemoteFileDescriptor file, byte[] data) throws Exception;
	byte[] get(PublicStorageProfile publicStorageProfile, RemoteFileDescriptor file) throws Exception;
	void delete(PrivateStorageProfile privateStorageProfile, RemoteFileDescriptor file) throws Exception;
}
