package org.cweb.storage.remote;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class AWSV4HeaderSigner {
	static final String EMPTY_BODY_SHA256 = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
	static final String ISO_8601_BASIC_FORMAT = "yyyyMMdd'T'HHmmss'Z'";

	private static final String SCHEME = "AWS4";
	private static final String ALGORITHM = "HMAC-SHA256";
	private static final String TERMINATOR = "aws4_request";
	private static final String DATE_STRING_FORMAT = "yyyyMMdd";

	private static final DateTimeFormatter dateTimeFormat;
	private static final DateTimeFormatter dateStampFormat;

	private static final String SIGNING_ALGORITHM = "HmacSHA256";

	static {
		dateTimeFormat = DateTimeFormatter.ofPattern(ISO_8601_BASIC_FORMAT).withZone(ZoneId.of("UTC"));
		dateStampFormat = DateTimeFormatter.ofPattern(DATE_STRING_FORMAT).withZone(ZoneId.of("UTC"));
	}

	public static String signRequest(URL endpointUrl, String httpMethod, String serviceName, String regionName, Map<String, String> headers, Map<String, String> queryParameters, String bodyHash, String awsAccessKey, String awsSecretKey) {
		LocalDateTime now = LocalDateTime.now().atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
		String dateTimeStamp = dateTimeFormat.format(now);

		headers.put("x-amz-date", dateTimeStamp);
		headers.put("Host", endpointUrl.getHost());

		String canonicalizedHeaderNames = getCanonicalizeHeaderNames(headers);
		String canonicalizedHeaders = getCanonicalizedHeaderString(headers);
		String canonicalizedQueryParameters = getCanonicalizedQueryString(queryParameters);
		String canonicalRequest = getCanonicalRequest(endpointUrl, httpMethod,
				canonicalizedQueryParameters, canonicalizedHeaderNames,
				canonicalizedHeaders, bodyHash);

		String dateStamp = dateStampFormat.format(now);
		String scope = dateStamp + "/" + regionName + "/" + serviceName + "/" + TERMINATOR;
		String stringToSign = getStringToSign(SCHEME, ALGORITHM, dateTimeStamp, scope, canonicalRequest);

		byte[] kSecret = (SCHEME + awsSecretKey).getBytes(StandardCharsets.UTF_8);
		byte[] kDate = sign(dateStamp, kSecret);
		byte[] kRegion = sign(regionName, kDate);
		byte[] kService = sign(serviceName, kRegion);
		byte[] kSigning = sign(TERMINATOR, kService);
		byte[] signature = sign(stringToSign, kSigning);

		String credentialsAuthorizationHeader =
				"Credential=" + awsAccessKey + "/" + scope;
		String signedHeadersAuthorizationHeader =
				"SignedHeaders=" + canonicalizedHeaderNames;
		String signatureAuthorizationHeader =
				"Signature=" + toHex(signature);

		return SCHEME + "-" + ALGORITHM + " "
				+ credentialsAuthorizationHeader + ", "
				+ signedHeadersAuthorizationHeader + ", "
				+ signatureAuthorizationHeader;
	}

	private static String getCanonicalizeHeaderNames(Map<String, String> headers) {
		List<String> sortedHeaders = new ArrayList<>(headers.keySet());
		Collections.sort(sortedHeaders, String.CASE_INSENSITIVE_ORDER);

		StringBuilder buffer = new StringBuilder();
		for (String header : sortedHeaders) {
			if (buffer.length() > 0) buffer.append(";");
			buffer.append(header.toLowerCase());
		}

		return buffer.toString();
	}

	private static String getCanonicalizedHeaderString(Map<String, String> headers) {
		List<String> sortedHeaders = new ArrayList<>(headers.keySet());
		Collections.sort(sortedHeaders, String.CASE_INSENSITIVE_ORDER);

		StringBuilder buffer = new StringBuilder();
		for (String key : sortedHeaders) {
			buffer.append(key.toLowerCase().replaceAll("\\s+", " ")).append(":").append(headers.get(key).replaceAll("\\s+", " "));
			buffer.append("\n");
		}

		return buffer.toString();
	}

	private static String getCanonicalizedQueryString(Map<String, String> parameters) {
		if (parameters == null || parameters.isEmpty()) {
			return "";
		}

		SortedMap<String, String> sorted = new TreeMap<>();
		for (Map.Entry<String, String> pair : parameters.entrySet()) {
			String key = pair.getKey();
			String value = pair.getValue();
			sorted.put(urlEncode(key, false), urlEncode(value, false));
		}

		StringBuilder builder = new StringBuilder();
		for (Map.Entry<String, String> pair : sorted.entrySet()) {
			if (builder.length() > 0) {
				builder.append("&");
			}
			builder.append(pair.getKey());
			builder.append("=");
			builder.append(pair.getValue());
		}

		return builder.toString();
	}

	private static String getCanonicalRequest(URL endpoint, String httpMethod, String queryParameters, String canonicalizedHeaderNames, String canonicalizedHeaders, String bodyHash) {
		return httpMethod + "\n" +
				getCanonicalizedResourcePath(endpoint) + "\n" +
				queryParameters + "\n" +
				canonicalizedHeaders + "\n" +
				canonicalizedHeaderNames + "\n" +
				bodyHash;
	}

	private static String getCanonicalizedResourcePath(URL endpoint) {
		String path = endpoint.getPath();
		if (path == null || path.isEmpty()) {
			return "/";
		}

		String encodedPath = urlEncode(path, true);
		if (encodedPath.startsWith("/")) {
			return encodedPath;
		} else {
			return "/".concat(encodedPath);
		}
	}

	private static String getStringToSign(String scheme, String algorithm, String dateTime, String scope, String canonicalRequest) {
		return scheme + "-" + algorithm + "\n" +
				dateTime + "\n" +
				scope + "\n" +
				toHex(hash(canonicalRequest));
	}

	static byte[] hash(byte[] data) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(data);
			return md.digest();
		} catch (Exception e) {
			throw new RuntimeException("Hashing request failed: " + e.getMessage(), e);
		}
	}

	private static byte[] hash(String text) {
		return hash(text.getBytes(StandardCharsets.UTF_8));
	}

	private static byte[] sign(String stringData, byte[] key) {
		try {
			byte[] data = stringData.getBytes(StandardCharsets.UTF_8);
			Mac mac = Mac.getInstance(SIGNING_ALGORITHM);
			mac.init(new SecretKeySpec(key, SIGNING_ALGORITHM));
			return mac.doFinal(data);
		} catch (Exception e) {
			throw new RuntimeException("Signing request failed: " + e.getMessage(), e);
		}
	}

	static String toHex(byte[] data) {
		StringBuilder str = new StringBuilder(data.length * 2);
		for (byte b : data) {
			String hex = Integer.toHexString(b);
			if (hex.length() == 1) {
				str.append("0");
			} else if (hex.length() == 8) {
				hex = hex.substring(6);  // Remove prefix from negative numbers
			}
			str.append(hex);
		}
		return str.toString().toLowerCase();
	}

	private static String urlEncode(String url, boolean keepPathSlash) {
		String encoded;
		try {
			// Android 25 doesn't support non-string Charset param
			encoded = URLEncoder.encode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		if (keepPathSlash) {
			encoded = encoded.replace("%2F", "/");
		}
		return encoded;
	}
}
