package org.cweb.storage.remote;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.Config;
import org.cweb.identity.PublicStorageProfiles;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.storage.InboundDataRecord;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.RemoteSyncStatus;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class RemoteReadService {
	private static final Logger log = LoggerFactory.getLogger(RemoteReadService.class);
	private static final long REMOTE_CACHE_TTL = 1000L * 60 * 60 * 24;  // 1 day
	private final InboundCacheService inboundCacheService;
	private final PublicStorageProfiles publicStorageProfiles;
	private final RemoteStorageClient remoteStorageClient;
	private final String tracePrefix;

	public RemoteReadService(String tracePrefix, InboundCacheService inboundCacheService, PublicStorageProfiles publicStorageProfiles, RemoteStorageClient remoteStorageClient) {
		this.tracePrefix = tracePrefix;
		this.inboundCacheService = inboundCacheService;
		this.publicStorageProfiles = publicStorageProfiles;
		this.remoteStorageClient = remoteStorageClient;

		Threads.submitBackgroundTaskPeriodically(this::deleteStale, 0, Config.LOCAL_CACHE_GC_PERIOD);
	}

	private boolean isExpired(RemoteSyncStatus syncStatus, long now) {
		return syncStatus.isSetLastSuccessTime() && now - syncStatus.getLastSuccessTime() > REMOTE_CACHE_TTL ||
				syncStatus.isSetLastAttemptTime() && now - syncStatus.getLastAttemptTime() > REMOTE_CACHE_TTL;
	}

	private void deleteStale() {
		long now = System.currentTimeMillis();
		List<Pair<byte[], RemoteFileDescriptor>> fileNames = inboundCacheService.listFiles();
		for (Pair<byte[], RemoteFileDescriptor> fileName : fileNames) {
			InboundDataRecord dataRecord = inboundCacheService.get(fileName.getLeft(), fileName.getRight());
			if (dataRecord == null) {
				log.trace("Deleting unreadable file from " + Utils.getDebugStringFromId(fileName.getLeft()) + "/" + fileName.getRight());
				boolean success = inboundCacheService.delete(fileName.getLeft(), fileName.getRight());
				continue;
			}
			if (isExpired(dataRecord.getFetchStatus(), now)) {
				inboundCacheService.delete(fileName.getLeft(), fileName.getRight());
			}
		}
		long elapsed = System.currentTimeMillis() - now;
		log.trace("RemoteReadService.deleteStale took " + elapsed + " ms");
	}

	public void deleteLocalCache() {
		inboundCacheService.deleteAll();
	}

	public boolean deleteLocalCache(byte[] id, byte[] name, String nameSuffix) {
		return deleteLocalCache(id, RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)));
	}

	public boolean deleteLocalCache(byte[] id, RemoteFileDescriptor file) {
		return inboundCacheService.delete(id, file);
	}

	public RemoteFetchResultRaw read(IdentityDescriptor identityDescriptor, byte[] name) {
		return read(identityDescriptor.getId(), RemoteFileDescriptor.from(NameConversionUtils.toString(name)));
	}

	public RemoteFetchResultRaw read(byte[] id, byte[] name, String nameSuffix) {
		return read(id, RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)));
	}

	public RemoteFetchResultRaw readNonCached(byte[] id, PublicStorageProfile publicStorageProfile, byte[] name, String nameSuffix) {
		return readNonCached(id, publicStorageProfile, RemoteFileDescriptor.from(NameConversionUtils.toString(name, nameSuffix)));
	}

	public RemoteFetchResultRaw read(byte[] id, byte[] name) {
		return read(id, RemoteFileDescriptor.from(NameConversionUtils.toString(name)));
	}

	public RemoteFetchResultRaw read(byte[] id, RemoteFileDescriptor file) {
		PublicStorageProfile profile = publicStorageProfiles.get(id);
		InboundDataRecord dataRecord = inboundCacheService.get(id, file);
		RemoteSyncStatus syncStatus = dataRecord != null ? dataRecord.getFetchStatus() : null;
		long now = System.currentTimeMillis();
		if (dataRecord != null && dataRecord.getData() != null && !isExpired(syncStatus, now)) {
			return new RemoteFetchResultRaw(dataRecord.getData());
		}
		if (dataRecord == null) {
			syncStatus = new RemoteSyncStatus();
			dataRecord = new InboundDataRecord(syncStatus);
		}
		RemoteFetchResultRaw fetchResultRaw = readInternal(id, profile, file, dataRecord);
		inboundCacheService.put(id, file, dataRecord);
		return fetchResultRaw;
	}

	public RemoteFetchResultRaw readNonCached(byte[] id, PublicStorageProfile publicStorageProfile, RemoteFileDescriptor file) {
		InboundDataRecord dataRecord = new InboundDataRecord(new RemoteSyncStatus());
		return readInternal(id, publicStorageProfile, file, dataRecord);
	}

	private RemoteFetchResultRaw readInternal(byte[] id, PublicStorageProfile publicStorageProfile, RemoteFileDescriptor file, InboundDataRecord dataRecord) {
		RemoteSyncStatus syncStatus = dataRecord.getFetchStatus();
		long now = System.currentTimeMillis();
		syncStatus.setLastAttemptTime(now);
		RemoteFetchResultRaw result;
		byte[] data = null;
		if (publicStorageProfile != null) {
			try {
				if (Config.SIMULATE_REMOTE_STORAGE_READ_LATENCY > 0) {
					Threads.sleepChecked(Config.SIMULATE_REMOTE_STORAGE_READ_LATENCY);
				}
				data = remoteStorageClient.get(publicStorageProfile, file);
				dataRecord.setData(data);
				dataRecord.setDataSize(data != null ? data.length : 0);
				syncStatus.setLastSuccessTime(now);
				syncStatus.setLastError(null);
				syncStatus.setNumConsecutiveErrors(0);
				result = new RemoteFetchResultRaw(data);
			} catch (RemoteStorageException e) {
				log.info("Failed fetch file " + file);
				syncStatus.setLastError(e.getMessage());
				syncStatus.setNumConsecutiveErrors(syncStatus.getNumConsecutiveErrors() + 1);
				result = new RemoteFetchResultRaw(e, syncStatus.getNumConsecutiveErrors());
			} catch (Exception e) {
				log.error("Error fetching file " + file, e);
				syncStatus.setLastError(e.getMessage());
				syncStatus.setNumConsecutiveErrors(syncStatus.getNumConsecutiveErrors() + 1);
				result = new RemoteFetchResultRaw(e, syncStatus.getNumConsecutiveErrors());
			}
		} else {
			String errorMsg = "storageProfile not found for " + Utils.getDebugStringFromId(id);
			syncStatus.setLastError(errorMsg);
			syncStatus.setNumConsecutiveErrors(syncStatus.getNumConsecutiveErrors() + 1);
			result = new RemoteFetchResultRaw(new IllegalArgumentException(errorMsg), syncStatus.getNumConsecutiveErrors());
		}
		long completionTime = System.currentTimeMillis();
		log.trace(tracePrefix + (data != null ? " Fetched " + data.length + " bytes from " : " Not found ") + Utils.getDebugStringFromId(id) + ":" + file + " in " + (completionTime - now) + " ms");
		return result;
	}
}
