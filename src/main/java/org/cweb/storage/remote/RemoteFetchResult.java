package org.cweb.storage.remote;

public class RemoteFetchResult<T> {
	private final T data;
	private final Exception error;
	private final int numConsecutiveErrors;

	public RemoteFetchResult(T data) {
		this.data = data;
		this.error = null;
		this.numConsecutiveErrors = 0;
	}

	public RemoteFetchResult(Exception error, int numConsecutiveErrors) {
		this.data = null;
		this.error = error;
		this.numConsecutiveErrors = numConsecutiveErrors;
	}

	public T getData() {
		return data;
	}

	public Exception getError() {
		return error;
	}

	public int getNumConsecutiveErrors() {
		return numConsecutiveErrors;
	}
}
