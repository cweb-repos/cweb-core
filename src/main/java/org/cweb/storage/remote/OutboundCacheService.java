package org.cweb.storage.remote;

import org.cweb.schemas.storage.OutboundDataRecord;
import org.cweb.storage.local.LocalStorageInterface;
import org.cweb.utils.ThriftUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class OutboundCacheService {
	private static final Logger log = LoggerFactory.getLogger(OutboundCacheService.class);
	private static final String SELF_DIRECTORY = "oc/";
	private static final String OUTBOUND_DIRECTORY = SELF_DIRECTORY + "out/";
	private final LocalStorageInterface localStorageInterface;

	public OutboundCacheService(LocalStorageInterface localStorageInterface) {
		this.localStorageInterface = localStorageInterface;
	}

	private static String getUploadedName(RemoteFileDescriptor file) {
		return SELF_DIRECTORY + file.getName();
	}

	private static String getPendingName(RemoteFileDescriptor file) {
		return OUTBOUND_DIRECTORY + file.getName();
	}

	public OutboundDataRecord get(RemoteFileDescriptor file) {
		byte[] data = localStorageInterface.read(getPendingName(file));
		if (data == null) {
			data = localStorageInterface.read(getUploadedName(file));
		}
		if (data == null) {
			return null;
		}
		return ThriftUtils.deserializeSafe(data, OutboundDataRecord.class);
	}

	public void put(RemoteFileDescriptor file, OutboundDataRecord dataRecord) {
		localStorageInterface.write(getPendingName(file), ThriftUtils.serialize(dataRecord));
	}

	public List<RemoteFileDescriptor> listOutboundPending() {
		List<String> list = localStorageInterface.listFiles(OUTBOUND_DIRECTORY);
		return RemoteStorageUtils.convertToDescriptors(list);
	}

	public List<RemoteFileDescriptor> listUploaded() {
		List<String> list = localStorageInterface.listFiles(SELF_DIRECTORY);
		return RemoteStorageUtils.convertToDescriptors(list);
	}

	public void onOutboundUploadCompleted(RemoteFileDescriptor file, OutboundDataRecord dataRecord) {
		if (dataRecord.isDeleted()) {
			boolean success = delete(file);
			if (!success) {
				log.warn("Failed to delete " + file.toString());
			}
		} else {
			String pendingName = getPendingName(file);
			String uploadedName = getUploadedName(file);
			boolean success = localStorageInterface.rename(pendingName, uploadedName);
			if (!success) {
				log.warn("Failed to rename outbound " + file);
			}
		}
	}

	public boolean checkIfExists(RemoteFileDescriptor file) {
		return localStorageInterface.checkIfExists(getUploadedName(file)) || localStorageInterface.checkIfExists(getPendingName(file));
	}

	public boolean delete(RemoteFileDescriptor file) {
		String pendingName = getPendingName(file);
		String uploadedName = getUploadedName(file);
		boolean deletedUploaded = localStorageInterface.delete(uploadedName);
		boolean deletedPending = localStorageInterface.delete(pendingName);
		return deletedUploaded || deletedPending;
	}
}
