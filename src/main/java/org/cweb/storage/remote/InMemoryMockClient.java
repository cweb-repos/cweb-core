package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class InMemoryMockClient implements RemoteStorageClient {
	private static final Logger log = LoggerFactory.getLogger(InMemoryMockClient.class);
	private final Map<String, byte[]> name2data = new HashMap<>();
	private FilePredicate readPredicate;

	public interface FilePredicate {
		boolean match(String fullName);
	}

	public FilePredicate getReadPredicate() {
		return readPredicate;
	}

	public void setReadPredicate(FilePredicate readPredicate) {
		this.readPredicate = readPredicate;
	}

	private String getFullName(PublicStorageProfile storageProfile, RemoteFileDescriptor file) {
		String pathPrefix = StorageProfileUtils.getLocalPathPrefix(storageProfile);
		return pathPrefix + "/" + file.getName();
	}

	@Override
	public void put(PrivateStorageProfile storageProfile, RemoteFileDescriptor file, byte[] data) throws Exception {
		String fullName = getFullName(StorageProfileUtils.toPublicStorageProfile(storageProfile), file);
		name2data.put(fullName, data);
	}

	@Override
	public byte[] get(PublicStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception {
		String fullName = getFullName(storageProfile, file);
		if (readPredicate != null && !readPredicate.match(fullName)) {
			return null;
		}
		return name2data.get(fullName);
	}

	@Override
	public void delete(PrivateStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception {
		String fullName = getFullName(StorageProfileUtils.toPublicStorageProfile(storageProfile), file);
		name2data.remove(fullName);
	}
}
