package org.cweb.storage.remote;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class RemoteStorageUtils {
	public static List<RemoteFileDescriptor> convertToDescriptors(List<String> list) {
		List<RemoteFileDescriptor> result = new ArrayList<>();
		for (String name : list) {
			result.add(RemoteFileDescriptor.from(name));
		}
		return result;
	}

	public static List<Pair<byte[], RemoteFileDescriptor>> convertToDescriptors(byte[] id, List<String> list) {
		List<Pair<byte[], RemoteFileDescriptor>> result = new ArrayList<>();
		for (String name : list) {
			result.add(Pair.of(id, RemoteFileDescriptor.from(name)));
		}
		return result;
	}
}
