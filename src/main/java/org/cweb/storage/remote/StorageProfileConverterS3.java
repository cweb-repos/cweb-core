package org.cweb.storage.remote;

import org.apache.commons.lang3.StringUtils;
import org.cweb.schemas.storage.PrivateS3StorageProfile;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicS3StorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.schemas.storage.S3Credentials;

import java.util.function.Function;

public class StorageProfileConverterS3 implements StorageProfileConverter {
	private static final String TYPE_S3 = "s3";
	private static final int PUBLIC_PROFILE_COMPONENTS = 7;
	private static final char SEPARATOR_CHAR = '!';

	@Override
	public boolean isOwnType(PublicStorageProfile publicStorageProfile) {
		return publicStorageProfile.getPublicS3StorageProfile() != null;
	}

	@Override
	public boolean isOwnType(PrivateStorageProfile privateStorageProfile) {
		return privateStorageProfile.getPrivateS3StorageProfile() != null;
	}

	@Override
	public String getUniquePathPrefix(PublicStorageProfile publicStorageProfile) {
		PublicS3StorageProfile profile = publicStorageProfile.getPublicS3StorageProfile();
		if (profile == null) {
			return null;
		}
		return profile.getHost() + "-" + profile.getBucket() + "-" + profile.getPathPrefix();
	}

	@Override
	public PublicStorageProfile toPublicStorageProfile(PrivateStorageProfile privateStorageProfile) {
		if (!isOwnType(privateStorageProfile)) {
			return null;
		}
		PublicStorageProfile publicStorageProfile = new PublicStorageProfile();
		publicStorageProfile.setPublicS3StorageProfile(privateStorageProfile.getPrivateS3StorageProfile().getPublicS3StorageProfile());
		return publicStorageProfile;
	}

	@Override
	public PublicStorageProfile parsePublicStorageProfileHumanReadable(String publicStorageProfileStr) {
		PublicStorageProfile publicStorageProfile = new PublicStorageProfile();
		String[] split = StringUtils.splitPreserveAllTokens(publicStorageProfileStr, SEPARATOR_CHAR);
		if (split.length < PUBLIC_PROFILE_COMPONENTS) {
			return null;
		}
		String type = split[0];
		if (!type.equals(TYPE_S3)) {
			return null;
		}
		String host = split[1];
		S3Credentials readCredentials = null;
		if (!StringUtils.isAnyBlank(split[5], split[6])) {
			readCredentials = new S3Credentials(split[5], split[6]);
		}
		PublicS3StorageProfile publicS3StorageProfile = new PublicS3StorageProfile(host, split[2], split[3], split[4], readCredentials);
		publicStorageProfile.setPublicS3StorageProfile(publicS3StorageProfile);
		return publicStorageProfile;
	}

	@Override
	public String toHumanReadableString(PublicStorageProfile publicStorageProfile) {
		PublicS3StorageProfile profile = publicStorageProfile.getPublicS3StorageProfile();
		if (profile == null) {
			return null;
		}
		S3Credentials readCredentials = profile.getReadCredentials();
		return TYPE_S3 +
				SEPARATOR_CHAR + profile.getHost() +
				SEPARATOR_CHAR + profile.getRegion() +
				SEPARATOR_CHAR + profile.getBucket() +
				SEPARATOR_CHAR + profile.getPathPrefix() +
				SEPARATOR_CHAR + (readCredentials != null ? readCredentials.getAccessKeyId() : "") +
				SEPARATOR_CHAR + (readCredentials != null ? readCredentials.getSecretAccessKey() : "");
	}

	@Override
	public PrivateStorageProfile parsePrivateStorageProfileHumanReadable(String privateStorageProfileStr) {
		PublicStorageProfile publicStorageProfile = parsePublicStorageProfileHumanReadable(privateStorageProfileStr);
		if (publicStorageProfile == null || !isOwnType(publicStorageProfile)) {
			return null;
		}
		String[] split = StringUtils.splitPreserveAllTokens(privateStorageProfileStr, SEPARATOR_CHAR);
		if (split.length != PUBLIC_PROFILE_COMPONENTS + 2) {
			return null;
		}
		S3Credentials credentials = new S3Credentials(split[PUBLIC_PROFILE_COMPONENTS], split[PUBLIC_PROFILE_COMPONENTS + 1]);
		PrivateS3StorageProfile privateS3StorageProfile = new PrivateS3StorageProfile(publicStorageProfile.getPublicS3StorageProfile(), credentials);
		PrivateStorageProfile privateStorageProfile = new PrivateStorageProfile();
		privateStorageProfile.setPrivateS3StorageProfile(privateS3StorageProfile);
		return privateStorageProfile;
	}

	@Override
	public String toHumanReadableString(PrivateStorageProfile privateStorageProfile) {
		if (!isOwnType(privateStorageProfile)) {
			return null;
		}
		PrivateS3StorageProfile profile = privateStorageProfile.getPrivateS3StorageProfile();
		StringBuilder result = new StringBuilder(toHumanReadableString(StorageProfileUtils.toPublicStorageProfile(privateStorageProfile)));
		S3Credentials credentials = profile.getCredentials();
		if (credentials != null) {
			result.append(SEPARATOR_CHAR).append(credentials.getAccessKeyId())
					.append(SEPARATOR_CHAR).append(credentials.getSecretAccessKey());

		}
		return result.toString();
	}

	@Override
	public PrivateStorageProfile modifyPaths(PrivateStorageProfile privateStorageProfile, Function<String, String> updater) {
		PrivateStorageProfile privateStorageProfileNew = new PrivateStorageProfile(privateStorageProfile);
		PublicS3StorageProfile s3Profile = privateStorageProfileNew.getPrivateS3StorageProfile().getPublicS3StorageProfile();
		s3Profile.setPathPrefix(updater.apply(s3Profile.getPathPrefix()));
		return privateStorageProfileNew;
	}

	@Override
	public boolean isLocationEqual(PublicStorageProfile profile1, PublicStorageProfile profile2) {
		PublicS3StorageProfile profile1WithoutCredentials = new PublicS3StorageProfile(profile1.getPublicS3StorageProfile());
		profile1WithoutCredentials.unsetReadCredentials();
		PublicS3StorageProfile profile2WithoutCredentials = new PublicS3StorageProfile(profile2.getPublicS3StorageProfile());
		profile2WithoutCredentials.unsetReadCredentials();
		return profile1WithoutCredentials.equals(profile2WithoutCredentials);
	}
}
