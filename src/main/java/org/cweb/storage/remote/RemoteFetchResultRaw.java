package org.cweb.storage.remote;

public class RemoteFetchResultRaw extends RemoteFetchResult<byte[]> {
	public RemoteFetchResultRaw(byte[] data) {
		super(data);
	}

	public RemoteFetchResultRaw(Exception error, int numConsecutiveErrors) {
		super(error, numConsecutiveErrors);
	}
}
