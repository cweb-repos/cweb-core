package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.utils.Utils;

public class StorageProfileUtils {

	public static PublicStorageProfile toPublicStorageProfile(PrivateStorageProfile privateStorageProfile) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(privateStorageProfile);
		return handler.getConverter().toPublicStorageProfile(privateStorageProfile);
	}

	public static PublicStorageProfile parsePublicStorageProfileHumanReadable(String publicStorageProfileStr) {
		for (RemoteStorageHandler handler : RemoteStorageContainer.getHandlers()) {
			PublicStorageProfile publicStorageProfile = handler.getConverter().parsePublicStorageProfileHumanReadable(publicStorageProfileStr);
			if (publicStorageProfile != null) {
				return publicStorageProfile;
			}
		}
		return null;
	}

	public static String toHumanReadableString(PublicStorageProfile publicStorageProfile) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(publicStorageProfile);
		return handler.getConverter().toHumanReadableString(publicStorageProfile);
	}

	public static PrivateStorageProfile parsePrivateStorageProfileHumanReadable(String privateStorageProfileStr) {
		for (RemoteStorageHandler handler : RemoteStorageContainer.getHandlers()) {
			PrivateStorageProfile privateStorageProfile = handler.getConverter().parsePrivateStorageProfileHumanReadable(privateStorageProfileStr);
			if (privateStorageProfile != null) {
				return privateStorageProfile;
			}
		}
		return null;
	}

	public static String toHumanReadableString(PrivateStorageProfile privateStorageProfile) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(privateStorageProfile);
		return handler.getConverter().toHumanReadableString(privateStorageProfile);
	}

	static String getLocalPathPrefix(PublicStorageProfile storageProfile) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(storageProfile);
		return handler.getConverter().getUniquePathPrefix(storageProfile);
	}

	public static PrivateStorageProfile appendToPaths(PrivateStorageProfile privateStorageProfile, String suffix) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(privateStorageProfile);
		return handler.getConverter().modifyPaths(privateStorageProfile, path -> Utils.appendPath(path, suffix));
	}

	public static PrivateStorageProfile removeFromPaths(PrivateStorageProfile privateStorageProfile, String suffix) {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(privateStorageProfile);
		return handler.getConverter().modifyPaths(privateStorageProfile, path -> {
			int index = path.lastIndexOf('/' + suffix);
			if (index == 0) {
				index = path.lastIndexOf(suffix);
			}
			if (index > 0) {
				path = path.substring(0, index);
			}
			return path;
		});
	}

	public static boolean isLocationEqual(PublicStorageProfile profile1, PublicStorageProfile profile2) {
		RemoteStorageHandler handler1 = RemoteStorageContainer.getHandler(profile1);
		RemoteStorageHandler handler2 = RemoteStorageContainer.getHandler(profile2);
		if (handler1 != handler2) {
			return false;
		}
		return handler1.getConverter().isLocationEqual(profile1, profile2);
	}
}
