package org.cweb.storage.remote;

import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;

public class RemoteStorageClientImpl implements RemoteStorageClient {
	@Override
	public void put(PrivateStorageProfile storageProfile, RemoteFileDescriptor file, byte[] data) throws Exception {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(storageProfile);
		handler.getClient().put(storageProfile, file, data);
	}

	@Override
	public byte[] get(PublicStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(storageProfile);
		return handler.getClient().get(storageProfile, file);
	}

	@Override
	public void delete(PrivateStorageProfile storageProfile, RemoteFileDescriptor file) throws Exception {
		RemoteStorageHandler handler = RemoteStorageContainer.getHandler(storageProfile);
		handler.getClient().delete(storageProfile, file);
	}
}
