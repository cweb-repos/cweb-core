package org.cweb.storage.local;

import java.util.List;

public interface LocalStorageInterface {
	byte[] read(String name);

	boolean write(String name, byte[] data);

	boolean delete(String name);

	List<String> listFiles(String directory);

	List<String> listAllFiles();

	List<String> listFilesWithSuffix(String directory, String suffix);

	List<String> listDirectories(String directory);

	boolean rename(String srcName, String destName);

	boolean checkIfExists(String name);

	boolean deleteAll();
}
