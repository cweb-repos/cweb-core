package org.cweb.storage.local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class InMemoryLocalFileSystem implements LocalFileSystemInterface {
	private static final Logger log = LoggerFactory.getLogger(InMemoryLocalFileSystem.class);
	private final Map<String, byte[]> name2data = new LinkedHashMap<>();

	@Override
	public boolean checkIfExists(String name) {
		return name2data.containsKey(name);
	}

	@Override
	public long getLength(String name) {
		byte[] fileData = name2data.get(name);
		if (fileData == null) {
			return 0;
		}
		return fileData.length;
	}

	@Override
	public byte[] read(String name, long offset, int length) {
		byte[] fileData = name2data.get(name);
		if (fileData == null) {
			return null;
		}
		return Arrays.copyOfRange(fileData, (int)offset, (int) offset + length);
	}

	@Override
	public boolean createFile(String name, long length) {
		byte[] data = new byte[(int) length];
		name2data.put(name, data);
		return true;
	}

	@Override
	public boolean write(String name, long offset, byte[] data) {
		byte[] fileData = name2data.get(name);
		System.arraycopy(data, 0, fileData, (int) offset, data.length);
		return true;
	}

	@Override
	public boolean createAndWrite(String name, byte[] data) {
		createFile(name, data.length);
		write(name, 0, data);
		return true;
	}
}
