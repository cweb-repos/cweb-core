package org.cweb.storage.local;

import org.cweb.schemas.keys.KeyPair;
import org.cweb.schemas.local.LocalPreKeyState;
import org.cweb.schemas.local.LocalPreKeyPair;
import org.cweb.utils.ThriftUtils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocalPreKeyService {
	private static final String FILE_NAME = "localPreKeyState";
	private static final long TIMESTAMP_TTL = 1000L * 60 * 60 * 24 * 30;
	private final SecretStorageService secretStorageService;

	private final List<LocalPreKeyPair> keyPairs = new ArrayList<>();

	public LocalPreKeyService(SecretStorageService secretStorageService) {
		this.secretStorageService = secretStorageService;

		LocalPreKeyState state = loadAndPurgeExpired();
		keyPairs.addAll(state.getKeyPairs());

		save();
	}

	private LocalPreKeyState loadAndPurgeExpired() {
		long now = System.currentTimeMillis();
		LocalPreKeyState state = load();
		List<LocalPreKeyPair> currentList = state.getKeyPairs();
		List<LocalPreKeyPair> newList = new ArrayList<>(currentList.size());
		for (LocalPreKeyPair keyPair : currentList) {
			if (now - keyPair.getTime() < TIMESTAMP_TTL) {
				newList.add(keyPair);
			}
		}
		state.setKeyPairs(newList);
		return state;
	}

	private LocalPreKeyState load() {
		byte[] data = secretStorageService.read(FILE_NAME);
		LocalPreKeyState state = data != null ? ThriftUtils.deserializeSafe(data, LocalPreKeyState.class) : null;
		if (state == null) {
			state = new LocalPreKeyState();
		}
		return state;
	}

	private void save() {
		LocalPreKeyState state = new LocalPreKeyState();
		state.setKeyPairs(keyPairs);
		byte[] data = ThriftUtils.serialize(state);
		secretStorageService.write(FILE_NAME, data);
	}

	public KeyPair findKeyPairByHash(byte[] publicKeyHash) {
		for (LocalPreKeyPair keyPair : keyPairs) {
			if (Arrays.equals(keyPair.getPublicKeyHash(), publicKeyHash)) {
				return keyPair.getKeyPair();
			}
		}
		return null;
	}

	private LocalPreKeyPair findKeyPairByPublicKey(byte[] publicKey) {
		for (LocalPreKeyPair keyPair : keyPairs) {
			if (Arrays.equals(keyPair.getKeyPair().getPublicKey(), publicKey)) {
				return keyPair;
			}
		}
		return null;
	}

	public Long getCreationTime(byte[] publicKey) {
		LocalPreKeyPair keyPair = findKeyPairByPublicKey(publicKey);
		if (keyPair == null) {
			return null;
		}
		return keyPair.getTime();
	}

	public void addKeyPair(byte[] publicKeyHash, KeyPair keyPair, long time) {
		keyPairs.add(new LocalPreKeyPair(time, ByteBuffer.wrap(publicKeyHash), keyPair));
		save();
	}
}
