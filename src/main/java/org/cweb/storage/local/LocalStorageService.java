package org.cweb.storage.local;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Not synchronized, users to synchronize when needed
 */
public class LocalStorageService implements LocalStorageInterface {
	private static final Logger log = LoggerFactory.getLogger(LocalStorageService.class);
	private final String localRootPath;

	public LocalStorageService(String localRootPath) {
		this.localRootPath = localRootPath;
	}

	private String getFullName(String name) {
		return Utils.appendPath(localRootPath, name);
	}

	@Override
	public byte[] read(String name) {
		try {
			File file = new File(getFullName(name));
			if (file.isFile()) {
				return FileUtils.readFileToByteArray(file);
			}
			return null;
		} catch (IOException e) {
			log.error("Error reading " + name, e);
			// throw new RuntimeException(e);
			return null;
		}
	}

	@Override
	public boolean write(String name, byte[] data) {
		try {
			String fullName = getFullName(name);
			FileUtils.writeByteArrayToFile(new File(fullName), data);
			return true;
		} catch (IOException e) {
			log.error("Error writing " + name, e);
			return false;
		}
	}

	@Override
	public boolean delete(String name) {
		try {
			String fullName = getFullName(name);
			return (new File(fullName)).delete();
		} catch (Exception e) {
			log.error("Error deleting " + name, e);
			return false;
		}
	}

	@Override
	public List<String> listFiles(String directory) {
		List<String> result = new ArrayList<>();
		File dir = new File(getFullName(directory));
		if (!dir.isDirectory()) {
			return result;
		}
		for (File file : dir.listFiles()) {
			if (file.isFile()) {
				result.add(file.getName());
			}
		}
		return result;
	}

	@Override
	public List<String> listAllFiles() {
		File root = new File(localRootPath);
		if (!root.isDirectory()) {
			return Collections.emptyList();
		}
		Collection<File> files = FileUtils.listFiles(root, null, true);
		List<String> result = new ArrayList<>();
		for (File file : files) {
			result.add(file.getPath().substring(localRootPath.length() + 1));
		}
		return result;
	}

	@Override
	public List<String> listFilesWithSuffix(String directory, String suffix) {
		List<String> result = new ArrayList<>();
		File dir = new File(getFullName(directory));
		if (!dir.isDirectory()) {
			return result;
		}
		for (File file : dir.listFiles()) {
			if (file.isFile() && file.getName().endsWith(suffix)) {
				result.add(file.getName());
			}
		}
		return result;
	}

	@Override
	public List<String> listDirectories(String directory) {
		List<String> result = new ArrayList<>();
		File dir = new File(getFullName(directory));
		if (!dir.isDirectory()) {
			return result;
		}
		for (File file : dir.listFiles()) {
			if (file.isDirectory()) {
				result.add(file.getName());
			}
		}
		return result;
	}

	@Override
	public boolean rename(String srcName, String destName) {
		File srcFile = new File(getFullName(srcName));
		File destFile = new File(getFullName(destName));
		File destParentFile = destFile.getParentFile();
		if (destParentFile != null && !destParentFile.isDirectory()) {
			log.debug("Creating new directory " + destParentFile);
			boolean result = destParentFile.mkdirs();
			Preconditions.checkArgument(result, "Failed to create directory " + destParentFile);
		}
		return srcFile.renameTo(destFile);
	}

	@Override
	public boolean checkIfExists(String name) {
		File file = new File(getFullName(name));
		return file.isFile();
	}

	public void moveRoot(String newLocalRootPath) {
		File newRoot = new File(newLocalRootPath);
		if ((newRoot.exists())) {
			throw new RuntimeException("File already exists: " + newLocalRootPath);
		}
		boolean success = (new File(localRootPath)).renameTo(newRoot);
		if (!success) {
			throw new RuntimeException("Failed to rename " + localRootPath + " to " + newLocalRootPath);
		}
	}

	@Override
	public boolean deleteAll() {
		try {
			FileUtils.deleteDirectory(new File(localRootPath));
		} catch (Exception e) {
			log.error("Error deleting " + localRootPath, e);
			return false;
		}
		return true;
	}
}
