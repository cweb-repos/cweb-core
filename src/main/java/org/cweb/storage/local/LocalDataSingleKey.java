package org.cweb.storage.local;

import org.apache.thrift.TBase;
import org.cweb.storage.NameConversionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LocalDataSingleKey<T extends TBase> extends LocalDataBase<T> {
	private static final Logger log = LoggerFactory.getLogger(LocalDataSingleKey.class);
	private static final String LOCAL_DATA_PATH_PREFIX = "ld/";
	private final String nameSuffix;

	public LocalDataSingleKey(String nameSuffix, String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
		this.nameSuffix = nameSuffix;
	}

	private String getName(byte[] id) {
		return LOCAL_DATA_PATH_PREFIX + NameConversionUtils.toString(id, nameSuffix);
	}

	private byte[] fromStringName(String name) {
		return NameConversionUtils.fromString(name, nameSuffix);
	}

	public boolean checkIfExists(byte[] id) {
		return checkIfExists(getName(id));
	}

	public T get(byte[] id, Class<T> klass) {
		return get(getName(id), klass);
	}

	public void put(byte[] id, T value) {
		put(getName(id), value);
	}

	public boolean delete(byte[] id) {
		return delete(getName(id));
	}

	public synchronized List<byte[]> list() {
		List<String> files = localStorageInterface.listFilesWithSuffix(LOCAL_DATA_PATH_PREFIX, nameSuffix);
		List<byte[]> result = new ArrayList<>();
		for (String file : files) {
			byte[] name = fromStringName(file);
			if (name == null) {
				log.trace(tracePrefix + " Invalid file " + file);
				continue;
			}
			result.add(name);
		}
		return result;
	}
}
