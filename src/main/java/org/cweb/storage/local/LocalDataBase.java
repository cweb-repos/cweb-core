package org.cweb.storage.local;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.thrift.TBase;
import org.cweb.utils.ThriftUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class LocalDataBase<T extends TBase> {
	private static final Logger log = LoggerFactory.getLogger(LocalDataBase.class);
	protected final String tracePrefix;
	protected final LocalStorageInterface localStorageInterface;
	private final Cache<String, T> cache;

	protected LocalDataBase(String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		this.tracePrefix = tracePrefix;
		this.localStorageInterface = localStorageInterface;
		this.cache = CacheBuilder.newBuilder()
				.maximumSize(memoryCacheSize)
				.expireAfterWrite(memoryCacheExpirationMinutes, TimeUnit.MINUTES)
				.build();
	}

	protected boolean checkIfExists(String name) {
		if (cache.getIfPresent(name) != null) {
			return true;
		}
		return localStorageInterface.checkIfExists(name);
	}

	private byte[] get(String name) {
		return localStorageInterface.read(name);
	}

	protected synchronized void put(String name, T value) {
		byte[] data = ThriftUtils.serialize(value);
		cache.put(name, value);
		localStorageInterface.write(name, data);
		log.trace(tracePrefix + " Stored " + name);
	}

	protected synchronized T get(String name, Class<T> klass) {
		T result = cache.getIfPresent(name);
		if (result == null) {
			byte[] data = get(name);
			if (data != null) {
				result = ThriftUtils.deserializeSafe(data, klass);
				if (result == null) {
					log.trace(tracePrefix + " Failed to deserialize " + name);
				} else {
					cache.put(name, result);
				}
			}
			log.trace(tracePrefix + (result != null ? " Fetched " : " Failed to fetch ") + name);
		}
		return result;
	}

	protected synchronized boolean delete(String name) {
		cache.invalidate(name);
		return localStorageInterface.delete(name);
	}
}
