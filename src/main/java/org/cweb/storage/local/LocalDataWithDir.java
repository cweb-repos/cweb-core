package org.cweb.storage.local;

import org.apache.thrift.TBase;
import org.cweb.storage.NameConversionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LocalDataWithDir<T extends TBase> extends LocalDataBase<T> {
	private static final Logger log = LoggerFactory.getLogger(LocalDataWithDir.class);
	private static final String LOCAL_DATA_PATH_PREFIX = "ld2/";
	private final String dirName;

	public LocalDataWithDir(String dirName, String tracePrefix, LocalStorageInterface localStorageInterface, int memoryCacheSize, int memoryCacheExpirationMinutes) {
		super(tracePrefix, localStorageInterface, memoryCacheSize, memoryCacheExpirationMinutes);
		this.dirName = dirName;
	}

	private String getName(byte[] dir, byte[] id) {
		return LOCAL_DATA_PATH_PREFIX + dirName + '/' + NameConversionUtils.toString(dir) + '/' + NameConversionUtils.toString(id);
	}

	private String getName(byte[] dirOrId) {
		return NameConversionUtils.toString(dirOrId);
	}

	private byte[] fromStringName(String name) {
		return NameConversionUtils.fromString(name);
	}

	public boolean checkIfExists(byte[] dir, byte[] id) {
		return checkIfExists(getName(dir, id));
	}

	public T get(byte[] dir, byte[] id, Class<T> klass) {
		return get(getName(dir, id), klass);
	}

	public void put(byte[] dir, byte[] id, T value) {
		put(getName(dir, id), value);
	}

	public boolean delete(byte[] dir, byte[] id) {
		return delete(getName(dir, id));
	}

	public List<byte[]> listDirs() {
		List<String> dirs = localStorageInterface.listDirectories(LOCAL_DATA_PATH_PREFIX + dirName);
		List<byte[]> result = new ArrayList<>(dirs.size());
		for (String dir : dirs) {
			byte[] name = fromStringName(dir);
			if (name == null) {
				log.trace(tracePrefix + " Invalid dir name " + dir);
				continue;
			}
			result.add(name);
		}
		return result;
	}

	public List<byte[]> list(byte[] dir) {
		String fullDir = LOCAL_DATA_PATH_PREFIX + dirName + "/" + getName(dir);
		List<String> files = localStorageInterface.listFiles(fullDir);
		List<byte[]> result = new ArrayList<>(files.size());
		for (String file : files) {
			byte[] name = fromStringName(file);
			if (name == null) {
				log.trace(tracePrefix + " Invalid file " + file);
				continue;
			}
			result.add(name);
		}
		return result;
	}
}
