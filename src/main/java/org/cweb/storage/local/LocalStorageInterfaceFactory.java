package org.cweb.storage.local;

import org.cweb.Config;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Utils;

import java.util.HashMap;
import java.util.Map;

public class LocalStorageInterfaceFactory {
	private static final Map<String, LocalStorageInterface> localRootPath2interface = new HashMap<>();

	public static LocalStorageInterface getLocalStorageInterface(String localRootPathRoot, byte[] id) {
		return getLocalStorageInterface(Utils.appendPath(localRootPathRoot, NameConversionUtils.toString(id)));
	}

	public static LocalStorageInterface getLocalStorageInterface(String localRootPath) {
		LocalStorageInterface result = localRootPath2interface.get(localRootPath);
		if (result != null) {
			return result;
		}
		switch (Config.localStorageType) {
			case FILE_SYSTEM:
				result = new LocalStorageService(localRootPath);
				break;
			case MEMORY:
				result = new InMemoryLocalStorage();
				break;
		}
		localRootPath2interface.put(localRootPath, result);
		return result;
	}
}
