package org.cweb.storage.local;

import org.cweb.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class InMemoryLocalStorage implements LocalStorageInterface {
	private final Map<String, byte[]> name2data = new LinkedHashMap<>();

	@Override
	public byte[] read(String name) {
		return name2data.get(name);
	}

	@Override
	public synchronized boolean write(String name, byte[] data) {
		name2data.put(name, data);
		return true;
	}

	@Override
	public synchronized boolean delete(String name) {
		byte[] removed = name2data.remove(name);
		return removed != null;
	}

	@Override
	public synchronized List<String> listFiles(String directory) {
		List<String> result = new ArrayList<>();
		String prefix = Utils.appendPath(directory, "");
		for (Map.Entry<String, byte[]> entry : name2data.entrySet()) {
			String name = entry.getKey();
			if (name.startsWith(prefix)) {
				result.add(name.substring(prefix.length()));
			}
		}
		return result;
	}

	@Override
	public List<String> listAllFiles() {
		return new ArrayList<>(name2data.keySet());
	}

	@Override
	public synchronized List<String> listFilesWithSuffix(String directory, String suffix) {
		List<String> result = new ArrayList<>();
		String prefix = Utils.appendPath(directory, "");
		for (Map.Entry<String, byte[]> entry : name2data.entrySet()) {
			String name = entry.getKey();
			if (name.startsWith(prefix) && name.endsWith(suffix)) {
				result.add(name.substring(prefix.length()));
			}
		}
		return result;
	}

	@Override
	public synchronized List<String> listDirectories(String directory) {
		List<String> results = new ArrayList<>();
		String prefix = Utils.appendPath(directory, "");
		for (Map.Entry<String, byte[]> entry : name2data.entrySet()) {
			String name = entry.getKey();
			if (name.startsWith(prefix)) {
				int nextSeparatorIdx = name.indexOf('/', prefix.length());
				if (nextSeparatorIdx != -1) {
					directory = name.substring(prefix.length(), nextSeparatorIdx);
					if (!results.contains(directory)) {
						results.add(directory);
					}
				}
			}
		}
		return results;
	}

	@Override
	public synchronized boolean rename(String srcName, String destName) {
		byte[] removed = name2data.remove(srcName);
		if (removed == null) {
			return false;
		}
		name2data.put(destName, removed);
		return true;
	}

	@Override
	public boolean checkIfExists(String name) {
		return name2data.containsKey(name);
	}

	@Override
	public boolean deleteAll() {
		name2data.clear();
		return true;
	}
}
