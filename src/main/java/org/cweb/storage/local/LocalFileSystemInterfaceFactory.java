package org.cweb.storage.local;

import org.cweb.Config;

public class LocalFileSystemInterfaceFactory {
	public static LocalFileSystemInterface createLocalStorageInterface() {
		switch (Config.localFileSystemType) {
			case FILE_SYSTEM:
				return new LocalFileSystemService();
			case MEMORY:
				return new InMemoryLocalFileSystem();
			default:
				return null;
		}
	}
}
