package org.cweb.storage.local;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class LocalFileSystemService implements LocalFileSystemInterface {
	private static final Logger log = LoggerFactory.getLogger(LocalFileSystemService.class);

	@Override
	public boolean checkIfExists(String name) {
		return (new File(name)).isFile();
	}

	@Override
	public long getLength(String name) {
		return (new File(name)).length();
	}

	@Override
	public byte[] read(String name, long offset, int length) {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(name, "r");
			file.seek(offset);
			byte[] data = new byte[length];
			int lengthRead = file.read(data);
			Preconditions.checkArgument(lengthRead == length, "Failed to read " + length + " bytes, got only " + lengthRead);
			return data;
		} catch (IOException | IllegalArgumentException e) {
			log.debug("Exception reading from file " + name + ": " + e.getMessage());
			return null;
		} finally {
			closeSafe(file);
		}
	}

	@Override
	public boolean createFile(String name, long length) {
		RandomAccessFile file = null;
		try {
			File parentFile = new File(name).getParentFile();
			if (parentFile != null && !parentFile.isDirectory()) {
				boolean result = parentFile.mkdirs();
				Preconditions.checkArgument(result, "Failed to create directory " + parentFile.getAbsolutePath());
			}
			file = new RandomAccessFile(name, "rw");
			file.setLength(length);
			return true;
		} catch (IOException | IllegalArgumentException e) {
			log.debug("Exception creating file " + name + ": " + e.getMessage());
			return false;
		} finally {
			closeSafe(file);
		}
	}

	@Override
	public boolean write(String name, long offset, byte[] data) {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(name, "rw");
			file.seek(offset);
			file.write(data);
			return true;
		} catch (IOException e) {
			log.debug("Exception writing to file " + name + ": " + e.getMessage());
			return false;
		} finally {
			closeSafe(file);
		}
	}

	@Override
	public boolean createAndWrite(String name, byte[] data) {
		boolean created = createFile(name, data.length);
		if (!created) {
			return false;
		}
		boolean wrote = write(name, 0, data);
		return wrote;
	}

	private void closeSafe(RandomAccessFile file) {
		if (file == null) {
			return;
		}
		try {
			file.close();
		} catch (IOException ignored) {
		}
	}
}
