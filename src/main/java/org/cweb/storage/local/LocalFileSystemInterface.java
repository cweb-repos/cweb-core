package org.cweb.storage.local;

public interface LocalFileSystemInterface {
	boolean checkIfExists(String name);

	long getLength(String name);

	byte[] read(String name, long offset, int length);

	boolean createFile(String name, long length);

	boolean write(String name, long offset, byte[] data);

	boolean createAndWrite(String name, byte[] data);
}
