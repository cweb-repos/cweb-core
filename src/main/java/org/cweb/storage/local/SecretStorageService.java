package org.cweb.storage.local;

import org.cweb.crypto.lib.AEAD;

public class SecretStorageService {
	private static final String LOCAL_ROOT_PATH_PREFIX = "mk/";
	private static final byte[] ASSOCIATED_DATA = new byte[] {41, 110, 94};
	private final byte[] key;

	private final LocalStorageInterface localStorageInterface;

	public SecretStorageService(LocalStorageInterface localStorageInterface, byte[] key) {
		this.localStorageInterface = localStorageInterface;
		this.key = key;
	}

	private String getFullName(String name) {
		return LOCAL_ROOT_PATH_PREFIX + name;
	}

	private byte[] encrypt(byte[] data) {
		if (key == null) {
			return data;
		}
		return AEAD.encrypt(key, data, ASSOCIATED_DATA);
	}

	private byte[] decrypt(byte[] data) {
		if (key == null || data == null) {
			return data;
		}
		return AEAD.decrypt(key, data, ASSOCIATED_DATA);
	}

	public static byte[] generateKey() {
		return AEAD.generateCompositeKey();
	}

	public byte[] read(String name) {
		return decrypt(localStorageInterface.read(getFullName(name)));
	}

	public void write(String name, byte[] data) {
		localStorageInterface.write(getFullName(name), encrypt(data));
	}
}
