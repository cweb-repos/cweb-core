package org.cweb;

public class Config {

	// RemoteStorageFactory
	public enum RemoteStorageType {
		REAL_REMOTE,
		LOCAL_FILE_SYSTEM,
		LOCAL_MEMORY
	}
	public static RemoteStorageType remoteStorageType = RemoteStorageType.REAL_REMOTE;
	public static String remoteStorageLocalFileSystemRootPath;

	// LocalStorageInterfaceFactory
	public enum LocalStorageType {
		FILE_SYSTEM,
		MEMORY
	}
	public static LocalStorageType localStorageType = LocalStorageType.FILE_SYSTEM;

	// LocalStorageInterfaceFactory
	public enum LocalFileSystemType {
		FILE_SYSTEM,
		MEMORY
	}
	public static LocalFileSystemType localFileSystemType = LocalFileSystemType.FILE_SYSTEM;

	// RemoteReadService
	public static long SIMULATE_REMOTE_STORAGE_READ_LATENCY = 0;

	// RemoteWriteService
	public static long SIMULATE_REMOTE_STORAGE_WRITE_LATENCY = 0;

	// CommSessionService
	public static long COMM_SESSION_REMOTE_MESSAGE_DELETION_DELAY = 1000L * 60 * 60;

	// CommSessionService, SharedSessionService
	public static final long LOCAL_CACHE_GC_PERIOD = 1000L * 60 * 60 * 24;

	// SharedObjectPostService
	public static final long SHARED_OBJECT_REPUBLISH_CHECK_PERIOD = 1000L * 60 * 60 * 24 * 2;
}
